<%@ page import= "ordersync.* "%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>订单同步状态统计</title>
</head>
<body>
服务器启动时间：<%=OrderStats.getStartTime()%>
<BR>当前待传输队列长度：<%=OrderUtil.pendingQueue.getSize() %>
<BR>当前待传输队列订单号：<%=OrderUtil.pendingQueue.getOrderIds()%>
<BR>
<BR>总订单成功获取数：<%=OrderStats.getTotalOrderPolled()%>
<BR>总订单失败获取数：<%=OrderStats.getTotalOrderPollFailedCount()%>
<BR>
<BR>总订单成功传输数：<%=OrderStats.getTotalOrderSent()%>
<BR>总订单失败传输数：<%=OrderStats.getTotalOrderSendFailedCount()%>
<HR>
<BR>今日订单成功获取数：<%=OrderStats.getThisDayOrderPolled() %>>
<BR>今日订单失败获取数：<%=OrderStats.getThisDayOrderPollFailedCount()%>
<BR>
<BR>今日订单成功传输数：<%=OrderStats.getThisDayOrderSent()%>
<BR>今日订单失败传输数：<%=OrderStats.getThisDayOrderSendFailedCount()%>
<HR>
<BR>最后订单获取时间：<%=OrderStats.getLastOrderPollTime()%>
<BR>最后订单获取号：<%=OrderStats.getLastOrderPollNo()%>
<BR>最后订单获取失败消息：<%=OrderStats.getLastOrderPollFailedMessage()%>
<BR>
<BR>最后订单传输时间：<%=OrderStats.getLastOrderSendTime()%>
<BR>最后订单传输号：<%=OrderStats.getLastOrderSendNo()%>
<BR>最后订单传输失败消息：<%=OrderStats.getLastOrderSendFailedMessage()%>
</body>
</html>