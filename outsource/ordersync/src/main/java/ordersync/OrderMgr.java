package ordersync;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ordersync.model.OrderInfo;
import ordersync.util.FileUtil;

/**
 * 在一个目录永久保存Order
 */
@Service
public class OrderMgr {
	private static final Logger logger = LoggerFactory.getLogger(OrderMgr.class);

	private static final String ORDER_FILE_SUFFIX = ".ini";

	private File ordersDir;

	@PostConstruct
	public void init(){
		File appDir = OrderUtil.getAppDir();
		ordersDir = new File(appDir, "orders");
		logger.info("Load order info from directory: "+ordersDir);
		ordersDir.mkdirs();
	}

	public boolean orderExists(String orderId){
		File file = new File(ordersDir, orderId+ORDER_FILE_SUFFIX);
		boolean result = file.exists() && file.isFile();
		return result;
	}

	public void saveOrder(OrderInfo order) throws IOException
	{
		File file = new File(ordersDir, order.getId()+ORDER_FILE_SUFFIX);
		//Set order parameters
		order.save(file);
	}

	public OrderInfo loadOrder(String orderId) throws IOException
	{
		File file = new File(ordersDir, orderId+ORDER_FILE_SUFFIX);
		if ( !file.exists()){
		    return null;
		}
		return OrderInfo.load(file);
	}

	public File saveOrderResponseFile(OrderInfo order, String content) throws IOException
	{
	    File file = new File(ordersDir, order.getId()+".response.xml");
	    FileUtil.save(file, content);
	    return file.getCanonicalFile();
	}

	public File saveOrderRequestFile(OrderInfo order, String content) throws IOException
	{
	    File file = new File(ordersDir, order.getId()+".request.xml");
	    FileUtil.save(file, content);
	    return file.getCanonicalFile();
	}
}
