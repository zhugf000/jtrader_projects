package ordersync;

import static org.jdom2.JDOMConstants.NS_PREFIX_DEFAULT;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ordersync.model.OrderInfo;
import ordersync.model.OrderItem;
import ordersync.model.OrderQueue;
import ordersync.model.OrderState;
import ordersync.model.OrderTender;
import ordersync.util.StringUtil;

/**
 * 向宜家发送订单
 */
@Service
public class OrderSender {
    private static final Logger logger = LoggerFactory.getLogger(OrderSender.class);

    private static final ContentType APPLICATION_SOAP_XML = ContentType.create("application/soap+xml", Consts.UTF_8);

    @Autowired
    private OrderQueueService queueService;

    @Autowired
    private OrderMgr orderMgr;

    private OrderQueue pendingQueue;
    private OrderQueue finishedQueue;

    @PostConstruct
    public void init()
    {
        pendingQueue = queueService.getQueue(OrderQueueService.QUEUE_PENDING);
        finishedQueue = queueService.getQueue(OrderQueueService.QUEUE_FINISHED);
    }

    @Scheduled(cron="0 */10 * * * *")
    public void sendAll()
    {
        OrderState orderState = null;
        while( (orderState=pendingQueue.peek())!=null ){
            logger.info("order to send: "+orderState);
            OrderInfo order = null;
            try{
                order = orderMgr.loadOrder(orderState.getOrderId());
            }catch(Throwable t){
                logger.error("Load order "+orderState.getOrderId()+" failed", t);
                orderState.state(OrderState.STATE_FAILED);
                orderState.logFail(orderState.getOrderId()+"/"+t.toString());
            }
            if ( order==null ){
                try {
                    finishedQueue.offer(orderState);
                } catch (Throwable t) {
                    logger.error("Move order state "+orderState+" to finish queue failed", t);
                    OrderStats.incOrderSendFailedCount(orderState.getOrderId()+"/"+t.toString());
                }
                continue;
            }
            try{
                String reqXml = generateSendXml(order);
                orderMgr.saveOrderRequestFile(order, reqXml);
                AtomicInteger statusCode = new AtomicInteger();
            	String responseXml = null;
            	long beforeSend = System.currentTimeMillis();
                try {
                	responseXml = sendOrder(reqXml, statusCode);
                }finally {
                	long afterSend = System.currentTimeMillis();
                	logger.info("Send order "+order.getId()+"/"+order.getTxDocNo()+" uses "+(afterSend-beforeSend)+" ms");
                }
                File responseFile = null;
                if ( !StringUtil.isEmpty(responseXml)) {
                	responseFile = orderMgr.saveOrderResponseFile(order, responseXml);
                }
                String failMessage = checkResponseXml(order.getId(), responseXml, statusCode.intValue());
                if ( failMessage!=null ){
                    logger.error("Send order "+order.getId()+"/"+order.getTxDocNo()+" failed, server response code: "+statusCode+", response:\n"+responseXml);
                    orderState.logFail(failMessage);
                    OrderStats.incOrderSendFailedCount(failMessage);
                }else{
                    orderState.state(OrderState.STATE_FINISHED);
                    finishedQueue.offer(orderState);
                    logger.info("Send order "+order.getId()+"/"+order.getTxDocNo()+" FINISHED, response content was saved to "+responseFile);
                    OrderStats.incOrderSendCount(order.getTxDocNo()+"/"+order.getId());
                }
            }catch(Throwable t){
                logger.error("Send order "+order.getId()+"/"+order.getTxDocNo()+" failed", t);
                orderState.logFail("Send order "+order.getId()+"/"+order.getTxDocNo()+" failed: "+t.toString());
                OrderStats.incOrderSendFailedCount(order.getTxDocNo()+"/"+order.getId()+"/"+t.toString());
            }
        }
    }

    private String sendOrder(String reqXml, AtomicInteger statusCode) throws Throwable
    {
        int timeout = 30*1000;
        StringBuilder response = new StringBuilder();
        String url = Settings.getItem(Settings.ITEM_SEND_URL);
        try(CloseableHttpClient httpClient = HttpClientBuilder.create().build();){
            HttpPost req = new HttpPost(url);
            RequestConfig reqConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(timeout)
                    .setConnectTimeout(timeout)
                    .setSocketTimeout(timeout)
                    .build();
            req.setEntity(new StringEntity(reqXml, APPLICATION_SOAP_XML));
            req.setConfig(reqConfig);
            CloseableHttpResponse resp = httpClient.execute(req);
            statusCode.set( resp.getStatusLine().getStatusCode() );
            HttpEntity respEntity = resp.getEntity();
            if (respEntity!=null ){
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                InputStream is = respEntity.getContent();
                byte[] data = new byte[1024];
                int len=0;
                while( (len=is.read(data))>0 ){
                    baos.write(data, 0, len);
                }
                if ( baos.size()>0 ){
                    response.append(new String(baos.toByteArray(), "UTF-8"));
                }
                EntityUtils.consume(respEntity);
            }
            resp.close();
        }
        return response.toString();
    }

    private String generateSendXml(OrderInfo order) throws Exception
    {
        Namespace soap12 = Namespace.getNamespace("soap12","http://www.w3.org/2003/05/soap-envelope");;
        Namespace tempurl = Namespace.getNamespace(NS_PREFIX_DEFAULT, "http://tempurl.org");
        Element headElem = (new Element("header", tempurl))
                .addContent((new Element("username", tempurl)).setText(Settings.getItem(Settings.ITEM_SEND_USERNAME)))
                .addContent((new Element("password", tempurl)).setText(Settings.getItem(Settings.ITEM_SEND_PASSWORD)))
                .addContent((new Element("messagetype", tempurl)).setText(Settings.getItem(Settings.ITEM_MESSAGE_TYPE)))
                .addContent((new Element("messageid", tempurl)).setText(Settings.getItem(Settings.ITEM_MESSAGE_ID)))
                .addContent((new Element("version", tempurl)).setText(Settings.getItem(Settings.ITEM_MESSAGE_VERSION)))
                ;

        Element salestotalElem = (new Element("salestotal", tempurl))
                .addContent((new Element("txdate_yyyymmdd", tempurl)).setText(order.getTxDate()))
                .addContent((new Element("txtime_hhmmss", tempurl)).setText(order.getTxTime()))
                .addContent((new Element("storecode", tempurl)).setText(order.getStoreCode()))
                .addContent((new Element("tillid", tempurl)).setText(order.getTillId()))
                .addContent((new Element("txdocno", tempurl)).setText(order.getTxDocNo()))
                .addContent((new Element("cashier", tempurl)).setText(order.getCashier()))
                .addContent((new Element("salesman", tempurl)).setText(order.getSalesMan()))
                ;

        Element salesitems = new Element("salesitems", tempurl);
        for(OrderItem item:order.getItems()){
            Element salesitem = (new Element("salesitem", tempurl))
                    .addContent(new Element("itemcode", tempurl).setText(order.getItemCode()!=null?order.getItemCode():item.getItemCode()))
                    .addContent(new Element("qty", tempurl).setText(item.getQty()))
                    .addContent(new Element("netamount", tempurl).setText(item.getNetAmount()))
                    ;
            salesitems.addContent(salesitem);
        }

        Element salestenders = new Element("salestenders", tempurl);
        for(OrderTender tender:order.getTenders()){
            Element salestender = (new Element("salestender", tempurl))
                    .addContent(new Element("tendercode", tempurl).setText(tender.getTenderCode()))
                    .addContent(new Element("payamount", tempurl).setText(tender.getPayAmount()))
                    .addContent(new Element("baseamount", tempurl).setText(tender.getBaseAmount()))
                    ;
            salestenders.addContent(salestender);
        }

        Element astrRequestElem = (new Element("astr_request", tempurl))
                .addContent(headElem)
                .addContent(salestotalElem)
                .addContent(salesitems)
                .addContent(salestenders)
                ;

        Element postsalescreateElem = (new Element("postsalescreate", tempurl))
                .addContent(astrRequestElem)
                ;

        Element bodyElem = (new Element("Body", soap12))
                .addContent(postsalescreateElem);

        Namespace xsi = Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
        Namespace xsd = Namespace.getNamespace("xsd","http://www.w3.org/2001/XMLSchema");
        Element rootElem = (new Element("Envelope", soap12))
                //.setAttribute(new Attribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", xmlns) )
                //.setAttribute(new Attribute("xsd", "http://www.w3.org/2001/XMLSchema", xmlns))
                .addContent(bodyElem);
        rootElem.addNamespaceDeclaration(xsi);
        rootElem.addNamespaceDeclaration(xsd);

        Document doc = new Document(rootElem);

        XMLOutputter outputter = new XMLOutputter();
        outputter.setFormat(Format.getPrettyFormat());
        return outputter.outputString(doc);
    }

    private String checkResponseXml(String orderId, String responseXml, int statusCode)
    {
    	if ( statusCode!=HttpStatus.SC_OK) {
    		return "Invalid status code: "+statusCode;
    	}
    	try {
			Document doc = (new SAXBuilder()).build(new StringReader(responseXml));
			Element root = doc.getRootElement();
			Namespace soap = root.getNamespace();
			Namespace tempurl = Namespace.getNamespace("http://tempurl.org");
			Element body = root.getChild("Body", soap);
			if ( body==null) {
				return "Invalid response xml, no body: "+root.getChildren();
			}
			Element postsalescreateResponse = body.getChild("postsalescreateResponse", tempurl);
			if ( postsalescreateResponse==null) {
				return "Invalid response xml, no postsalescreateResponse: "+body.getChildren();
			}
			Element postsalescreateResult = postsalescreateResponse.getChild("postsalescreateResult", tempurl);
			if ( postsalescreateResult==null) {
				return "Invalid response xml, no postsalescreateResult: "+postsalescreateResponse.getChildren();
			}
			Element header = postsalescreateResult.getChild("header", tempurl);
			if ( header==null) {
				return "Invalid response xml, no header: "+postsalescreateResult.getChildren();
			}
			Element responsecode = header.getChild("responsecode", tempurl);
			if ( responsecode==null) {
				return "Invalid response xml, no responsecode: "+header.getChildren();
			}
			//正常
			if ( StringUtil.strcmp("0", responsecode.getTextTrim()) ){
				return null;
			}
			Element responsemessage = header.getChild("responsemessage", tempurl);
			if ( responsemessage==null) {
				return "Invalid response xml, no responsemessage: "+responsemessage.getChildren();
			}
			return responsemessage.getTextTrim();
		} catch (Exception e) {
			logger.error("Parse order "+orderId+" response file failed", e);
			return "parse order "+orderId+" response file failed "+e;
		}
    }

}
