package ordersync;

import java.io.File;

import ordersync.model.OrderQueue;

public class OrderUtil {

	private static File appDir;

	public synchronized static File getAppDir(){
		if ( appDir==null ){
			appDir = new File(".");
			try{
				appDir = appDir.getCanonicalFile();
			}catch(Exception e){}
		}
		return appDir;
	}

	public static OrderQueue pendingQueue;
}
