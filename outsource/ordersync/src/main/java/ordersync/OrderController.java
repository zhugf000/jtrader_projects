package ordersync;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OrderController {

	@Autowired
	private OrderQueueService queueService;

	@PostConstruct
	public void init() {
		OrderUtil.pendingQueue = queueService.getQueue(OrderQueueService.QUEUE_PENDING);
	}

    @RequestMapping("/")
    public String index(Model model)
    {
        return "index";
    }

}
