package ordersync;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ordersync.model.OrderQueue;

@Service
public class OrderQueueService {
	private static final Logger logger = LoggerFactory.getLogger(OrderQueueService.class);

	public static final String QUEUE_PENDING = "pending";
	public static final String QUEUE_FINISHED = "finished";
	public static final String QUEUE_RECYLE = "recyle";

	private File queuesDir;
	private File recyleDir;
	private Map<String, OrderQueue> queues = new HashMap<>();

	@PostConstruct
	public void init(){
		File appDir = OrderUtil.getAppDir();
		queuesDir = new File(appDir, "queues");
		queuesDir.mkdirs();
		recyleDir = new File(queuesDir, QUEUE_RECYLE);
		recyleDir.mkdirs();
	}

	public synchronized OrderQueue getQueue(String queueName){
		OrderQueue result = queues.get(queueName);
		if ( result ==null ){
			result = new OrderQueue(new File(queuesDir, queueName), recyleDir);
			queues.put(queueName, result);
		}
		return result;
	}

}
