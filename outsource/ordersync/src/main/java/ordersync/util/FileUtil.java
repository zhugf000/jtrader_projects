package ordersync.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.*;

public class FileUtil {

    public static String read(File file) throws IOException
    {
        if ( !file.exists() || !file.canRead() ){
            throw new IOException("File "+file+" not exists or not readable");
        }
        StringWriter writer = new StringWriter();
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));){
            char cbuf[] = new char[4096];
            int clen;
            while ( (clen=reader.read(cbuf))>0 ){
                writer.write(cbuf,0,clen);
            }
        }
        return writer.toString();
    }

    public static Properties readProps(File file) throws IOException
    {
        try(InputStreamReader reader = new InputStreamReader(new FileInputStream(file),"UTF-8");){
            Properties props = new Properties();
            props.load(reader);
            return props;
        }
    }

    public static BufferedReader bufferedRead(File file, String encoding) throws IOException {
        if ( encoding==null ){
            return new BufferedReader(new FileReader(file));
        }else{
            return new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
        }
    }

    public static String read(InputStream is, String encoding) throws IOException
    {
        if ( encoding==null ){
            encoding = "UTF-8";
        }
        char[] cbuf = new char[4096];
        StringBuilder text= new StringBuilder(4096);
        try( InputStreamReader reader = new InputStreamReader(is, encoding); ){
            int len=0;
            while( (len=reader.read(cbuf))>0 ){
                text.append(cbuf, 0, len);
            }
        }
        return text.toString();
    }

    public static void save(File file, List<String> confText) throws IOException
    {
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8")); ){
            boolean needsNewLine = false;
            for(String line:confText){
                if ( needsNewLine ){
                    writer.write("\n");
                }
                writer.write(line);
                needsNewLine = true;
            }
        }
    }

    public static void save(File file, String text) throws IOException
    {
        try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8")); ){
            writer.write(text);
        }
    }

    public static void save(File file, InputStream is) throws IOException
    {
        try(FileOutputStream fos = new FileOutputStream(file);
            InputStream fis = is;)
        {
            byte[] data=new byte[4096];
            int len=0;
            while( (len=fis.read(data))>0 ){
                fos.write(data, 0, len);
            }
            fos.flush();
        }
    }

    public static void save(File file, Properties props) throws IOException
    {
        Properties tmp = new Properties() {
            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<Object>(super.keySet()));
            }
        };
        tmp.putAll(props);

        try(FileOutputStream fos = new FileOutputStream(file);)
        {
            tmp.store(fos, null);
        }
    }

    public static void setLastModifiedTime(File file, Instant lastModified) throws IOException
    {
        Files.setLastModifiedTime(file.toPath(), FileTime.from(lastModified));
    }

    public static void copy(File dstFile, File src) throws IOException
    {
        try(FileInputStream fis = new FileInputStream(src);){
            save(dstFile, fis);
        }
    }

    public static boolean delete(File file){
        if ( !file.exists() ){
            return false;
        }
        if ( file.isDirectory() ){
            File[] files = file.listFiles();
            if ( files!=null ){
                for(File f:files){
                    delete(f);
                }
            }
            return file.delete();
        }else{
            return file.delete();
        }
    }

    public static File changeSuffix(File file, String suffix)
    {
        String apath = file.getAbsolutePath();
        String npath = null;
        if ( apath.lastIndexOf(".")>0 ){
            int idx = apath.lastIndexOf(".");
            npath = apath.substring(0, idx)+"."+suffix;
        }else{
            npath = apath+"."+suffix;
        }
        return new File(npath);
    }

    public static String md5(File file) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        try (InputStream is = new FileInputStream(file);)
        {
            byte[] buffer = new byte[4096];
            int len=0;
            while( (len=is.read(buffer))>0 ){
                md.update(buffer, 0, len);
            }
        }
        byte[] digest = md.digest();
        StringBuilder result = new StringBuilder();
        for (int i=0; i < digest.length; i++) {
            result.append( Integer.toString( ( digest[i] & 0xff ) + 0x100, 16).substring(1) );
        }
        return result.toString();
    }

}
