package ordersync.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;

public class StringUtil
{
    public static final Charset UTF8 = Charset.forName("UTF-8");

    public static boolean isEmpty(String str)
    {
        return str==null || str.trim().length()==0;
    }

    public static String trim(String str)
    {
        if ( str==null ){
            return null;
        }
        return str.trim();
    }

    public static boolean contains(String str, String str2)
    {
        if ( isEmpty(str2) ){
            return true;
        }
        if ( isEmpty(str) ){
            return false;
        }
        return str.contains(str2);
    }

    public static String append(String str, String toAppend, String delimiter)
    {
        boolean textIsEmpty = isEmpty(str);
        boolean toAppendIsEmpty = isEmpty(toAppend);

        if ( !textIsEmpty && !toAppendIsEmpty ){
            return str+delimiter+toAppend;
        }
        if ( textIsEmpty ){
            return toAppend;
        }else {
            return str;
        }
    }

    /**
     * 大小写敏感比较
     */
    public static boolean strcmp(String str1, String str2){
        boolean str1e = isEmpty(str1);
        boolean str2e = isEmpty(str2);
        //both empty
        if ( str1e && str2e ){
            return true;
        }
        if ( str1e!=str2e ){
            return false;
        }
        //both not empty
        return str1.equals(str2);
    }

    /**
     * 大小写无关比较
     */
    public static boolean stricmp(String str1, String str2){
        boolean str1e = isEmpty(str1);
        boolean str2e = isEmpty(str2);
        //both empty
        if ( str1e && str2e ){
            return true;
        }
        if ( str1e!=str2e ){
            return false;
        }
        return str1.equalsIgnoreCase(str2);
    }

    public static String md5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    /**
     * 去引号
     */
    public static String unquotes(String str)
    {
        if ( StringUtil.isEmpty(str)){
            return str;
        }
        if ( str.charAt(0)=='"' && str.charAt(str.length()-1)=='"' ){
            return str.substring(1, str.length()-1);
        }
        if ( str.charAt(0)=='\'' && str.charAt(str.length()-1)=='\'' ){
            return str.substring(1, str.length()-1);
        }
        return str;
    }

    public static boolean booleanValue(String boolStr, boolean defaultValue){
        if( isEmpty(boolStr) ){
            return defaultValue;
        }
        boolStr = boolStr.trim().toLowerCase();
        return "true".equalsIgnoreCase(boolStr) || "yes".equalsIgnoreCase(boolStr);
    }

    public static String urlEscape(String url) {
        int i;
        char j;
        StringBuffer tmp = new StringBuffer();
        tmp.ensureCapacity(url.length() * 6);
        for (i = 0; i < url.length(); i++) {
            j = url.charAt(i);
            if (Character.isDigit(j) || Character.isLowerCase(j)
                    || Character.isUpperCase(j))
                tmp.append(j);
            else if (j < 256) {
                tmp.append("%");
                if (j < 16)
                    tmp.append("0");
                tmp.append(Integer.toString(j, 16));
            } else {
                tmp.append("%u");
                tmp.append(Integer.toString(j, 16));
            }
        }
        return tmp.toString();
    }

    /***
     * 转义特殊字符
     *
     * @param keyword
     * @return
     */
    public static String escapeExprSpecialWord(String keyword) {
        StringBuilder result = new StringBuilder(keyword.length() + 32);
        int len = keyword.length();
        for (int i = 0; i < len; i++) {
            char ch = keyword.charAt(i);
            switch (ch) {
            case '\t':
                result.append("\\t");
                break;
            case '\n':
                result.append("\\n");
                break;
            case '\r':
                result.append("\\r");
                break;
            case '"':
                result.append("'");
                break;
            case '\\':
            	result.append("\\\\");
            	break;
            default:
                result.append(ch);
            }
        }
        return result.toString();
    }
}
