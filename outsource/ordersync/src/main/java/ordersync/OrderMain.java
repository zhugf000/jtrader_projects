package ordersync;

import java.time.LocalDateTime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OrderMain extends SpringBootServletInitializer  {

    public static void main(String[] args) throws Exception
    {
        SpringApplication app = new SpringApplication(OrderMain.class);
        ConfigurableApplicationContext context = app.run(args);
        OrderStats.startTime = LocalDateTime.now();
    }

}
