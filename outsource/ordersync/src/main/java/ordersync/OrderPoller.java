package ordersync;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ordersync.model.BranchInfo;
import ordersync.model.ItemInfo;
import ordersync.model.OrderInfo;
import ordersync.model.OrderItem;
import ordersync.model.OrderQueue;
import ordersync.model.OrderState;
import ordersync.model.OrderTender;
import ordersync.util.Base16;
import ordersync.util.Base58;

/**
 * 读取云POS的数据库，找到新的OrderInfo, 并保存。
 */
@Service
public class OrderPoller {
    private static final Logger logger = LoggerFactory.getLogger(OrderPoller.class);

    @Autowired
    private OrderQueueService queueService;

    @Autowired
    private OrderMgr orderMgr;

    private OrderQueue pendingQueue;

    private Set<String> ignoredOrderIds = Collections.synchronizedSet(new HashSet<>());

    private Map<String, ItemInfo> itemInfos = Collections.synchronizedMap(new HashMap<>());

    private Map<Integer, BranchInfo> branchInfos = Collections.synchronizedMap(new HashMap<>());

    @PostConstruct
    public void init(){
        pendingQueue = queueService.getQueue(OrderQueueService.QUEUE_PENDING);
    }

    public void setOrderMgr(OrderMgr orderMgr){
        this.orderMgr = orderMgr;
    }

    @Scheduled(cron="0 */10 * * * *")
    //@Scheduled(cron="0 * * * * *")
    public void pollOrders(){
        if ( logger.isDebugEnabled()){
            logger.debug("Begin to poll orders");
        }
        String url = Settings.getItem(Settings.ITEM_POLL_DB_URL);
        String usr = Settings.getItem(Settings.ITEM_POLL_DB_USR);
        String pwd = Settings.getItem(Settings.ITEM_POLL_DB_PWD);
        try( Connection conn = DriverManager.getConnection(url, usr, pwd);
             Statement stmt=conn.createStatement(); )
        {
            List<OrderInfo> orders = new ArrayList<>();

            ResultSet rs = stmt.executeQuery("SELECT * FROM yh_pos_bill_master where operdate>DATEADD(dd, -7, CURRENT_TIMESTAMP)");
            while(rs.next()) {
                OrderInfo order = createOrderInfo(conn, rs);
                if (order!=null){
                    orders.add(order);
                }
            }
            rs.close();

            for(OrderInfo order:orders){
                try{
                    loadOrderDetails(conn, order);
                    orderMgr.saveOrder(order);
                    OrderState orderState = new OrderState(order.getId());
                    pendingQueue.offer(orderState);
                    logger.info("Queue new order "+order.getId()+"/"+order.getTxDocNo()+" to pending queue.");
                    OrderStats.incOrderPollCount(order.getTxDocNo()+"/"+order.getId());
                }catch(Throwable t){
                    logger.error("Load order "+order.getId()+" details and save failed", t);
                    OrderStats.incOrderPollFailedCount(t.toString());
                }
            }
        }catch(Throwable t) {
            logger.error("query yunpos failed", t);
            OrderStats.incOrderPollFailedCount(t.toString());
        }
    }

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HHmmss");

    private OrderInfo createOrderInfo(Connection conn, ResultSet rs) throws SQLException
    {
        String orderId = rs.getString("flowid");
        String sale_way = rs.getString("sale_way").trim();
        if ( !(sale_way.equalsIgnoreCase("S") || sale_way.equalsIgnoreCase("R")) ){
            if ( logger.isDebugEnabled()){
                logger.debug("Ignore order "+orderId+" sale_way: "+sale_way);
            }
            return null;
        }
        if ( orderMgr.orderExists(orderId)){
            if ( logger.isDebugEnabled()){
                logger.debug("Ignore Order "+orderId+" since exists");
            }
            return null;
        }
        if ( ignoredOrderIds.contains(orderId)){
            if ( logger.isDebugEnabled()){
                logger.debug("Ignore Order "+orderId+" since rememberd to ignore");
            }
            return null;
        }
        String flowno = rs.getObject("flowno").toString();
        int branchId = rs.getInt("branchid");
        String branchNo = getBranchNo(conn, branchId);
        String storeCode = Settings.getItem("mapping.branchNo."+branchNo+".storeCode");
        if ( storeCode==null ){
            if ( logger.isInfoEnabled() ){
                logger.debug("Ignore order "+orderId+"/"+flowno+" due to unsupported branch: "+branchId+"/"+branchNo);
            }
            ignoredOrderIds.add(orderId);
            return null;
        }
        String mappedItemCode = Settings.getItem("mapping.branchNo."+branchNo+".itemCode");

        String rawTillId = flowno.substring(0, 4);
        String tillId = Settings.getItem("mapping.tillId."+rawTillId);
        if ( tillId==null ){
            tillId = Settings.getItem("mapping.tillId.default");
            logger.info("Order "+orderId+" use default tillId since no mapping for "+rawTillId);
        }
        OrderInfo order = new OrderInfo(orderId);
        order.setStoreCode(storeCode);
        order.setTxDocNo(flowno);
        order.setItemCode(mappedItemCode);
        Date operDate = (Date)rs.getObject("operdate");
        order.setTxDate(dateFormat.format(operDate));
        order.setTxTime(timeFormat.format(operDate));
        order.setCashier(""+rs.getObject("userid"));
        order.setSalesMan(order.getCashier());
        order.setTillId(tillId);
        order.setSaleWay(sale_way);
        return order;
    }

    private OrderInfo loadOrderDetails(Connection conn, OrderInfo order) throws SQLException
    {
    	//退货时，数量为负数
    	BigDecimal qtyUnit = new BigDecimal(1);
    	if ( "R".equalsIgnoreCase(order.getSaleWay()) ){
    		qtyUnit = new BigDecimal(-1);
    	}
        String orderId = order.getId();
        List<OrderItem> orderItems = new ArrayList<>();
        try(Statement stmt = conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM yh_pos_bill_sale where flowid='"+orderId+"'");){
            while(rs.next()){
                String item_no = rs.getString("item_no");
                BigDecimal sale_qty = rs.getBigDecimal("sale_qty");
                BigDecimal sale_amt = rs.getBigDecimal("sale_amt");
                OrderItem orderItem = new OrderItem();

                orderItem.setItemCode( getItemCode(conn, item_no) );
                orderItem.setQty(sale_qty.multiply(qtyUnit).setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                orderItem.setNetAmount(sale_amt.setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                orderItems.add(orderItem);
            }
        }

        List<OrderTender> orderTenders = new ArrayList<>();
        try(Statement stmt = conn.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM yh_pos_bill_pay where flowid='"+orderId+"'");){
            while(rs.next()){
                String pay_way = rs.getString("pay_way");
                BigDecimal pay_amt = rs.getBigDecimal("pay_amt");

                OrderTender tender = new OrderTender();
                String tenderCode = Settings.getItem("mapping.tenderCode."+pay_way);
                if ( tenderCode==null ){
                    tenderCode = Settings.getItem("mapping.tenderCode.default");
                }
                tender.setTenderCode(tenderCode);
                tender.setPayAmount(pay_amt.setScale(4, BigDecimal.ROUND_HALF_UP).toString());
                tender.setBaseAmount(tender.getPayAmount());
                orderTenders.add(tender);
            }
        }
        order.setItems(orderItems);
        order.setTenders(orderTenders);
        return order;
    }

    private String getItemCode(Connection conn, String itemNo) throws SQLException
    {
        ItemInfo item = itemInfos.get(itemNo);
        if ( item==null ){
            int loadItems = 0;
            try(Statement stmt = conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM yh_item");)
            {
                while(rs.next()){
                    ItemInfo i = new ItemInfo();
                    i.setNo(rs.getString("item_no"));
                    i.setCode(rs.getString("item_code"));
                    i.setSubno(rs.getString("item_subno"));
                    i.setName(rs.getString("item_name"));
                    i.setSubname(rs.getString("item_subname"));

                    itemInfos.put(i.getNo(), i);
                    loadItems++;
                }
            }
            logger.info("Load "+loadItems+" items, total items in memory: "+itemInfos.size()+", caused by "+itemNo);
        }

        item = itemInfos.get(itemNo);
        if ( item!=null ){
            return item.getCode();
        }
        String result = Base58.encode(Base16.decode(itemNo));
        logger.warn("No item for "+itemNo+", will use BASE58 encoded value: "+result);
        //Convert itemCode from BASE16 UUID to BASE58 UUID
        return result;
    }

    private String getBranchNo(Connection conn, int branchId) throws SQLException{
        BranchInfo branchInfo = branchInfos.get(branchId);
        if ( branchInfo==null ){
            int loadBranches = 0;
            try(Statement stmt = conn.createStatement();
                ResultSet rs=stmt.executeQuery("SELECT * FROM yh_ba_branch_info");)
            {
                while(rs.next()){
                    BranchInfo i = new BranchInfo();
                    i.setId(rs.getInt("branchid"));
                    i.setNo(rs.getString("branchno"));
                    i.setName(rs.getString("branchname"));

                    branchInfos.put(i.getId(), i);
                    loadBranches++;
                }
            }
            logger.info("Load "+loadBranches+" branches, total branches in memory: "+branchInfos.size()+", caused by "+branchId);
        }

        branchInfo = branchInfos.get(branchId);
        if ( branchInfo!=null ){
            return branchInfo.getNo();
        }else{
            String result = String.format("%04d", branchId);
            logger.warn("No branch for "+branchId+", will use "+result);
            return result;
        }
    }
}
