package ordersync;

import java.io.*;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Settings {
	private static final Logger logger = LoggerFactory.getLogger(Settings.class);

	public static final String ITEM_MESSAGE_TYPE = "message.type";
	public static final String ITEM_MESSAGE_ID = "message.id";
	public static final String ITEM_MESSAGE_VERSION = "message.version";

	public static final String ITEM_MALLID = "mallId";
	public static final String ITEM_SEND_URL = "send.URL";
    public static final String ITEM_SEND_USERNAME = "send.username";
    public static final String ITEM_SEND_PASSWORD = "send.password";


	public static final String ITEM_POLL_DB_URL = "poll.DBURL";
	public static final String ITEM_POLL_DB_USR = "poll.DBUsr";
	public static final String ITEM_POLL_DB_PWD = "poll.DBPwd";

	public static final String ITEM_CONTROLL_MAX_TRYCOUNT = "controll.maxTryCount";

	private static volatile Properties props = new Properties();
	private static volatile long lastModifiedTime = 0;

	static{
		loadAndPoll();
	}

	public static String getItem(String item){
		return props.getProperty(item);
	}

	private static void loadAndPoll(){
		File etcDir = new File(OrderUtil.getAppDir(), "etc");
		etcDir.mkdirs();
		final File etcFile = new File(etcDir, "settings.properties");
		//Init with default values
        try(InputStream is=Settings.class.getResourceAsStream("/settings.properties");){
            props.load(is);
        }catch(Throwable t) {}
        loadSettings(etcFile);

		Thread settingsPollThread = new Thread(()->{
			pollSettingsFunc(etcFile);
		}, "settings poll thread");
		settingsPollThread.setDaemon(true);
		settingsPollThread.start();
	}

	private static void loadSettings(File etcFile) {
	    if ( etcFile.exists()  ) {
            try(InputStream fis=new FileInputStream(etcFile);){
                Properties props2 = new Properties();
                props2.load(fis);
                props = props2;
                lastModifiedTime = etcFile.lastModified();
                logger.info("Load settings file "+etcFile+" last modified: "+lastModifiedTime);
            } catch (Throwable e) {
                logger.error("Load settings file "+etcFile+" failed", e);
            }
        }
	}

	private static void pollSettingsFunc(File etcFile){
	    //Load default values from resource
		while(true){
			if ( !etcFile.exists() ){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				continue;
			}
			if ( etcFile.lastModified()!=lastModifiedTime ){
			    loadSettings(etcFile);
			}
			try {
				Thread.sleep(60*1000);
			} catch (InterruptedException e) {}
		}
	}
}
