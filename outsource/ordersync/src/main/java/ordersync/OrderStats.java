package ordersync;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class OrderStats {
    public static LocalDateTime startTime;
    private static LocalDate thisDay;

    private static long totalOrderPolled;
    private static long totalOrderPollFailedCount;
    private static long totalOrderSent;
    private static long totalOrderSendFailedCount;

    private static long thisDayOrderPolled;
    private static long thisDayOrderPollFailedCount;
    private static long thisDayOrderSent;
    private static long thisDayOrderSendFailedCount;

    private static String lastOrderPollFailedMessage;
    private static String lastOrderSendFailedMessage;

    private static LocalDateTime lastOrderPollTime;
    private static LocalDateTime lastOrderSendTime;
    private static String lastOrderPollNo;
    private static String lastOrderSendNo;

    public static LocalDateTime getStartTime(){
        return startTime;
    }

    public static long getTotalOrderPolled() {
        return totalOrderPolled;
    }

    public static long getTotalOrderPollFailedCount() {
        return totalOrderPollFailedCount;
    }

    public static long getTotalOrderSent() {
        return totalOrderSent;
    }

    public static long getTotalOrderSendFailedCount() {
        return totalOrderSendFailedCount;
    }

    public static long getThisDayOrderPolled() {
        return thisDayOrderPolled;
    }

    public static long getThisDayOrderPollFailedCount() {
        return thisDayOrderPollFailedCount;
    }

    public static long getThisDayOrderSent() {
        return thisDayOrderSent;
    }

    public static long getThisDayOrderSendFailedCount() {
        return thisDayOrderSendFailedCount;
    }


    public static String getLastOrderPollNo() {
        return lastOrderPollNo;
    }

    public static LocalDateTime getLastOrderPollTime() {
    	return lastOrderPollTime;
    }

    public static String getLastOrderPollFailedMessage() {
        return lastOrderPollFailedMessage;
    }

    public static String getLastOrderSendNo() {
        return lastOrderSendNo;
    }

    public static LocalDateTime getLastOrderSendTime() {
    	return lastOrderSendTime;
    }

    public static String getLastOrderSendFailedMessage() {
        return lastOrderSendFailedMessage;
    }

    public static void incOrderPollCount(String orderNo){
        resetThisDayCounters();
        totalOrderPolled++;
        thisDayOrderPolled++;
        lastOrderPollNo = orderNo;
        lastOrderPollTime = LocalDateTime.now();
        lastOrderPollFailedMessage = "";
    }

    public static void incOrderPollFailedCount(String failMessage){
        resetThisDayCounters();
        totalOrderPollFailedCount++;
        thisDayOrderPollFailedCount++;
        lastOrderPollTime = LocalDateTime.now();
        lastOrderPollFailedMessage = failMessage;
    }

    public static void incOrderSendCount(String orderNo){
        resetThisDayCounters();
        totalOrderSent++;
        thisDayOrderSent++;
        lastOrderSendNo = orderNo;
        lastOrderSendTime = LocalDateTime.now();
        lastOrderSendFailedMessage = "";
    }

    public static void incOrderSendFailedCount(String failMessage){
        resetThisDayCounters();
        totalOrderSendFailedCount++;
        thisDayOrderSendFailedCount++;
        lastOrderSendTime = LocalDateTime.now();
        lastOrderSendFailedMessage = failMessage;
    }

    private static void resetThisDayCounters(){
        LocalDate now = LocalDate.now();
        if ( thisDay==null ){
            thisDay = now;
            return;
        }
        if( !now.isEqual(thisDay) ){
            thisDayOrderPolled = 0;
            thisDayOrderSent = 0;
            thisDayOrderSendFailedCount = 0;
            thisDay = now;
        }
    }
}
