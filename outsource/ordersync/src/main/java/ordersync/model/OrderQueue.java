package ordersync.model;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderQueue {
	private static final Logger logger = LoggerFactory.getLogger(OrderQueue.class);

	private File dir;
	private File recyleDir;
	private LinkedList<OrderState> queue = new LinkedList<>();

	public OrderQueue(File directory, File recyleDir){
		this.dir = directory;
		this.recyleDir = recyleDir;
		directory.mkdirs();
		recyleDir.mkdirs();
		//Load orders from directory
		loadOrderStates();
	}

    public String getName() {
        return dir.getName();
    }

	public File getQueueDirectory(){
		return dir;
	}

	public int getSize() {
		return queue.size();
	}

	public List<String> getOrderIds(){
		List<String> result = new ArrayList<>();
		for(OrderState order:queue) {
			result.add(order.getOrderId());
		}
		return result;
	}

	private void loadOrderStates(){
	    File stateFiles[] = dir.listFiles();
	    List<String> orderStates = new ArrayList<>();
	    if ( stateFiles!=null && stateFiles.length>0 ) {
	        for(File f:stateFiles) {
	            String fname = f.getName();
	            if ( fname.endsWith(OrderState.FILE_SUFFIX)) {
	                orderStates.add(fname.substring(0, fname.length()-OrderState.FILE_SUFFIX.length()));
	            }
	        }
	    }
	    Collections.sort(orderStates);
	    for(String orderId:orderStates) {
	        try {
                queue.add(OrderState.load(this, orderId));
            } catch (Throwable e) {
                File file = new File(getQueueDirectory(), orderId+OrderState.FILE_SUFFIX);
                logger.error("Load order state from "+file+" failed, move to "+recyleDir, e);
                file.renameTo(new File(recyleDir, orderId+OrderState.FILE_SUFFIX));
            }
	    }
	    logger.info("Order queue "+getName()+" recovers "+queue.size()+" order states");
	}

	public synchronized OrderState peek(){
        long nowSeconds = Instant.now().getEpochSecond();
		for(Iterator<OrderState> it=queue.iterator(); it.hasNext(); ){
			OrderState state = it.next();
			switch(state.state()) {
			case OrderState.STATE_PENDING:
	            if ( (nowSeconds-state.getLastFailSeconds())>=60 ){
	                return state;
	            }
	            break;
            default:
                return state;
			}
		}
		return null;
	}

	public synchronized void offer(OrderState order) throws IOException
	{
	    order.moveTo(this);
	    if ( !queue.contains(order) ) {
	        queue.add(order);
	    }
	}

	synchronized void remove(OrderState order){
		queue.remove(order);
	}

}
