package ordersync.model;

public class OrderTender {

	private String tenderCode;
	private String payAmount;
	private String baseAmount;

	public String getTenderCode() {
		return tenderCode;
	}
	public void setTenderCode(String tenderCode) {
		this.tenderCode = tenderCode;
	}
	public String getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	public String getBaseAmount() {
		return baseAmount;
	}
	public void setBaseAmount(String baseAmount) {
		this.baseAmount = baseAmount;
	}


}
