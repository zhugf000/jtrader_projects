package ordersync.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import ordersync.util.IniFile;
import ordersync.util.IniWriter;

public class OrderInfo {

	public static final String SALE_WAY_SELL = "S";
	public static final String SALE_WAY_REVERSE = "R";

	private String id;
	private String txDate;
	private String txTime;
	private String storeCode;
	private String tillId;
	private String txDocNo;
	private String cashier;
	private String salesMan;

	/**
	 * S销售, R退货, G 赠送
	 */
	private String saleWay;
	private String itemCode;

	private List<OrderItem> items = new ArrayList<>();
	private List<OrderTender> tenders = new ArrayList<>();

	public OrderInfo(String id){
		this.id = id;
	}

	OrderInfo(){
	}

	public String getId() {
		return id;
	}

	public String getTxDate() {
		return txDate;
	}

	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}

	public String getTxTime() {
		return txTime;
	}

	public void setTxTime(String txTime) {
		this.txTime = txTime;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public String getTillId() {
		return tillId;
	}

	public void setTillId(String tillId) {
		this.tillId = tillId;
	}

	public String getTxDocNo() {
		return txDocNo;
	}

	public void setTxDocNo(String txDocNo) {
		this.txDocNo = txDocNo;
	}

	public String getCashier() {
		return cashier;
	}

	public void setCashier(String cashier) {
		this.cashier = cashier;
	}

	public String getSalesMan() {
		return salesMan;
	}

	public void setSalesMan(String salesMan) {
		this.salesMan = salesMan;
	}

	public String getSaleWay() {
		return saleWay;
	}

	public void setSaleWay(String saleWay) {
		this.saleWay = saleWay;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public void setItems(List<OrderItem> items) {
		this.items = items;
	}

	public List<OrderItem> getItems(){
		return items;
	}

	public void save(File file) throws IOException
	{
		try(IniWriter iniWriter = new IniWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));){
			iniWriter.writeSection("common");
			iniWriter.writeProperty("id", getId());
			iniWriter.writeProperty("txDate", getTxDate());
			iniWriter.writeProperty("txTime", getTxTime());
			iniWriter.writeProperty("storeCode", getStoreCode());
			iniWriter.writeProperty("itemCode", getItemCode());
			iniWriter.writeProperty("tillId", getTillId());
			iniWriter.writeProperty("txDocNo", getTxDocNo());
			iniWriter.writeProperty("cashier", getCashier());
			iniWriter.writeProperty("salesMan", getSalesMan());
			iniWriter.writeProperty("saleWay", getSaleWay());

			for(OrderItem item:getItems()){
				iniWriter.writeSection("item-"+item.getItemCode());
				iniWriter.writeProperty("itemCode", item.getItemCode());
				iniWriter.writeProperty("qty", item.getQty());
				iniWriter.writeProperty("netAmount", item.getNetAmount());
			}

			for(OrderTender tender:getTenders()){
				iniWriter.writeSection("tender-"+tender.getTenderCode());
				iniWriter.writeProperty("tenderCode", tender.getTenderCode());
				iniWriter.writeProperty("payAmount", tender.getPayAmount());
				iniWriter.writeProperty("baseAmount", tender.getBaseAmount());
			}
		}
	}

	public static OrderInfo load(File file) throws IOException
	{
		OrderInfo result = new OrderInfo();
		IniFile iniFile = new IniFile(file);
		for(IniFile.Section section:iniFile.getAllSections()){
			if ( section.getName().equals("common")){
				result.id = section.get("id");
				result.txDate = section.get("txDate");
				result.txTime = section.get("txTime");
				result.storeCode = section.get("storeCode");
				result.itemCode = section.get("itemCode");
				result.tillId = section.get("tillId");
				result.txDocNo = section.get("txDocNo");
				result.cashier = section.get("cashier");
				result.salesMan = section.get("salesMan");
				result.saleWay = section.get("saleWay");
			}else if ( section.getName().startsWith("item")){
				OrderItem item = new OrderItem();
				item.setItemCode(section.get("itemCode"));
				item.setQty(section.get("qty"));
				item.setNetAmount(section.get("netAmount"));
				result.getItems().add(item);
			}else if ( section.getName().startsWith("tender")){
				OrderTender tender = new OrderTender();
				tender.setTenderCode(section.get("tenderCode"));
				tender.setPayAmount(section.get("payAmount"));
				tender.setBaseAmount(section.get("baseAmount"));
				result.getTenders().add(tender);
			}else{
				throw new IOException("Unsupported section "+section.getName());
			}
		}
		return result;
	}

	public void setTenders(List<OrderTender> orderTenders) {
		this.tenders = orderTenders;
	}

	public List<OrderTender> getTenders() {
		return tenders;
	}

}
