package ordersync.model;

import java.io.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Properties;

public class OrderState {
    public static final String FILE_SUFFIX = ".properties";

	public static final String STATE_PENDING = "pending";
	public static final String STATE_FINISHED = "finished";
	public static final String STATE_FAILED = "failed";

	private OrderQueue queue;
	private String orderId;
	private String state;
	private String receiveTime = "";
	private String finishTime = "";
	private int tryCount;
	private long lastFailSeconds;
	private String lastFailTime = "";
	private String lastFailMessage = "";

	public OrderState(String orderId){
		this.orderId = orderId;
		state = STATE_PENDING;
		receiveTime = LocalDateTime.now().toString();
	}

	OrderState(OrderQueue queue){
	    this.queue = queue;
	}

	public OrderQueue getQueue() {
	    return queue;
	}

	public String state(){
		return state;
	}

	public String getOrderId(){
		return orderId;
	}

	public void state(String newState){
		state = newState;
		if ( state.equalsIgnoreCase(STATE_FINISHED)){
			finishTime = LocalDateTime.now().toString();
		}
	}

	public void logFail(String failMessage){
        tryCount++;
        lastFailSeconds = Instant.now().getEpochSecond();
        lastFailTime = LocalDateTime.now().toString();
		lastFailMessage = failMessage;
	}

	public long getLastFailSeconds(){
		return lastFailSeconds;
	}

	public void save() throws IOException
	{
	    Properties props = new Properties();
	    props.setProperty("orderId", orderId);
	    props.setProperty("state", state);
	    props.setProperty("receiveTime", receiveTime);
	    props.setProperty("finishTime", finishTime);
	    props.setProperty("tryCount", ""+tryCount);
	    props.setProperty("lastFailSeconds", ""+lastFailSeconds);
	    props.setProperty("lastFailTime", lastFailTime);
	    props.setProperty("lastFailMessage", lastFailMessage);
	    try(OutputStream os = new FileOutputStream( getStateFile(queue, orderId) );){
	        props.store(os, null);
	    }
	}

	private static File getStateFile(OrderQueue queue, String orderId) {
	    if ( queue==null ){
	        return null;
	    }
	    return new File(queue.getQueueDirectory(), orderId+FILE_SUFFIX);
	}

	public static OrderState load(OrderQueue queue, String orderId) throws IOException
	{
	    File file = getStateFile(queue, orderId);
		OrderState state = new OrderState(queue);
		Properties props = new Properties();
		try(InputStream is=new FileInputStream(file);){
			props.load(is);
		}
		state.orderId = props.getProperty("orderId");
		state.state = props.getProperty("state");
		state.receiveTime = props.getProperty("receiveTime");
		state.finishTime = props.getProperty("finishTime");
		state.tryCount = Integer.parseInt(props.getProperty("tryCount"));
		state.lastFailSeconds = Long.parseLong(props.getProperty("lastFailSeconds"));
		state.lastFailTime = props.getProperty("lastFailTime");
		state.lastFailMessage = props.getProperty("lastFailMessage");
		return state;
	}

	boolean moveTo(OrderQueue newQueue) throws IOException
	{
	    if ( queue==newQueue ) {
	        return true;
	    }
	    boolean result = false;
	    File stateFile = getStateFile(queue, orderId);
	    File newStateFile = getStateFile(newQueue, orderId);
	    if ( stateFile==null || !stateFile.exists() ) {
	        result = true;
	    }else {
	        if ( stateFile.renameTo(newStateFile) ){
	            result = true;
	        }else{
	            result = result = stateFile.delete();
	        }
	    }
	    if ( !result ) {
	        return false;
	    }
	    if ( queue!=null ){
	    	queue.remove(this);
	    }
	    queue = newQueue;
        save();
        return true;
	}

	@Override
    public String toString(){
	    return "OrderState["+orderId+", "+state+" in queue "+(queue!=null?queue.getName():"");
	}
}
