package ordersync.model;

public class ItemInfo {
    /**
     * 唯一
     */
    private String no;

    /**
     * 货号
     */
    private String code;

    /**
     * 条码
     */
    private String subno;

    private String name;

    private String subname;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSubno() {
        return subno;
    }

    public void setSubno(String subno) {
        this.subno = subno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

}
