package net.jtrader.security;

public interface CmdFactory {

    Cmd create(String args[]);

}
