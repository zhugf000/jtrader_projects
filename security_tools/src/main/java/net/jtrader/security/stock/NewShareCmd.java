package net.jtrader.security.stock;

import java.time.LocalDate;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import common.exchangeable.Exchange;
import common.util.*;
import net.jtdxapi.JtdxApi;
import net.jtrader.security.Cmd;
import net.jtrader.security.CmdContext;

public class NewShareCmd implements Cmd {
    private static Logger logger = LoggerFactory.getLogger(NewShareCmd.class);
    private String account;

    public NewShareCmd(String accountSection)
    {
        this.account = accountSection;
    }

    @Override
    public void execute(CmdContext ctx) throws Exception
    {
        List<NewShareInfo> newShares = NewShareInfo.load();
        LocalDate today = LocalDate.now();
        List<NewShareInfo> newShareToPurchase = getNewShareToPurchase(newShares, today);
        if ( newShareToPurchase.isEmpty()){
            logger.info("今日无新股: "+today);
            System.exit(0);
        }
        for(NewShareInfo info:newShareToPurchase){
            logger.info("可申购新股: "+info.purchaseCode+" "+info.securityName);
        }
        Config config = ctx.getAccountConfig(account);
        //忽略不能买的新股
        //目前TDX api不能查询新股申购限额，必须要从配置文件读取
        for(Iterator<NewShareInfo>it=newShareToPurchase.iterator();it.hasNext();){
            NewShareInfo info = it.next();
            int maxPurchaseVolume = config.getInt(info.exchange.name().toLowerCase()+".maxPurchaseVolume",0);
            if ( maxPurchaseVolume==0 ){
                logger.info("忽略新股 "+info.purchaseCode+" "+info.securityName+", 因为申购额度为0");
                it.remove();
                continue;
            }
        }
        if ( newShareToPurchase.size()==0 ){
            logger.info("无新股可申购");
            return;
        }
        JtdxApi tdxApi = ctx.createTdxConn(account);

        //解析 股东代码
        //股东代码    股东名称    资金帐号    帐号类别    融资融券标识  保留信息
        //A000000000  DDD 09000000000001  1   0   
        //0000000000  DDD 09000000000001  0   0   
        String sseStockOwnerCode = null; //上海
        String szseStockOwnerCode = null; //深圳
        {
            String stockOwners = tdxApi.query(5);
            CSVDataSet ds = CSVUtil.parse(stockOwners, '\t', true);
            while(ds.next()){
                String stockOwneCode = ds.get("股东代码");
                String stockOwnerType = ds.get("帐号类别");
                if ( "1".equals(stockOwnerType)){
                    sseStockOwnerCode = stockOwneCode;
                }else if ( "0".equals(stockOwnerType)){
                    szseStockOwnerCode = stockOwneCode;
                }
            }
        }
        
        //今日委托
        //委托时间  股东代码    证券代码    证券名称    买卖标志    委托类别    交易所代码   委托价格    委托数量    成交价格    成交数量    撤单数量    委托编号    报价方式    状态说明    冻结资金    保留信息
        {
            String todayOrders = tdxApi.query(3); 
            CSVDataSet todayOrdersDs = CSVUtil.parse(todayOrders, '\t', true);
            //查询已有委托, 避免重复
            while(todayOrdersDs.next()){
                String orderId = todayOrdersDs.get("委托编号");
                String securityCode = todayOrdersDs.get("证券代码");
                for(Iterator<NewShareInfo>it=newShareToPurchase.iterator();it.hasNext();){
                    NewShareInfo info = it.next();
                    if ( securityCode.equals(info.securityCode) || securityCode.equals(info.purchaseCode)){
                        logger.info("忽略新股 "+info.purchaseCode+" "+info.securityName+", 因为当日已申购: "+todayOrdersDs.getLine());
                        it.remove();
                    }
                }
            }
            if ( newShareToPurchase.size()==0 ){
                logger.info("无新股可申购");
                return;
            }
        }
        //报单
        for(NewShareInfo info: newShareToPurchase){
            int maxPurchaseVolume = config.getInt(info.exchange.name().toLowerCase()+".maxPurchaseVolume",0);
            String stockOwnerCode = null;
            if ( info.exchange==Exchange.SSE ){
                stockOwnerCode = sseStockOwnerCode;
            }else if ( info.exchange==Exchange.SZSE ){
                stockOwnerCode = szseStockOwnerCode;
            }
            double price = PriceUtil.long2price(info.purchasePrice);
            long quantity = Math.min(info.maxPurchaseVolume, maxPurchaseVolume);
            String result = tdxApi.sendOrder(1, stockOwnerCode, info.purchaseCode, price, (int)quantity);
            logger.info("申购新股: "+info.securityCode+" "+info.securityName+":\n"+result);
        }
        //exit
        tdxApi.logout();
    }

    private static List<NewShareInfo> getNewShareToPurchase(List<NewShareInfo> newShares, LocalDate today)
    {
        List<NewShareInfo> result = new LinkedList<>();
        for(NewShareInfo info:newShares){
            if ( today.equals(info.purchaseDate) ){
                result.add(info);
            }
        }
        return result;
    }

}
