package net.jtrader.security.stock;

import net.jtrader.security.Cmd;
import net.jtrader.security.CmdFactory;

/**
 * 新股申购
 */
public class NewShareCmdFactory implements CmdFactory{

    @Override
    public Cmd create(String[] args)
    {
        String accountSection = args[0];
        return new NewShareCmd(accountSection);
    }

}
