package net.jtrader.security.stock;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import common.exchangeable.Exchange;
import common.exchangeable.ExchangeableUtil;
import common.util.*;

/**
 * 新股上市信息
 */
public class NewShareInfo {

    /**
     * 证券代码
     */
    public String securityCode;
    /**
     * 申购代码
     */
    public String purchaseCode;
    /**
     * 证券名称
     */
    public String securityName;

    /**
     * 申购日期
     */
    public LocalDate purchaseDate;

    /**
     * 上市日期
     */
    public LocalDate ipoDate;

    /**
     * 发行数量
     */
    public long issueAmount;

    /**
     * 上网发行数量
     */
    public long issueOnlineAmount;

    /**
     * 申购价格.
     * <BR>PriceUtil
     */
    public long purchasePrice;

    /**
     * PE
     */
    public String pe;

    /**
     * 个人申购上限
     */
    public long maxPurchaseVolume;
    /**
     * 交易所----根据股票代码推断
     */
    public Exchange exchange;


    public static List<NewShareInfo> load() throws IOException
    {
        List<NewShareInfo> result = new LinkedList<>();
        String url = "http://money.finance.sina.com.cn/corp/view/vRPD_NewStockIssue.php";
        String text = NetUtil.readHttpAsText(url, NetUtil.gbkCharset);
        Document doc = Jsoup.parse(text);
        Element newStockTable = doc.select("#NewStockTable").first();
        for(Element tr : newStockTable.select("tr") ){
            Elements tds = tr.select("td");
            if ( tds.size()<10 ){
                continue;
            }
            String securityCode = tds.get(0).text().trim();
            if (securityCode.length()==0 || !Character.isDigit(securityCode.charAt(0))){
                continue;
            }
            NewShareInfo share = new NewShareInfo();
            share.securityCode = securityCode;
            share.purchaseCode = tds.get(1).text().trim();
            share.securityName = tds.get(2).text().trim();
            share.purchaseDate = DateUtil.str2localdate(tds.get(3).text().trim());
            share.ipoDate = DateUtil.str2localdate( StringUtil.trim(tds.get(4).text()));
            share.issueAmount = parseIssueAmount(tds.get(5).text());
            share.issueOnlineAmount = parseIssueAmount(tds.get(6).text());
            share.purchasePrice = PriceUtil.price2long( Double.parseDouble(tds.get(7).text().trim()) );
            share.pe = tds.get(8).text().trim();
            share.maxPurchaseVolume = ((new BigDecimal(tds.get(9).text().trim())).multiply(new BigDecimal(10000))).longValue();
            share.exchange = ExchangeableUtil.detectAShareStockExchange(share.securityCode);
            result.add(share);
        }
        return result;
    }

    private static long parseIssueAmount(String amount){
        amount = amount.trim();
        if ( amount.startsWith("*")){
            amount = amount.substring(1).trim();
        }
        return Long.parseLong(amount)*10000;
    }

}
