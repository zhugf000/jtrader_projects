package net.jtrader.security;

import java.io.PrintWriter;

import net.jtrader.security.stock.NewShareCmdFactory;

public class Main {

    public static void main(String[] args) throws Throwable
    {
        if ( args.length==0 ){
            usage();
            return;
        }
        CmdContext ctx = new CmdContext();
        createCommander(args).execute(ctx);
    }

    private static void usage(){
        PrintWriter w = new PrintWriter(System.out,true);
        w.println("security_tools Command CommandOptions");
        w.println("");
        w.println("new_share command option:");
        w.println("\t[account section]");
    }

    private static Cmd createCommander(String[] args) throws Exception
    {
        String cmd = args[0];
        String[] cmdArgs = new String[args.length-1];
        System.arraycopy(args, 1, cmdArgs, 0, cmdArgs.length);
        switch(cmd){
        case "new_share":
            return (new NewShareCmdFactory()).create(cmdArgs);
        }
        throw new RuntimeException("Unknown command: "+cmd);
    }

}
