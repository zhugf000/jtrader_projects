package net.jtrader.security;

public interface Cmd {
    public void execute(CmdContext ctx) throws Exception;
}
