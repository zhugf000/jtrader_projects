package net.jtrader.security;

import java.io.*;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import common.util.*;
import net.jtdxapi.JtdxApi;

public class CmdContext {
    private static boolean tdxInit;
    private Config config;

    public File getAppDir()
    {
        String appDir = System.getProperty("trader.appDir");
        if ( appDir==null ){
            File jarFile = ResourceUtil.detectJarFile(CmdContext.class);
            appDir = jarFile.getParentFile().getParentFile().getAbsolutePath();
        }
        return new File(appDir);
    }

    public Config getAccountConfig(String account)
    {
        if ( config==null ){
            try{
                Reader configFile = new StringReader( FileUtil.load( new File(getAppDir(), "etc/config.yaml")) );
                Map<String,Object> rootMap = (Map<String,Object>)(new Yaml()).load(configFile);
                config = new Config(rootMap, null);
            }catch(IOException ioe){
                throw new RuntimeException(ioe);
            }
        }
        return config.getConfig("accounts/"+account);
    }

    public JtdxApi createTdxConn(String account) throws Exception
    {
        Config config = getAccountConfig(account);
        if ( !tdxInit ){
            JtdxApi.init(config.getString("tdx.host"), config.getInt("tdx.port"), config.getString("tdx.version"));
            tdxInit = true;
        }
        JtdxApi api = JtdxApi.login(config.getString("tdx.account"), config.getString("tdx.txnPassword"), config.getString("tdx.commPassword"));
        return api;
    }

}
