#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -classpath "lib/*" xitou.XitouApp "$@"
