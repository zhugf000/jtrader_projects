package trader.common.exchangeable.indicator;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import trader.common.TestHelper;
import trader.common.exchangeable.*;
import trader.common.exchangeable.price.TimeSeries;
import trader.common.util.PriceUtil;

public class TestATR {
    ExchangeableData marketData;

    @Before
    public void initTest()
            throws Exception
    {
        marketData = TestHelper.getExchangeableData();
    }

    @Test
    public void test_600030() throws Exception
    {
        Security security = new Security(Exchange.SSE,"600030");
        LocalDate beginDate = LocalDate.of(2005, 07, 01);
        TimeSeries ts = TimeSeries.loadData(marketData, security, beginDate, null, ExchangeableData.DAY);
        ATRIndicator atrIndicator = new ATRIndicator(ts, 14);
        long atr = atrIndicator.getValue();
        long base = PriceUtil.price2long(15.66);
        long atrPercent = PriceUtil.percent2long(atr, base);
        System.out.println("SH600030 ATR="+atr+", "+PriceUtil.percent2str(atrPercent)+"%");
    }

    @Test
    public void test() throws Exception
    {
        Exchangeable exchangeable = Exchangeable.fromString("sse.000300");
        LocalDate beginDate = LocalDate.of(2005, 07, 01);
        TimeSeries ts = TimeSeries.loadData(marketData, exchangeable, beginDate, null, ExchangeableData.DAY);
        ATRIndicator atrIndicator = new ATRIndicator(ts, 20);
        long atr = atrIndicator.getValue();
        long base = ts.getLastTick().getClose();
        long atrPercent = PriceUtil.percent2long(atr, base);
        System.out.println("SH000300 ATR="+atr+", "+PriceUtil.percent2str(atrPercent)+"%");
    }

}
