package trader.common.exchangeable.price.chanlun;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import trader.common.TestHelper;
import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.price.PriceLevel;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;
import trader.common.util.DateUtil;
import trader.common.util.PriceUtil;

public class SectionTest {

    @Test
    public void test() {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section1.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()>=4);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==3);

        List<TickPattern> tp = (new TickTrendAnalyser(s)).analyse(Type.Section);
        assertTrue(tp.size()>0);
    }

    /**
     * simple section: L-S-L
     */
    @Test
    public void test_lsl() {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section2.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==4);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==3);

        List<TickPattern> tps = (new TickTrendAnalyser(s)).analyse(Type.Section);
        assertTrue(tps.size()==1);
        assertTrue(tps.get(0).getType()==Type.Section);
        TickPattern tp = tps.get(0);
        assertTrue(tp.getBeginPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-01") ));
        assertTrue(tp.getBeginPoint().getPrice() == PriceUtil.price2long(1000));
        assertTrue(tp.getEndPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-10") ));
        assertTrue(tp.getEndPoint().getPrice() == PriceUtil.price2long(1400));
    }

    /**
     * simple section: S-L-S
     */
    @Test
    public void test_sls() {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section3.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==4);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==3);

        List<TickPattern> tps = (new TickTrendAnalyser(null)).analyseSection(strokes);
        assertTrue(tps.size()==1);
        assertTrue(tps.get(0).getType()==Type.Section);
        TickPattern tp = tps.get(0);
        assertTrue(tp.getBeginPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-01") ));
        assertTrue(tp.getBeginPoint().getPrice() == PriceUtil.price2long(2000));
        assertTrue(tp.getEndPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-10") ));
        assertTrue(tp.getEndPoint().getPrice() == PriceUtil.price2long(1600));
    }


    /**
     * section: S-L-S, no section
     */
    @Test
    public void test_sls_no_section(){
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section6.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=10);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==5);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==3);

        List<TickPattern> tps = (new TickTrendAnalyser(null)).analyseSection(strokes);

    }

    /**
     * simple section: L-S-L-S-L
     */
    @Test
    public void test_lsls() {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section4.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=10);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==8);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==5);

        List<TickPattern> tps = (new TickTrendAnalyser(s)).analyse(Type.Section);
        assertTrue(tps.size()==1);
        assertTrue(tps.get(0).getType()==Type.Section);
        TickPattern tp = tps.get(0);
        assertTrue(tp.getBeginPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-01") ));
        assertTrue(tp.getBeginPoint().getPrice() == PriceUtil.price2long(1000));
        assertTrue(tp.getEndPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-19") ));
        assertTrue(tp.getEndPoint().getPrice() == PriceUtil.price2long(1600));
    }

    @Test
    public void test_break_by_line(){
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("section5.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        TimeSeries s = TimeSeries.fromTemplate(t, ticks);
        assertTrue(ticks.size()>=10);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==6);

        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==4);

        List<TickPattern> tps = (new TickTrendAnalyser(s)).analyse(Type.Section);
        assertTrue(tps.size()==2);
        assertTrue(tps.get(0).getType()==Type.Section);
        TickPattern s1 = tps.get(0);
        assertTrue(s1.getBeginPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-01") ));
        assertTrue(s1.getBeginPoint().getPrice() == PriceUtil.price2long(1000));
        assertTrue(s1.getEndPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-12") ));
        assertTrue(s1.getEndPoint().getPrice() == PriceUtil.price2long(1500));

        TickPattern s2 = tps.get(1);
        assertTrue(s2.getType()==Type.Section);
        assertTrue(!s2.getDirection());
        assertTrue(s2.getPatterns().size()==1);

        assertTrue(s2.getBeginPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-12") ));
        assertTrue(s2.getBeginPoint().getPrice() == PriceUtil.price2long(1500));
        assertTrue(s2.getEndPoint().getTime().toLocalDate().equals( DateUtil.str2localdate("2000-01-16") ));
        assertTrue(s2.getEndPoint().getPrice() == PriceUtil.price2long(900));

    }

}
