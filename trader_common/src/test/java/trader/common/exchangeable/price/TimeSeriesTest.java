package trader.common.exchangeable.price;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import trader.common.TestHelper;
import trader.common.exchangeable.ExchangeableData;
import trader.common.exchangeable.Future;

public class TimeSeriesTest {
    ExchangeableData marketData;

    @Before
    public void initTest()
            throws Exception
    {
        marketData = TestHelper.getExchangeableData();
    }

    @Test
    public void test() throws Exception
    {
        Future future = Future.fromInstrument("zn1601");
        LocalDate tradingDay = LocalDate.of(2016, 1, 4);
        TimeSeriesLoader loader = new TimeSeriesLoader(marketData, future);
        loader.setBeginDate(tradingDay);
        loader.setEndDate(tradingDay);
        loader.setClassification(ExchangeableData.MIN1);
        TimeSeries min1Series = loader.load();
        assertTrue(min1Series.getTickCount()>0);
        TimeSeries min3Series = min1Series.convertTo(PriceLevel.MIN3);
        assertTrue(min3Series.getTickCount()*3 == min1Series.getTickCount());
    }

}
