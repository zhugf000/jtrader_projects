package trader.common.exchangeable.price.chanlun;

import static org.junit.Assert.assertTrue;

import java.io.InputStreamReader;
import java.util.List;

import org.junit.Test;

import trader.common.exchangeable.price.PriceLevel;
import trader.common.util.DateUtil;

public class CentreTest {

    @Test
    public void centrum_long() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes1.csv"), "UTF-8"));
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(new AnalyserOptions());
        assertTrue(centres.size() ==1);
        TickPattern centrum = centres.get(0);
        assertTrue(centrum.getDirection());
        assertTrue(centrum.getClose()==1400*10000);
        assertTrue(centrum.getOpen()==1350*10000);
    }

    @Test
    public void centrum_short() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes2.csv"), "UTF-8"));
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(new AnalyserOptions());
        assertTrue(centres.size() ==1);
        TickPattern centrum = centres.get(0);
        assertTrue(!centrum.getDirection());
        assertTrue(centrum.getOpen()==1800*10000);
        assertTrue(centrum.getClose()==1550*10000);
    }

    @Test
    public void centrum_long_extend() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes3.csv"), "UTF-8"));
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(new AnalyserOptions());
        assertTrue(centres.size() ==1);
        TickPattern centrum = centres.get(0);
        assertTrue(centrum.getDirection());
        assertTrue(centrum.getClose()==1400*10000);
        assertTrue(centrum.getOpen()==1350*10000);
        assertTrue(DateUtil.date2str(centrum.getEndPoint().getTime()).equals("2016-01-06 15:00:00"));
    }

    @Test
    public void centrum_long_new() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes4.csv"), "UTF-8"));
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(new AnalyserOptions());
        assertTrue(centres.size() ==2);
        TickPattern centrum = centres.get(0);
        assertTrue(centrum.getDirection());
        assertTrue(centrum.getClose()==1400*10000);
        assertTrue(centrum.getOpen()==1350*10000);

        centrum = centres.get(1);
        assertTrue(centrum.getDirection());
        assertTrue(centrum.getClose()==1800*10000);
        assertTrue(centrum.getOpen()==1720*10000);
    }

    @Test
    public void centrum_trend_with_edge_point() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes4.csv"), "UTF-8"));
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreCentreEdge = false;
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(options);
        assertTrue(centres.size() ==4);
        TickPattern edge = centres.get(0);
        assertTrue(edge.getType()==TickPattern.Type.Edge);

        edge = centres.get(3);
        assertTrue(edge.getType()==TickPattern.Type.Edge);

        TickPattern centre = centres.get(1);
        assertTrue(centre.getType()==TickPattern.Type.Centre);
        assertTrue(centre.getDirection());
        assertTrue(centre.getClose()==1400*10000);
        assertTrue(centre.getOpen()==1350*10000);

        centre = centres.get(2);
        assertTrue(centre.getType()==TickPattern.Type.Centre);
        assertTrue(centre.getDirection());
        assertTrue(centre.getClose()==1800*10000);
        assertTrue(centre.getOpen()==1720*10000);
    }

    @Test
    public void centrum_long2_short1() throws Exception
    {
        List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(new InputStreamReader(SectionTest3.class.getResourceAsStream("centrum_strokes5.csv"), "UTF-8"));
        List<TickPattern> centres = (new CentreAnalyser(strokes, PriceLevel.MIN5)).parse(new AnalyserOptions());
        assertTrue(centres.size() ==3);
        TickPattern centre = centres.get(0);
        assertTrue(centre.getDirection());
        assertTrue(centre.getClose()==1400*10000);
        assertTrue(centre.getOpen()==1350*10000);
        assertTrue(DateUtil.date2str(centre.getEndPoint().getTime()).equals("2016-01-04 15:00:00"));

        centre = centres.get(1);
        assertTrue(centre.getDirection());

        centre = centres.get(2);
        assertTrue(!centre.getDirection());
    }

}
