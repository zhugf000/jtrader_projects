package trader.common.exchangeable.price.chanlun;

import java.io.InputStreamReader;
import java.util.List;

import org.junit.Test;

public class SectionTest3 {

	@Test
	public void test_long_123() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes1.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==1);
		assert(sections.get(0).getDirection()==true);
	}

	@Test
	public void test_short_123() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes2.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==1);
		assert(sections.get(0).getDirection()==false);
	}

	@Test
	public void test_short_12345() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes3.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==1);
		assert(sections.get(0).getDirection()==true);
	}


	/**
	 * 线段破坏-标准：上-下
	 * @throws Exception
	 */
	@Test
	public void test_long_short() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes4.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==2);
		assert(sections.get(0).getDirection()==true);
		assert(sections.get(1).getDirection()==false);
	}

	/**
	 * 线段破坏-标准：下-上
	 * @throws Exception
	 */
	@Test
	public void test_short_lone() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes5.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==2);
		assert(sections.get(0).getDirection()==false);
		assert(sections.get(1).getDirection()==true);
	}

	/**
	 * 线段破坏--特征序列存在缺口，且在缺口上方形成底分型
	 * @throws Exception
	 */
	@Test
	public void test_gap_break_section() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes6.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==3);
		assert(sections.get(0).getDirection()==true);
		assert(sections.get(1).getDirection()==false);
		assert(sections.get(2).getDirection()==true);
	}

	/**
	 * 线段没有破坏--特征序列存在缺口，且在缺口下方不形成顶分型，标准化
	 * @throws Exception
	 */
	@Test
	public void test_gap_no_break_by_normalize() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes7.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==1);
		assert(sections.get(0).getDirection()==false);
	}

	/**
	 * TEST GAP by stroke
	 */
	@Test
	public void test_stroke_gap() throws Exception
	{
		List<TickPattern> strokes = ChanlunTestHelper.loadStrokes(
				new InputStreamReader(SectionTest3.class.getResourceAsStream("section_strokes8.csv"),"UTF-8"));
		List<TickPattern> sections = (new SectionAnalyser(strokes)).parse(new AnalyserOptions());
		assert(sections.size()==3);
		assert(sections.get(0).getDirection()==true);
		assert(sections.get(1).getDirection()==false);
		assert(sections.get(2).getDirection()==true);
	}

}
