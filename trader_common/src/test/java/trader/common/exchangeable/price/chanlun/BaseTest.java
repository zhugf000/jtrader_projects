package trader.common.exchangeable.price.chanlun;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import trader.common.TestHelper;
import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.price.PriceLevel;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

public class BaseTest {

	@Test
	public void testCompositeTick()
	{
		List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("composite_tick1.txt"));
		Tick t1 = ticks.get(0);
		Tick t2 = ticks.get(1);
		CompositeTick ct = new CompositeTick(t1, t2, true);
		assertTrue( ct.getBeginTime().equals(t1.getBeginTime()));
		assertTrue( ct.getEndTime().equals(t2.getEndTime()));
		assertTrue( ct.getHigh() == 2000*10000);
		assertTrue( ct.getLow() == 1500*10000);
	}

	@Test
	public void testCompositeTick2()
	{
		List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("composite_tick1.txt"));
		Tick t1 = ticks.get(0);
		Tick t2 = ticks.get(1);
		CompositeTick ct = new CompositeTick(t1, t2, false);
		assertTrue( ct.getBeginTime().equals(t1.getBeginTime()));
		assertTrue( ct.getEndTime().equals(t2.getEndTime()));
		assertTrue( ct.getHigh() == 1800*10000);
		assertTrue( ct.getLow() == 1000*10000);
	}

	/**
	 * k线合并向上处理
	 * @throws Exception
	 */
	@Test
	public void testKLineProcess()
	{
		List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline1.txt"));
		TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
		TimeSeries s = TimeSeries.fromTemplate(t, ticks);
		assertTrue(ticks.size()>=4);
		List<Tick> klines = (new TickAnalyser(s)).parse(new AnalyserOptions());
		assertTrue(klines.size()==1);
		Tick ct = klines.get(0);
		assertTrue( ct.getHigh()== 2000*10000);
		assertTrue( ct.getLow()== 1300*10000);
	}

	/**
	 * k线合并向下处理
	 */
	@Test
	public void testKLineProcess2()
	{
		List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline2.txt"));
		TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
		TimeSeries s = TimeSeries.fromTemplate(t, ticks);
		assertTrue(ticks.size()>=4);
		List<Tick> klines = (new TickAnalyser(s)).parse(new AnalyserOptions());
		assertTrue(klines.size()==2);
		Tick ct = klines.get(klines.size()-1);
		assertTrue( ct.getHigh()== 1600*10000);
		assertTrue( ct.getLow()== 1000*10000);
	}

	@Test
	public void testFractal()
	{
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal1.txt"));
        assertTrue(ticks.size()>=3);
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==1);
    }

    @Test
    public void testFractal_nogap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal2.txt"));
        assertTrue(ticks.size()>=3);
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Fractal);
    }

    @Test
    public void testFractal_gap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal2.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = false;
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Line);
    }

    @Test
    public void testLine_gap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline3.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = false;
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==2);

        assertTrue(fractals.get(1).getType()==Type.Line);
        assertTrue(fractals.get(1).getTicks().size()==2);
    }

    @Test
    public void testLine_nogap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline3.txt"));
        assertTrue(ticks.size()==4);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = true;
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(options);
        assertTrue(fractals.size()==1);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==4);
    }

    @Test
    public void testFractal_gap_2()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal3.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = true;
        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Fractal);

        options.ignoreOpenGap = false;
        fractals = (new FractalAnalyser(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==2);
        assertTrue(fractals.get(1).getType()==Type.Fractal);
    }

    @Test
    public void testFractal2()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal1.txt"));
        assertTrue(ticks.size()>=3);
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==3);
    }

    @Test
    public void testLine2_gap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline3.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = false;
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==2);

        assertTrue(fractals.get(1).getType()==Type.Line);
        assertTrue(fractals.get(1).getTicks().size()==2);
    }

    @Test
    public void testLine2_nogap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("kline3.txt"));
        assertTrue(ticks.size()==4);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = true;
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(options);
        assertTrue(fractals.size()==1);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==4);
    }
    @Test
    public void testFractal2_nogap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal2.txt"));
        assertTrue(ticks.size()>=3);
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==3);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Fractal);
        assertTrue(fractals.get(2).getType()==Type.Line);
    }

    @Test
    public void testFractal2_gap()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal2.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = false;
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(options);
        assertTrue(fractals.size()==2);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Line);
    }

    @Test
    public void testFractal2_gap_2()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("fractal3.txt"));
        assertTrue(ticks.size()>=3);
        AnalyserOptions options = new AnalyserOptions();
        options.ignoreOpenGap = true;
        List<TickPattern> fractals = (new FractalAnalyser2(ticks)).parse(options);
        assertTrue(fractals.size()==3);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(1).getType()==Type.Fractal);
        assertTrue(fractals.get(2).getType()==Type.Line);

        options.ignoreOpenGap = false;
        fractals = (new FractalAnalyser2(ticks)).parse(options);
        assertTrue(fractals.size()==4);
        assertTrue(fractals.get(0).getType()==Type.Line);
        assertTrue(fractals.get(0).getTicks().size()==2);
        assertTrue(fractals.get(1).getType()==Type.Line);
        assertTrue(fractals.get(2).getType()==Type.Fractal);
        assertTrue(fractals.get(3).getType()==Type.Line);
    }

}
