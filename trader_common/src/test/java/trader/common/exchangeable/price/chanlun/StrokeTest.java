package trader.common.exchangeable.price.chanlun;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import trader.common.TestHelper;
import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.price.PriceLevel;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;

/**
 * 缠论.test
 */
public class StrokeTest {

    /**
     * 笔划.single line
     */
    @Test
    public void testStroke()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("stroke1.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==1);
        //List<TickPattern> strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        List<TickPattern> strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==1);
    }

    /**
     * 笔划.fractial + single line
     */
    @Test
    public void testStroke2()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("stroke2.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==2);
        //List<TickPattern> strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        List<TickPattern> strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==1);
    }

    /**
     * Stroke: single line + fractal
     */
    @Test
    public void testStroke3()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("stroke3.txt"));
        TimeSeries t = TimeSeries.createTemplate(Exchangeable.fromString("sse.000001"), PriceLevel.DAY);
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==2);
        //List<TickPattern> strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        List<TickPattern> strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==1);

    }

    /**
     * Stroke: single line + fractal + single line
     */
    @Test
    public void testStroke4()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("stroke4.txt"));
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==3);
        //List<TickPattern> strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        List<TickPattern> strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        assertTrue(strokes.size()==1);
    }

    /**
     * Stroke: single line + fractal+fractal+ single line
     */
    @Test
    public void testStroke5()
    {
        List<Tick> ticks = TestHelper.loadTicks(StrokeTest.class.getResourceAsStream("stroke5.txt"));
        assertTrue(ticks.size()>=4);

        List<TickPattern> fractals = (new FractalAnalyser(ticks)).parse(new AnalyserOptions());
        assertTrue(fractals.size()==4);
        List<TickPattern> strokes = null;
        //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
        //ChanlunTestHelper.checkAndDumpStroke(strokes);

        strokes = (new StrokeAnalyser2(fractals)).parse(new AnalyserOptions());
        ChanlunTestHelper.checkAndDumpStroke(strokes);


        assertTrue(strokes.size()==1); // ==3 ???
    }

}
