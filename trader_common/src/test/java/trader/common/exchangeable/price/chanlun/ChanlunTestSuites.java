package trader.common.exchangeable.price.chanlun;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    BaseTest.class,
    StrokeTest.class,
    SectionTest.class,

    Stroke2Test.class,
    SectionTest2.class,
    SectionTest3.class,

    CentreTest.class
    })
public class ChanlunTestSuites {

}
