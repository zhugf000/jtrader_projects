package trader.common.exchangeable.price.chanlun;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;
import trader.common.util.CSVDataSet;
import trader.common.util.CSVUtil;

public class ChanlunTestHelper {
    private static final boolean dump = true;

    /**
     * load strokes from text fle
     */
    public static List<TickPattern> loadStrokes(Reader reader) throws IOException
    {
    	List<TickPattern> result = new LinkedList<>();

    	CSVDataSet csv = CSVUtil.parse(reader, ',', true);
    	while(csv.next()){
    		boolean direction = csv.getBoolean("direction");

    		LocalDateTime beginTime = csv.getDateTime("beginTime");
    		long beginPrice = csv.getPrice("beginPrice");
    		Point beginPoint = new Point(beginTime, beginPrice);

    		LocalDateTime endTime = csv.getDateTime("endTime");
    	    long endPrice = csv.getPrice("endPrice");
    	    Point endPoint = new Point(endTime, endPrice);
    	    Point highPoint = beginPoint;
    	    Point lowPoint = endPoint;
    	    if ( beginPoint.isLower(endPoint)){
    	    	highPoint = endPoint;
    	    	lowPoint = beginPoint;
    	    }else{
    	    	highPoint = beginPoint;
    	    	lowPoint = endPoint;
    	    }

    	    TickPattern line = TickPattern.create(TickPattern.Type.Line, direction, new Tick[]{
    	    		new Tick(beginPoint, beginPoint),
    	    		new Tick(highPoint, highPoint),
    	    		new Tick(lowPoint, lowPoint),
    	    		new Tick(endPoint, endPoint)
    	    });

    	    TickPattern stroke = TickPattern.createStroke(direction, beginPoint, endPoint, line);
    	    assertTrue(stroke.getBeginPoint().equals(beginPoint));
    	    assertTrue(stroke.getHighPoint().equals(endPoint) || stroke.getHighPoint().equals(beginPoint));
    	    assertTrue(stroke.getLowPoint().equals(endPoint) || stroke.getLowPoint().equals(beginPoint));
    	    assertTrue(stroke.getEndPoint().equals(endPoint));

    	    result.add(stroke);
    	}
        return result;
    }

    static void checkAndDumpSection(List<TickPattern> patterns)
    {
        if ( dump ){
            for(TickPattern p:patterns){
                p.dump(new PrintWriter(System.out,true));
            }
        }
        assertTrue(patterns.size()>0);
        for(TickPattern p:patterns){
            assertTrue(p.getType()==Type.Section);
            if ( p.getDirection() ){
                assertTrue(p.getOpen()<p.getClose());
            }else{
                assertTrue(p.getOpen()>p.getClose());
            }
            Boolean lastPPDir = null;
            for(TickPattern pp:p.getPatterns()){
                assertTrue(pp.getType()==TickPattern.Type.Stroke);
                if ( lastPPDir!=null ){
                    assertTrue(pp.getDirection()!=lastPPDir);
                }
                if ( pp.getDirection() ){
                    assertTrue(pp.getOpen()<pp.getClose());
                }else{
                    assertTrue(pp.getOpen()>pp.getClose());
                }
                lastPPDir = pp.getDirection();
            }
        }
    }

    static void checkAndDumpStroke(List<TickPattern> patterns)
    {
        if ( dump ){
            for(TickPattern p:patterns){
                p.dump(new PrintWriter(System.out,true));
            }
        }
        assertTrue(patterns.size()>0);
    }

}
