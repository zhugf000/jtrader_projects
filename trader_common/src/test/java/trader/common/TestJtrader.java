package trader.common;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import net.jctp.CThostFtdcDepthMarketDataField;
import trader.common.util.CSVDataSet;
import trader.common.util.CSVUtil;
import trader.common.util.StringUtil;
import trader.common.util.csv.CtpCSVMarshallHelper;

public class TestJtrader {

    @Test
    public void testCtpDepthMarketData() {
        String csvText = "TradingDay,InstrumentID,ExchangeID,ExchangeInstID,LastPrice,PreSettlementPrice,PreClosePrice,PreOpenInterest,OpenPrice,HighestPrice,LowestPrice,Volume,Turnover,OpenInterest,ClosePrice,SettlementPrice,UpperLimitPrice,LowerLimitPrice,PreDelta,CurrDelta,UpdateTime,UpdateMillisec,BidPrice1,BidVolume1,AskPrice1,AskVolume1,BidPrice2,BidVolume2,AskPrice2,AskVolume2,BidPrice3,BidVolume3,AskPrice3,AskVolume3,BidPrice4,BidVolume4,AskPrice4,AskVolume4,BidPrice5,BidVolume5,AskPrice5,AskVolume5,AveragePrice,ActionDay\r\n" +
                "20150902,IF1509,,,2999.00,3007.20,2999.00,54062.00,N/A,N/A,N/A,0,0.00,54062.00,N/A,N/A,3307.80,2706.60,N/A,N/A,07:08:02,100,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,0.00,20150902\r\n"+
                "20150902,IF1509,,,2924.80,3007.20,2999.00,54062.00,2939.80,2954.00,2910.00,19013,16713511140.00,56742.00,N/A,N/A,3307.80,2706.60,N/A,N/A,09:23:14,600,2924.00,1,2924.60,1,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,879057.021,20150902\r\n" +
                "20150902,IF1509,,,2924.60,3007.20,2999.00,54062.00,2939.80,2954.00,2910.00,19029,16727549340.00,56749.00,N/A,N/A,3307.80,2706.60,N/A,N/A,09:23:15,100,2924.20,1,2926.20,4,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,N/A,0,879055.617,20150902";

        ArrayList<CThostFtdcDepthMarketDataField> l = new ArrayList<>();
        CThostFtdcDepthMarketDataField f=null;
        CSVDataSet csvReader = CSVUtil.parse(csvText, ',',true);
        CtpCSVMarshallHelper helper = new CtpCSVMarshallHelper();
        while( csvReader.next() ){
            String[] columns = csvReader.getColumns();
            assertTrue( StringUtil.arrrayEquals(columns, helper.getHeader(), true));
            l.add( helper.unmarshall(csvReader.getRow()) );
        }
        csvReader.close();

        assertTrue(l.get(0).toString().equals("CThostFtdcDepthMarketDataField[TradingDay=20150902,InstrumentID=IF1509,ExchangeID=,ExchangeInstID=,LastPrice=2999.0,PreSettlementPrice=3007.2,PreClosePrice=2999.0,PreOpenInterest=54062.0,OpenPrice=1.7976931348623157E308,HighestPrice=1.7976931348623157E308,LowestPrice=1.7976931348623157E308,Volume=0,Turnover=0.0,OpenInterest=54062.0,ClosePrice=1.7976931348623157E308,SettlementPrice=1.7976931348623157E308,UpperLimitPrice=3307.8,LowerLimitPrice=2706.6,PreDelta=1.7976931348623157E308,CurrDelta=1.7976931348623157E308,UpdateTime=07:08:02,UpdateMillisec=100,BidPrice1=1.7976931348623157E308,BidVolume1=0,AskPrice1=1.7976931348623157E308,AskVolume1=0,BidPrice2=1.7976931348623157E308,BidVolume2=0,AskPrice2=1.7976931348623157E308,AskVolume2=0,BidPrice3=1.7976931348623157E308,BidVolume3=0,AskPrice3=1.7976931348623157E308,AskVolume3=0,BidPrice4=1.7976931348623157E308,BidVolume4=0,AskPrice4=1.7976931348623157E308,AskVolume4=0,BidPrice5=1.7976931348623157E308,BidVolume5=0,AskPrice5=1.7976931348623157E308,AskVolume5=0,AveragePrice=0.0,ActionDay=20150902]"));
        assertTrue(l.get(1).toString().equals("CThostFtdcDepthMarketDataField[TradingDay=20150902,InstrumentID=IF1509,ExchangeID=,ExchangeInstID=,LastPrice=2924.8,PreSettlementPrice=3007.2,PreClosePrice=2999.0,PreOpenInterest=54062.0,OpenPrice=2939.8,HighestPrice=2954.0,LowestPrice=2910.0,Volume=19013,Turnover=1.671351114E10,OpenInterest=56742.0,ClosePrice=1.7976931348623157E308,SettlementPrice=1.7976931348623157E308,UpperLimitPrice=3307.8,LowerLimitPrice=2706.6,PreDelta=1.7976931348623157E308,CurrDelta=1.7976931348623157E308,UpdateTime=09:23:14,UpdateMillisec=600,BidPrice1=2924.0,BidVolume1=1,AskPrice1=2924.6,AskVolume1=0,BidPrice2=1.7976931348623157E308,BidVolume2=0,AskPrice2=1.7976931348623157E308,AskVolume2=0,BidPrice3=1.7976931348623157E308,BidVolume3=0,AskPrice3=1.7976931348623157E308,AskVolume3=0,BidPrice4=1.7976931348623157E308,BidVolume4=0,AskPrice4=1.7976931348623157E308,AskVolume4=0,BidPrice5=1.7976931348623157E308,BidVolume5=0,AskPrice5=1.7976931348623157E308,AskVolume5=0,AveragePrice=879057.021,ActionDay=20150902]"));
        assertTrue(l.get(2).toString().equals("CThostFtdcDepthMarketDataField[TradingDay=20150902,InstrumentID=IF1509,ExchangeID=,ExchangeInstID=,LastPrice=2924.6,PreSettlementPrice=3007.2,PreClosePrice=2999.0,PreOpenInterest=54062.0,OpenPrice=2939.8,HighestPrice=2954.0,LowestPrice=2910.0,Volume=19029,Turnover=1.672754934E10,OpenInterest=56749.0,ClosePrice=1.7976931348623157E308,SettlementPrice=1.7976931348623157E308,UpperLimitPrice=3307.8,LowerLimitPrice=2706.6,PreDelta=1.7976931348623157E308,CurrDelta=1.7976931348623157E308,UpdateTime=09:23:15,UpdateMillisec=100,BidPrice1=2924.2,BidVolume1=4,AskPrice1=2926.2,AskVolume1=0,BidPrice2=1.7976931348623157E308,BidVolume2=0,AskPrice2=1.7976931348623157E308,AskVolume2=0,BidPrice3=1.7976931348623157E308,BidVolume3=0,AskPrice3=1.7976931348623157E308,AskVolume3=0,BidPrice4=1.7976931348623157E308,BidVolume4=0,AskPrice4=1.7976931348623157E308,AskVolume4=0,BidPrice5=1.7976931348623157E308,BidVolume5=0,AskPrice5=1.7976931348623157E308,AskVolume5=0,AveragePrice=879055.617,ActionDay=20150902]"));
    }

}
