package trader.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.ExchangeableData;
import trader.common.exchangeable.price.Tick;
import trader.common.util.*;

public class TestHelper {

    public static ExchangeableData getExchangeableData()
    {
        File traderHome = new File(System.getProperty("user.home"),"traderHome");
        File marketData = new File(traderHome,"data/market");
        if ( marketData.exists() ){
            return new ExchangeableData(marketData, true);
        }
        File internalSecondZipFile = ResourceUtil.getFile("data/sse/600030/day.csv", null);
        File dataDir = internalSecondZipFile.getParentFile().getParentFile().getParentFile();
        return new ExchangeableData(dataDir, true);
    }

    public static List<Tick> loadTicks(InputStream is)
    {
        List<Tick> ticks = new LinkedList<>();
        try( BufferedReader br = IOUtil.createBufferedReader(is); ){
            String line = null;
            while( (line=br.readLine())!=null ){
                line = line.trim();
                if ( line.length()==0 || line.startsWith("#") ){
                    continue;
                }
                String[] parts = line.split("(\t|,) *");
                Tick tick = new Tick(DateUtil.str2localdatetime(parts[0]),
                        DateUtil.str2localdatetime(parts[1]),
                        PriceUtil.price2long(Double.parseDouble(parts[2])), //O
                        PriceUtil.price2long(Double.parseDouble(parts[3])), //H
                        PriceUtil.price2long(Double.parseDouble(parts[4])), //L
                        PriceUtil.price2long(Double.parseDouble(parts[5])), //C
                        PriceUtil.price2long(Double.parseDouble(parts[6])), //Volume
                        PriceUtil.price2long(Double.parseDouble(parts[7])) //Turnover
                        );
                ticks.add(tick);
            }
        }catch(Exception e){
            throw new RuntimeException(e);
        }
        return ticks;
    }


}
