package trader.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import trader.common.exchangeable.TestExchange;

@RunWith(Suite.class)
@SuiteClasses({ TestCSVUtil.class
    , TestPriceUtil.class
    , TestDateUtil.class
    , TestMarketDayUtil.class
    , TestJtrader.class
    , TestResourceUtil.class
    , TestExchange.class })
public class AllTests {

}
