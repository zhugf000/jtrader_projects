package trader.tools.repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.time.LocalDate;
import java.util.*;

import trader.common.exchangeable.ExchangeableData;
import trader.common.util.*;
import trader.tools.common.CommandOptions;

public class RepositoryMerger {

    private ExchangeableData target;
    private ExchangeableData source;

    public static void main(String[] args)
            throws Throwable
    {
        if ( args.length==0){
            usage();
            System.exit(0);
        }
        CommandOptions opt = CommandOptions.parse(args);
        String target = opt.getString(CommandOptions.Option.target);
        String source = opt.getString(CommandOptions.Option.source);
        File targetFile = new File(target);
        ExchangeableData targetData = new ExchangeableData(targetFile);
        File sourceFile = new File(source);
        ExchangeableData sourceData = new ExchangeableData(sourceFile);
        if ( !targetFile.exists() || !targetFile.isDirectory() ){
            throw new IOException("File "+targetFile+" is not accessible");
        }
        if ( !sourceFile.exists() || !sourceFile.isDirectory() ){
            throw new IOException("File "+sourceFile+" is not accessible");
        }
        RepositoryMerger merger = new RepositoryMerger();
        merger.target = targetData;
        merger.source = sourceData;
        merger.merge();
    }

    private static void usage()
    {
        System.out.println("repository_merge --target=<repository dir> --source=<repository dir>");
    }

    private static class MergeEntry{
        String subdir;
        String exchange;
        String exchangeable;
        String file;
        Path path;
    }

    public void merge() throws Exception
    {
        for(MergeEntry entry : loadMergeEntries(source)){
            System.out.println("Mergeing file from "+entry.path+" ...");
            if ( entry.subdir!=null ){
                mergeSubdirFile(entry);
            }else{
                mergeExchangeableFile(entry);
            }
        }
    }

    private List<MergeEntry> loadMergeEntries(ExchangeableData source) throws IOException
    {
        LinkedList<MergeEntry> result = new LinkedList<>();
        File sourceFile = source.getDataDir();
        try(DirectoryStream<Path> subdirStream= Files.newDirectoryStream(Paths.get(sourceFile.toURI()));){
            for(Path subdirPath: subdirStream){
                String subdirName = subdirPath.getFileName().toString();
                if ( subdirName.equalsIgnoreCase(ExchangeableData.SUBDIR_SUM)){
                    try(DirectoryStream<Path> dirStream= Files.newDirectoryStream(subdirPath);){
                        for(Path path: dirStream){
                            MergeEntry entry = new MergeEntry();
                            entry.subdir = ExchangeableData.SUBDIR_SUM;
                            entry.file = path.getFileName().toString();
                            entry.path = path;
                            result.add(entry);
                        }
                    }
                }else{
                    String exchange = subdirName;

                    try(DirectoryStream<Path> exchangeableStream= Files.newDirectoryStream(subdirPath);){
                        for(Path exchangeablePath: exchangeableStream){
                            String exchangeable = exchangeablePath.getFileName().toString();
                            if ( !exchangeablePath.toFile().isDirectory() ){
                                if ( exchangeablePath.getFileName().toString().equals(ExchangeableData.SECURITY_LIST)){
                                    MergeEntry entry = new MergeEntry();
                                    entry.exchange = exchange;
                                    entry.file = exchangeablePath.getFileName().toString();
                                    entry.path = exchangeablePath;
                                    result.add(entry);
                                }
                                continue;
                            }
                            try(DirectoryStream<Path> fileStream = Files.newDirectoryStream(exchangeablePath);){
                                for(Path filePath: fileStream){
                                    if ( isIgnoredFile(filePath) ){
                                        continue;
                                    }
                                    MergeEntry entry = new MergeEntry();
                                    entry.exchange = exchange;
                                    entry.exchangeable = exchangeable;
                                    entry.file = filePath.getFileName().toString();
                                    entry.path = filePath;
                                    result.add(entry);
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    private void mergeSubdirFile(MergeEntry entry) throws IOException
    {
        Path sourcePath = entry.path;
        Path targetPath = getTargetPath(entry);
        mergeZipFile(targetPath, sourcePath);
    }

    private void mergeExchangeableFile(MergeEntry entry) throws IOException
    {
        Path sourcePath = entry.path;
        Path targetPath = getTargetPath(entry);
        if ( entry.file.startsWith(ExchangeableData.DAY.name().toLowerCase())){
            mergeDailyZipFile(targetPath, sourcePath);
        }else if ( entry.file.equals(ExchangeableData.SECURITY_LIST)){
            mergeSecurityListCsv(targetPath, sourcePath);
        }else{
            mergeZipFile(targetPath, sourcePath);
        }

    }

    private Path getTargetPath(MergeEntry sourceEntry)
    {
        if ( sourceEntry.subdir!=null ){
            Path targetSubdir =  target.getDataDir().toPath().resolve(ExchangeableData.SUBDIR_SUM);
            return targetSubdir.resolve( sourceEntry.file );
        }else if (sourceEntry.file.equals(ExchangeableData.SECURITY_LIST) ){
            Path exchangePath = target.getDataDir().toPath().resolve( sourceEntry.exchange );
            return exchangePath.resolve(sourceEntry.file);
        }else{
            Path exchangePath = target.getDataDir().toPath().resolve( sourceEntry.exchange );
            Path exchangeablePath = exchangePath.resolve(sourceEntry.exchangeable);
            Path filePath = exchangeablePath.resolve( sourceEntry.file );
            return filePath;
        }
    }

    private void mergeZipFile(Path target, Path source) throws IOException
    {
        File targetFile = target.toFile();
        File sourceFile = source.toFile();
        if ( !targetFile.exists()){
            targetFile.getParentFile().mkdirs();
            FileUtil.copy(sourceFile, targetFile);
            return;
        }
        List<String> sourceFileNameAndContents = ZipFileUtil.archiveReadAll(sourceFile);

        List<String> names = new LinkedList<>();
        List<byte[]> datas = new LinkedList<>();
        for(int i=0;i<sourceFileNameAndContents.size();i+=2){
            String name = sourceFileNameAndContents.get(i);
            String cont = sourceFileNameAndContents.get(i+1);
            names.add(name);
            datas.add(cont.getBytes("UTF-8"));
        }
        ZipFileUtil.archiveAddAll(targetFile, names, datas);
    }

    /**
     * The file contains one csv file only
     * @param target
     * @param source
     */
    private void mergeDailyZipFile(Path target, Path source) throws IOException
    {
        File targetFile = target.toFile();
        File sourceFile = source.toFile();
        if ( !targetFile.exists()){
            targetFile.getParentFile().mkdirs();
            FileUtil.copy(sourceFile, targetFile);
            return;
        }
        List<String> sourceFileNameAndContents = ZipFileUtil.archiveReadAll(sourceFile);
        List<String> targetFileNameAndContents = ZipFileUtil.archiveReadAll(targetFile);
        String sourceFname = sourceFileNameAndContents.get(0);
        String sourceFcsv = sourceFileNameAndContents.get(1);
        String targetFname = targetFileNameAndContents.get(0);
        String targetFcsv = targetFileNameAndContents.get(1);
        if ( !sourceFname.equalsIgnoreCase(targetFname)){
            throw new IOException("source zip file "+source+" has csv file "+sourceFname+" doesn't equal to the file "+targetFname+" in target zip "+target);
        }
        CSVDataSet sourceDs = CSVUtil.parse(sourceFcsv);
        CSVDataSet targetDs = CSVUtil.parse(targetFcsv);
        List<String[]> rows = new LinkedList<>();
        Set<LocalDate> dates = new TreeSet<>();
        while(targetDs.next()){
            rows.add( targetDs.getRow() );
            dates.add( targetDs.getDate(ExchangeableData.COLUMN_DATE));
        }
        while(sourceDs.next()){
            LocalDate date = sourceDs.getDate(ExchangeableData.COLUMN_DATE);
            if ( dates.contains(date)){
                continue;
            }
            rows.add( sourceDs.getRow() );
            dates.add(date);
        }
        final int dateColumnIndex = Arrays.asList(ExchangeableData.DAY_COLUMNS).indexOf(ExchangeableData.COLUMN_DATE);
        Collections.sort(rows, (a1, a2) -> DateUtil.str2localdate(a1[dateColumnIndex]).compareTo(DateUtil.str2localdate(a2[dateColumnIndex])));
        CSVWriter writer = new CSVWriter(targetDs.getColumns());
        for(String[] row:rows){
            Object[] arow = new Object[row.length];
            System.arraycopy(row, 0, arow, 0, row.length);
            writer.append(arow);
        }
        ZipFileUtil.archiveAdd(targetFile, writer.toString().getBytes("UTF-8"), targetFname);
    }

    private void mergeSecurityListCsv(Path target, Path source) throws IOException
    {
        File targetFile = target.toFile();
        File sourceFile = source.toFile();
        if ( !targetFile.exists()){
            targetFile.getParentFile().mkdirs();
            FileUtil.copy(sourceFile, targetFile);
            return;
        }
        CSVDataSet sourceDs = CSVUtil.parse(sourceFile);
        CSVDataSet targetDs = CSVUtil.parse(targetFile);
        LinkedHashMap<String,String> codeNames = new LinkedHashMap<>();
        while(targetDs.next()){
            String code = targetDs.get("code");
            String name = targetDs.get("name");
            codeNames.put(code, name);
        }
        while(sourceDs.next()){
            String code = sourceDs.get("code");
            String name = sourceDs.get("name");
            codeNames.put(code, name);
        }
        CSVWriter writer = new CSVWriter("code","name");
        for(String code:codeNames.keySet()){
            String name = codeNames.get(code);
            writer.append(code,name);
        }
        FileUtil.save(targetFile, writer.toString());
    }

    private boolean isIgnoredFile(Path path){
        if ( path.getFileName().toString().startsWith("_")){
            return true;
        }
        return false;
    }
}
