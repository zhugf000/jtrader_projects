package trader.tools.dataimport.netease;

import java.io.*;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.Exchange;
import trader.common.exchangeable.Exchangeable;
import trader.common.util.*;
import trader.tools.dataimport.DataImporter;

/**
 * http://quotes.money.163.com/service/chddata.html?code=0601901&start=20110810&end=20141008&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP
 */
public class NeteaseDataImporter extends DataImporter {
    private static final Charset gbkCharset = Charset.forName("GBK");

    @Override
    public void importData(Exchangeable exchangeable) throws Exception
    {
        importSecurityQuotationData(exchangeable);
    }

    @Override
    protected String[] getAllDataTypes(){
        return new String[]{DATATYPE_DAY};
    }

    private void importSecurityQuotationData(Exchangeable securityId) throws IOException
    {
        if ( needsDataType(DATATYPE_DAY)) {
            importSecurityDayData(securityId);
        }
    }

    private void importSecurityDayData(Exchangeable security) throws IOException
    {
        if ( beginDate==null ) {
            beginDate = DateUtil.str2localdate("20010101");
        }
        LocalDate lastDate = getEndDate(security, true);

        File file=null;
        System.out.print("Importing netease security data "+security+" "+beginDate+"-"+lastDate+" ... ");

        String str = fetchSecurityDataDay(security, beginDate, lastDate);

        if ( str==null ){
            System.out.println("NO DATA");
        }else{
            System.out.println("done");
            try(BufferedReader reader = new BufferedReader(new StringReader(str));
                    BufferedWriter writer = IOUtil.createBufferedFileWriter(file);   )
            {
                String head = reader.readLine();
                List<String[]> rows = new LinkedList<>();
                String line;
                while ((line=reader.readLine())!=null){
                    line = line.trim();
                    if (line.length()==0) {
                        continue;
                    }
                    String[] rawRow = CSVUtil.parseLine(line, ',');
                    String[] stdRow = new String[7];
                    stdRow[0] = yyyy_mm_dd2yyyymmdd(rawRow[0]);
                    stdRow[1] = rawRow[6]; //Open
                    stdRow[2] = rawRow[4]; //High
                    stdRow[3] = rawRow[5]; //Low
                    stdRow[4] = rawRow[3]; //Close
                    stdRow[5] = rawRow[11];
                    stdRow[6] = rawRow[12];
                    rows.add(0, stdRow);

                }
                writer.write("Date,Open,High,Low,Close,Volume,Turnover\n");
                for(String[] row:rows) {
                    CSVUtil.writeCSVLine(writer, row, ',');
                }
            }
        }
    }

    private static String fetchSecurityDataDay(Exchangeable security, LocalDate beginDate,LocalDate endDate)
            throws IOException
    {
        String ex = "sh";
        if ( security.exchange()==Exchange.SSE ) {
            ex = "0";
        } else if ( security.exchange()==Exchange.SZSE ) {
            ex = "1";
        } else {
            throw new RuntimeException("Unknown exchange "+security.exchange());
        }
        String url = "http://quotes.money.163.com/service/chddata.html?code="+ex+security.id()+"&start="+ DateUtil.date2str(beginDate)+"&end="+DateUtil.date2str(endDate)
        +"&fields=TCLOSE;HIGH;LOW;TOPEN;LCLOSE;CHG;PCHG;TURNOVER;VOTURNOVER;VATURNOVER;TCAP;MCAP";
        try{
            String text = NetUtil.readHttpAsText(url, gbkCharset);
            return text;
        }catch(Exception e){
            return null;
        }
    }

    private String yyyy_mm_dd2yyyymmdd(String yyyy_mm_dd){
        String yyyy = yyyy_mm_dd.substring(0,4);
        String mm = yyyy_mm_dd.substring(5,7);
        String dd = yyyy_mm_dd.substring(8,10);

        return yyyy+mm+dd;
    }
}
