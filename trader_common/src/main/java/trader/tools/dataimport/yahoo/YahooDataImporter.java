package trader.tools.dataimport.yahoo;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.*;
import trader.common.util.CSVUtil;
import trader.common.util.NetUtil;
import trader.common.util.StringUtil;
import trader.tools.dataimport.DataImporter;

public class YahooDataImporter extends DataImporter {

    @Override
    public void preImportData() throws Exception
    {
        System.out.println("YAHOO DAILY DATA IMPORTER IS DEPRECATED FOR CHINA SECURITY MARKET!");
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[] { DATATYPE_DAY };
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {
        importSecurityQuotationData(exchangeable);
    }

    private void importSecurityQuotationData(Exchangeable securityId) throws IOException {
        if (needsDataType(DATATYPE_DAY)) {
            importSecurityDayData(securityId);
        }
    }

    private void importSecurityDayData(Exchangeable security) throws IOException {
        if (security.getType() != ExchangeableType.STOCK) {
            return;
        }
        StringWriter csvText = new StringWriter(4096);
        File file = null;// securityDataFile(security, null,
        // ExchangeableData.Classification.DAY_YAHOO);
        System.out.print("Importing yahoo day security data " + security + " ... ");
        String text = fetchSecurityDayData(security);
        if (text == null) {
            System.out.println("NO DATA");
            return;
        } else {
            System.out.println(" " + text.length() + " bytes.");
        }
        try (BufferedReader reader = new BufferedReader(new StringReader(text));) {
            String csvHead = reader.readLine();
            // 排序
            List<String[]> rows = new LinkedList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.trim().length() == 0) {
                    continue;
                }
                String rawRow[] = CSVUtil.parseLine(line, ',');
                String stdRow[] = new String[7];
                stdRow[0] = rawRow[0];// Date
                stdRow[1] = rawRow[1];// Open
                stdRow[2] = rawRow[2];// High
                stdRow[3] = rawRow[3];// Low
                stdRow[4] = rawRow[4];// Close
                stdRow[5] = rawRow[5];// Volume
                stdRow[6] = "";// Turnover

                if ( stdRow[1].equals(stdRow[2])
                        && stdRow[2].equals(stdRow[3])
                        && Integer.parseInt(stdRow[5])==0)
                {
                    continue;
                }
                rows.add(stdRow);
            }
            Collections.sort(rows, (String[] x, String[] y) -> {
                return x[0].compareTo(y[0]);
            });

            csvText.write(StringUtil.array2str(ExchangeableData.DAY_COLUMNS, ","));
            csvText.write("\n");
            for (String[] row : rows) {
                CSVUtil.writeCSVLine(csvText, row, ',');
            }
        }
        exchangeableData.save(security, ExchangeableData.DAY, null, csvText.toString());
    }

    private static String fetchSecurityDayData(Exchangeable security) throws IOException {
        String ex = "ss";
        if (security.exchange() == Exchange.SSE) {
            ex = "ss";
        } else if (security.exchange() == Exchange.SZSE) {
            ex = "sz";
        } else {
            throw new RuntimeException("Unknown exchange " + security.exchange());
        }
        String url = "http://table.finance.yahoo.com/table.csv?s=" + security.id() + "." + ex;
        try {
            return NetUtil.readHttpAsText(url, Charset.defaultCharset());
        } catch (IOException ioe) {
            return null;
        }
    }
}