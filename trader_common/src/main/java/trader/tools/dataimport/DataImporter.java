package trader.tools.dataimport;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.*;

import trader.common.exchangeable.*;
import trader.common.exchangeable.ExchangeableData.Classification;

public abstract class DataImporter {

    protected static final String DATATYPE_ALL      = "all";
    protected static final String DATATYPE_DAY      = "day";
    protected static final String DATATYPE_TICK     = "tick";
    protected static final String DATATYPE_MINUTE1  = "minute1";
    protected static final String DATATYPE_MINUTE5  = "minute5";
    protected static final String DATATYPE_MINUTE15 = "minute15";
    protected static final String DATATYPE_MINUTE30 = "minute30";
    protected static final String DATATYPE_FINANCE  = "finance";

    protected PrintStream out = System.out;
    protected File securityDataDir;
    protected ExchangeableData exchangeableData;
    protected Properties props;
    private List<String> resolvedDataTypes = new LinkedList<>();
    protected LocalDate beginDate;
    protected LocalDate endDate;
    protected boolean overwrite;
    protected boolean exitOnError;

    public void preImportData() throws Exception
    {

    }

    public void postImportData() throws Exception
    {

    }

    /**
     * 按照 data type 导入
     */
    public void importDataByDataTypes() throws Exception
    {

    }

    /**
     * 按exchangeable导入
     */
    public void importData(List<Exchangeable> exchangeables) throws Exception
    {
        for(Exchangeable e:exchangeables){
            importData(e);
        }
    }

    public abstract void importData(Exchangeable exchangeable) throws Exception;

    public List<Exchangeable> processArgs(String[] securityIds, String[] dataTypes, LocalDate beginDate, LocalDate endDate, Properties props)
            throws Exception
    {
        overwrite = "true".equalsIgnoreCase(props.getProperty("overwrite", "false"));
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.props = props;
        if ( dataTypes==null ){
            resolvedDataTypes.addAll(Arrays.asList(getAllDataTypes()));
        }else{
            if ( dataTypes.length==1&&dataTypes[0].equalsIgnoreCase(DATATYPE_ALL) ){
                resolvedDataTypes.addAll(Arrays.asList(getAllDataTypes()));
            }else{
                resolvedDataTypes.addAll(Arrays.asList(dataTypes));
            }
        }

        return resolveExchangeables(securityIds);
    }

    public boolean canImportParallel(){
        return true;
    }

    protected abstract String[] getAllDataTypes();

    protected List<Exchangeable> getAllSupportedExchangeables()throws Exception
    {
        List<Exchangeable> result = ExchangeableUtil.loadAllExchangeables(securityDataDir);
        return result;
    }

    protected List<Exchangeable> resolveExchangeables(String[] securityIds) throws Exception
    {
        Collection<Exchangeable> allSecurityIds = getAllSupportedExchangeables();
        List<Exchangeable> resolvedSecurities = new LinkedList<>();
        for( Exchangeable e: ExchangeableUtil.resolveExchangeables(securityIds, allSecurityIds)){
            //if ( canImport(e) )
            {
                resolvedSecurities.add(e);
            }
        }
        if (resolvedSecurities.size()==0) {
            throw new IllegalArgumentException("No security to import");
        }

        return resolvedSecurities;
    }

    protected boolean needsDataType(String dataType){
        if ( resolvedDataTypes==null ){
            return false;
        }
        for(String d:resolvedDataTypes){
            if ( d.equalsIgnoreCase(dataType)){
                return true;
            }
        }
        return false;
    }

    protected boolean needsDataType(Classification dataType){
        if ( resolvedDataTypes==null ){
            return false;
        }
        for(String d:resolvedDataTypes){
            if ( d.equalsIgnoreCase(dataType.name()) || d.equalsIgnoreCase(dataType.altName()) ){
                return true;
            }
        }
        return false;
    }

    protected LocalDate getEndDate(Exchangeable exchangeable, boolean completed){
        LocalDate lastDate = endDate;
        if ( lastDate==null ){
            lastDate = MarketDayUtil.lastMarketDay(exchangeable.exchange(), completed);
        }
        return lastDate;
    }

    protected static String security2hscode(Exchangeable security) {
        String ex = "sh";
        if ( security.exchange()==Exchange.SSE ) {
            ex = "sh";
        } else if ( security.exchange()==Exchange.SZSE ) {
            ex = "sz";
        } else {
            throw new RuntimeException("Unknown exchange "+security.exchange());
        }
        return ex+security.id();
    }


    protected static Classification datatype2classification(String dataType) {
        switch(dataType) {
        case DATATYPE_MINUTE5:
            return ExchangeableData.MIN5;
        case DATATYPE_MINUTE15:
            return ExchangeableData.MIN15;
        case DATATYPE_MINUTE30:
            return ExchangeableData.MIN30;
        default:
            throw new RuntimeException("Unsupported data type: "+dataType);
        }
    }

}
