package trader.tools.dataimport.ctp;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import net.jctp.*;
import trader.common.exchangeable.*;
import trader.common.exchangeable.Exchange.MarketType;
import trader.common.util.*;
import trader.common.util.csv.CtpCSVMarshallHelper;
import trader.tools.dataimport.DataImporter;

public class CtpDataImporter extends DataImporter {

    private volatile MdApi mdApi;

    private volatile boolean closed;
    private String mdFrontUrl;
    private String mdFrontUrl2;
    private String brokerId;
    private String username;
    private String password;
    private String workdir;
    private int reconnectDelay = 60;
    private List<Exchangeable> exchangeablesToImport;
    private AtomicInteger ticketCount = new AtomicInteger();
    private LinkedBlockingQueue<CThostFtdcDepthMarketDataField> fields = new LinkedBlockingQueue<>();
    private Map<String, CThostFtdcDepthMarketDataField> lastTicketMap = new HashMap<>();
    private Map<String, Writer> writerMap = new HashMap<>();

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_TICK};
    }

    @Override
    public boolean canImportParallel(){
        return false;
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception
    {
        throw new RuntimeException("operation not supported");
    }

    @Override
    public void importData(List<Exchangeable> exchangeables)
            throws Exception
    {
        exchangeablesToImport = exchangeables;
        mdFrontUrl = props.getProperty("mdFrontUrl");
        mdFrontUrl2 = props.getProperty("mdFrontUrl2");
        if ( mdFrontUrl2==null) {
            mdFrontUrl2 = mdFrontUrl;
        }
        brokerId = props.getProperty("brokerId");
        username = props.getProperty("username");
        password = props.getProperty("password");
        workdir = props.getProperty("tmpdir","tmp")+"/"+"ctp-"+DateUtil.date2str(LocalDate.now());
        reconnectDelay = Integer.parseInt(props.getProperty("reconnectDelay","60"));
        File csvDataDir = new File(workdir);
        csvDataDir.mkdirs();
        csvDataDir.deleteOnExit();
        //logon and save market data as csv at market time.
        LocalTime marketCloseTime = Exchange.CFFEX.getDefaultOpenCloseTime(MarketType.Day)[1];
        mdApi = new MdApi();
        mdApi.setListener(new MyListener());
        mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);
        subscribleSecurities(mdApi);
        Thread thread = new Thread(()->{saveMarketDataProc();});
        thread.setName("CTP data import save thread");
        thread.setDaemon(true);
        thread.start();

        System.out.println(DateUtil.getCurrentTimeAsString()+" CTP 总订阅数: "+exchangeables.size());
        int lastMinuteTickts = 999;
        while( DateUtil.getCurrentTime().toLocalTime().isBefore(marketCloseTime) || lastMinuteTickts!=0 ){
            Thread.sleep(60*1000);
            lastMinuteTickts = ticketCount.getAndSet(0);
            System.out.println( DateUtil.date2str(System.currentTimeMillis())+" : "+lastMinuteTickts);
        }
        closed = true;
        if ( mdApi!=null ){
            mdApi.Close();
            mdApi = null;
        }

        //import and save market data as zip
        File[] csvs = csvDataDir.listFiles();
        if ( csvs==null|| csvs.length==0 ){
            System.out.println(DateUtil.getCurrentTimeAsString()+" 没有CTP市场数据可归集");
            return;
        }
        for(File csv:csvs){
            String fparts[] = csv.getName().split("\\.");
            //lts.exchange.instrument.csv
            if ( fparts.length!=3
                    || !fparts[0].equals("ctp")
                    || !fparts[2].equals("csv")
                    ) {
                continue;
            }
            String instrument = fparts[1];
            StringBuilder ctpText = new StringBuilder(40960);
            CSVDataSet dataSet = CSVUtil.parse(csv);
            String header = dataSet.getLine();
            ctpText.append(header).append("\n");
            String lastLine = null;
            String line = null;
            int rowCount=0;
            while(dataSet.next()){
                line = dataSet.getLine();
                if ( line.equals(lastLine)){
                    continue;
                }
                ctpText.append(line).append("\n");
                lastLine = line;
                rowCount++;
            }
            exchangeableData.save(Future.fromString(instrument), ExchangeableData.TICK_CTP, LocalDate.now(), ctpText.toString());
            System.out.println(DateUtil.getCurrentTimeAsString()+" 导入 "+csv.getName()+" : "+rowCount);
        }
        csvDataDir.delete();
    }

    private void subscribleSecurities(MdApi mdApiToSubscribe) throws Exception
    {
        for(Exchangeable e: exchangeablesToImport){
            mdApiToSubscribe.SubscribeMarketData(new String[]{e.id()});
        }
    }

    private void saveMarketDataProc()
    {
        CThostFtdcDepthMarketDataField field = null;
        while(!closed){
            try {
                field = fields.poll(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {}
            if ( field==null ) {
                continue;
            }
            appendRow(field);
        }
    }

    private boolean isNewTicket(CThostFtdcDepthMarketDataField lastField, CThostFtdcDepthMarketDataField newField)
    {
        if ( lastField==null ) {
            return true;
        }
        int r = newField.UpdateTime.compareTo(lastField.UpdateTime);
        if ( r>0 ) {
            return true;
        }
        if ( r<0 ) {
            return false;
        }
        //相等,检查毫秒值
        return newField.UpdateMillisec>lastField.UpdateMillisec;
    }

    private void appendRow(CThostFtdcDepthMarketDataField field)
    {
        String writerId = field.InstrumentID;
        CThostFtdcDepthMarketDataField lastField = lastTicketMap.get(writerId);
        if ( isNewTicket(lastField,field) ){
            lastTicketMap.put(writerId, field);
        }else{
            return;
        }

        try {
            Writer writer = writerMap.get(writerId);
            if ( writer==null ){
                File file = new File(workdir,"ctp."+writerId+".csv");
                boolean needHeader = !(file.exists() && file.length()>0);
                writer = new OutputStreamWriter(new FileOutputStream(file, true),"UTF-8");
                if ( needHeader ){
                    writer.write(StringUtil.array2str(new CtpCSVMarshallHelper().getHeader(), ","));
                    writer.write("\n");
                }
                writerMap.put(writerId, writer);
            }
            writer.write(StringUtil.array2str(new CtpCSVMarshallHelper().marshall(field),","));
            writer.write("\n");
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asyncConnectMdApi()
    {
        if ( closed ) {
            return;
        }
        Thread t = new Thread(){
            @Override
            public void run(){
                if ( closed ) {
                    return;
                }
                if ( reconnectDelay>0 ){
                    try {
                        Thread.sleep(reconnectDelay*1000);
                    } catch (InterruptedException e) {}
                }
                boolean needReconnectAgain = false;
                try {
                    mdApi = new MdApi();
                    mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);
                    mdApi.setListener(new MyListener());
                    subscribleSecurities(mdApi);
                } catch (Exception e) {
                    System.out.println("异步连接MdApi失败:"+e);
                    needReconnectAgain = true;
                }
                if ( needReconnectAgain ) {
                    asyncConnectMdApi();
                }
            }
        };
        t.setDaemon(true);
        t.start();
    }

    class MyListener implements MdApiListener {

        @Override
        public void OnFrontConnected() {
            System.out.println(DateUtil.getCurrentTimeAsString()+" CTP 行情已连接");
        }

        @Override
        public void OnFrontDisconnected(int nReason) {
            System.err.println(DateUtil.getCurrentTimeAsString()+" CTP 行情连接断开: "+nReason);
            asyncConnectMdApi();
        }

        @Override
        public void OnHeartBeatWarning(int nTimeLapse) {
        }

        @Override
        public void OnRspUserLogin(CThostFtdcRspUserLoginField pRspUserLogin, CThostFtdcRspInfoField pRspInfo,
                int nRequestID, boolean bIsLast) {
            System.out.println(DateUtil.getCurrentTimeAsString()+" CTP 行情已登录:"+pRspUserLogin+" "+pRspInfo);
        }

        @Override
        public void OnRspUserLogout(CThostFtdcUserLogoutField pUserLogout, CThostFtdcRspInfoField pRspInfo, int nRequestID,
                boolean bIsLast) {
            System.err.println(DateUtil.getCurrentTimeAsString()+" CTP 行情已登出:"+pRspInfo);
        }

        @Override
        public void OnRspError(CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
            System.err.println(DateUtil.getCurrentTimeAsString()+" 错误:"+pRspInfo);
        }

        @Override
        public void OnRspSubMarketData(CThostFtdcSpecificInstrumentField pSpecificInstrument,
                CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
            System.out.println(DateUtil.getCurrentTimeAsString()+" CTP 行情订阅: "+pSpecificInstrument.InstrumentID+" "+pRspInfo);
        }

        @Override
        public void OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField pSpecificInstrument,
                CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        }

        @Override
        public void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField pDepthMarketData) {
            fields.add(pDepthMarketData);
            ticketCount.incrementAndGet();
        }

        @Override
        public void OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField pSpecificInstrument,
                CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {

        }

        @Override
        public void OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField pSpecificInstrument,
                CThostFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {

        }

		@Override
        public void OnRtnForQuoteRsp(net.jctp.CThostFtdcForQuoteRspField q)
		{

		}
    }
}
