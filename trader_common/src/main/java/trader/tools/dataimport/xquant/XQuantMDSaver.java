package trader.tools.dataimport.xquant;

import java.io.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import com.dfitc.stp.market.Bar;
import com.dfitc.stp.market.MD;

public class XQuantMDSaver {
    static final ZoneId     ZONEID_BEIJING = ZoneId.of("Asia/Shanghai");
    private File rootDir;
    private ConcurrentHashMap<String, Writer> writers = new ConcurrentHashMap<>();

    public XQuantMDSaver(File rootDir){
        this.rootDir = rootDir;
    }

    public void close(){
        for(Writer writer: writers.values()){
            try {
                writer.flush();
            } catch (IOException e) {
            }
            try {
                writer.close();
            } catch (IOException e) {
            }
        }
    }

    public void save(MD md) throws IOException
    {
        Writer writer = getWriter(md);
        StringBuilder line = new StringBuilder();

        Instant instant = Instant.ofEpochMilli(md.getTimeStamp().getTime());
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZONEID_BEIJING);
        DateTimeFormatter dateFormatTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        line.append(dateFormatTimestamp.format(ldt)).append(",")
        .append(md.getOpen()).append(",")
        .append(md.getHigh()).append(",")
        .append(md.getLow()).append(",")

        .append(md.getLatestPrice()).append(",")
        .append(md.getLatestVol()).append(",")
        .append(md.getTurnover()).append(",")
        .append(md.getVol()).append(",")
        .append(md.getOpenInt()).append(",")
        .append(md.getAvgLatestPrice()).append(",")

        .append(md.getAskPrice1()).append(",")
        .append(md.getAskVol1()).append(",")
        .append(md.getBidPrice1()).append(",")
        .append(md.getBidVol1()).append(",")

        .append(md.getYesterdayClosePrice()).append(",")
        .append(md.getYesterdayOpenInt()).append(",")
        .append(md.getPreSettlementPrice()).append(",")

        .append(md.getTodayClosePrice()).append(",")
        .append(md.getSettlementPrice()).append(",")
        .append(md.getUpperLimitPrice()).append(",")
        .append(md.getLowerLimitPrice())

        .append("\n");

        writer.write(line.toString());
        writer.flush();
    }

    private Writer getWriter(MD md) throws IOException
    {
        DateTimeFormatter yyyymmdd2 = DateTimeFormatter.ofPattern("yyyyMMdd");
        String future = md.getContractCode(); //IF1606
        Date date = md.getTimeStamp();
        Instant instant = Instant.ofEpochMilli(date.getTime());
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZONEID_BEIJING);
        String id = future+"-"+yyyymmdd2.format(ldt.toLocalDate())+".tick-xquant";
        Writer writer = writers.get(id);
        if ( writer==null ){
            File file = new File(rootDir,id+".csv");
            writer = new FileWriter(file);
            writer.write("Time,Open,High,Low"
                    +",LatestPrice,LatestVol,Turnover,Vol,OpenInt,AvgLatestPrice"
                    +",AskPrice1,AskVol1,BidPrice1,BidVol1"
                    +",YesterdayClosePrice,YesterdayOpenInt"
                    +",TodayClosePrice,PreSettlementPrice,TodayClosePrice,SettlementPrice"
                    +",UpperLimitPrice,LowerLimitPrice"
                    +"\n");
            writers.put(id, writer);
        }
        return writer;
    }

    public void save(Bar bar) throws IOException
    {
        Writer writer = getWriter(bar);
        StringBuilder line = new StringBuilder();

        Instant instant = Instant.ofEpochMilli(bar.getBeginTime().getTime());
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZONEID_BEIJING);
        DateTimeFormatter dateFormatTimestamp = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        Instant instant2 = Instant.ofEpochMilli(bar.getEndTime().getTime());
        LocalDateTime ldt2 = LocalDateTime.ofInstant(instant2, ZONEID_BEIJING);

        line.append(dateFormatTimestamp.format(ldt)).append(",")
        .append(dateFormatTimestamp.format(ldt2)).append(",")
        .append(bar.getOpen()).append(",")
        .append(bar.getHigh()).append(",")
        .append(bar.getLow()).append(",")
        .append(bar.getClose()).append(",")

        .append(bar.getVol()).append(",")
        .append("0").append(",")
        .append(bar.getOpenInt()).append(",")
        .append("\n");

        writer.write(line.toString());
        writer.flush();
    }

    private Writer getWriter(Bar bar) throws IOException
    {
        DateTimeFormatter yyyymmdd2 = DateTimeFormatter.ofPattern("yyyyMMdd");
        String future = bar.getContractCode();
        Date date = bar.getTimeStamp();
        Instant instant = Instant.ofEpochMilli(date.getTime());
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZONEID_BEIJING);

        String id = future+"-"+yyyymmdd2.format(ldt.toLocalDate())+".min"+bar.getBarCycle().getUnitLength()+"-xquant";
        Writer writer = writers.get(id);
        if ( writer==null ){
            File file = new File(rootDir,id+".csv");
            writer = new FileWriter(file);
            writer.write("BeginTime,EndTime,Open,High,Low,Close,Volume,Turnover,OpenInt"
                    +"\n");
            writers.put(id, writer);
        }
        return writer;
    }

}
