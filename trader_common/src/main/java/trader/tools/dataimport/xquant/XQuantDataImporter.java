package trader.tools.dataimport.xquant;

import java.io.File;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.ExchangeableData;
import trader.common.util.DateUtil;
import trader.common.util.FileUtil;
import trader.tools.dataimport.DataImporter;

public class XQuantDataImporter extends DataImporter {

    private String xquantMdDir = "";

    @Override
    protected List<Exchangeable> getAllSupportedExchangeables() throws Exception {
        List<Exchangeable> result = new LinkedList<>();
        xquantMdDir = props.getProperty("xquantMdDir");
        if ( xquantMdDir==null ){
            throw new RuntimeException("property xquantMdDir is required");
        }
        for(File file:(new File(xquantMdDir)).listFiles()){
            String fname = file.getName();
            if ( fname.indexOf('-')<0 ){
                continue;
            }
            String exchangeableName = fname.substring(0, fname.indexOf('-'));
            Exchangeable e = Exchangeable.fromString(exchangeableName);
            if ( !result.contains(e) ){
                result.add(e);
            }
        }
        return result;
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[] { DATATYPE_TICK, DATATYPE_MINUTE1, DATATYPE_MINUTE5, DATATYPE_MINUTE15, DATATYPE_MINUTE30 };
    }

    @Override
    public void preImportData() throws Exception {
        xquantMdDir = props.getProperty("xquantMdDir");
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {
        File exchangeMdDir = new File(xquantMdDir);
        if ( !exchangeMdDir.exists() ) {
            throw new Exception("The exchange data dir \""+exchangeMdDir+"\" doesn't exist");
        }
        System.out.print("Import "+exchangeable+" XQUANT tick data: ");
        for (File f:exchangeMdDir.listFiles()){
            //C1609-20150731.tick.csv, C1609-20150731.min1.csv
            String fnameParts[] = f.getName().split("\\.");
            if ( fnameParts.length!=3 && !fnameParts[2].equals("csv")){
                continue;
            }
            String ftype = fnameParts[1];
            String fname = fnameParts[0];
            if ( fname.indexOf('-')<0 ){
                continue;
            }
            String exchangeableName = fname.substring(0, fname.indexOf('-'));
            if ( !exchangeable.toString().equalsIgnoreCase(exchangeableName)
                    && !exchangeable.id().equalsIgnoreCase(exchangeableName)){
                continue;
            }
            String date = fname.substring(fname.indexOf('-')+1);
            LocalDate day = DateUtil.str2localdate(date);
            if ( beginDate!=null && day.isBefore(beginDate) ) {
                continue;
            }
            if ( endDate!=null && day.isAfter(endDate) ) {
                continue;
            }
            switch(ftype){
            case "tick-xquant":
                exchangeableData.save(exchangeable, ExchangeableData.TICK_XQUANT, day, FileUtil.load(f));
                break;
            case "min1-xquant":
                exchangeableData.save(exchangeable, ExchangeableData.MIN1, day, FileUtil.load(f));
                break;
            case "min5-xquant":
                exchangeableData.save(exchangeable, ExchangeableData.MIN5, day, FileUtil.load(f));
                break;
            case "min15-xquant":
                exchangeableData.save(exchangeable, ExchangeableData.MIN15, day, FileUtil.load(f));
                break;
            case "min30-xquant":
                exchangeableData.save(exchangeable, ExchangeableData.MIN30, day, FileUtil.load(f));
                break;
            default:
                System.out.println("Ignore "+f);
            }
            System.out.print(".");
        }
        System.out.println();
    }

}
