package trader.tools.dataimport.sina;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import trader.common.exchangeable.*;
import trader.common.util.*;
import trader.tools.dataimport.DataImporter;

public class SinaDataImporter extends DataImporter {
    private static final Charset gbkCharset = Charset.forName("GBK");
    private static final String NODATA = "no data";

    @Override
    public void preImportData() throws Exception
    {
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {
        if ( needsDataType(DATATYPE_DAY) ) {
            switch ( exchangeable.getType()){
            case STOCK:
                importStockDayData(exchangeable);
                break;
            case INDEX:
            case FUND:
                importIndexDayData(exchangeable);
                break;
            case FUTURE:
                importFutureDayData(exchangeable);
                break;
            default:
                out.println(DateUtil.getCurrentTimeAsString()+" Ignore unsupported exchangeable: "+exchangeable.toPrintableString());
                break;
            }
        }
        if ( needsDataType(DATATYPE_TICK) || needsDataType("second") ) {
            importSecurityTickData(exchangeable);
        }
    }

    @Override
    protected String[] getAllDataTypes(){
        return new String[]{DATATYPE_DAY, DATATYPE_TICK};
    }

    /**
     * 导入期货日线数据
     */
    private void importFutureDayData(Exchangeable exchangeable) throws IOException
    {
        Future future = (Future)exchangeable;
        //http://vip.stock.finance.sina.com.cn/q/view/vFutures_History.php?jys=cffex&pz=IF&hy=IF1610&breed=IF1610&type=inner&start=2016-09-03&end=2016-10-16
        LocalDate lastDate = getEndDate(exchangeable, true);
        LocalDate currDate = beginDate;
        if ( currDate==null ){
            currDate = MarketDayUtil.lastMarketDay(exchangeable.exchange(), true);
        }
        LinkedList<Object[]> rows = new LinkedList<>();
        out.print(DateUtil.getCurrentTimeAsString()+" Import future "+exchangeable.toPrintableString()+" daily data("+currDate+"-"+lastDate+"): ");
        String originalDays = null;
        if ( exchangeableData.exists(exchangeable, ExchangeableData.DAY, null)){
            originalDays = exchangeableData.load(exchangeable, ExchangeableData.DAY, null);
        }
        Exception lastException = null;

        while(currDate.isBefore(lastDate) || currDate.isEqual(lastDate)){
            String url = "http://vip.stock.finance.sina.com.cn/q/view/vFutures_History.php?jys="+future.exchange().name()+"&pz="+future.commodity()+"&hy="+future.id()+"&breed="+future.id()+"&type=inner&start="+currDate.toString()+"&end="+currDate.toString();
            currDate = MarketDayUtil.nextMarketDay(exchangeable.exchange(), currDate);
            if ( !MarketDayUtil.isMarketDay(future.exchange(), currDate) ){
                continue;
            }
            String html = null;
            try{
                html = NetUtil.<String>doAction(()->{ return NetUtil.readHttpAsText(url, NetUtil.gbkCharset);});
            }catch(Exception e){
                out.print("E");
                lastException = e;
                continue;
            }
            Document doc = Jsoup.parse(html);
            Elements divHistoryList = doc.select(".historyList");
            Element table = divHistoryList.first().select("table").first();
            Elements trs = table.select("tr.tr_2");
            boolean empty = true;
            if ( !trs.isEmpty()){
                empty = false;
                Elements tds = trs.first().select("td");
                String date = tds.get(0).text().trim();
                String close = tds.get(1).text().trim();
                String open = tds.get(2).text().trim();
                String high = tds.get(3).text().trim();
                String low = tds.get(4).text().trim();
                String volume = tds.get(5).text().trim();
                rows.add(new Object[]{DateUtil.str2localdate(date), open, high, low, close, volume, ""});
            }
            if ( !empty ){
                out.print(".");
            }else{
                out.print("N");
            }
        }
        Collections.sort(rows, (o1, o2) -> ((LocalDate)o1[0]).compareTo((LocalDate)o2[0]));
        CSVWriter csvWriter = new CSVWriter(ExchangeableData.DAY_COLUMNS);
        for(Object[] row:rows){
            csvWriter.append(row);
        }
        if ( originalDays==null ){
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvWriter.toString());
        }else{
            String csvText = CSVUtil.merge(originalDays, csvWriter.toString(), ExchangeableData.COLUMN_DATE, overwrite);
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvText);
        }
        out.println(" : "+rows.size());
        if ( lastException!=null){
            out.print(", "+lastException.toString());
        }
        out.println();
    }

    /**
     * 导入股票日线数据
     */
    private void importStockDayData(Exchangeable exchangeable) throws IOException
    {
        LocalDate lastDate = getEndDate(exchangeable, true);
        int lastYear=0, lastQuarter=0;
        LocalDate currDate = beginDate;
        if ( currDate==null ){
            currDate = MarketDayUtil.lastMarketDay(exchangeable.exchange(), true);
        }
        LinkedList<Object[]> rows = new LinkedList<>();
        out.print(DateUtil.getCurrentTimeAsString()+" Import "+exchangeable.getType()+" "+exchangeable.toPrintableString()+" daily data("+currDate+"-"+lastDate+"): ");
        String originalDays = null;
        if ( exchangeableData.exists(exchangeable, ExchangeableData.DAY, null)){
            originalDays = exchangeableData.load(exchangeable, ExchangeableData.DAY, null);
        }
        Exception lastException = null;

        while(currDate.isBefore(lastDate) || currDate.isEqual(lastDate)){
            int currYear = currDate.getYear();
            int currQuarter = DateUtil.quarter(currDate);
            currDate = currDate.plusMonths(1);
            if ( lastYear==currYear && lastQuarter==currQuarter){
                continue;
            }
            lastYear = currYear; lastQuarter=currQuarter;
            String url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vMS_FuQuanMarketHistory/stockid/"+exchangeable.id()+".phtml?year="+currYear+"&jidu="+currQuarter;
            String html = null;
            try{
                html = NetUtil.<String>doAction(()->{ return NetUtil.readHttpAsText(url, NetUtil.gbkCharset);});
            }catch(Exception e){
                out.print("E");
                lastException = e;
                continue;
            }
            Document doc = Jsoup.parse(html);
            Elements tables = doc.select("#FundHoldSharesTable");
            boolean empty = true;
            if ( !tables.isEmpty()){
                Element table = tables.get(0);
                for(Element tr:table.select("tr")){
                    Elements tds = tr.select("td");
                    if ( tds.isEmpty() || tds.get(0).text().length()<=3 ){
                        continue;
                    }
                    String date = tds.get(0).text();
                    String open = tds.get(1).text();
                    String high = tds.get(2).text();
                    String close = tds.get(3).text();
                    String low = tds.get(4).text();
                    String volume  = tds.get(5).text();
                    String turnover  = tds.get(6).text();
                    String factor = tds.get(7).text();
                    empty = false;
                    //2015-03-31,65.196,67.055,63.200,62.064,504674816.000,9453716480.000,3.442
                    volume = ""+(new BigDecimal(volume)).longValue();
                    date = DateUtil.date2str( DateUtil.str2localdate(date) );
                    BigDecimal factor0 = new BigDecimal(factor);
                    open = (new BigDecimal(open)).divide(factor0, 2, RoundingMode.HALF_UP).toString();
                    high = (new BigDecimal(high)).divide(factor0, 2, RoundingMode.HALF_UP).toString();
                    close = (new BigDecimal(close)).divide(factor0, 2, RoundingMode.HALF_UP).toString();
                    low = (new BigDecimal(low)).divide(factor0, 2, RoundingMode.HALF_UP).toString();
                    rows.add(new Object[]{date, open, high, low, close, volume, turnover, factor});
                }
            }
            if ( !empty ){
                out.print(".");
            }else{
                out.print("N");
            }
        }
        Collections.sort(rows, (o1, o2) -> ((String)o1[0]).compareTo((String)o2[0]));
        CSVWriter csvWriter = new CSVWriter(ExchangeableData.STOCK_DAY_COLUMNS);
        for(Object[] row:rows){
            csvWriter.append(row);
        }
        if ( originalDays==null ){
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvWriter.toString());
        }else{
            String csvText = CSVUtil.merge(originalDays, csvWriter.toString(), ExchangeableData.COLUMN_DATE, overwrite);
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvText);
        }
        out.print(" : "+rows.size());
        if ( lastException!=null){
            out.print(", "+lastException.toString());
        }
        out.println();
    }

    /**
     * 导入指数日线数据
     */
    private void importIndexDayData(Exchangeable exchangeable) throws IOException
    {
        LocalDate lastDate = getEndDate(exchangeable, true);
        int lastYear=0, lastQuarter=0;
        LocalDate currDate = beginDate;
        if ( currDate==null ){
            currDate = MarketDayUtil.lastMarketDay(exchangeable.exchange(), true);
        }
        LinkedList<Object[]> rows = new LinkedList<>();
        out.print(DateUtil.getCurrentTimeAsString()+" Import "+exchangeable.getType()+" "+exchangeable.toPrintableString()+" daily data("+currDate+"-"+lastDate+"): ");
        String originalDays = null;
        if ( exchangeableData.exists(exchangeable, ExchangeableData.DAY, null)){
            originalDays = exchangeableData.load(exchangeable, ExchangeableData.DAY, null);
        }
        Exception lastException = null;

        while(currDate.isBefore(lastDate) || currDate.isEqual(lastDate)){
            int currYear = currDate.getYear();
            int currQuarter = DateUtil.quarter(currDate);
            currDate = currDate.plusMonths(1);
            if ( lastYear==currYear && lastQuarter==currQuarter){
                continue;
            }
            lastYear = currYear; lastQuarter=currQuarter;
            String url = null;
            if ( exchangeable.getType()==ExchangeableType.INDEX){
                url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vMS_MarketHistory/stockid/"+exchangeable.id()+"/type/S.phtml?year="+currYear+"&jidu="+currQuarter;
            }else{
                url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vMS_MarketHistory/stockid/"+exchangeable.id()+".phtml?year="+currYear+"&jidu="+currQuarter;
            }
            final String url2 = url;
            String html = null;
            try{
                html = NetUtil.<String>doAction(()->{ return NetUtil.readHttpAsText(url2, NetUtil.gbkCharset);});
            }catch(Exception e){
                out.print("E");
                lastException = e;
                continue;
            }
            Document doc = Jsoup.parse(html);
            Elements tables = doc.select("#FundHoldSharesTable");
            boolean empty = true;
            if ( !tables.isEmpty()){
                Element table = tables.get(0);
                for(Element tr:table.select("tr")){
                    Elements tds = tr.select("td");
                    if ( tds.isEmpty() || tds.get(0).text().length()<=3 ){
                        continue;
                    }
                    String date = tds.get(0).text();
                    String open = tds.get(1).text();
                    String high = tds.get(2).text();
                    String close = tds.get(3).text();
                    String low = tds.get(4).text();
                    String volume  = tds.get(5).text();
                    String turnover  = tds.get(6).text();
                    empty = false;
                    //2015-03-31,65.196,67.055,63.200,62.064,504674816.000,9453716480.000,3.442
                    volume = ""+(new BigDecimal(volume)).longValue();
                    date = DateUtil.date2str( DateUtil.str2localdate(date) );
                    rows.add(new Object[]{date, open, high, low, close, volume, turnover});
                }
            }
            if ( !empty ){
                out.print(".");
            }else{
                out.print("N");
            }
        }
        Collections.sort(rows, (o1, o2) -> ((String)o1[0]).compareTo((String)o2[0]));
        CSVWriter csvWriter = new CSVWriter(ExchangeableData.DAY_COLUMNS);
        for(Object[] row:rows){
            csvWriter.append(row);
        }
        if ( originalDays==null ){
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvWriter.toString());
        }else{
            String csvText = CSVUtil.merge(originalDays, csvWriter.toString(), ExchangeableData.COLUMN_DATE, overwrite);
            exchangeableData.save(exchangeable, ExchangeableData.DAY, null, csvText);
        }
        out.println(" : "+rows.size());
        if ( lastException!=null){
            out.print(", "+lastException.toString());
        }
        out.println();
    }

    /**
     * 导入股票分时历史数据
     */
    private LinkedList<LocalDate> importSecurityTickData(Exchangeable security) throws IOException
    {
        if ( security.getType() != ExchangeableType.STOCK) {
            return new LinkedList<>();
        }
        LinkedList<LocalDate> loadedDays = new LinkedList<>();
        List<LocalDate> suspensionDays = exchangeableData.getSuspensionDays(security);
        LocalDate nextDate = beginDate;
        LocalDate lastDate = getEndDate(security, true);

        if ( nextDate ==null ){
            out.print(DateUtil.getCurrentTimeAsString()+" Import stock "+security.name()+" "+security.id()+" second data: ");
            nextDate = lastDate;
        }else{
            out.print(DateUtil.getCurrentTimeAsString()+" Import stock "+security.name()+" "+security.id()+" second data("+beginDate+"-"+lastDate+"): ");
        }
        int totalCount=0;
        int ignoreCount=0;
        int saveCount=0;
        int noDataCount=0;
        StringBuilder noDataDays = new StringBuilder();
        do{
            LocalDate fileDate = nextDate;
            nextDate = MarketDayUtil.nextMarketDay(security.exchange(), nextDate);
            if ( !MarketDayUtil.isMarketDay(security.exchange(), fileDate)) {
                continue;
            }
            if ( !overwrite && suspensionDays.contains(fileDate)){
                continue;
            }
            totalCount++;
            if( !overwrite && exchangeableData.exists(security, ExchangeableData.TICK_SINA, fileDate) ){
                out.print("I");
                ignoreCount++;
                loadedDays.add(fileDate);
                continue;
            }
            String str = fetchSecurityDataSecond(security,fileDate);
            if ( NODATA.equals(str) ){
                out.print("N");
                noDataCount++;
                if ( noDataDays.length()>0 ) {
                    noDataDays.append(",");
                }
                noDataDays.append(fileDate);
                exchangeableData.addSuspensionDay(security, fileDate);
            }else{

                StringWriter text = new StringWriter();
                try(BufferedReader reader = new BufferedReader(new StringReader(str));
                        BufferedWriter writer = new BufferedWriter(text)   )
                {
                    List<String[]> rows = new LinkedList<>();
                    String line;
                    String head = reader.readLine();
                    while ((line=reader.readLine())!=null){
                        line = line.trim();
                        if (line.length()==0) {
                            continue;
                        }
                        String[] rawRow = CSVUtil.parseLine(line, '\t');
                        String[] stdRow = new String[5];
                        stdRow[0] = rawRow[0];
                        stdRow[1] = rawRow[1];
                        stdRow[2] = rawRow[3];
                        stdRow[3] = rawRow[4];
                        String buySell = rawRow[5].trim();
                        if ( buySell.equals("买盘") ) {
                            buySell = "buy";
                        } else if ( buySell.equals("卖盘")) {
                            buySell = "sell";
                        } else if ( buySell.equals("中性盘")) {
                            buySell = "neutral";
                        }
                        stdRow[4] = buySell;
                        rows.add(0, stdRow);//sina股票明细时间从后到前
                    }
                    if ( rows.size()==0 ){
                        out.print("N");
                        noDataCount++;
                        if ( noDataDays.length()>0 ) {
                            noDataDays.append(",");
                        }
                        noDataDays.append(fileDate);
                        continue;
                    }else{
                        out.print(".");
                        saveCount++;
                        loadedDays.add(fileDate);
                    }
                    writer.write(StringUtil.array2str(ExchangeableData.TICK_SINA_COLUMNS, ","));//"Time,Price,Volume,Turnover,Buy/Sell\n");
                    writer.write("\n");
                    for(String[] row:rows) {
                        CSVUtil.writeCSVLine(writer, row, ',');
                    }
                }
                exchangeableData.save(security, ExchangeableData.TICK_SINA, fileDate, text.toString());
            }
        }while(lastDate.compareTo(nextDate)>=0);
        out.print(" TOTAL "+totalCount+", SAVE "+saveCount+", IGNORE "+ignoreCount+", NO DATA "+noDataCount);
        if( noDataDays.length()>0 ) {
            out.print(", NO DATA DAYS: "+noDataDays);
        }
        out.println();
        return loadedDays;
    }

    private String fetchSecurityDataSecond(Exchangeable security, LocalDate date)
            throws IOException
    {
        String datestr = DateUtil.date2str(date);
        String date2 = datestr.substring(0, 4)+"-"+datestr.substring(4,6)+"-"+datestr.substring(6);
        String url = "http://market.finance.sina.com.cn/downxls.php?date="+date2+"&symbol="+security2hscode(security);
        String text = "";
        try{
            text = NetUtil.readHttpAsText(url, gbkCharset);
        }catch(IOException ioe){
            if ( exitOnError ) {
                throw ioe;
            }
        }
        //alert("当天没有数据");
        if ( text.indexOf("alert")>=0 ) {
            return NODATA;
        }
        return text;
    }
}
