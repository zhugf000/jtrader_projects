package trader.tools.dataimport.lts;

import java.io.*;
import java.text.Format;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import net.jlts.*;
import trader.common.exchangeable.*;
import trader.common.exchangeable.Exchange.MarketType;
import trader.common.util.*;
import trader.tools.dataimport.DataImporter;

public class LtsDataImporter extends DataImporter {

    private static final String CSV_HEADER =
            "TradingDay"
                    +",InstrumentID"
                    +",ExchangeID"
                    +",ExchangeInstID"
                    +",LastPrice"
                    +",PreSettlementPrice"
                    +",PreClosePrice"
                    +",PreOpenInterest"
                    +",OpenPrice"
                    +",HighestPrice"
                    +",LowestPrice"
                    +",Volume"
                    +",Turnover"
                    +",OpenInterest"
                    +",ClosePrice"
                    +",SettlementPrice"
                    +",UpperLimitPrice"
                    +",LowerLimitPrice"
                    +",PreDelta"
                    +",CurrDelta"
                    +",UpdateTime"
                    +",UpdateMillisec"
                    +",BidPrice1"
                    +",BidVolume1"
                    +",AskPrice1"
                    +",AskVolume1"
                    +",BidPrice2"
                    +",BidVolume2"
                    +",AskPrice2"
                    +",AskVolume2"
                    +",BidPrice3"
                    +",BidVolume3"
                    +",AskPrice3"
                    +",AskVolume3"
                    +",BidPrice4"
                    +",BidVolume4"
                    +",AskPrice4"
                    +",AskVolume4"
                    +",BidPrice5"
                    +",BidVolume5"
                    +",AskPrice5"
                    +",AskVolume5"
                    +",AveragePrice"
                    +",ActionDay"
                    +"\n"
                    ;
    private volatile MdApi mdApi;
    private volatile MdApi mdApiR;
    private volatile boolean closed;
    private String mdFrontUrl;
    private String brokerId;
    private String username;
    private String password;
    private String workdir;
    private int reconnectDelay = 60;
    private LinkedBlockingQueue<CSecurityFtdcDepthMarketDataField> fields = new LinkedBlockingQueue<>();
    private Map<String, Writer> writerMap = new HashMap<>();
    private Map<String, CSecurityFtdcDepthMarketDataField> lastTicketMap = new HashMap<>();
    private AtomicInteger ticketCount = new AtomicInteger();
    private List<Exchangeable> exchangeablesToImport;

    @Override
    public boolean canImportParallel(){
        return false;
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception
    {
        throw new RuntimeException("operation not supported");
    }

    @Override
    public void importData(List<Exchangeable> exchangeables)
            throws Exception
    {
        exchangeablesToImport = exchangeables;
        mdFrontUrl = props.getProperty("mdFrontUrl");
        brokerId = props.getProperty("brokerId");
        username = props.getProperty("username");
        password = props.getProperty("password");
        boolean redundancy = "true".equalsIgnoreCase(props.getProperty("redundancy"));
        workdir = props.getProperty("tmpdir","tmp")+"/"+"lts-"+DateUtil.date2str(LocalDate.now());
        reconnectDelay = Integer.parseInt(props.getProperty("reconnectDelay","60"));
        File csvDataDir = new File(workdir);
        csvDataDir.mkdirs();
        csvDataDir.deleteOnExit();
        //logon and save market data as csv at market time.
        LocalTime marketCloseTime = Exchange.SSE.getDefaultOpenCloseTime(MarketType.Day)[1];
        mdApi = new MdApi();
        mdApi.setListener(new MyListener(false));
        mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);
        if ( redundancy ){
            mdApiR = new MdApi();
            mdApiR.setListener(new MyListener(true));
            mdApiR.SyncConnect(mdFrontUrl, brokerId, username, password);
        }

        Thread thread = new Thread(()->{saveMarketDataProc();});
        thread.setName("LTS data import save thread");
        thread.setDaemon(true);
        thread.start();

        subscribleSecurities(mdApi);
        if ( redundancy)
            subscribleSecurities(mdApiR);

        System.out.println("LTS 总订阅数: "+exchangeables.size());
        int lastMinuteTickts = 999;
        while( DateUtil.getCurrentTime().toLocalTime().isBefore(marketCloseTime) || lastMinuteTickts!=0 ){
            Thread.sleep(60*1000);
            lastMinuteTickts = ticketCount.getAndSet(0);
            System.out.println( DateUtil.date2str(System.currentTimeMillis())+" : "+lastMinuteTickts);
        }
        closed = true;
        if ( mdApi!=null ){
            mdApi.Close();
            mdApi = null;
        }
        if ( mdApiR!=null ){
            mdApiR.Close();
            mdApiR = null;
        }
        //import and save market data as zip
        File[] csvs = csvDataDir.listFiles();
        if ( csvs==null|| csvs.length==0 ){
            System.out.println("没有LTS市场数据可归集");
            return;
        }
        for(File csv:csvs){
            String fparts[] = csv.getName().split("\\.");
            //lts.exchange.instrument.csv
            if ( fparts.length!=4
                    || !fparts[0].equals("lts")
                    || !fparts[3].equals("csv")
                    )
                continue;
            String exchange = fparts[1];
            String instrument = fparts[2];
            StringBuilder ltsText = new StringBuilder(40960);
            CSVDataSet dataSet = CSVUtil.parse(csv);
            String header = dataSet.getLine();
            ltsText.append(header).append("\n");
            String lastLine = null;
            String line = null;
            int rowCount=0;
            while(dataSet.next()){
                line = dataSet.getLine();
                if ( line.equals(lastLine)){
                    continue;
                }
                ltsText.append(line).append("\n");
                lastLine = line;
                rowCount++;
            }
            exchangeableData.save(Security.fromString(exchange+"."+instrument), ExchangeableData.TICK_LTS, LocalDate.now(), ltsText.toString());
            csv.delete();
            System.out.println("导入 "+csv.getName()+" : "+rowCount);
        }
        csvDataDir.delete();
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_TICK};
    }

    private void saveMarketDataProc()
    {
        CSecurityFtdcDepthMarketDataField field = null;
        while(!closed){
            try {
                field = fields.poll(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {}
            if ( field==null )
                continue;
            appendRow(field);
        }
    }

    private boolean isNewTicket(CSecurityFtdcDepthMarketDataField lastField, CSecurityFtdcDepthMarketDataField newField)
    {
        if ( lastField==null )
            return true;
        int r = newField.UpdateTime.compareTo(lastField.UpdateTime);
        if ( r>0 )
            return true;
        if ( r<0 )
            return false;
        //相等,检查毫秒值
        return newField.UpdateMillisec>lastField.UpdateMillisec;
    }

    private void appendRow(CSecurityFtdcDepthMarketDataField field)
    {
        String writerId = field.ExchangeID+"."+field.InstrumentID;
        CSecurityFtdcDepthMarketDataField lastField = lastTicketMap.get(writerId);
        if ( isNewTicket(lastField,field) ){
            lastTicketMap.put(writerId, field);
        }else{
            return;
        }
        Format millisecFormat = FormatUtil.getDecimalFormat("000");
        StringBuilder line = new StringBuilder(512);
        line
        .append(field.TradingDay)
        .append(",").append(field.InstrumentID)
        .append(",").append(field.ExchangeID)
        .append(",").append(field.ExchangeInstID)
        .append(",").append(PriceUtil.price2str(field.LastPrice))
        .append(",").append(PriceUtil.price2str(field.PreSettlementPrice))
        .append(",").append(PriceUtil.price2str(field.PreClosePrice))
        .append(",").append(PriceUtil.price2str(field.PreOpenInterest))
        .append(",").append(PriceUtil.price2str(field.OpenPrice))
        .append(",").append(PriceUtil.price2str(field.HighestPrice))
        .append(",").append(PriceUtil.price2str(field.LowestPrice))
        .append(",").append(field.Volume)
        .append(",").append(PriceUtil.price2str(field.Turnover))
        .append(",").append(PriceUtil.price2str(field.OpenInterest))
        .append(",").append(PriceUtil.price2str(field.ClosePrice))
        .append(",").append(PriceUtil.price2str(field.SettlementPrice))
        .append(",").append(PriceUtil.price2str(field.UpperLimitPrice))
        .append(",").append(PriceUtil.price2str(field.LowerLimitPrice))
        .append(",").append(PriceUtil.price2str(field.PreDelta))
        .append(",").append(PriceUtil.price2str(field.CurrDelta))
        .append(",").append(field.UpdateTime)
        .append(",").append(millisecFormat.format(field.UpdateMillisec))
        .append(",").append(PriceUtil.price2str(field.BidPrice1))
        .append(",").append(field.BidVolume1)
        .append(",").append(PriceUtil.price2str(field.AskPrice1))
        .append(",").append(field.AskVolume1)
        .append(",").append(PriceUtil.price2str(field.BidPrice2))
        .append(",").append(field.BidVolume2)
        .append(",").append(PriceUtil.price2str(field.AskPrice2))
        .append(",").append(field.AskVolume2)
        .append(",").append(PriceUtil.price2str(field.BidPrice3))
        .append(",").append(field.BidVolume3)
        .append(",").append(PriceUtil.price2str(field.AskPrice3))
        .append(",").append(field.AskVolume3)
        .append(",").append(PriceUtil.price2str(field.BidPrice4))
        .append(",").append(field.BidVolume4)
        .append(",").append(PriceUtil.price2str(field.AskPrice4))
        .append(",").append(field.AskVolume4)
        .append(",").append(PriceUtil.price2str(field.BidPrice5))
        .append(",").append(field.BidVolume5)
        .append(",").append(PriceUtil.price2str(field.AskPrice5))
        .append(",").append(field.AskVolume5)
        .append(",").append(PriceUtil.price2str(field.AveragePrice))
        .append(",").append(field.ActionDay)
        .append("\n");
        try {
            Writer writer = writerMap.get(writerId);
            if ( writer==null ){
                File file = new File(workdir,"lts."+writerId+".csv");
                boolean needHeader = !(file.exists() && file.length()>0);
                writer = new OutputStreamWriter(new FileOutputStream(file, true),"UTF-8");
                if ( needHeader )
                    writer.write(CSV_HEADER);
                writerMap.put(writerId, writer);
            }
            writer.write(line.toString());
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asyncConnectMdApi(boolean redundancy)
    {
        if ( closed )
            return;
        Thread t = new Thread(){
            @Override
            public void run(){
                if ( closed )
                    return;
                if ( reconnectDelay>0 ){
                    try {
                        Thread.sleep(reconnectDelay*1000);
                    } catch (InterruptedException e) {}
                }
                boolean needReconnectAgain = false;
                if ( !redundancy ){
                    try {
                        mdApi = new MdApi();
                        mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);
                        mdApi.setListener(new MyListener(false));
                        subscribleSecurities(mdApi);
                    } catch (Exception e) {
                        System.out.println("异步连接MdApi失败:"+e);
                        needReconnectAgain = true;
                    }
                }else{
                    try {
                        mdApiR = new MdApi();
                        mdApiR.SyncConnect(mdFrontUrl, brokerId, username, password);
                        mdApiR.setListener(new MyListener(true));
                        subscribleSecurities(mdApiR);
                    } catch (Exception e) {
                        System.out.println("异步连接MdApiR失败:"+e);
                        needReconnectAgain = true;
                    }
                }

                if ( needReconnectAgain )
                    asyncConnectMdApi(redundancy);
            }
        };
        t.setDaemon(true);
        t.start();
    }

    private void subscribleSecurities(MdApi mdApiToSubscribe) throws Exception
    {
        for(Exchangeable e: exchangeablesToImport){
            String exchangeName = e.exchange().name();
            if (e.exchange()==Exchange.SZSE)
                exchangeName = "SZE";
            mdApiToSubscribe.SubscribeMarketData(new String[]{e.id()}, exchangeName.toUpperCase());
        }
    }

    class MyListener implements MdApiListener{
        private boolean redundancy;
        private String prefix;

        MyListener(boolean redundancy){
            this.redundancy = redundancy;
            if ( !redundancy )
                prefix = "MdApi";
            else
                prefix = "MdApiR";
        }

        @Override
        public void OnFrontConnected() {
            System.out.println(prefix+" 已连接");
        }

        @Override
        public void OnFrontDisconnected(int nReason) {
            System.out.println(prefix+" 连接断开: "+nReason);
            asyncConnectMdApi(redundancy);
        }

        @Override
        public void OnHeartBeatWarning(int nTimeLapse) {
        }

        @Override
        public void OnRspError(CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
            System.out.println(prefix+" 错误回应: "+pRspInfo);
        }

        @Override
        public void OnRspUserLogin(CSecurityFtdcRspUserLoginField pRspUserLogin, CSecurityFtdcRspInfoField pRspInfo,
                int nRequestID, boolean bIsLast) {
            System.out.println(prefix+" 登录: "+pRspUserLogin+" 响应: "+pRspInfo);
        }

        @Override
        public void OnRspUserLogout(CSecurityFtdcUserLogoutField pUserLogout, CSecurityFtdcRspInfoField pRspInfo,
                int nRequestID, boolean bIsLast) {
            System.out.println(prefix+" 登出: "+pUserLogout+" 响应: "+pRspInfo);
        }

        @Override
        public void OnRspSubMarketData(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
                CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
            System.out.println(prefix+" 订阅行情: "+pSpecificInstrument+" 响应: "+pRspInfo);
        }

        @Override
        public void OnRspUnSubMarketData(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
                CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
            System.out.println(prefix+" 取消订阅行情: "+pSpecificInstrument+" 响应: "+pRspInfo);
        }

        @Override
        public void OnRtnDepthMarketData(CSecurityFtdcDepthMarketDataField pDepthMarketData) {
            fields.add(pDepthMarketData);
            ticketCount.incrementAndGet();
        }
    }
}
