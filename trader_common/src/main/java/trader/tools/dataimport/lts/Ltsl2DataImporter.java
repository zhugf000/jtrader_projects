package trader.tools.dataimport.lts;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import net.jlts.*;
import trader.common.exchangeable.*;
import trader.common.exchangeable.Exchange.MarketType;
import trader.common.util.*;
import trader.tools.dataimport.DataImporter;

public class Ltsl2DataImporter extends DataImporter implements MdL2ApiListener {

    public static final String DEVICE_ID = "8683310111770409";
    public static final String USERNAME = "zhugf000@gmai.com";
    public static final String PASSWORD = "123456";

    private static final String MD_CSV_HEADER =
            "TradingDay"
                    +",TimeStamp"
                    +",ExchangeID"
                    +",InstrumentID"
                    +",PreClosePrice"
                    +",OpenPrice"
                    +",ClosePrice"
                    +",IOPV"
                    +",YieldToMaturity"
                    +",HighPrice"
                    +",LowPrice"
                    +",LastPrice"
                    +",TradeCount"
                    +",TotalTradeVolume"
                    +",TotalTradeValue"
                    +",TotalBidVolume"
                    +",WeightedAvgBidPrice"
                    +",AltWeightedAvgBidPrice"
                    +",TotalOfferVolume"
                    +",WeightedAvgOfferPrice"
                    +",AltWeightedAvgOfferPrice"

            +",BidPriceLevel"
            +",OfferPriceLevel"
            +",BidPrice1"
            +",BidVolume1"
            +",BidCount1"
            +",BidPrice2"
            +",BidVolume2"
            +",BidCount2"
            +",BidPrice3"
            +",BidVolume3"
            +",BidCount3"
            +",BidPrice4"
            +",BidVolume4"
            +",BidCount4"
            +",BidPrice5"
            +",BidVolume5"
            +",BidCount5"
            +",BidPrice6"
            +",BidVolume6"
            +",BidCount6"
            +",BidPrice7"
            +",BidVolume7"
            +",BidCount7"
            +",BidPrice8"
            +",BidVolume8"
            +",BidCount8"
            +",BidPrice9"
            +",BidVolume9"
            +",BidCount9"
            +",BidPriceA"
            +",BidVolumeA"
            +",BidCountA"

            +",OfferPrice1"
            +",OfferVolume1"
            +",OfferCount1"
            +",OfferPrice2"
            +",OfferVolume2"
            +",OfferCount2"
            +",OfferPrice3"
            +",OfferVolume3"
            +",OfferCount3"
            +",OfferPrice4"
            +",OfferVolume4"
            +",OfferCount4"
            +",OfferPrice5"
            +",OfferVolume5"
            +",OfferCount5"
            +",OfferPrice6"
            +",OfferVolume6"
            +",OfferCount6"
            +",OfferPrice7"
            +",OfferVolume7"
            +",OfferCount7"
            +",OfferPrice8"
            +",OfferVolume8"
            +",OfferCount8"
            +",OfferPrice9"
            +",OfferVolume9"
            +",OfferCount9"
            +",OfferPriceA"
            +",OfferVolumeA"
            +",OfferCountA"
            +"\n"
            ;

    private static final String INDEX_CSV_HEADER =
            "TradingDay"
                    +",TimeStamp"
                    +",ExchangeID"
                    +",InstrumentID"
                    +",PreCloseIndex"
                    +",OpenIndex"
                    +",CloseIndex"
                    +",HighIndex"
                    +",LowIndex"
                    +",LastIndex"
                    +",TurnOver"
                    +",TotalVolume"
                    +"";

    private MdL2Api mdApi;
    private volatile boolean closed;
    private String mdFrontUrl;
    private String brokerId;
    private String username;
    private String password;
    private String workdir;
    private int reconnectDelay = 60;
    private LinkedBlockingQueue<CSecurityFtdcL2MarketDataField> marketDataFields = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<CSecurityFtdcL2IndexField> indexFields = new LinkedBlockingQueue<>();
    private Map<String, Writer> writerMap = new HashMap<>();
    private volatile String tradingDay = null;
    private List<Exchangeable> exchangeablesToImport;

    @Override
    public boolean canImportParallel(){
        return false;
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception
    {
        throw new RuntimeException("operation not supported");
    }

    @Override
    public void importData(List<Exchangeable> exchangeables)
            throws Exception
    {
        exchangeablesToImport = exchangeables;
        mdFrontUrl = props.getProperty("mdFrontUrl");
        brokerId = props.getProperty("brokerId");
        username = props.getProperty("username");
        password = props.getProperty("password");
        workdir = props.getProperty("tmpdir","tmp")+"/"+"ltsl2-"+tradingDay;

        //logon and save market data as csv at market time.
        LocalTime marketCloseTime = Exchange.SSE.getDefaultOpenCloseTime(MarketType.Day)[1];
        if ( DateUtil.getCurrentTime().toLocalTime().isBefore(marketCloseTime) )
        {

            mdApi = new MdL2Api();
            mdApi.setListener(this);
            mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);

            File dir = new File(workdir);
            dir.mkdirs();

            Thread thread = new Thread(()->{saveMarketDataThreadProc();});
            thread.setName("LTS data import save thread");
            thread.setDaemon(true);
            thread.start();

            Thread threadIndex = new Thread(()->{saveIndexThreadProc();});
            thread.setName("LTS index import save thread");
            thread.setDaemon(true);
            thread.start();

            LinkedList<Exchangeable> toSubscribe = new LinkedList<>();
            for(Exchangeable e: exchangeablesToImport){
                Exchangeable last = toSubscribe.peekLast();
                if ( (last!=null&&last.exchange()!=e.exchange())
                        || toSubscribe.size()>10 )
                {
                    subscribeSecurities(toSubscribe);
                    toSubscribe.clear();
                }
                toSubscribe.add(e);
            }
            if ( toSubscribe.size()>0){
                subscribeSecurities(toSubscribe);
            }
            System.out.println("MdApi 总订阅数: "+exchangeablesToImport.size());
            while(DateUtil.getCurrentTime().toLocalTime().isBefore(marketCloseTime) ){
                Thread.sleep(60*1000);
            }
            mdApi.Close();
        }
        //import and save market data as zip
        File dir = new File(workdir);
        File[] csvs = dir.listFiles();
        if ( csvs==null|| csvs.length==0 ){
            System.out.println("没有市场数据可归集");
        }
        for(File csv:csvs){
            String fparts[] = csv.getName().split("\\.");
            //ltsl2.tradingDay.exchange.instrument.csv
            if ( fparts.length!=5
                    || !fparts[0].equals("ltsl2")
                    || !fparts[4].equals("csv")
                    )
                continue;
            String tradingDay = fparts[1];
            String exchange = fparts[2];
            String instrument = fparts[3];
            StringBuilder ltsText = new StringBuilder(40960);
            CSVDataSet dataSet = CSVUtil.parse(csv);
            String header = dataSet.getLine();
            ltsText.append(header).append("\n");
            String lastLine = null;
            String line = null;
            int rowCount=0;
            while(dataSet.next()){
                line = dataSet.getLine();
                if ( line.equals(lastLine)){
                    continue;
                }
                ltsText.append(line).append("\n");
                lastLine = line;
                rowCount++;
            }
            exchangeableData.save(Security.fromString(exchange+"."+instrument), ExchangeableData.TICK_LTSL2, LocalDate.now(), ltsText.toString());
            csv.delete();
            System.out.println("导入 "+csv.getName()+" : "+rowCount);
        }
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_TICK};
    }

    private void saveMarketDataThreadProc()
    {
        CSecurityFtdcL2MarketDataField field = null;
        while(!closed){
            try {
                field = marketDataFields.poll(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {}
            if ( field==null )
                continue;
            tradingDay = field.TradingDay;
            appendMarketDataRow(field);
        }
    }

    private void saveIndexThreadProc()
    {
        CSecurityFtdcL2IndexField field = null;
        while(!closed){
            try {
                field = indexFields.poll(1000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {}
            if ( field==null )
                continue;
            tradingDay = field.TradingDay;
            appendIndexRow(field);
        }
    }

    private void appendMarketDataRow(CSecurityFtdcL2MarketDataField field)
    {
        String writerId = tradingDay+"."+field.ExchangeID+"."+field.InstrumentID;
        StringBuilder line = new StringBuilder(512);
        line
        .append(field.TradingDay)
        .append(",").append(field.TimeStamp)
        .append(",").append(field.ExchangeID)
        .append(",").append(field.InstrumentID)
        .append(",").append(PriceUtil.price2str(field.PreClosePrice))
        .append(",").append(PriceUtil.price2str(field.OpenPrice))
        .append(",").append(PriceUtil.price2str(field.ClosePrice))
        .append(",").append(PriceUtil.price2str(field.IOPV))
        .append(",").append(PriceUtil.price2str(field.YieldToMaturity))
        .append(",").append(PriceUtil.price2str(field.HighPrice))
        .append(",").append(PriceUtil.price2str(field.LowPrice))
        .append(",").append(PriceUtil.price2str(field.LastPrice))
        .append(",").append((field.TradeCount))
        .append(",").append(PriceUtil.price2str(field.TotalTradeVolume))
        .append(",").append(PriceUtil.price2str(field.TotalTradeValue))
        .append(",").append(PriceUtil.price2str(field.TotalBidVolume))
        .append(",").append(PriceUtil.price2str(field.WeightedAvgBidPrice))
        .append(",").append(PriceUtil.price2str(field.AltWeightedAvgBidPrice))
        .append(",").append(PriceUtil.price2str(field.TotalOfferVolume))
        .append(",").append(PriceUtil.price2str(field.WeightedAvgOfferPrice))
        .append(",").append(PriceUtil.price2str(field.AltWeightedAvgOfferPrice))

        .append(",").append((field.BidPriceLevel))
        .append(",").append((field.OfferPriceLevel))
        .append(",").append(PriceUtil.price2str(field.BidPrice1))
        .append(",").append(field.BidVolume1)
        .append(",").append(field.BidCount1)
        .append(",").append(PriceUtil.price2str(field.BidPrice2))
        .append(",").append(field.BidVolume2)
        .append(",").append(field.BidCount2)
        .append(",").append(PriceUtil.price2str(field.BidPrice3))
        .append(",").append(field.BidVolume3)
        .append(",").append(field.BidCount3)
        .append(",").append(PriceUtil.price2str(field.BidPrice4))
        .append(",").append(field.BidVolume4)
        .append(",").append(field.BidCount4)
        .append(",").append(PriceUtil.price2str(field.BidPrice5))
        .append(",").append(field.BidVolume5)
        .append(",").append(field.BidCount5)
        .append(",").append(PriceUtil.price2str(field.BidPrice6))
        .append(",").append(field.BidVolume6)
        .append(",").append(field.BidCount6)
        .append(",").append(PriceUtil.price2str(field.BidPrice7))
        .append(",").append(field.BidVolume7)
        .append(",").append(field.BidCount7)
        .append(",").append(PriceUtil.price2str(field.BidPrice8))
        .append(",").append(field.BidVolume8)
        .append(",").append(field.BidCount8)
        .append(",").append(PriceUtil.price2str(field.BidPrice9))
        .append(",").append(field.BidVolume9)
        .append(",").append(field.BidCount9)
        .append(",").append(PriceUtil.price2str(field.BidPriceA))
        .append(",").append(field.BidVolumeA)
        .append(",").append(field.BidCountA)
        .append(",").append(PriceUtil.price2str(field.OfferPrice1))
        .append(",").append(field.OfferVolume1)
        .append(",").append(field.OfferCount1)
        .append(",").append(PriceUtil.price2str(field.OfferPrice2))
        .append(",").append(field.OfferVolume2)
        .append(",").append(field.OfferCount2)
        .append(",").append(PriceUtil.price2str(field.OfferPrice3))
        .append(",").append(field.OfferVolume3)
        .append(",").append(field.OfferCount3)
        .append(",").append(PriceUtil.price2str(field.OfferPrice4))
        .append(",").append(field.OfferVolume4)
        .append(",").append(field.OfferCount4)
        .append(",").append(PriceUtil.price2str(field.OfferPrice5))
        .append(",").append(field.OfferVolume5)
        .append(",").append(field.OfferCount5)
        .append(",").append(PriceUtil.price2str(field.OfferPrice6))
        .append(",").append(field.OfferVolume6)
        .append(",").append(field.OfferCount6)
        .append(",").append(PriceUtil.price2str(field.OfferPrice7))
        .append(",").append(field.OfferVolume7)
        .append(",").append(field.OfferCount7)
        .append(",").append(PriceUtil.price2str(field.OfferPrice8))
        .append(",").append(field.OfferVolume8)
        .append(",").append(field.OfferCount8)
        .append(",").append(PriceUtil.price2str(field.OfferPrice9))
        .append(",").append(field.OfferVolume9)
        .append(",").append(field.OfferCount9)
        .append(",").append(PriceUtil.price2str(field.OfferPriceA))
        .append(",").append(field.OfferVolumeA)
        .append(",").append(field.OfferCountA)
        .append("\n");
        try {
            Writer writer = writerMap.get(writerId);
            if ( writer==null ){
                writer = new OutputStreamWriter(new FileOutputStream( new File(workdir,"ltsl2."+writerId+".csv")),"UTF-8");
                writer.write(MD_CSV_HEADER);
                writerMap.put(writerId, writer);
            }
            writer.write(line.toString());
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void appendIndexRow(CSecurityFtdcL2IndexField field)
    {
        String writerId = tradingDay+"."+field.ExchangeID+"."+field.InstrumentID;
        StringBuilder line = new StringBuilder(512);
        line
        .append(field.TradingDay)
        .append(",").append(field.TimeStamp)
        .append(",").append(field.ExchangeID)
        .append(",").append(field.InstrumentID)
        .append(",").append(PriceUtil.price2str(field.PreCloseIndex))
        .append(",").append(PriceUtil.price2str(field.OpenIndex))
        .append(",").append(PriceUtil.price2str(field.CloseIndex))
        .append(",").append(PriceUtil.price2str(field.HighIndex))
        .append(",").append(PriceUtil.price2str(field.LowIndex))
        .append(",").append(PriceUtil.price2str(field.LastIndex))
        .append(",").append(PriceUtil.price2str(field.TurnOver))
        .append(",").append(PriceUtil.price2str(field.TotalVolume))
        .append("\n");
        try {
            Writer writer = writerMap.get(writerId);
            if ( writer==null ){
                writer = new OutputStreamWriter(new FileOutputStream( new File(workdir,"ltsl2."+writerId+".csv")),"UTF-8");
                writer.write(INDEX_CSV_HEADER);
                writerMap.put(writerId, writer);
            }
            writer.write(line.toString());
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void subscribeSecurities(List<Exchangeable> toSubscribe) throws Exception
    {
        String exchangeName = toSubscribe.get(0).exchange().name();
        String[] ids = new String[toSubscribe.size()];
        for(int i=0;i<ids.length;i++)
            ids[i] = toSubscribe.get(i).id();
        mdApi.SubscribeL2MarketData(ids, exchangeName);
        toSubscribe.clear();
    }


    private void asyncConnectMdApi()
    {
        if ( closed )
            return;
        Thread t = new Thread(){
            @Override
            public void run(){
                if ( closed )
                    return;
                if ( reconnectDelay>0 ){
                    try {
                        Thread.sleep(reconnectDelay*1000);
                    } catch (InterruptedException e) {}
                }
                boolean needReconnectAgain = false;
                try {
                    mdApi = new MdL2Api();
                    mdApi.SyncConnect(mdFrontUrl, brokerId, username, password);
                    mdApi.setListener(Ltsl2DataImporter.this);
                    subscribleSecurities(mdApi);
                } catch (Exception e) {
                    System.out.println("异步连接MdL2Api失败:"+e);
                    needReconnectAgain = true;
                }
                if ( needReconnectAgain )
                    asyncConnectMdApi();
            }
        };
        t.setDaemon(true);
        t.start();
    }

    private void subscribleSecurities(MdL2Api mdApiToSubscribe) throws Exception
    {
        for(Exchangeable e: exchangeablesToImport){
            String exchangeName = e.exchange().name();
            if (e.exchange()==Exchange.SZSE)
                exchangeName = "SZE";
            if ( e.getType()==ExchangeableType.INDEX )
                mdApiToSubscribe.SubscribeL2Index(new String[]{e.id()}, exchangeName.toUpperCase());
            else
                mdApiToSubscribe.SubscribeL2MarketData(new String[]{e.id()}, exchangeName.toUpperCase());
        }
    }

    @Override
    public void OnFrontConnected() {
        System.out.println("MdApi 已连接");
    }

    @Override
    public void OnFrontDisconnected(int nReason) {
        System.out.println("MdL2Api 连接断开: "+nReason);
        asyncConnectMdApi();
    }

    @Override
    public void OnHeartBeatWarning(int nTimeLapse) {
    }

    @Override
    public void OnRspError(CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 错误回应: "+pRspInfo);
    }

    @Override
    public void OnRspUserLogout(CSecurityFtdcUserLogoutField pUserLogout, CSecurityFtdcRspInfoField pRspInfo,
            int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 登出: "+pUserLogout+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRspUserLogin(CSecurityFtdcUserLoginField pRspUserLogin, CSecurityFtdcRspInfoField pRspInfo,
            int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 登录: "+pRspUserLogin+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRspSubL2MarketData(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
            CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 订阅行情: "+pSpecificInstrument+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRspUnSubL2MarketData(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
            CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 取消订阅行情: "+pSpecificInstrument+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRspSubL2Index(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
            CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 订阅指数: "+pSpecificInstrument+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRspUnSubL2Index(CSecurityFtdcSpecificInstrumentField pSpecificInstrument,
            CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
        System.out.println("MdL2Api 取消订阅指数: "+pSpecificInstrument+" 响应: "+pRspInfo);
    }

    @Override
    public void OnRtnL2MarketData(CSecurityFtdcL2MarketDataField pL2MarketData) {
        marketDataFields.add(pL2MarketData);
    }

    @Override
    public void OnRtnL2Index(CSecurityFtdcL2IndexField pL2Index) {
        indexFields.add(pL2Index);
    }

    @Override
    public void OnRspSubL2OrderAndTrade(CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
    }

    @Override
    public void OnRspUnSubL2OrderAndTrade(CSecurityFtdcRspInfoField pRspInfo, int nRequestID, boolean bIsLast) {
    }

    @Override
    public void OnRtnL2Order(CSecurityFtdcL2OrderField pL2Order) {
    }

    @Override
    public void OnRtnL2Trade(CSecurityFtdcL2TradeField pL2Trade) {
    }

    @Override
    public void OnNtfCheckOrderList(String InstrumentID, String FunctionCode) {
    }
}
