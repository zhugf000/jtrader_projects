package trader.tools.dataimport.tencent;

import java.io.StringWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.ExchangeableData;
import trader.common.exchangeable.MarketDayUtil;
import trader.common.util.CSVWriter;
import trader.common.util.DateUtil;
import trader.common.util.NetUtil;
import trader.tools.dataimport.DataImporter;

/**
 *
 * http://ifzq.gtimg.cn/appstock/app/kline/mkline?param=sh600036,m5,,640&_var=m5_today&r=0.8434137891688348
 */
public class TencentDataImporter extends DataImporter {

    @Override
    protected String[] getAllDataTypes() {
        return new String[] {DATATYPE_MINUTE5, DATATYPE_MINUTE15, DATATYPE_MINUTE30};
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {

        if ( needsDataType(DATATYPE_MINUTE5) ) {
            String url = tourl(exchangeable, DATATYPE_MINUTE5);
            saveMinuteData(exchangeable, url, DATATYPE_MINUTE5);
        }
        if ( needsDataType(DATATYPE_MINUTE15) ) {
            String url = tourl(exchangeable, DATATYPE_MINUTE15);
            saveMinuteData(exchangeable, url, DATATYPE_MINUTE15);
        }
        if ( needsDataType(DATATYPE_MINUTE30) ) {
            String url = tourl(exchangeable, DATATYPE_MINUTE30);
            saveMinuteData(exchangeable, url, DATATYPE_MINUTE30);
        }
    }

    private String tourl(Exchangeable exchangeable, String dataType) {
        String ktype=datatype2ktype(dataType);
        String url = "http://ifzq.gtimg.cn/appstock/app/kline/mkline?param="+security2hscode(exchangeable)+","+ktype+",,640&_var=m5_today&r="+(new Random()).nextDouble();
        return url;
    }

    private String datatype2ktype(String dataType) {
        return "m"+datetype2min(dataType);
    }

    private int datetype2min(String dataType) {
        switch(dataType) {
        case DATATYPE_MINUTE5:
            return 5;
        case DATATYPE_MINUTE15:
            return 15;
        case DATATYPE_MINUTE30:
            return 30;
        default:
            throw new RuntimeException("Unsupported data type: "+dataType);
        }
    }

    /**
     * 加载URL数据,并用script engine解析(java script格式).
     * <BR>返回数据格式: ["201803211455","31.16","31.21","31.21","31.13","13772.00"],["201803211500","31.20","31.26","31.32","31.20","12015.00"]
     */
    private void saveMinuteData(Exchangeable security, String url, String dataType) throws Exception {
        LocalDate currDate = beginDate;
        LocalDate lastDate = getEndDate(security, true);
        if ( currDate ==null ){
            currDate = lastDate;
        }
        out.print(DateUtil.getCurrentTimeAsString()+" Import "+security.getType()+" "+security.toPrintableString()+" tencent "+datatype2ktype(dataType)+" data ("+currDate+"-"+lastDate+"): ");

        String jsonText = null;
        try{
            jsonText = NetUtil.<String>doAction(()->{ return NetUtil.readHttpAsText(url, NetUtil.gbkCharset);});
        }catch(Exception e){
            out.print("E");
            return;
        }
        JSONObject json = new JSONObject(jsonText.substring("m5_today=".length()));
        if ( !json.has("data") ) {
            out.print("N");
            return;
        }
        JSONObject data = json.getJSONObject("data");
        String code = data.keys().next();
        JSONObject data0 = data.getJSONObject(code);
        JSONArray kdatas = data0.getJSONArray(datatype2ktype(dataType));

        //过滤和保存
        List<LocalDate> suspensionDays = exchangeableData.getSuspensionDays(security);
        do{
            LocalDate fileDate = currDate;
            currDate = MarketDayUtil.nextMarketDay(security.exchange(), currDate);
            if ( !MarketDayUtil.isMarketDay(security.exchange(), fileDate)) {
                continue;
            }
            if ( !overwrite && suspensionDays.contains(fileDate)){
                continue;
            }
            String datePrefix = DateUtil.date2str(fileDate);
            StringWriter text = new StringWriter();
            CSVWriter csvWriter = new CSVWriter(ExchangeableData.FUTURE_MIN_COLUMNS);
            boolean hasData = false;
            for(int i=0;i<kdatas.length();i++) {
                JSONArray kdata = kdatas.getJSONArray(i);
                String kendTime = kdata.getString(0);
                if ( !kendTime.startsWith(datePrefix) ) {
                    continue;
                }
                LocalDateTime kendTime0 = DateUtil.str2localdatetime(kendTime);
                LocalDateTime kbeginTime0 = kendTime0.minusMinutes(datetype2min(dataType));
                csvWriter.append(
                        DateUtil.date2str(kbeginTime0)
                        ,DateUtil.date2str(kendTime0)
                        , kdata.getString(1) //Open
                        , kdata.getString(3) //High
                        , kdata.getString(2) //Close
                        , kdata.getString(4) //Low
                        , kdata.getString(5) //Volume
                        );
                hasData = true;
            }

            if ( hasData ) {
                exchangeableData.save(security, datatype2classification(dataType) , fileDate, csvWriter.toString());
                out.print(".");
            }
        }while(lastDate.compareTo(currDate)>=0);
        out.println();
        return;
    }

}
