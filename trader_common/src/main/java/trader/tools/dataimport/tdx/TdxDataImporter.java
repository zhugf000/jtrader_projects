package trader.tools.dataimport.tdx;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import trader.common.exchangeable.Exchange;
import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.ExchangeableData;
import trader.common.util.PriceUtil;
import trader.common.util.StringUtil;
import trader.tools.dataimport.DataImporter;

public class TdxDataImporter extends DataImporter {

    private String tdxDir = "";

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_DAY};
    }

    @Override
    public void preImportData() throws Exception
    {
        tdxDir = props.getProperty("tdxDir");
    }

    @Override
    public void importData(Exchangeable exchangeable)
            throws Exception
    {
        importSecurityQuotationData( exchangeable);
    }

    private void importSecurityQuotationData(Exchangeable securityId) throws IOException
    {
        if ( needsDataType(DATATYPE_DAY)) {
            importSecurityDayData(securityId);
        }
    }

    private void importSecurityDayData(Exchangeable securityId)
            throws IOException
    {
        Exchange exchange = securityId.exchange();
        String prefix = null;
        File dataDir = null;
        if ( exchange==Exchange.SSE){
            dataDir = new File(tdxDir,"Vipdoc/sh/lday");
            prefix = "sh";
        }else if ( exchange==Exchange.SZSE){
            prefix = "sz";
            dataDir = new File(tdxDir,"Vipdoc/sz/lday");
        }

        File dayFile = new File(dataDir,prefix+securityId.id()+".day");
        System.out.print("导入证券 "+securityId+" 日数据 ... ");
        if ( !dayFile.exists() ){
            System.out.println(" 不存在.");
            return;
        }
        String beginDate = null;
        String endDate = null;
        int dayCount = 0;

        //TODO 加入合并功能
        StringBuilder csvText = new StringBuilder(256);
        csvText.append(StringUtil.array2str(ExchangeableData.DAY_COLUMNS,",")+"\n");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[4096];
        FileInputStream fis = new FileInputStream(dayFile);
        int buflen = -1;
        while ( (buflen=fis.read(buf))>0){
            baos.write(buf, 0, buflen);
        }
        fis.close();
        ByteBuffer buffer = ByteBuffer.allocate(baos.size());
        buffer.put(baos.toByteArray());
        buffer.clear();
        assert( dayFile.length() == baos.size() );
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        int count = (int)dayFile.length()/32;
        for(int i=0;i<count;i++){
            int date = buffer.getInt();
            double open = buffer.getInt();
            double high = buffer.getInt();
            double low = buffer.getInt();
            double close = buffer.getInt();
            float amount = buffer.getFloat();
            int volume = buffer.getInt();
            int reserve = buffer.getInt();

            open = open/100;
            close = close/100;
            high = high/100;
            low = low/100;
            csvText.append(date)
            .append(",").append(PriceUtil.price2str(open))
            .append(",").append(PriceUtil.price2str(high))
            .append(",").append(PriceUtil.price2str(low))
            .append(",").append(PriceUtil.price2str(close))
            .append(",").append((volume))
            .append(",").append(PriceUtil.price2str(amount))
            .append("\n");
            if ( beginDate==null ) {
                beginDate = ""+date;
            }
            endDate = ""+date;
            dayCount++;
        }

        exchangeableData.save(securityId, ExchangeableData.DAY, null, csvText.toString());
        System.out.println(beginDate+"-"+endDate+", 共: "+dayCount);
    }
}
