package trader.tools.dataimport.finance;

import java.io.BufferedReader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import trader.common.exchangeable.*;
import trader.common.util.*;
import trader.tools.dataimport.DataImporter;

/**
 * SSE上海证券交易所 JSON 查询:
 * <pre>
 * query.sse.com.cn

GET /commonQuery.do?jsonCallBack=jsonpCallback3883&isPagination=false&sqlId=COMMON_SSE_ZQPZ_GP_GPLB_C&productid=600036&_=1431770811251
GET /commonQuery.do?jsonCallBack=jsonpCallback3883&isPagination=true&sqlId=COMMON_SSE_ZQPZ_GG_LYFP_AGFH_L&productid=600036&pageHelp.pageSize=20&_=1431770811251
GET /commonQuery.do?jsonCallBack=jsonpCallback3883&isPagination=true&sqlId=COMMON_SSE_ZQPZ_GG_LYFP_BGFH_L&productid=600036&pageHelp.pageSize=20&_=1431770811251
GET /commonQuery.do?jsonCallBack=jsonpCallback8577&isPagination=true&sqlId=COMMON_SSE_ZQPZ_GG_LYFP_AGSG_L&productid=600036&pageHelp.pageSize=20&_=1431770811251
GET /commonQuery.do?jsonCallBack=jsonpCallback6491&isPagination=true&sqlId=COMMON_SSE_ZQPZ_GG_LYFP_BGSG_L&productid=600036&pageHelp.pageSize=20&_=1431770811251
 * </pre>
 */
public class StockFinaceDataImporter extends DataImporter {
    private final static String PROVIDER_TENCENT = "tencent";
    private final static String PROVIDER_SINA = "sina";
    private String provider;

    @Override
    public void preImportData() throws Exception
    {
        provider = props.getProperty("provider", PROVIDER_SINA);
    }

    @Override
    public void importData(List<Exchangeable> exchangeables) throws Exception {
        //导入股票沪深代码
        updateStockListFiles();
        //实际导入股票财务数据
        for(Exchangeable exchangeable:exchangeables){
            if ( exchangeable.getType()!=ExchangeableType.STOCK ){
                System.out.println("Ignore stock finance for: "+exchangeable.toPrintableString());
                continue;
            }
            importStockFinanceData(exchangeable);
            importStockDividendData(exchangeable);
        }
    }

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {
        importStockFinanceData(exchangeable);
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_FINANCE};
    }

    /**
     * 更新 sse/szse的security_list.csv文件
     * @throws Exception
     */
    private void updateStockListFiles() throws Exception
    {
        LocalDate lastMarketDay = MarketDayUtil.lastMarketDay(Exchange.SSE, true);

        String url= String.format("http://file.tushare.org/tsdata/h/%04d%02d/%04d%02d%02d.csv",
                    lastMarketDay.getYear(), lastMarketDay.getMonth().getValue()
                    ,lastMarketDay.getYear(), lastMarketDay.getMonth().getValue(), lastMarketDay.getDayOfMonth());
        System.out.print("Update security lists from "+url+" ... ");
        String csvText = NetUtil.<String>doAction(()->{return NetUtil.readHttpAsText(url, StringUtil.UTF8); });
        CSVDataSet csv = CSVUtil.parse(csvText, ',', true);
        System.out.println(" done ");

        List<Exchangeable> sseList = this.exchangeableData.loadExchangeabeIds(Exchange.SSE);
        List<Exchangeable> szseList = this.exchangeableData.loadExchangeabeIds(Exchange.SZSE);

        List<Exchangeable> newSecs = new ArrayList<>();
        while(csv.next()) {
            String code = csv.get("code");
            String name = csv.get("name");

            Exchange exchange = ExchangeableUtil.detectAShareStockExchange(code);
            Exchangeable security = Exchangeable.fromString(exchange.name(), code, name);
            if ( exchange==Exchange.SSE) {
                if ( !sseList.contains(security) ) {
                    sseList.add(security);
                    newSecs.add(security);
                }
            }else if ( exchange==Exchange.SZSE ) {
                if ( !szseList.contains(security) ) {
                    szseList.add(security);
                    newSecs.add(security);
                }
            }
        }
        if ( newSecs.size()>0 ) {
            exchangeableData.saveExchangeableIds(Exchange.SSE, sseList);
            exchangeableData.saveExchangeableIds(Exchange.SZSE, szseList);
            System.out.println("Total securities added "+newSecs.size()+" : "+newSecs);
        }
    }

    void importStockFinanceData(Exchangeable exchangeable)
            throws Exception
    {
        System.out.print("Fetching finance data from "+provider+" for "+exchangeable.toPrintableString()+" ... ");
        Map<String,String> financeData = null;
        switch(provider){
        case PROVIDER_TENCENT:
            financeData = importFromTencent(exchangeable);
            break;
        case PROVIDER_SINA:
            financeData = importFromSina(exchangeable);
            break;
        default:
            throw new Exception("Unknown stock finance data provider: "+provider);
        }
        CSVWriter csvWriter = new CSVWriter("Key","Value");
        for(String key:financeData.keySet()){
            csvWriter.append(key,financeData.get(key));
        }
        exchangeableData.saveMisc(exchangeable, ExchangeableData.MISC_STOCK_FINANCE, csvWriter.toString());
        System.out.println("done");
    }

    void importStockDividendData(Exchangeable exchangeable) throws Exception
    {
        System.out.print("Fetching dividend data from "+provider+" for "+exchangeable.toPrintableString()+" ... ");
        importDividendFromSina(exchangeable);
        System.out.println("done");
    }

    void importDividendFromSina(Exchangeable exchangeable) throws Exception
    {
        String url = "http://vip.stock.finance.sina.com.cn/corp/go.php/vISSUE_ShareBonus/stockid/"+exchangeable.id()+".phtml";
        Document doc = NetUtil.<Document>doAction(()->{return Jsoup.connect(url).get();});
        Elements tables = doc.select("table#sharebonus_1");
        if ( !tables.isEmpty() ){
            Element table = tables.first();
            Element tbody = table.getElementsByTag("tbody").first();
            if ( tbody!=null && tbody.html().indexOf("没有数据")<0 ){
                CSVWriter writer = new CSVWriter("announceDate","presentedShares","convertedShares","dividend","exDividendDay","stockRightsRegistrationDay","bonusShareDay");
                for(Element tr:tbody.select("tr")){
                    Elements tdList = tr.select("td");
                    String announceDate = filterSinaDate(tdList.get(0).text());
                    String presentedShares = tdList.get(1).text().trim();
                    String convertedShares = tdList.get(2).text().trim();
                    String dividend = tdList.get(3).text().trim();
                    String progress = tdList.get(4).text().trim();
                    if ( !"实施".equalsIgnoreCase(progress)) {
                        continue;
                    }
                    String exDividendDay = filterSinaDate(tdList.get(5).text());
                    String stockRightsRegistrationDay = filterSinaDate(tdList.get(6).text());
                    String bonusShareDay = filterSinaDate(tdList.get(7).text());
                    writer.append(announceDate
                            ,presentedShares
                            ,convertedShares
                            ,dividend
                            ,exDividendDay
                            ,stockRightsRegistrationDay
                            ,bonusShareDay
                            );
                }
                exchangeableData.saveMisc(exchangeable, ExchangeableData.MISC_STOCK_DIVIDEND_SINA, writer.toString());
            }
        }
        tables = doc.select("table#sharebonus_2");
        if ( !tables.isEmpty() ){
            Element table = tables.first();
            Element tbody = table.getElementsByTag("tbody").first();
            if ( tbody!=null && tbody.html().indexOf("没有数据")<0 ){
                CSVWriter writer = new CSVWriter("announceDate"
                        ,"shareBaseDay"
                        ,"registrationDay"
                        ,"exDividendDay"
                        ,"onMarketDay"
                        ,"allotmentRate"
                        ,"allotmentPrice"
                        ,"allotmentCount"
                        ,"actualAllotmentRate"
                        ,"totalSharesBeforeAllotment"
                        );
                for(Element tr:tbody.select("tr")){
                    Elements tdList = tr.select("td");
                    String announceDate = filterSinaDate(tdList.get(0).text());
                    Map<String,String> detailMap = fetchSinaStockAllotmentDetail(exchangeable, announceDate);
                    writer.append(announceDate
                            ,detailMap.get("shareBaseDay")
                            ,detailMap.get("registrationDay")
                            ,detailMap.get("exDividendDay")
                            ,detailMap.get("onMarketDay")
                            ,detailMap.get("allotmentRate")
                            ,detailMap.get("allotmentPrice")
                            ,detailMap.get("allotmentCount")
                            ,detailMap.get("actualAllotmentRate")
                            ,detailMap.get("totalSharesBeforeAllotment")
                            );
                }
                exchangeableData.saveMisc(exchangeable, ExchangeableData.MISC_STOCK_ALLOTMENT_SINA, writer.toString());
            }
        }
    }

    private Map<String,String> fetchSinaStockAllotmentDetail(Exchangeable exchangeable, String date) throws Exception
    {
        Map<String,String> result = new HashMap<>();
        String url = "http://money.finance.sina.com.cn/corp/view/vISSUE_ShareBonusDetail.php?stockid="+exchangeable.id()+"&type=2&end_date="+date;
        Document doc = NetUtil.<Document>doAction(()->{return Jsoup.connect(url).get();});
        Elements tables = doc.select("table#sharebonusdetail");
        if ( !tables.isEmpty() ){
            Element table = tables.first();
            Element tbody = table.getElementsByTag("tbody").first();
            if ( tbody!=null ){
                for(Element tr:tbody.select("tr")){
                    Elements tdList = tr.select("td");
                    String key = tdList.get(0).text().trim();
                    String value = tdList.get(1).text().trim();
                    switch(key){
                    case "股本基准日":
                        result.put("shareBaseDay",value);
                        break;
                    case "登记日":
                        result.put("registrationDay",value);
                        break;
                    case "除息日":
                        result.put("exDividendDay",value);
                        break;
                    case "上市日":
                        result.put("onMarketDay",value);
                        break;
                    case "配股比例（10配）":
                        result.put("allotmentRate",value);
                        break;
                    case "配股价":
                        result.put("allotmentPrice",value);
                        break;
                    case "实际配股数":
                        result.put("allotmentCount",value);
                        break;
                    case "实际配股比例":
                        result.put("actualAllotmentRate",value);
                        break;
                    case "配股前总股本":
                        result.put("totalSharesBeforeAllotment",value);
                        break;
                    }
                }
            }
        }
        return result;
    }

    private static String filterSinaDate(String sinaDate){
        sinaDate = sinaDate.trim();
        if ( sinaDate.length()>=10) {
            return sinaDate;
        }
        return "";
    }

    Map<String,String> importFromSina(Exchangeable exchangeable)
            throws Exception
    {
        LinkedHashMap<String,String> result = new LinkedHashMap<>();
        String []basic = parseSinaBasic(exchangeable);
        result.put("TotalShares",basic[0]);
        result.put("MarketShares",basic[1]);
        result.put("NetValuePerShare",basic[2]);
        result.put("FourQuartersEarnPerShare",basic[3]);
        result.put("LastYearEarnPerShare",basic[4]);
        result.put("FourQuartersProfit",basic[5]);
        result.put("LastYearProfit",basic[6]);
        result.put("CorrHKStock",basic[7]);
        String[] blocks = parseSinaBlocks(exchangeable);
        result.put("IndustryBlock",blocks[0]);
        if ( blocks[1]!=null ) {
            result.put("ConceptBlocks",blocks[1]);
        }
        return result;
    }

    private String[] parseSinaBasic(Exchangeable exchangeable)
            throws Exception
    {
        String result[] = new String[8];
        String url = "http://finance.sina.com.cn/realstock/company/"+security2hscode(exchangeable)+"/nc.shtml";
        String text = NetUtil.<String>doAction(()->{ return NetUtil.readHttpAsText(url, Charset.forName("gbk"));});
        BufferedReader reader = new BufferedReader(new StringReader(text));
        String line=null;
        while( (line=reader.readLine())!=null ){
            if ( line.indexOf("var totalcapital")>=0 ){
                result[0] = parseJavaScriptValue(line,"totalcapital");
            }else if ( line.indexOf("var currcapital")>=0){
                result[1] = parseJavaScriptValue(line,"currcapital");
            }else if ( line.indexOf("var mgjzc")>=0){
                result[2] = parseJavaScriptValue(line,"var mgjzc");
            }else if ( line.indexOf("var fourQ_mgsy")>=0){
                result[3] = parseJavaScriptValue(line,"fourQ_mgsy");
            }else if ( line.indexOf("var lastyear_mgsy")>=0){
                result[4] = parseJavaScriptValue(line,"lastyear_mgsy");
            }else if ( line.indexOf("var profit_four")>=0){
                result[5] = parseJavaScriptValue(line,"profit_four");
            }else if ( line.indexOf("var profit")>=0){
                result[6] = parseJavaScriptValue(line,"profit");
            }else if ( line.indexOf("var corr_hkstock")>=0){
                result[7] = parseJavaScriptValue(line,"corr_hkstock");
            }
        }
        reader.close();
        return result;
    }

    private String parseJavaScriptValue(String line, String jsVar)
    {
        int lineEnd = line.indexOf(";");
        if ( lineEnd>0 ){
            line = line.substring(0,lineEnd);
        }
        if ( line.indexOf(jsVar)>=0 ){
            line = line.substring(line.indexOf(jsVar)+jsVar.length()).trim();
        }
        if ( line.startsWith("=")) {
            line = line.substring(1);
        }
        line = line.trim();
        if ( line.startsWith("'") && line.endsWith("'")){
            line = line.substring(1);
            line = line.substring(0,line.length()-1);
        }
        line = line.trim();
        return line;
    }

    /**
     *
     * @param exchangeable
     * @return
     * @throws Exception
     */
    private String[] parseSinaBlocks(Exchangeable exchangeable)
            throws Exception
    {
        String industryBlock = null;
        String conceptBlocks = null;
        String url="http://vip.stock.finance.sina.com.cn/corp/go.php/vCI_CorpOtherInfo/stockid/"+exchangeable.id()+"/menu_num/2.phtml";

        Document doc = NetUtil.<Document>doAction(()->{return Jsoup.connect(url).get();});
        Elements tables = doc.select("table.comInfo1");
        {
            Element table = tables.first();
            Element tr = table.select("tr").first();
            if ( tr!=null) {
                tr = tr.nextElementSibling();
            }
            if (tr!=null) {
                tr = tr.nextElementSibling();
            }
            Element td = tr.select("td").first();
            if ( td!=null) {
                industryBlock = td.text();
            }
        }
        {
            List<String> blocks = new LinkedList<>();
            Element table = tables.last();
            Element tr = table.select("tr").first();
            if ( tr!=null) {
                tr = tr.nextElementSibling();
            }
            if (tr!=null) {
                tr = tr.nextElementSibling();
            }
            while(tr!=null){
                String blk = tr.select("td").first().text();
                if ( blk.indexOf("没有")>=0 ){
                    break;
                }
                blocks.add(blk);
                tr = tr.nextElementSibling();
            }
            conceptBlocks = StringUtil.collection2str(blocks, "|");
        }

        return new String[]{industryBlock,conceptBlocks};
    }

    private Long parseSinaTotalShares(Exchangeable exchangeable)
            throws Exception
    {
        String url="http://vip.stock.finance.sina.com.cn/corp/go.php/vCI_StockStructure/stockid/"+exchangeable.id()+".phtml";
        Document doc = NetUtil.<Document>doAction(()->{return Jsoup.connect(url).get();});
        Element table = doc.select("table#StockStructureNewTable0").first();
        if (table==null){
            return null;
        }
        Elements totalTDs = table.select("td");
        Element totalShareTD=null;
        for(Element td:totalTDs){
            if ( td.hasText() && td.text().indexOf("总股本")>=0){
                totalShareTD = td;
                break;
            }
        }
        Element totalShare = totalShareTD.nextElementSibling();
        String text = totalShare.text().trim();
        long unit=1;
        if (text.endsWith("万股")){
            text = text.substring(0, text.length()-2);
            unit=10000;
        }
        if ( text.indexOf(",")>0){
            NumberFormat ukFormat = NumberFormat.getNumberInstance(Locale.UK);
            return (long)(ukFormat.parse(text).doubleValue()*unit);
        }
        return (long)(Double.parseDouble(text)*unit);
    }

    Map<String,String> importFromTencent(Exchangeable exchangeable)
            throws Exception
    {
        LinkedHashMap<String,String> result = new LinkedHashMap<>();
        String url="http://stock.finance.qq.com/corp1/stk_struct.php?zqdm="+exchangeable.id();
        Document doc = Jsoup.connect(url).get();
        Long totalShares = parseTencentTotalShares(doc);
        if ( totalShares!=null ){
            result.put("totalShares", totalShares.toString());
        }
        return result;
    }

    private static Long parseTencentTotalShares(Document doc)
            throws Exception
    {
        Elements elems = doc.select("table");
        Element table = null;
        for(Element elem: elems){
            if ( "list list_d".equalsIgnoreCase(elem.className()) ){
                table = elem;
                break;
            }
        }
        if (table==null){
            return null;
        }
        Element totalShareHead = table.select("th.fs14").first();
        if ( totalShareHead==null || totalShareHead.toString().indexOf("总股本")<0 ){
            return null;
        }
        Element totalShare = totalShareHead.nextElementSibling();
        String text = totalShare.text().trim();
        if ( text.indexOf(",")>0){
            NumberFormat ukFormat = NumberFormat.getNumberInstance(Locale.UK);
            return (long)(ukFormat.parse(text).doubleValue()*10000);
        }
        return (long)(Double.parseDouble(text)*10000);
    }
}
