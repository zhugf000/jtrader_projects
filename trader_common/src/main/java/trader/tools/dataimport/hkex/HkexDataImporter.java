package trader.tools.dataimport.hkex;

import trader.common.exchangeable.Exchange;
import trader.common.exchangeable.Exchangeable;
import trader.tools.dataimport.DataImporter;

/**
 * 香港 沪港通/深港通 数据导入
 */
public class HkexDataImporter extends DataImporter {

    @Override
    public void importData(Exchangeable exchangeable) throws Exception {
        if ( exchangeable.exchange()!=Exchange.HKEX ) {
            throw new Exception("只支持港股通/深股通");
        }
    }

    @Override
    protected String[] getAllDataTypes() {
        return new String[]{DATATYPE_DAY};
    }

}
