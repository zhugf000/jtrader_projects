package trader.tools.dataimport;

import java.io.File;
import java.io.InputStream;
import java.time.*;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

import trader.common.exchangeable.*;
import trader.common.util.*;
import trader.tools.common.CommandOptions;
import trader.tools.dataimport.ctp.CtpDataImporter;
import trader.tools.dataimport.finance.StockFinaceDataImporter;
import trader.tools.dataimport.hkex.HkexDataImporter;
import trader.tools.dataimport.lts.LtsDataImporter;
import trader.tools.dataimport.netease.NeteaseDataImporter;
import trader.tools.dataimport.sina.SinaDataImporter;
import trader.tools.dataimport.tdx.TdxDataImporter;
import trader.tools.dataimport.tencent.TencentDataImporter;
import trader.tools.dataimport.xquant.XQuantDataImporter;
import trader.tools.dataimport.yahoo.YahooDataImporter;

public class DataImportTool {
    private static final ZoneId BJTZ_ID = TimeZone.getTimeZone("Asia/Shanghai").toZoneId();

    private static String rootDir;
    private static String source;
    private static String[] securityIds;
    private static String[] dataTypes;
    private static LocalDate beginDate;
    private static LocalDate endDate;
    private static LocalTime at;
    private static Properties props = new Properties();
    private static boolean after;
    private static boolean checkTradingDay;
    private static int thread=1;
    private static boolean useNetworkBejingTime;
    private static boolean archive;

    public static void main(String[] args) throws Throwable
    {
        if (args.length == 0) {
            usage();
            return;
        }
        parseArgs(args);
        if ( archive ){
            (new ExchangeableData(new File(rootDir), false)).archive(new ExchangeableDataArchiveListener(){
                @Override
                public void onArchiveBegin(Exchangeable e)
                {
                    System.out.print("Archive "+e+" ... ");
                }

                @Override
                public void onArchiveEnd(Exchangeable e, int archivedFileCount)
                {
                    System.out.println(" DONE: "+archivedFileCount);
                }

                @Override
                public void onArchiveBegin(String subDir)
                {
                    System.out.print("Archive "+subDir+" ... ");
                }

                @Override
                public void onArchiveEnd(String subDir, int archivedFileCount)
                {
                    System.out.println(" DONE: "+archivedFileCount);
                }
            });
            return;
        }
        //启动北京时间
        if ( useNetworkBejingTime ){
            NetworkBeijingTime.startUpdateThread(null);
            while( NetworkBeijingTime.getLastUpdateTime() ==0 ){
                Thread.sleep(100);
            }
            DateUtil.setTimeSource(()->{ return ZonedDateTime.ofInstant( Instant.ofEpochMilli(NetworkBeijingTime.getTime()),BJTZ_ID).toLocalDateTime(); });
        }else{
            DateUtil.setTimeSource(()->{ return LocalDateTime.now();});
        }
        if ( checkTradingDay ){
            LocalDate lastTradingDay = MarketDayUtil.lastMarketDay(null, false);
            LocalDate today = LocalDate.now();
            if ( !today.isEqual(lastTradingDay) ){
                System.out.println("Today "+today+" is not a trading day, last trading day is: "+lastTradingDay);
                System.exit(1);
            }
        }
        final DataImporter importer = createDataImporter();
        importer.securityDataDir = new File(rootDir);
        importer.exchangeableData = new ExchangeableData(importer.securityDataDir, false);
        List<Exchangeable> resolvedExchangeables = importer.processArgs(securityIds, dataTypes, beginDate, endDate, props);

        waitAt(importer, resolvedExchangeables);
        importer.preImportData();
        importer.importDataByDataTypes();
        if ( thread==1 || !importer.canImportParallel() ){
            importer.importData(resolvedExchangeables);
        }else{
            //create worker threads
            final Queue<Exchangeable> exchangeablesToImport= new ConcurrentLinkedQueue<>(resolvedExchangeables);
            for(int i=0;i<thread;i++){
                Thread t = new Thread(){
                    @Override
                    public void run(){
                        while(true)
                        {
                            Exchangeable e = exchangeablesToImport.poll();
                            if ( e==null ) {
                                break;
                            }
                            try {
                                importer.importData(e);
                            } catch (Exception e1) {
                                System.out.println("Import "+e+" failed: "+e1);
                            }
                        }
                    }
                };
                t.setDaemon(true);
                t.setName("data import thread #"+i);
                t.start();
            }
            //check when the exchangeablesToImport is empty
            while(true){
                Thread.sleep(100);
                synchronized(exchangeablesToImport){
                    if ( exchangeablesToImport.size()==0 ){
                        break;
                    }
                }
            }
            System.out.println("ALL TASK COMPLETED");
        }
        importer.postImportData();
    }

    private static void waitAt(DataImporter importer, List<Exchangeable> resolvedExchangeables) throws Exception
    {
        if ( at== null ) {
            return;
        }
        List<Exchangeable> exchangeables = resolvedExchangeables;
        Exchange e = exchangeables.get(0).exchange();
        LocalDate currDate = LocalDate.now();
        LocalDate marketDay = MarketDayUtil.thisOrNextMarketDay(null, null, false);
        if ( at!=null && LocalTime.now().compareTo(at)>0 && !after && currDate.equals(marketDay)) {
            marketDay = MarketDayUtil.nextMarketDay(e, marketDay);
        }
        LocalDateTime targetDateTime = marketDay.atTime(at);
        long targetTime = Date.from(targetDateTime.atZone(e.getZoneId()).toInstant()).getTime();
        while(true){
            long waitTime = targetTime - NetworkBeijingTime.getTime();
            if ( waitTime<=0 ) {
                break;
            }
            long waitTime2 = Math.min(waitTime, 10*60*1000);
            System.out.println("Wait time "+DateUtil.milliSec2hhmmss(waitTime)+" , target time: "+DateUtil.date2str(targetTime));
            Thread.sleep(waitTime2);
        }
    }

    private static DataImporter createDataImporter() throws Exception
    {
        Map<String,DataImporter> externalDataImporters = loadExternalImporters();
        if ( "sina".equalsIgnoreCase(source)){
            return new SinaDataImporter();
        }else if ( "yahoo".equalsIgnoreCase(source)){
            return new YahooDataImporter();
        }else if ( "netease".equalsIgnoreCase(source)){
            return new NeteaseDataImporter();
        }else if ( "tencent".equalsIgnoreCase(source)){
            return new TencentDataImporter();
        }else if ( "tdx".equalsIgnoreCase(source)){
            return new TdxDataImporter();
        }else if ( "lts".equalsIgnoreCase(source)){
            return new LtsDataImporter();
        }else if ( "ctp".equalsIgnoreCase(source)){
            return new CtpDataImporter();
        }else if ( "stockFinance".equalsIgnoreCase(source)){
            return new StockFinaceDataImporter();
        }else if ( "xquant".equalsIgnoreCase(source)){
            return new XQuantDataImporter();
        }else if ( "hkex".equalsIgnoreCase(source)){
            return new HkexDataImporter();
        }
        for(Object k:externalDataImporters.keySet()){
            String key = k.toString();
            if ( key.equalsIgnoreCase(source)) {
                return externalDataImporters.get(key);
            }
        }
        throw new RuntimeException("Unknown source: " + source);
    }

    private static void usage()
    {
        System.out.println("data_import options:");
        System.out.println("\t--rootDir=<data root dir>");
        System.out.println("\t--source=[tencent|quantum|lts|ctp|sina|stockFinance]");
        System.out.println("\t--securityIds=[secId1,secId2|ALL]");
        System.out.println("\t--dataTypes=[minute1,minute5,forecast,season]");
        System.out.println("\t--beginDate=[20140101]");
        System.out.println("\t--endDate=[20140901]");
        System.out.println("\t--at=[12:00:00]");
        System.out.println("\t--overwrite=[true|false]");
        System.out.println("\t--thread=1");
        System.out.println("\t--archive=true");
        System.out.println("\t--account=<trader_account_section_name>");
        System.out.println("\t\t当前主要使用: second tick: sina, minute5-minute30: tencent");
    }

    private static void parseArgs(String[] args)
            throws Exception
    {
        CommandOptions opt = CommandOptions.parse(args);
        rootDir = opt.getString(CommandOptions.Option.rootDir);
        source = opt.getString(CommandOptions.Option.source);
        securityIds = opt.getStringArray(CommandOptions.Option.securityIds);
        dataTypes = opt.getStringArray(CommandOptions.Option.dataTypes);
        beginDate = opt.getDate(CommandOptions.Option.beginDate);
        endDate = opt.getDate(CommandOptions.Option.endDate);
        at = opt.getTime(CommandOptions.Option.at);
        String account = opt.getString(CommandOptions.Option.account);
        checkTradingDay = opt.getBoolean(CommandOptions.Option.checkTradingDay, false);
        if (opt.hasOption(CommandOptions.Option.after)){
            after = true;
            at = opt.getTime(CommandOptions.Option.after);
        }
        thread = opt.getInt(CommandOptions.Option.thread, 1);
        useNetworkBejingTime = opt.getBoolean(CommandOptions.Option.useNetworkBejingTime, false);
        archive = opt.getBoolean(CommandOptions.Option.archive, false);

        //加载trader_accounts.ini文件的对应配置项
        props = new Properties();
        if ( null!=account ) {
            IniFile traderAccount = TraderConfigUtil.loadIniFile(TraderConfigUtil.TRADER_ACCOUNTS);
            props.putAll(traderAccount.getSection(account).getProperties());
        }
        props.putAll(opt.getProps());
        if (rootDir == null) {
            throw new IllegalArgumentException("root dir is unknown.");
        }
        if (source == null && !archive) {
            throw new IllegalArgumentException("source is unknown.");
        }
    }

    private static Map<String,DataImporter> loadExternalImporters() throws Exception
    {
        Map<String,DataImporter> m = new HashMap<>();
        InputStream is = ResourceUtil.load("dataImporter.properties");
        if (is==null) {
            return m;
        }
        Properties props = new Properties();
        props.load(is);
        is.close();
        for(Object k:props.keySet()){
            String key = k.toString();
            String cls = props.getProperty(key);
            m.put(key, (DataImporter)Class.forName(cls).newInstance());
        }
        return m;
    }
}
