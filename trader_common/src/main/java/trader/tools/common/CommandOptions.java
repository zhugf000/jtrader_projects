package trader.tools.common;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Properties;

import trader.common.util.DateUtil;
import trader.common.util.FileUtil;

public class CommandOptions {
    public static enum Option{
        rootDir
        ,targetFile
        ,source
        ,securityIds
        ,dataTypes
        ,beginDate
        ,endDate
        ,at
        ,after
        ,thread
        ,useNetworkBejingTime
        ,checkTradingDay
        ,target
        ,archive
        ,account;
    }

    private Properties props = new Properties();
    private String[] values = new String[Option.values().length];

    public Properties getProps()
    {
        return props;
    }

    public boolean hasOption(Option option){
        return values[option.ordinal()]!=null;
    }

    public String getString(Option option){
        if (!hasOption(option)) {
            return null;
        }
        return values[option.ordinal()].trim();
    }

    public File getFile(Option option){
        if (!hasOption(option)) {
            return null;
        }
        return new File(getString(option));
    }

    public String[] getStringArray(Option option){
        if (!hasOption(option)) {
            return null;
        }
        return getString(option).split(",");
    }

    public LocalDate getDate(Option option){
        if (!hasOption(option)) {
            return null;
        }
        return DateUtil.str2localdate(getString(option));
    }

    public LocalTime getTime(Option option){
        if (!hasOption(option)) {
            return null;
        }
        return LocalTime.parse(getString(option));
    }

    public int getInt(Option option, int defaultValue){
        if (!hasOption(option)) {
            return defaultValue;
        }
        return Integer.parseInt(getString(option));
    }

    public boolean getBoolean(Option option, boolean defaultValue){
        if (!hasOption(option)) {
            return defaultValue;
        }
        return Boolean.parseBoolean(getString(option));
    }

    private void setValue(String k, String v)
    {
        props.setProperty(k, v);
    }
    private void setValue(Option option, String value)
    {
        values[option.ordinal()] = value;
    }

    private static Option getOption(String optionName)
    {
        for(Option opt:Option.values()){
            if (opt.name().equalsIgnoreCase(optionName)){
                return opt;
            }
        }
        return null;
    }

    private static String[] getOptionKV(String optionStr)
    {
        if ( !optionStr.startsWith("--") || optionStr.indexOf('=')<0) {
        	String[] propKV = new String[2];
        	propKV[0] = optionStr.substring(2);
        	propKV[1] = "true";
            return propKV;
        }
        String[] propKV = optionStr.substring(2).split("=");
        return propKV;
    }

    public static CommandOptions parse(String[] args) throws IOException
    {
        CommandOptions result = new CommandOptions();
        //Load argument from file
        if ( args.length==1 && args[0].startsWith("@file=") ){
            String file = args[0].substring(6);
            List<String> lines = FileUtil.loadLines(new File(file));
            args = lines.toArray(new String[lines.size()]);
        }
        for (String arg : args) {
            arg = arg.trim();
            String[] optionKV = getOptionKV(arg);
            Option option = getOption(optionKV[0]);
            if ( option!=null ) {
                result.setValue(option, optionKV[1]);
            } else {
                result.setValue(optionKV[0], optionKV[1]);
            }
        }
        return result;
    }
}
