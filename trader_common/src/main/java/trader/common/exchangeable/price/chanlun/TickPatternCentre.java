package trader.common.exchangeable.price.chanlun;

import java.util.List;

import trader.common.exchangeable.price.*;

/**
 * 中枢最重要的是highPoint/lowPoint
 */
public class TickPatternCentre extends TickPattern {
    protected PriceLevel level;
    protected PriceRange fluctuationRange;

    @Override
    public Type getType(){
        return Type.Centre;
    }

    public PriceLevel getPriceLevel(){
        return level;
    }

    public void extend(List<TickPattern> ps){
        assert(getPatterns().get(0).getType()==ps.get(0).getType());
        Point oldEnd = getEndPoint();
        patterns.addAll(ps);
        for(int i=0;i<ps.size();i++){
        	TickPattern p = ps.get(i);
        	if ( getDirection()!=p.getDirection() ){
        		if ( getHigh() < p.getHigh() ){
        			setHighPoint( getHighPoint().changePrice(p.getHigh()) );
        		}
        		if ( getLow() > p.getLow() ){
        			setLowPoint( getLowPoint().changePrice(p.getLow()) );
        		}
        	}
        }
        Point lastEndPoint = ps.get(ps.size()-1).getEndPoint();
        setEndPoint( lastEndPoint.changePrice(oldEnd.getPrice()) );
        setHighPoint( lastEndPoint.changePrice(getHigh()) );
        fluctuationRange=null;
    }

    /**
     * @return
     */
    static TickPattern create(PriceLevel level, boolean centreDir, Point centreOCHigh, Point centreOCLow, Tick... ticks){
        TickPatternCentre result = new TickPatternCentre();
        long centreHigh = 0;
        long centreLow = Long.MAX_VALUE;
        for(int i=0;i<ticks.length;i++){
        	TickPattern t = (TickPattern) ticks[i];
            result.getPatterns().add(t);
            if ( t.getDirection()!=centreDir ){
            	if ( t.getHigh()>centreHigh ){
            		centreHigh = t.getHigh();
            	}
            	if ( t.getLow()<centreLow ){
            		centreLow = t.getLow();
            	}
            }
        }
        assert(centreDir!=ticks[0].getDirection());
        result.level = level;
        result.direction = centreDir;
        Point beginPoint = ticks[0].getBeginPoint();
        Point endPoint = ticks[ticks.length-1].getEndPoint();
        if ( centreDir ){
            result.setBeginPoint( beginPoint.changePrice(centreOCLow.getPrice()));
            result.setEndPoint( endPoint.changePrice(centreOCHigh.getPrice()));
        }else{
            result.setBeginPoint( beginPoint.changePrice(centreOCHigh.getPrice()));
            result.setEndPoint( endPoint.changePrice(centreOCLow.getPrice()));
        }
        result.setLowPoint( result.getBeginPoint().changePrice(centreLow));
        result.setHighPoint( result.getEndPoint().changePrice(centreHigh));
        return result;
    }

}
