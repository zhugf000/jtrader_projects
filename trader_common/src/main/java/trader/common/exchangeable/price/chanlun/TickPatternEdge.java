package trader.common.exchangeable.price.chanlun;


import trader.common.exchangeable.price.Point;
import trader.common.util.DateUtil;
import trader.common.util.PriceUtil;

public class TickPatternEdge extends TickPattern {
    int index = -1;

    @Override
    public Type getType(){
        return Type.Edge;
    }

    public int getIndex(){
        return index;
    }

    TickPatternEdge(Point point, boolean direction){
        this.direction = direction;
        setHighPoint(point);
        setLowPoint(point);
        setBeginPoint(point);
        setEndPoint(point);
    }

    @Override
    public String toString(){
        return "TickPattern[ Edge "+(getDirection()?"High":"Low")+", "+DateUtil.date2str(getBeginTime())+" : "+PriceUtil.long2price(getOpen())+", "+index+"]";
    }
}
