package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Tick;

/**
 * 分析分形相关代码--简化版.
 * <BR>分形局限在一根K线上
 */
public class FractalAnalyser2 extends Analyser {
    private List<Tick> ticks;

    public FractalAnalyser2(List<Tick> ticks){
        this.ticks = ticks;
    }

    /**
     * 将处理过的K线解析为顶底分形
     */
    public List<TickPattern> parse(AnalyserOptions options) {
        LinkedList<TickPattern> result = new LinkedList<>();
        LinkedList<Tick> unparsedTicks = new LinkedList<>(ticks);
        LinkedList<Tick> lineTicks = new LinkedList<>();

        lineTicks.add(unparsedTicks.pollFirst());
        while (unparsedTicks.size()>=2) {
            if ( lineTicks.isEmpty() ){
                lineTicks.add(unparsedTicks.pollFirst());
                continue;
            }
            Tick lt = lineTicks.peekLast();
            Tick t = unparsedTicks.pollFirst();
            Tick nt = unparsedTicks.peekFirst();

            if ( !options.ignoreOpenGap ){ //do nothing if the 3 ticks are not in the same day
                if ( isOpenTick(lt, t) && canCreateFractal(result, true, t) ){
                    //create line
                    //TODO test it
                    result.add( createPatternLine(result.peekLast(), lineTicks) );
                    lineTicks.clear();
                    lineTicks.add(t);
                    continue;
                }
            }
            if ( isFractal(true, lt, t, nt)  && canCreateFractal(result, true, t) ) { // 顶分形
                if ( !lineTicks.isEmpty() ) {
                    result.add(createPatternLine(result.peekLast(), lineTicks));
                    lineTicks.clear();
                }
                TickPattern pattern = TickPattern.create(TickPattern.Type.Fractal, true, t);
                result.add(pattern);
                continue;
            }
            if (isFractal(false, lt, t, nt)  && canCreateFractal(result, false, t) ) {// 底分形
                if ( !lineTicks.isEmpty() ) {
                    result.add(createPatternLine(result.peekLast(), lineTicks));
                    lineTicks.clear();
                }
                TickPattern pattern = TickPattern.create(TickPattern.Type.Fractal, false, t);
                result.add(pattern);
                continue;
            }
            lineTicks.add(t);
        }
        if ( !unparsedTicks.isEmpty() ){
            lineTicks.addAll(unparsedTicks);
            unparsedTicks.clear();
        }
        if ( !lineTicks.isEmpty() ) {
            result.add(createPatternLine(result.peekLast(), lineTicks));
            lineTicks.clear();
        }
        if ( !unparsedTicks.isEmpty() ){
            result.add(createPatternLine(result.peekLast(), unparsedTicks));
            unparsedTicks.clear();
        }
        return result;
    }

    /**
     * 从一系列K线创建 LongLine/ShortLine Pattern
     */
    private TickPattern createPatternLine(TickPattern lastPattern, List<Tick> ticks)
    {
        if ( ticks.isEmpty() ){
            return null;
        }
        if (ticks.size() == 1) {
            boolean lineDirection = true;
            Tick t = ticks.get(0);
            if ( t.getBeginPoint().isHigher(t.getEndPoint()) ){
                lineDirection = false;
            }else{
                lineDirection = true;
            }
            return assertLine( TickPattern.create(TickPattern.Type.Line, lineDirection,
                    new Tick[] { t }));
        }
        Tick bt = ticks.get(0);
        Tick et = ticks.get(ticks.size()-1);

        boolean lineDirection = true;
        if ( bt.getOpen()< et.getClose() ) {
            lineDirection = true;
        } else if ((bt.getOpen()> et.getClose())) {
            lineDirection = false;
        } else {
            // 在目前放宽条件的情况下,如果K线的方向无法确定, 就与上个分形的顶点比较
            if (lastPattern != null && lastPattern.getType() == TickPattern.Type.Fractal) {
                boolean lastDirection = lastPattern.getDirection();
                if (lastDirection) {
                    lineDirection = (et.getHigh() >= lastPattern.getHigh());
                } else {
                    lineDirection = (et.getLow() >= lastPattern.getLow());
                }
            }
        }

        return assertLine(TickPattern.create(TickPattern.Type.Line, lineDirection, ticks.toArray(new Tick[ticks.size()])));
    }

    private TickPattern assertLine(TickPattern line){
        if ( line.getDirection() ){
            assert(line.getEndPoint().isHighOrEquals(line.getBeginPoint()));
        }else{
            assert(line.getBeginPoint().isHighOrEquals(line.getEndPoint()));
        }
        return line;
    }

    /**
     * @param patterns
     * @param fractalDir
     * @param p
     * @return
     */
    private boolean canCreateFractal(LinkedList<TickPattern> patterns, boolean fractalDir, Tick fractalTick)
    {
        boolean result = true;
        TickPattern lf = lastFractal(patterns);
        if ( lf==null ){
            return true;
        }

        if ( fractalDir == lf.getDirection()) {
            // 如果出现连续的顶,中间没有K线或只有下降K线,并且后顶低于前顶,则不生成后顶.
            // 如果出现连续的底,中间没有K线或只有上升K线,并且后底高于前底,则不生成后底.
            if (fractalDir && lf.getDirection()) {
                result = (fractalTick.getHigh() >= lf.getHigh());
            }
            if (!fractalDir && !lf.getDirection()) {
                result = (fractalTick.getLow() <= lf.getLow());
            }
        }else{
            if (fractalDir) { // 后面的顶分形必须高于前面的底分形
                result = fractalTick.getHigh() > lf.getHigh();
            } else {// 后面的底分形必须低于前面的顶分形
                result = fractalTick.getLow() < lf.getLow();
            }
        }
        return result;
    }

}
