package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

/**
 * 对应FractalAnalyser
 */
public class StrokeAnalyser2 extends StrokeAnalyser {
    private final static Logger logger = LoggerFactory.getLogger(StrokeAnalyser2.class);
    private final static LinkedList<TickPattern> EMPTY_PATTERNS = new LinkedList<>();

    private List<TickPattern> patterns;
    private LinkedList<TickPattern> unparsedPatterns;
    private LinkedList<TickPattern> strokes;

    private AnalyserOptions options;

    /**
     * share last pattern for next stroke
     */
    private boolean shareLastPattern;

    private List<TickPattern> skipPatterns;

    private Boolean acceptedStrokeDirection;

    private TickPattern gapStroke;

    public StrokeAnalyser2(List<TickPattern> patterns){
        this.patterns = patterns;
    }

    public List<TickPattern> parse(AnalyserOptions options){
        this.options = options;
        strokes = new LinkedList<>();
        unparsedPatterns = new LinkedList<>( patterns );

        int lastUnparsedPatterns = unparsedPatterns.size();
        while( unparsedPatterns.size()>0 ){
            this.acceptedStrokeDirection = null;
            this.shareLastPattern = false;
            this.skipPatterns = null;
            this.gapStroke = null;
            LinkedList<TickPattern> strokePatterns = detectStrokeEnd();
            if ( strokePatterns.isEmpty() ){
                if ( !unparsedPatterns.isEmpty()){
                    logger.info("Some patterns left unacceptable : "+unparsedPatterns);
                }
                break;
            }
            if ( gapStroke!=null ){
                logger.warn("Found GAP stroke: "+gapStroke);
                strokes.add(gapStroke);
            }
            gapStroke = null;

            TickPattern mergedStroke = mergeWithLastStroke(strokePatterns);
            if ( mergedStroke!=null ){
                TickPattern lastStroke = strokes.pollLast();
                strokes.add(mergedStroke);
                logger.warn("Stroke patterns are merged with prev one : "+lastStroke+", new patterns: "+strokePatterns);
            }else{

                TickPattern stroke = StrokeAnalyser.createStrokePattern(strokePatterns, acceptedStrokeDirection);
                if ( acceptedStrokeDirection!=null ){
                    assert(stroke.getDirection() == acceptedStrokeDirection);
                }
                strokes.add(stroke);
            }
            if ( shareLastPattern ){
                unparsedPatterns.offerFirst( strokePatterns.peekLast() );
            }
            if ( lastUnparsedPatterns == unparsedPatterns.size() ){
                throw new RuntimeException("Unable tp parse strokes at "+unparsedPatterns.peekFirst());
            }
            lastUnparsedPatterns = unparsedPatterns.size();
        }

        return strokes;
    }

    private LinkedList<TickPattern> detectStrokeEnd() {
        assert (unparsedPatterns.size() > 0);
        if (unparsedPatterns.size() <= 2) {
            return detectEdgeStroke(unparsedPatterns);
        }
        detectSkipPatterns();
        LinkedList<TickPattern> strangeStroke = null;
        if ( (strangeStroke=detectStrangeStroke()).size()>0 ){
            return strangeStroke;
        }
        acceptedStrokeDirection = detectStrokeDirection(unparsedPatterns);
        LinkedList<TickPattern> acceptedPatterns = new LinkedList<>();
        TickPattern fp = unparsedPatterns.peekFirst();
        acceptedPatterns.add(fp);
        unparsedPatterns.pollFirst();
        while (unparsedPatterns.size() > 0) {
            TickPattern p = unparsedPatterns.peek();
            if (detectGapStroke(acceptedStrokeDirection, acceptedPatterns, p) != null) {
                // create this stroke and gap stroke
                break;
            }
            TickPattern lp = acceptedPatterns.peekLast();
            TickPattern lf = lastFractal(acceptedPatterns);
            TickPattern np = nextPattern(unparsedPatterns, p);
            TickPattern nf = nextFractal(unparsedPatterns, p);
            Point acceptedBeginPoint = detectStrokeBeginPoint(acceptedStrokeDirection, acceptedPatterns);
            Point acceptedEndPoint = lp.getEndPoint();
            if (acceptedStrokeDirection) { // long stroke
                if (isLongLine(p)) { // accept long line
                    if ( p.getEndPoint().isLower(acceptedBeginPoint) ){
                        logger.info("Long stroke break by a lower long line :" +p);
                        break;
                    }
                    acceptedPatterns.add(p);
                    unparsedPatterns.poll();
                    continue;
                }
                if (isLongFractal(p)) { // accept long fractal
                    if (isLongFractal(lf)) {
                        if (!isFractalHigher(lf, p)) { // last long fractal is
                                                       // higher than this long
                                                       // fractal
                            logger.warn("Last long Fractal " + lf + " is not higher than this one: " + p);
                        }
                    }
                    acceptedPatterns.add(p);
                    unparsedPatterns.poll();
                    //If next pattern is short line or short fractal, break;
                    if ( isShortFractal(np) ){
                        break;
                    }
                    if ( isShortFractal(nf) && (isShort(np))){
                        break;
                    }
                    if ( isShortFractal(nf) && isLongLine(np) && np.getTicks().size()==1 ){
                        break;
                    }
                    //Break if the long pattern's close point is lower than begin point
                    if ( p.getEndPoint().isLower(acceptedBeginPoint) ){
                        logger.debug("Long stroke break on long fractal(lower on end): "+p);
                        break;
                    }
                    continue;
                }
                if (isShortLine(p)) { // break on short line
                    if (isLongFractal(nf)) {
                        if (p.getTicks().size() > 1 && p.getLowPoint().isLower(lf)) {
                            //throw new RuntimeException("The line with " + p.getTicks().size() + " ticks is not ignorable");
                            logger.warn("Long stroke, the line "+p+" is lower than last elem : "+lp);
                        }
                        logger.info("Long stroke continue on short line, since next fractal is long");
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
                    logger.debug("Long stroke break on short line: "+p);
                    break;
                }
                if (isShortFractal(p)) { // break on short fractal
                    //Continue if the short fractal goes higher at end
                    if ( p.getEndPoint().isHigher(p.getBeginPoint())
                         && p.getEndPoint().isHigher(acceptedEndPoint)
                         && p.getLowPoint().isHigher(lp.getLowPoint()))
                    {
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
                    //Continue if next pattern is long and this fractal's low price is higher than beginPoint
                    if ( isLong(np) && p.getLowPoint().isHigher(lp.getLowPoint()) ){
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
//                    if (!isLongFractal(lp)) { // need to check gap if last pattern is not long fractal
//                        logger.warn("Long stroke last line (" + lp + ") is break by short fractal :" + p);
//                    }
                    logger.info("Long stroke break on short fractal: "+p);
                    break;
                }
            } else { // short stroke
                if (isShortLine(p)) { // accept short line
                    if ( p.getEndPoint().isHigher(acceptedBeginPoint) ){
                        logger.info("Short stroke break by a higher short line :" +p);
                        break;
                    }
                    acceptedPatterns.add(p);
                    unparsedPatterns.poll();
                    continue;
                }
                if (isShortFractal(p)) { // accept short fractal
                    if (isShortFractal(lf)) {
                        if (isFractalHigher(lf, p)) { // this short fractal is
                                                      // higher than last short
                                                      // fractal
                            logger.warn("Last short fractal " + lf + " is lower than this one: " + p);
                        }
                    }
                    acceptedPatterns.add(p);
                    unparsedPatterns.poll();
                    //If next pattern is long line or long fractal, break;
                    if ( isLongFractal(np) ){
                        break;
                    }
                    if ( isLongFractal(nf) && isLong(np) ){
                        break;
                    }
                    if ( isLongFractal(nf) && isShortLine(np) && np.getTicks().size()==1 ){
                        break;
                    }
                    //Break if the short pattern's close point is higher than begin point
                    if ( p.getEndPoint().isHigher(detectStrokeBeginPoint(acceptedStrokeDirection, acceptedPatterns)) ){
                        logger.debug("Short stroke break on short fractal(higher on close): "+p);
                        break;
                    }
                    continue;
                }
                if (isLongLine(p)) { //break on long line
                    if (isShortFractal(nf)) {
                        if (p.getTicks().size() > 1 && p.getHighPoint().isHigher(lp.getHighPoint()) ) {
                            logger.warn("Short stroke, the line "+p+" is higher than last elem : "+lp);
                        }
                        logger.info("Short strok continue on long line, since next fractal is short");
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
                    logger.debug("Short stroke break on long line: "+p);
                    break;
                }
                if (isLongFractal(p)) { // break on long fractal if possible
                    //Continue if the ^ fractal goes lower at end
                    if ( p.getEndPoint().isLower(p.getBeginPoint())
                         && p.getEndPoint().isLower(acceptedEndPoint)
                         && p.getHighPoint().isLower(lp.getHighPoint()))
                    {
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
                    //Continue if next pattern is short and this fractal's high price is lower than beginPoint
                    if ( isShort(np) && p.getHighPoint().isLower(lp.getHighPoint()) ){
                        acceptedPatterns.add(p);
                        unparsedPatterns.poll();
                        continue;
                    }
//                    if (!isShortFractal(lp)) { // need to check gap if last pattern is not short fractal
//                        logger.info("Short stroke last line(" + lp + ") is break by long fractal " + p);
//                    }
                    logger.debug("Short stroke break on long fractal: "+p);
                    break;
                }
                throw new RuntimeException("Unaccepted pattern for short stroke: " + p);
            }
        }
        if (gapStroke == null) {
            TickPattern lp = acceptedPatterns.peekLast();
            if (isFractal(lp)) {
                this.shareLastPattern = true;
            }
        }
        return acceptedPatterns;
    }

    private LinkedList<TickPattern> detectEdgeStroke(LinkedList<TickPattern> edgePatterns){
        TickPattern fp = edgePatterns.peekFirst();
        if ( edgePatterns.size()==1){
            switch(fp.getType()){
            case Line:
                acceptedStrokeDirection = detectStrokeDirection(edgePatterns);
                return move(edgePatterns);
            default:
                logger.debug("Ignore edge patterns, invalid type: "+fp);
                return EMPTY_PATTERNS;
            }
        }else if ( edgePatterns.size()==2 ){
            //accepted combinations:
            //long fractal+short line, short fractal + long line
            //short line+short fractal, long line+long fratcal
            TickPattern np = edgePatterns.peekLast();
            switch(fp.getType()){
            case Line:
                if ( np.getType()==Type.Fractal && fp.getDirection()==np.getDirection() ){
                    acceptedStrokeDirection = detectStrokeDirection(edgePatterns);
                    return move(edgePatterns);
                }
                break;
            case Fractal:
                if ( np.getType()==Type.Line && fp.getDirection()!=np.getDirection() ){
                    acceptedStrokeDirection = detectStrokeDirection(edgePatterns);
                    return move(edgePatterns);
                }
                break;
            default:
                break;
            }
            logger.debug("Ignore edge patterns, invalid combination: "+edgePatterns);
            return EMPTY_PATTERNS;
        }else{
            throw new RuntimeException("Should not run here.");
        }
    }

    private LinkedList<TickPattern> detectStrangeStroke(){
        LinkedList<TickPattern> result = new LinkedList<>();

        TickPattern fp = unparsedPatterns.peekFirst();
        TickPattern np = unparsedPatterns.get(1);
        TickPattern np2 = null;
        if( unparsedPatterns.size()>=3){
            np2 = unparsedPatterns.get(2);
        }
        if ( isLongLine(fp) && isShortFractal(np) && isShort(np2) ){
            result.add(fp);
            unparsedPatterns.pollFirst();
        }
        if ( isShortLine(fp) && isLongFractal(np) && isLong(np2) ){
            result.add(fp);
            unparsedPatterns.pollFirst();
        }
        return result;
    }

    /**
     * Skip first pattern if:
     * <BR>short line+long fractal, long line+short fractal.
     */
    private List<TickPattern> detectSkipPatterns(){
        TickPattern fp = unparsedPatterns.peekFirst();
        TickPattern np = unparsedPatterns.get(1);
        if ( isLongLine(fp) && isShortFractal(np) && fp.getTicks().size()==1 ){
            skipPatterns = new LinkedList<>();
            skipPatterns.add(fp);
            unparsedPatterns.pollFirst();
            logger.warn("SKIP 1 tick long line "+fp+" with short fractal: "+np);
        }
        if ( isShortLine(fp) && isLongFractal(np) && fp.getTicks().size()==1 ) {
            skipPatterns = new LinkedList<>();
            skipPatterns.add(fp);
            unparsedPatterns.pollFirst();
            logger.warn("SKIP 1 tick short line "+fp+" with long fractal: "+np);
        }
        return skipPatterns;
    }

    private boolean detectStrokeDirection(LinkedList<TickPattern> patterns){
        boolean firstStroke = strokes.isEmpty();
        TickPattern fp = patterns.peekFirst();
        TickPattern np = nextPattern(patterns, fp);
        TickPattern nf = nextFractal(patterns, fp);
        {
            switch(fp.getType()){
            case Line:
                if ( !firstStroke ){
                    throw new RuntimeException("Next strkoe must be started with fractal");
                }
                boolean acceptedDir = fp.getDirection();
                if ( fp.getTicks().size()>1 ){
                    return fp.getDirection();
                }else{
                    logger.info("1 line stroke: "+fp);
                }
                return fp.getDirection();
            case Fractal:
                //short fractal+short line, long fractal+long line
                if ( isFractal(nf) ){
                    if ( isSameFractal(fp, nf) ){
                        return fp.getDirection();
                    }else{
                        return !fp.getDirection();
                    }
                }
                if ( isFractal(fp) && isLine(np) && (fp.getDirection()==np.getDirection())){
                    return fp.getDirection();
                }
                return !fp.getDirection();
            default:
                throw new RuntimeException("Stroke must be started with fractal or line: "+fp);
            }
        }
    }

    private TickPattern mergeWithLastStroke(LinkedList<TickPattern> acceptedPatterns){
        TickPattern lastStroke = strokes.peekLast();

        if ( lastStroke==null ){
            return null;
        }
        if ( acceptedStrokeDirection==lastStroke.getDirection() ){
            LinkedList<TickPattern> mergedPatterns = new LinkedList<>( lastStroke.getPatterns() );
            mergedPatterns.addAll(acceptedPatterns);
            TickPattern mergedStroke = StrokeAnalyser.createStrokePattern(mergedPatterns, acceptedStrokeDirection);
            return mergedStroke;
        }
        return null;
    }

    /**
     * check gap
     */
    private TickPattern detectGapStroke(boolean acceptedDir, LinkedList<TickPattern> acceptedPatterns, TickPattern p){
        if ( options.ignoreOpenGap || acceptedPatterns.size()<=0 ){
            return null;
        }

        Point strokeEnd = acceptedPatterns.peekLast().getEndPoint();
        long strokeBeginPrice = acceptedPatterns.peekFirst().getBeginPoint().getPrice();
        long strokeEndPrice = acceptedPatterns.peekLast().getEndPoint().getPrice();
        long strokeAvgPrice = (strokeBeginPrice+strokeEndPrice)/2;

        //long stroke with long line, begin point of long line is lower than (begin+end)/2 of stroke
        if ( acceptedDir ){
            Point pBegin = p.getBeginPoint();
            if ( isLongLine(p) ){
                long lineBeginPrice = p.getBeginPoint().getPrice();
                if ( lineBeginPrice<strokeAvgPrice ){
                    gapStroke = createStrokeGap(strokeEnd, pBegin);
                    return gapStroke;
                }
            }
            if ( isShortLine(p) ){
                long lineEndPrice = p.getEndPoint().getPrice();
                if ( lineEndPrice<strokeAvgPrice ){
                    gapStroke = createStrokeGap(strokeEnd, pBegin);
                    return gapStroke;
                }
            }
        }
        return null;
    }

    private static LinkedList<TickPattern> move(LinkedList<TickPattern> patterns){
        LinkedList<TickPattern> list = new LinkedList<>();
        list.addAll(patterns);
        patterns.clear();
        return list;
    }


    private static TickPattern createStrokeGap(Point beginPoint, Point endPoint){
        boolean direction = true;
        if ( beginPoint.isHigher(endPoint)){
            direction = false;
        }
        TickPattern result = TickPattern.createStroke( direction, beginPoint,
                endPoint );
        return result;
    }
}
