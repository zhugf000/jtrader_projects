package trader.common.exchangeable.price.chanlun;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

/**
 * 分析笔划相关代码
 */
class StrokeAnalyser extends Analyser {
    private final static Logger logger = LoggerFactory.getLogger(StrokeAnalyser.class);

    /**
     * 合并连续顶底
     */
    private static LinkedList<TickPattern> preprocessPatterns(List<TickPattern> patterns) {
        LinkedList<TickPattern> tmp = new LinkedList<>(patterns);
        LinkedList<TickPattern> result = new LinkedList<>();
        while (true) {
            for (Iterator<TickPattern> it = tmp.iterator(); it.hasNext();) {
                TickPattern p = it.next();
                TickPattern.Type ptype = p.getType();
                if (result.size() == 0) {
                    result.add(p);
                    continue;
                }
                TickPattern pp = result.getLast();
                TickPattern.Type pptype = pp.getType();
                if (ptype == TickPattern.Type.Line) {
                    //check gap
                    //http://blog.sina.com.cn/s/blog_714c5fc50100opkd.html
                    //1、如果缺口本来就处于高点下来的向下笔中，比如2804点向下笔中的缺口一般直接处理在该笔中
                    //2、如果缺口前面是向上笔，比如529当天最高点收盘，后面直接下来的大缺口，
                    //且该缺口直接打破最近的一个中枢的低点（注意是中枢区间的低点），那么该缺口可视为段。
                    //如果没有满足缺口直接打破最近的一个中枢的低点依然应该按照笔处理。
                    checkGapBeforeLine(result, p);
                    result.add(p);
                    continue;
                }
                TickPattern ppp = null;
                TickPattern.Type ppptype = null;
                if (result.size() >= 2) {
                    ppp = result.get(result.size() - 2);
                    ppptype = ppp.getType();
                }
                if (ptype == TickPattern.Type.Fractal && p.getDirection()) {
                    // 顶分形,顶分形 合并
                    if (pptype == TickPattern.Type.Fractal && pp.getDirection()) {
                        if ((pp.getHigh() < p.getHigh())) {
                            result.removeLast();
                            result.add(TickPattern
                                    .create(TickPattern.Type.Fractal, true, pp, p));
                            continue;
                        }
                    }
                    // 顶分形,向上K线,顶分形 合并
                    if (pptype == TickPattern.Type.Line
                            && pp.getDirection()
                            && ppptype == TickPattern.Type.Fractal
                            && ppp.getDirection()
                            && (ppp.getHigh()< p.getHigh())) {
                        result.removeLast();
                        result.removeLast();
                        result.add(TickPattern.create(TickPattern.Type.Fractal, true, ppp, pp,
                                p));
                        continue;
                    }
                }
                if (ptype == TickPattern.Type.Fractal && !p.getDirection()) {
                    // 底分形,底分形 合并
                    if (pptype == TickPattern.Type.Fractal && !pp.getDirection()) {
                        if ((pp.getLow() > p.getLow())) {
                            result.removeLast();
                            result.add(TickPattern.create(TickPattern.Type.Fractal, false, pp,
                                    p));
                            continue;
                        }
                    }
                    // 底分形,向下K线,底分形 合并
                    if (pptype == TickPattern.Type.Line
                            && !pp.getDirection()
                            && ppptype == TickPattern.Type.Fractal
                            && !ppp.getDirection()
                            && (ppp.getLow() > p.getLow())) {
                        result.removeLast();
                        result.removeLast();
                        result.add(TickPattern.create(TickPattern.Type.Fractal, false, ppp, pp,
                                p));
                        continue;
                    }
                }
                result.add(p);
            }

            // 判断是否已经重复合并完毕
            if (tmp.size() == result.size()) {
                break;
            } else {
                tmp = result;
                result = new LinkedList<>();
            }
        }
        return result;
    }

    /**
     * 是否需要在LINE前面加上一个GAP
     */
    private static void checkGapBeforeLine(LinkedList<TickPattern> patterns, TickPattern line ){
        assert(line.getType()==Type.Line);
        assert(patterns.size()>0);
        TickPattern pp =  patterns.peekLast();
        switch(pp.getType()){
        case Line:
            //another line
            if ( pp.getDirection()!= line.getDirection() ){
                logger.info("GET REVERSE LINE: "+pp+" BEFORE "+line);
            }
            break;
        case Fractal:
            if ( pp.getDirection()== line.getDirection() ){
                logger.info("GET SAME FRACTAL: "+pp+" BEFORE "+line);
                if ( line.getDirection() ){
                    if ( line.getLow() < pp.getLow() ){
                        logger.info("Prev LONG fractal: "+pp+" is HIGHER than "+line);
                    }
                }else{

                }
            }
            break;
        default:
            throw new RuntimeException("Unsupported type: "+pp.getType());
        }
        return;
    }

    /**
     * 将分形划分为笔划
     */
    static List<TickPattern> parseStrokePatterns(List<TickPattern> patterns_) {
        if ( patterns_.size()==0 ){
            return Collections.EMPTY_LIST;
        }
        // 合并连续顶底
        List<TickPattern> patterns = preprocessPatterns(patterns_);
        TickPattern lastp = patterns.get(patterns.size()-1);
        LinkedList<TickPattern> result = new LinkedList<>();
        LinkedList<TickPattern> strokePatterns = new LinkedList<>();

        for (Iterator<TickPattern> it = patterns.iterator(); it.hasNext();) {
            TickPattern pattern = it.next();
            TickPattern.Type patternType = pattern.getType();
            if (patternType == TickPattern.Type.Line) {
                strokePatterns.add(pattern);
                continue;
            }
            if (patternType == Type.Fractal) {
                strokePatterns.add(pattern);
                if ( strokePatterns.size()==2 && strokePatterns.get(0).getType()==Type.Line){
                    result.add(createEdgeStrokePattern(strokePatterns));
                    strokePatterns.clear();
                    if ( pattern!=lastp){
                        strokePatterns.add(pattern);
                    }
                    continue;
                }
                if (detectStrokePatterns(strokePatterns)) {
                    // 检查是否一笔完成
                    result.add(createStrokePattern(strokePatterns, null));
                    strokePatterns.clear();
                    // 留着最后一个分形用于合并下一个笔划
                    if ( pattern!=lastp){
                        strokePatterns.add(pattern);
                    }
                }
                continue;
            }
            // 只应该有K线和分形
            assert (false);
        }

        if ( needToExtendLastStroke(result.peekLast(), strokePatterns) ){
            //剩余同向,需要重新创建
            TickPattern lastStroke = result.pollLast();
            LinkedList<TickPattern> newStrokePatterns = new LinkedList<>( lastStroke.getPatterns());
            newStrokePatterns.addAll(strokePatterns);
            result.add(createStrokePattern(newStrokePatterns, null));
        }else{
            if ( strokePatterns.size()==1 && strokePatterns.get(0).getType()==TickPattern.Type.Line){
                result.add(createSingleLineStrokePattern(strokePatterns));
            }
            if (strokePatterns.size()==2 )
            {
                result.add(createEdgeStrokePattern(strokePatterns));
            }
        }
        // 校验笔划应该是低高低高序列,前后高低相等
        if (strokePatterns.size() > 0) {
            for (int i = 1; i < result.size(); i++) {
                TickPattern s0 = result.get(i - 1);
                TickPattern s1 = result.get(i);
                assert ( s1.getOpen()!=0 );
                assert ( s1.getClose()!=0 );
                assert (s0.getDirection() != s1.getDirection());
                assert (s0.getEndTime().equals(s1.getBeginTime()));
                if (s0.getDirection()) {
                    assert( (s0.getOpen() <s0.getClose()) );
                    assert( (s0.getHigh() == s0.getClose()));
                } else {
                    assert( (s0.getOpen() > s0.getClose()) );
                    assert( (s0.getLow() == s0.getClose()));
                }
            }
        }
        return result;
    }

    private static void assertStroke(TickPattern stroke){
        if ( stroke.getDirection() ){
            assert(stroke.getBeginPoint().compareTo(stroke.getEndPoint()) <=0);
        }else{
            assert(stroke.getBeginPoint().compareTo(stroke.getEndPoint()) >=0);
        }
    }

    private static boolean needToExtendLastStroke(TickPattern lastStroke, List<TickPattern> leftPatterns){
        if ( lastStroke==null || leftPatterns.size()==0 ){
            return false;
        }
        if ( leftPatterns.size() >=1 && leftPatterns.size()<=2 ){
            TickPattern t1 = leftPatterns.get(0);
            TickPattern t2 = null;
            if( leftPatterns.size()==2 ){
                t2 = leftPatterns.get(1);
            }
            if ( lastStroke.getDirection() == t1.getDirection()
                    && ( t2==null || lastStroke.getDirection()==t2.getDirection())
                    )
            {
                return true;
            }
        }
        return false;
    }

    private static TickPattern createSingleLineStrokePattern(List<TickPattern> strokePatterns){
        TickPattern tk = strokePatterns.get(0);
        boolean direction = true;
        if ( tk.getEndPoint().getPrice()>tk.getBeginPoint().getPrice()){
            direction = true;
        }else{
            direction = false;
        }
        TickPattern result = TickPattern.createStroke(direction, tk.getBeginPoint(),
                tk.getEndPoint(), strokePatterns.toArray(new TickPattern[strokePatterns.size()]));
        assertStroke(result);
        return result;
    }

    /**
     * fractial+line or line+fractial
     */
    private static TickPattern createEdgeStrokePattern(List<TickPattern> strokePatterns){
        TickPattern t1 = strokePatterns.get(0);
        TickPattern t2 = strokePatterns.get(1);

        Point beginPoint = null, endPoint = null;
        boolean directionLong = true;
        if ( t1.getType()==TickPattern.Type.Line ){
            //line+fractial
            beginPoint = t1.getBeginPoint();
            endPoint = ((TickPatternFractal)t2).getTopPoint();
            directionLong = t2.getDirection();
        }else{
            //fractial+line
            beginPoint = ((TickPatternFractal)t1).getTopPoint();
            endPoint = t2.getEndPoint();
            directionLong = !t1.getDirection();
        }
        TickPattern result = TickPattern.createStroke(directionLong, beginPoint,
                endPoint, strokePatterns.toArray(new TickPattern[strokePatterns.size()]));
        assertStroke(result);
        return result;
    }

    /**
     * 从分形列表创建笔划
     */
    static TickPattern createStrokePattern(List<TickPattern> strokePatterns, Boolean acceptedDir) {
        TickPattern fp = strokePatterns.get(0);
        boolean strokeDir = false;
        boolean looseMode = false;
        LinkedList<TickPattern> fps = new LinkedList<>();
        for (int i = 0; i < strokePatterns.size(); i++) {
            TickPattern pattern = strokePatterns.get(i);
            if (pattern.getType() != TickPattern.Type.Fractal) {
                continue;
            }
            fps.add(pattern);
            if ( pattern.getDirection()!=strokeDir){
                looseMode = true;
            }
        }
        if ( acceptedDir==null ){
            if ( fp.getType()==Type.Fractal ){
                strokeDir = !fp.getDirection();
            }else if ( fp.getType()==Type.Line ){
                strokeDir = fp.getDirection();
            }else{
                throw new RuntimeException("Should not run here");
            }
        }else{
            strokeDir = acceptedDir;
        }
        // 校验: 必然有前顶不低于后顶，前底不高于后底，而在连续的顶后，必须会出现新的底
        if (!looseMode ){
            for (int i = 0; i < fps.size(); i++) {
                TickPattern f = fps.get(i);
                if ( fp==f){
                    continue;
                }
                TickPattern lf = null;
                if ( i>0 && fps.get(i-1)!= fp){
                    lf = fps.get(i-1);
                }
                //assert (strokeDir == fractalPattern.getDirection());
                if ( lf!=null ){
                    Point fEndPoint = null;
                    if ( f.getDirection()!=strokeDir ){
                        fEndPoint = f.getEndPoint();
                    }else{
                        fEndPoint = f.getTopPoint();
                    }
                    if (!strokeDir) {
                        if ( !lf.getDirection() && !f.getDirection()){
                            assert ((lf.getLow() >= fEndPoint.getPrice()));
                        }else{
                            assert ((lf.getClose() >= fEndPoint.getPrice()));
                        }
                    } else {
                        if( lf.getDirection() && f.getDirection() ){
                            assert ((lf.getHigh() <= fEndPoint.getPrice()));
                        }else{
                            assert ((lf.getClose() <= fEndPoint.getPrice()));
                        }
                    }
                }
            }
        }
        TickPattern lp = strokePatterns.get(strokePatterns.size() - 1);

        Point beginPoint = detectStrokeBeginPoint(strokeDir, strokePatterns);
        Point endPoint = null;

        if ( lp.getType()==TickPattern.Type.Fractal){
            endPoint = ((TickPatternFractal)lp).getTopPoint();
        }else{
            endPoint = lp.getEndPoint();
        }
        TickPattern result = TickPattern.createStroke( strokeDir, beginPoint,
                endPoint, strokePatterns.toArray(new TickPattern[strokePatterns.size()]));
        assertStroke(result);
        return result;
    }

    static Point detectStrokeBeginPoint(boolean strokeDir, List<TickPattern> strokePatterns){
        TickPattern fp = strokePatterns.get(0);
        Point beginPoint = null;
        switch(fp.getType()){
        case Fractal:
            if ( strokeDir == fp.getDirection() ){
                //beginPoint = firstPattern.getBeginPoint();
                if ( !strokeDir ){
                    beginPoint = fp.getHighPoint();
                }else{
                    beginPoint = fp.getLowPoint();
                }
            }else{
                beginPoint = fp.getTopPoint();
            }
            break;
        default:
            beginPoint = fp.getBeginPoint();
            break;
        }
        return beginPoint;
    }

    /**
     * 确定是否分形列表构成一个笔划
     */
    private static boolean detectStrokePatterns(List<TickPattern> strokePatterns) {
        int totalFractals = 0;
        TickPattern firstFractal = null;
        int firstFractalIndex = -1;
        TickPattern lastFractal = null;
        int lastFractalIndex = -1;
        for (int i = 0; i < strokePatterns.size(); i++) {
            TickPattern pattern = strokePatterns.get(i);
            if (pattern.getType() != TickPattern.Type.Fractal) {
                continue;
            }
            if (firstFractal == null) {
                firstFractal = pattern;
                firstFractalIndex = i;
            }
            lastFractal = pattern;
            lastFractalIndex = i;
            totalFractals++;
        }
        if (totalFractals < 2 || firstFractalIndex == lastFractalIndex) {
            return false;
        }
        if (firstFractal.getDirection() == lastFractal.getDirection()) {
            return false;
        }
        if (firstFractal.getDirection()) { // 确认向下笔划真的向下
            long high = firstFractal.getHigh();
            long low = lastFractal.getLow();
            if ( (high<= low)) {
                throw new RuntimeException("Should not run here!");
            }
        } else {// 确认向上笔划真的向上
            long low = firstFractal.getLow();
            long high = lastFractal.getHigh();
            if ((high <= low)) {
                throw new RuntimeException("Should not run here!");
            }
        }
        return true;
    }

}