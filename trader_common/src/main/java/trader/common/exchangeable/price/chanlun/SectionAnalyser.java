package trader.common.exchangeable.price.chanlun;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.chanlun.TickPattern.Type;

/**
 * 分析线段相关代码
 */
class SectionAnalyser extends Analyser {
    private List<TickPattern> strokes;
    private LinkedList<TickPattern> section;
    private List<TickPattern> unparsedStroke;

    SectionAnalyser(List<TickPattern> strokes){
        this.strokes = strokes;
    }

    List<TickPattern> parse(AnalyserOptions options) {
        if ( strokes.size()<=3 && !detect3Strokes(strokes)){
            return Collections.EMPTY_LIST;
        }
        //strokes = normalizeStrokes(strokes);
        unparsedStroke = new LinkedList<>(strokes);
        section = new LinkedList<>();
        while (unparsedStroke.size() > 0) {
            boolean sectionDirection = unparsedStroke.get(0).getDirection();
            TickPattern sectionEndStroke = detectSectionEnd(unparsedStroke);
            if (sectionEndStroke == null) { // 线段未结束
                section.add(createSection(sectionDirection, unparsedStroke));
                break;
            }
            int sectionEndIndex = unparsedStroke.indexOf(sectionEndStroke);
            List<TickPattern> sectionPatterns = unparsedStroke.subList(0, sectionEndIndex);
            section.add(createSection(sectionDirection, sectionPatterns));
            unparsedStroke = unparsedStroke.subList(sectionEndIndex, unparsedStroke.size());
            assert (unparsedStroke.get(0) == sectionEndStroke);
        }
        for(TickPattern section:section){
            assert(section.getOpen()!=0);
            assert(section.getClose()!=0);
        }
        return section;
    }

    static boolean detect3Strokes(List<TickPattern> strokes){
        if ( strokes.size()<3 ){
            return false;
        }
        return isTickOverlapped(strokes.get(0), strokes.get(1), strokes.get(2));
    }

    /**
     * 对笔划列表进行判断, 剔除不属于第一个线段开始前的笔划
     */
    List<TickPattern> normalizeStrokes(List<TickPattern> strokes) {
        if ( strokes.size()<=3 ){
            return strokes;
        }
        List<TickPattern> result = new LinkedList<>(strokes);
        TickPattern initPattern = result.get(0);
        boolean initDirection = initPattern.getDirection();
        boolean seqDirection = false;

        List<TickPattern> eigenStrokes = parseEigenSequence(strokes);
        if (eigenStrokes.size() >= 2) {
            TickPattern s0 = eigenStrokes.get(0);
            TickPattern s1 = eigenStrokes.get(1);
            if (isTickContains(s0, s1) || isTickContains(s1, s0)) {
                return result;
            }
        }
        List<TickPattern> nomalizedEigenStroke = normalizePatterns(eigenStrokes);
        if (nomalizedEigenStroke.size() >= 2) {
            TickPattern s0 = nomalizedEigenStroke.get(0);
            TickPattern s1 = nomalizedEigenStroke.get(1);
            if ( s0.getHigh() > s1.getHigh()) {
                seqDirection = false;
            } else {
                // 从低到高
                seqDirection = true;
            }
        }

        if (initDirection != seqDirection) {
            result.remove(0);
        }
        return result;
    }

    TickPattern createSection(boolean sectionDirection, List<TickPattern> sectionStrokes) {
        LocalDateTime beginDate = null;
        LocalDateTime endDate = null;
        beginDate = sectionStrokes.get(0).getBeginTime();
        endDate = sectionStrokes.get(sectionStrokes.size() - 1).getEndTime();
        return TickPattern.create(TickPattern.Type.Section, sectionDirection, beginDate, endDate,
                sectionStrokes.toArray(new TickPattern[sectionStrokes.size()]));
    }

    /**
     * 判断线段结束位置
     *
     * @param strokePatterns
     * @return 返回找到的结束点, NULL代表线段尚未结束
     */
    private TickPattern detectSectionEnd(List<TickPattern> strokePatterns) {
        // 先针对目前的笔划序列进行特征序列提取,找分形.
        TickPattern result = null;
        TickPattern firstStroke = strokePatterns.get(0);
        boolean strokeDirection = firstStroke.getDirection();
        LinkedList<TickPattern> eigenStrokes = parseEigenSequence(strokePatterns);
        if ( eigenStrokes.size()==0 ){
            return null;
        }
        TickPattern absEnd = detectSectionAbsEnd(firstStroke, eigenStrokes);
        int absEndIndex = -1;
        if ( absEnd!=null ){
            absEndIndex = strokePatterns.indexOf(absEnd);
            if ( absEndIndex==1 ){
                return absEnd;
            }
        }

        LinkedList<TickPattern> normalizedEigenStrokes = new LinkedList<>();
        while( normalizeNextStroke(eigenStrokes, normalizedEigenStrokes) ){
            int nsize = normalizedEigenStrokes.size();
            if( nsize<2 ){
                continue;
            }
            TickPattern s0 = null;
            if ( nsize>=3 ){
                s0 = normalizedEigenStrokes.get(nsize-3);
            }
            TickPattern s1 = normalizedEigenStrokes.get(nsize-2);
            TickPattern s2 = normalizedEigenStrokes.get(nsize-1);
            if ( (strokeDirection&& s1.getLow()<=s2.getLow())
                 ||(!strokeDirection && s1.getHigh()>=s2.getHigh()) ) {
                continue;
            }
            // 找到分形,检查是否存在缺口
            if (!inSequenceGap(s0, s1)) {
                result = s1; // 无缺口, 第二个元素就是线段终点
                break;
            }
            // 有缺口,进一步检查
            if (nextReverseFractal(strokePatterns, s1)) {
                // 存在相反方向的分形
                result = s1;
                break;
            }
        }

        if ( result==null ){
            return absEnd;
        }
        // 如果是存在包含关系的笔划
        while (result.getPatterns().get(0).getType() == TickPattern.Type.Stroke) {
            long high = result.getHigh();
            long low = result.getLow();
            List<TickPattern> subpatterns = result.getPatterns();
            for (TickPattern pattern: subpatterns) {
                if (strokeDirection) {
                    if ((high == pattern.getHigh())) {
                        result = pattern;
                        break;
                    }
                } else {
                    if ((low == pattern.getLow())) {
                        result = pattern;
                        break;
                    }
                }
            }
        }
        // 检测线段虚假破坏
        int resultIndex = strokePatterns.indexOf(getMaxOfCompositeStroke(result));
        List<TickPattern> remainingPatterns = strokePatterns.subList(resultIndex, strokePatterns.size());
        if (!detectInvalidNextSectionDirection(firstStroke, remainingPatterns)) {
            List<TickPattern> xxxPatterns = strokePatterns.subList(resultIndex + 1, strokePatterns.size());
            result = detectSectionEnd2(xxxPatterns);
        }
        if ( absEnd!=null && resultIndex>absEndIndex ){ //线段结束在绝对结束之后
            return absEnd;
        }
        return result;
    }

    private boolean normalizeNextStroke(LinkedList<TickPattern> eigenStrokes, LinkedList<TickPattern> normalizedEigenStrokes)
    {
        if ( eigenStrokes.isEmpty() ){
            return false;
        }
        TickPattern strokeToAppend = eigenStrokes.pollFirst();
        TickPattern lastStroke = normalizedEigenStrokes.peekLast();
        if ( lastStroke!=null && isTickContains(lastStroke, strokeToAppend)) {
            assert(lastStroke.getDirection()==strokeToAppend.getDirection());
            normalizedEigenStrokes.pollLast();
            // 以向上笔开始的线段的特征序列，只考察顶分型,在处理包含关系的时候，按照方向向上处理
            TickPattern compositeStroke = TickPattern.createComposite(TickPattern.Type.Stroke, !lastStroke.getDirection(), lastStroke, strokeToAppend);
            strokeToAppend = compositeStroke;
        }
        normalizedEigenStrokes.offer(strokeToAppend);
        return true;
    }

    /**
     * 判断线段结束位置
     *
     * @param strokePatterns
     * @return 返回找到的结束点, NULL代表线段尚未结束
     */
    private TickPattern detectSectionEnd2(List<TickPattern> strokePatterns) {
        // 先针对目前的笔划序列进行特征序列提取,找分形.
        TickPattern result = null;
        TickPattern firstStroke = strokePatterns.get(0);
        boolean strokeDirection = firstStroke.getDirection();
        List<TickPattern> eigenStrokes = parseEigenSequence(strokePatterns);
        if ( eigenStrokes.size()==0 ){
            return null;
        }
        TickPattern absEnd = detectSectionAbsEnd(firstStroke, eigenStrokes);
        int absEndIndex = -1;
        if ( absEnd!=null ){
            absEndIndex = strokePatterns.indexOf(absEnd);
            if ( absEndIndex==1 || absEndIndex==3 ){
                return absEnd;
            }
        }
        absEnd = detectSectionEndAtFirstStroke();
        if ( absEnd!=null ){
            return absEnd;
        }
        List<TickPattern> normalizedEigenStrokes = normalizePatterns(eigenStrokes);
        if ( normalizedEigenStrokes.size()>=2 ){
            TickPattern s0 = normalizedEigenStrokes.get(0);
            TickPattern s1 = normalizedEigenStrokes.get(1);
            if ( (!strokeDirection && s0.getHigh()<s1.getHigh())
                 || (strokeDirection && s0.getLow()>s1.getLow()) )
            {
                result = s0;
            }
        }
        if ( result==null ){
            List<TickPattern> canonicalStrokes = new LinkedList<>();
            for (TickPattern s2 : normalizedEigenStrokes) {
                if (canonicalStrokes.size() < 2) {
                    canonicalStrokes.add(s2);
                    continue;
                }
                TickPattern s0 = canonicalStrokes.get(canonicalStrokes.size() - 2);
                TickPattern s1 = canonicalStrokes.get(canonicalStrokes.size() - 1);
                result = detectSectionEndSpecial(strokeDirection, s0, s1, s2);
                if (result != null) {
                    break;
                }
                if (!isFractal(strokeDirection, s0, s1, s2)) {
                    canonicalStrokes.add(s2);
                    continue;
                }
                // 找到分形,检查是否存在缺口
                if (!inSequenceGap(s0, s1)) {
                    result = s1; // 无缺口, 第二个元素就是线段终点
                    break;
                }
                // 有缺口,进一步检查
                if (nextReverseFractal(strokePatterns, s1)) {
                    // 存在相反方向的分形
                    result = s1;
                    break;
                }
                canonicalStrokes.add(s2);
            }
        }
        if (result == null) {
            return absEnd;
        }
        // 如果是存在包含关系的笔划
        while (result.getPatterns().get(0).getType() == TickPattern.Type.Stroke) {
            long high = result.getHigh();
            long low = result.getLow();
            List<TickPattern> subpatterns = result.getPatterns();
            for (TickPattern pattern: subpatterns) {
                if (strokeDirection) {
                    if ((high == pattern.getHigh())) {
                        result = pattern;
                        break;
                    }
                } else {
                    if ((low == pattern.getLow())) {
                        result = pattern;
                        break;
                    }
                }
            }
        }
        // 检测线段虚假破坏
        int resultIndex = strokePatterns.indexOf(getMaxOfCompositeStroke(result));
        List<TickPattern> remainingPatterns = strokePatterns.subList(resultIndex, strokePatterns.size());
        if (!detectInvalidNextSectionDirection(firstStroke, remainingPatterns)) {
            List<TickPattern> xxxPatterns = strokePatterns.subList(resultIndex + 1, strokePatterns.size());
            result = detectSectionEnd2(xxxPatterns);
        }
        if ( absEnd!=null && resultIndex>absEndIndex ){ //线段结束在绝对结束之后
            return absEnd;
        }
        return result;
    }

    private static TickPattern getMaxOfCompositeStroke(TickPattern stroke){
        if ( stroke.getPatterns().get(0).getType()!=Type.Stroke ){
            return stroke;
        }
        TickPattern p1 = stroke.getPatterns().get(0);
        TickPattern p2 = stroke.getPatterns().get(1);
        if ( p1.getHighPoint().isHigher(p2.getHighPoint()) && p1.getLowPoint().isLower(p2.getLowPoint())){
            return p1;
        }
        if ( p2.getHighPoint().isHigher(p1.getHighPoint()) && p2.getLowPoint().isLower(p1.getLowPoint())){
            return p2;
        }
        throw new RuntimeException("Should not run here");
    }

    /**
     * 检测线段绝对破坏,返回最后一根线段
     */
    private static TickPattern detectSectionAbsEnd(TickPattern firstStroke, List<TickPattern> charStrokes){
        TickPattern absEnd = null;
        for(TickPattern c:charStrokes){
            assert(firstStroke.getDirection() != c.getDirection());
            if ( firstStroke.getDirection() ){
                //first /, c \
                if ( firstStroke.getBeginPoint().getPrice() > c.getEndPoint().getPrice() ){
                    return c;
                }
            }else{//first \, c /
                if ( firstStroke.getBeginPoint().getPrice() < c.getEndPoint().getPrice() ){
                    return c;
                }
            }
        }
        return absEnd;
    }

    /**
     * 检查是否下一个线段的方向与第一根笔划方向相反, 防止线段的虚假破坏
     */
    private boolean detectInvalidNextSectionDirection(TickPattern sectionFirst, List<TickPattern> strokePatterns) {
        TickPattern firstStroke = strokePatterns.get(0);
        long sectionOpen = sectionFirst.getOpen();
        long sectionNextOpen = firstStroke.getOpen();
        long sectionNextClose = firstStroke.getClose();
        boolean strokeDirection = firstStroke.getDirection();
        List<TickPattern> characteristicSequence = parseEigenSequence(strokePatterns);
        List<TickPattern> canonicalSequence = normalizePatterns(characteristicSequence);
        if (canonicalSequence.size() <= 1) {
            return true;
        }
        TickPattern c0 = canonicalSequence.get(0);
        TickPattern c1 = canonicalSequence.get(1);
        List<TickPattern> c0Patterns = c0.getPatterns();
        boolean sequenceDirection = true;
        if ((c1.getHigh() > c0.getHigh())) {
            sequenceDirection = true;
        } else if ((c1.getLow()< c0.getLow())) {
            sequenceDirection = false;
        }
        // 如果方向不对,且第一根笔划不是复合笔划
        if (sequenceDirection == strokeDirection) {
            return true;
        }
        // 要求第一根笔划不能是复合笔划
        if (c0Patterns.get(0).getType() == TickPattern.Type.Stroke) {
            return true;
        }
        // 要求破坏的笔划不能低于(向上)/高于(向下)线段的初始位置
        // 且后续线段部分结束位置要高于(向上)/低于(向下)当前线段结束位置
        List<TickPattern> xxxPatterns = strokePatterns.subList(1, strokePatterns.size());
        TickPattern nextEnd = detectSectionEnd2(xxxPatterns);
        if (nextEnd == null) {
            return true;
        }
        long nextEndValue = nextEnd.getOpen();
        if (sequenceDirection && (nextEndValue >= sectionNextOpen)
                && (sectionNextClose>= sectionOpen)) {
            return false;
        }
        if (!sequenceDirection && (nextEndValue<= sectionNextOpen)
                && (sectionNextClose<= sectionOpen)) {
            return false;
        }
        return true;
    }

    /**
     * 检查是不是符合如下规则: <BR>
     * 如果线段的原始特征序列的第N根被第N+1根包含,且与第N+2根形成顶分形(底分形,视线段方向而定),则线段在第N+1根处结束.
     */
    private static TickPattern detectSectionEndSpecial(boolean strokeDirection, TickPattern s0, TickPattern s1, TickPattern s2) {
        if (s0.getSubType() != TickPattern.Type.Stroke) {
            return null;
        }
        List<TickPattern> characterSequence = new LinkedList<>();
        characterSequence.addAll(s0.getPatterns());
        characterSequence.addAll(s1.getPatterns(TickPattern.Type.Stroke));
        if (s2 != null) {
            characterSequence.addAll(s2.getPatterns(TickPattern.Type.Stroke));
        }
        TickPattern s00 = characterSequence.get(0);
        TickPattern s01 = characterSequence.get(1);
        if (isTickContains(s01, s00)) {
            List<TickPattern> subCanonicalSeq = new LinkedList<>(characterSequence);
            subCanonicalSeq.remove(0);
            subCanonicalSeq = normalizePatterns(subCanonicalSeq);
            s01 = subCanonicalSeq.get(0);
            if (subCanonicalSeq.size() < 2) {
                return null;
            }
            TickPattern s02 = subCanonicalSeq.get(1);
            //assert (!priceContains(s01, s02));
            {
                if (strokeDirection) {
                    // 向上线段, 第3根特征线段的低点必须低于第2根
                    if ((s02.getLow()< s01.getLow())) {
                        return s01;
                    }
                } else {
                    // 向下线段, 第3根特征线段的高点必须高于第2根
                    if ((s02.getHigh()> s01.getHigh())) {
                        return s01;
                    }
                }
            }
        }
        if ( strokeDirection && s0.getHighPoint().isHigher(s1.getHighPoint()) && s1.getHighPoint().isHigher(s2.getHighPoint())){
            return s0;
        }
        if ( !strokeDirection && s0.getLowPoint().isLower(s1.getLowPoint()) && s1.getLowPoint().isLower(s2.getLowPoint())){
            return s0;
        }
        return null;
    }

    /**
     * 探测缺口/第一个笔划就结束线段，出现在第一个笔划波动巨大(超过上一个线段)，后续至少3条笔划全部相反的情况
     * <BR>
     * firstStroke.height >= lastSection.height
     * 2ndStroke.height >= firstStroke.height *2/3
     * (4thStroke.low <= secondStroke.low) || (4thStroke.high>=2ndStroke.high)
     */
    private TickPattern detectSectionEndAtFirstStroke(){
        TickPattern lastSection = this.section.peekLast();
        if ( lastSection==null || unparsedStroke.size()<4 ){
            return null;
        }
        TickPattern firstStroke = unparsedStroke.get(0);
        TickPattern secondStroke = unparsedStroke.get(1);
        TickPattern forthStroke = unparsedStroke.get(3);
        long firstHeight = Math.abs(firstStroke.getHigh()-firstStroke.getLow());
        long secondHeight = Math.abs(secondStroke.getHigh()-secondStroke.getLow());

        if ( firstHeight >= Math.abs(lastSection.getOpen()-lastSection.getClose())
             && secondHeight >= firstHeight*2/3)
        {
            if ( firstStroke.getDirection() && forthStroke.getLow()<=secondStroke.getLow() ){
                return secondStroke;
            }
            if ( !firstStroke.getDirection() && forthStroke.getHigh()>=secondStroke.getHigh() ){
                return secondStroke;
            }
            return null;
        }
        return null;
    }

    /**
     * 探测笔划的顶点
     */
    private static TickPattern detectStrokeTopPoint(boolean directionLong, TickPattern... patterns) {
        List<TickPattern> myPatterns = new LinkedList<>();
        for (int i = 0; i < patterns.length; i++) {
            List<TickPattern> subPatterns = patterns[i].getPatterns();
            if (subPatterns.get(0).getType() == TickPattern.Type.Stroke) {
                myPatterns.addAll(subPatterns);
            } else {
                myPatterns.add(patterns[i]);
            }
        }
        TickPattern topPattern = myPatterns.get(0);
        for (TickPattern p : myPatterns) {
            if (directionLong) {
                if ((p.getHigh()> topPattern.getHigh())) {
                    topPattern = p;
                }
            } else {
                if ((p.getLow()< topPattern.getLow())) {
                    topPattern = p;
                }
            }
        }

        return topPattern;
    }

    /**
     * 检查含有缺口的分形后续是否存在相反方向的分形.
     *
     * <BR>特征序列的顶分型中，第一和第二元素间存在特征序列的缺口，
     * 如果从该分型最高点开始的向下一笔开始的序列的特征序列出现底分型，
     * 那么该线段在该顶分型的高点处结束，该高点是该线段的终点；
     *
     * <BR>特征序列的底分型中，第一和第二元素间存在特征序列的缺口，
     * 如果从该分型最低点开始的向上一笔开始的序列的特征序列出现顶分型，
     * 那么该线段在该底分型的低点处结束，该低点是该线段的终点
     *
     */
    private boolean nextReverseFractal(List<TickPattern> strokePatterns, TickPattern topStroke) {
        boolean sectionDir = strokePatterns.get(0).getDirection();
        topStroke = getTopStroke(sectionDir, topStroke); //向上线段，寻找最高点, 向下线段，寻找最低点
        // 从最高点开始的下一个笔划序列
        LinkedList<TickPattern> nextStrokes = getPatternsAfter(strokePatterns, topStroke);
        boolean nextDirection = nextStrokes.get(0).getDirection();
        // 下一个特征序列
        List<TickPattern> reverseCharSequence = getPatternsByDirection(nextStrokes, !sectionDir);
        List<TickPattern> nextCanonicalSequence = normalizePatterns(reverseCharSequence);

        assert (nextStrokes.get(0) == topStroke);
        assert (sectionDir != topStroke.getDirection());
        assert (sectionDir != nextDirection);
        assert (sectionDir != reverseCharSequence.get(0).getDirection());

        if (nextCanonicalSequence.size() >= 3) {
            TickPattern ns0 = nextCanonicalSequence.get(0);
            TickPattern ns1 = nextCanonicalSequence.get(1);
            TickPattern ns2 = nextCanonicalSequence.get(2);
            if (detectSectionEndSpecial(nextDirection, ns0, ns1, ns2) != null) {
                // ns0+ns1就已经构成分形
                return true;
            }
            // 相反方向的分形
            if (isFractal(nextDirection, ns0, ns1, ns2)) {
                return true;
            }
            if (nextDirection) {
                if ( (ns1.getHigh()> ns0.getHigh())
                        && (ns2.getHigh()> ns1.getHigh())) {
                    return true;
                }
            } else {
                if ((ns1.getLow()< ns0.getLow()) && (ns2.getLow()< ns1.getLow())) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean inSequenceGap(TickPattern t, TickPattern t2){
        if ( t==null ){
            return false;
        }
        if ( (t.getLow() > t2.getHigh()) || (t.getHigh()< t2.getLow()) ){
            return true;
        }
        return false;
    }

    /**
     * 分析笔划特征序列
     */
    private static LinkedList<TickPattern> parseEigenSequence(List<TickPattern> patterns) {
        // 分析特征序列
        LinkedList<TickPattern> characterSequence = new LinkedList<>();
        boolean firstDirection = patterns.get(0).getDirection();
        int index = 0;
        for (TickPattern pattern: patterns) {
            if (index % 2 == 1) {
                characterSequence.add(pattern);
            }
            index++;
        }
        for (TickPattern pattern : characterSequence) {
            assert (pattern.getDirection() != firstDirection);
        }
        return characterSequence;
    }

    private static LinkedList<TickPattern> getPatternsByDirection(List<TickPattern> patterns, boolean dir){
        LinkedList<TickPattern> result = new LinkedList<>();
        for (TickPattern pattern: patterns) {
            if ( pattern.getDirection()==dir){
                result.add(pattern);
            }
        }
        return result;
    }

    private static LinkedList<TickPattern> getPatternsAfter(List<TickPattern> patterns, TickPattern anchor){
        LinkedList<TickPattern> result = new LinkedList<>();
        boolean foundAnchor = false;
        for (TickPattern pattern: patterns) {
            if ( pattern == anchor){
                foundAnchor = true;
            }
            if ( foundAnchor ){
                result.add(pattern);
            }
        }
        return result;
    }

    /**
     * 特征序列去包含化
     */
    private static List<TickPattern> normalizePatterns(List<TickPattern> characteristicSequence) {
        boolean firstDirection = characteristicSequence.get(0).getDirection();
        // 特征序列去包含化
        LinkedList<TickPattern> canonicalStrokes = new LinkedList<>();
        for (TickPattern stroke : characteristicSequence) {
            if (canonicalStrokes.size() == 0) {
                canonicalStrokes.add(stroke);
                continue;
            }

            TickPattern strokeToAppend = stroke;
            while(!canonicalStrokes.isEmpty()){
                TickPattern lastStroke = canonicalStrokes.peekLast();
                if (!isTickContains(lastStroke, strokeToAppend)) {
                    break;
                }
                canonicalStrokes.pollLast();
                assert(firstDirection==lastStroke.getDirection());
                assert(firstDirection==strokeToAppend.getDirection());
                // 以向上笔开始的线段的特征序列，只考察顶分型,在处理包含关系的时候，按照方向向上处理
                TickPattern compositeStroke = TickPattern.createComposite(TickPattern.Type.Stroke, !firstDirection, lastStroke, strokeToAppend);
                strokeToAppend = compositeStroke;
            }
            canonicalStrokes.add(strokeToAppend);
        }

        // 校验
        for (TickPattern pattern: canonicalStrokes) {
            assert (pattern.getDirection() == firstDirection);
        }
        return canonicalStrokes;
    }

    private static TickPattern getTopStroke(boolean direction, TickPattern compositeStroke) {
        List<TickPattern> sublist = compositeStroke.getPatterns();
        if (!(sublist.get(0).getType() == TickPattern.Type.Stroke)) {
            return compositeStroke;
        }
        TickPattern result = sublist.get(0);
        for (int i = 1; i < sublist.size(); i++) {
            TickPattern p = sublist.get(i);
            if (direction) {
                if ( p.getHigh() > result.getHigh()) {
                    result = p;
                }
            } else {
                if ((p.getLow()< result.getLow())) {
                    result = p;
                }
            }
        }
        return getTopStroke(direction, result);
    }

}