package trader.common.exchangeable.price.chanlun;

import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.price.PriceLevel;

public class AnalyserOptions {
    /**
     * 忽略日间开盘跳空.
     * <BR>影响: KLineAnalyser, FractalAnalyser, StrokeAnalyser
     */
    public boolean ignoreOpenGap = true;

    /**
     * 不生成中枢.极值点
     */
    public boolean ignoreCentreEdge = true;

    /**
     * create Edge only
     */
    public boolean createCentreEdgeOnly = false;

    Exchangeable exchangeable;

    PriceLevel priceLevel;
}
