package trader.common.exchangeable.price;

import java.time.LocalDateTime;

import trader.common.util.DateUtil;

/**
 * 在某个时刻的价量信息
 * READONLY
 */
public class Point implements Comparable<Point> {

    protected LocalDateTime time;
    protected long price;
    protected long volume;
    protected long turnover;

    protected Point(){
    }

    public Point(LocalDateTime time, long price){
        this(time, price, 0, 0);
    }

    public Point(LocalDateTime time, long price, long volume, long turnover){
        this.time = time;
        this.price = price;
        this.volume = volume;
        this.turnover = turnover;
    }

    public LocalDateTime getTime(){
        return time;
    }

    public long getPrice(){
        return price;
    }

    protected void setPrice(long price){
        this.price = price;
    }

    public long getTurnover(){
        return turnover;
    }

    protected void setTurnover(long v){
        this.turnover = v;
    }

    public long getVolume(){
        return volume;
    }

    protected void setVolume(long v){
        this.volume = v;
    }

    protected void setTime(LocalDateTime t)
    {
        time = t;
    }

    public boolean isLower(Point o){
        if ( o==null ){
            return false;
        }
        return getPrice()<o.getPrice();
    }

    public boolean isHigher(Point o){
        if ( o==null ){
            return false;
        }
        return getPrice()>o.getPrice();
    }

    public boolean isLowOrEquals(Point o){
        if ( o==null ){
            return false;
        }
        return getPrice()<=o.getPrice();
    }

    public boolean isHighOrEquals(Point o){
        if ( o==null ){
            return false;
        }
        return getPrice()>=o.getPrice();
    }

    public boolean isBefore(Point o){
        //assert(getTime().toLocalDate().equals(o.getTime().toLocalDate()));
        return getTime().isBefore(o.getTime());
    }

    public boolean isAfter(Point o){
        //assert(getTime().toLocalDate().equals(o.getTime().toLocalDate()));
        return getTime().isAfter(o.getTime());
    }

    @Override
    public int compareTo(Point o)
    {
        return (int)(getPrice()-o.getPrice());
    }

    @Override
    public boolean equals(Object o){
        if ( o==null || !(o instanceof Point) ){
            return false;
        }
        Point p = (Point)o;
        return time.equals(p.time)
                && price==p.price
                && volume==p.volume
                && turnover==p.turnover;
    }

    @Override
    public String toString(){
        return "Point["+DateUtil.time2hhmmss(getTime().toLocalTime())+":"+getPrice()+"]";
    }

    public static Point max(Point ...points){
        Point r = points[0];
        for(Point p:points){
            if ( r.isLower(p)){
                r = p;
            }
        }
        return r;
    }

    public static Point min(Point ...points){
        Point r = points[0];
        for(Point p:points){
            if ( r.isHigher(p)){
                r = p;
            }
        }
        return r;
    }

    public Point changePrice(long newPrice){
        Point result = new Point(getTime(), newPrice, getVolume(), getTurnover());
        return result;
    }

    public Point toPoint(){
        Point result = new Point( getTime(), getPrice() );
        return result;
    }

}
