package trader.common.exchangeable.price.chanlun;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

public class Analyser {

    /**
     * 中枢是否和某个次级别走势有重合
     */
    static boolean isCentrumOverlapped(TickPatternCentre centrum, TickPattern nextSection)
    {
        assert(centrum.getType()==Type.Centre);
        assert(centrum.getPatterns().get(0).getType()==nextSection.getType());
        assert( nextSection.getHighPoint().isHigher(nextSection.getLowPoint()));

        Point high = centrum.getHighPoint();
        Point low = centrum.getLowPoint();
        if ( high.isLower(nextSection.getLowPoint()) || low.isHigher(nextSection.getHighPoint())){
            return false;
        }
        return true;
    }

    /**
     * 中枢是否可以和下一中枢扩展
     */
    static boolean isCentreExpandable(TickPatternCentre c1, TickPatternCentre c2)
    {
        if (c1.level!=c2.level)
            return false;

        long gg = Math.max(c1.getHigh(), c2.getHigh());
        long dd = Math.min(c1.getLow(), c2.getLow());

        return (gg-dd) < (c1.getHigh()-c1.getLow()) + (c2.getHigh()-c2.getLow());
    }

    static Point getAvg(Tick p){
        Point h = p.getHighPoint();
        Point l = p.getLowPoint();
        return new Point(h.getTime(), (h.getPrice()+l.getPrice())/2);
    }

    static boolean isShort(TickPattern p){
        return p!=null && !p.getDirection();
    }

    static boolean isLong(TickPattern p){
        return p!=null && p.getDirection();
    }

    static boolean isLongLine(TickPattern p){
        return p!=null && p.getType()==Type.Line && p.getDirection();
    }

    static boolean isShortLine(TickPattern p){
        return p!=null && p.getType()==Type.Line && !p.getDirection();
    }

    static boolean isLongFractal(TickPattern p){
        return p!=null && p.getType()==Type.Fractal && p.getDirection();
    }

    static boolean isShortFractal(TickPattern p){
        return p!=null && p.getType()==Type.Fractal && !p.getDirection();
    }

    static boolean isFractal(TickPattern p){
        return p!=null && p.getType()==Type.Fractal;
    }

    static boolean isLine(TickPattern p){
        return p!=null && p.getType()==Type.Line;
    }

    /**
     * f2 higher than f1
     * @param f1
     * @param f2
     * @return
     */
    static boolean isFractalHigher(TickPattern f1, TickPattern f2){
        if ( f1!=null
                && f2!=null
                && f1.getType()==Type.Fractal
                && f2.getType()==Type.Fractal
                && f2.getTopPoint().getPrice() > f1.getTopPoint().getPrice()
                )
        {
            return true;
        }
        return false;
    }

    static boolean isSameFractal(TickPattern f1, TickPattern f2){
        return isFractal(f1) && isFractal(f2) && (f1.getDirection()==f2.getDirection());
    }


    /**
     * 寻找是否出现分形
     *
     * @param direction
     *            true 顶分形,false 底分形
     * @return
     */
    static boolean isFractal(boolean direction, Tick p0, Tick p1, Tick p2)
    {
        if (direction) {
            if ( (p1.getHigh() > p0.getHigh()) && (p1.getHigh() > p2.getHigh()) ){
                assert((p1.getLow()> p0.getLow()) && (p1.getLow()> p2.getLow()));
                return true;
            }else{
                return false;
            }
        } else {
            if ((p1.getLow()< p0.getLow()) && (p1.getLow()< p2.getLow()) ) {
                assert( (p1.getHigh()<p0.getHigh()) && (p1.getHigh()<p2.getHigh()) );
                return true;
            } else {
                return false;
            }
        }
    }

    static TickPattern nextPattern(LinkedList<TickPattern> patterns, TickPattern after){
        if ( after==null ){
            return patterns.peekFirst();
        }
        boolean foundAfter =false;
        for(TickPattern p:patterns){
            if ( p==after ){
                foundAfter = true;
                continue;
            }
            if ( foundAfter ){
                return p;
            }
        }
        return null;
    }

    static TickPattern lastFractal(LinkedList<TickPattern> patterns){
        for(Iterator<TickPattern> it=patterns.descendingIterator(); it.hasNext(); ){
            TickPattern p = it.next();
            if ( isFractal(p) ){
                return p;
            }
        }
        return null;
    }

    static TickPattern nextFractal(LinkedList<TickPattern> patterns, TickPattern after){
        boolean foundAfter = true;
        if( after!=null ){
            foundAfter = false;
        }
        for(TickPattern p:patterns){
            if ( after!=null && p==after ){
                foundAfter = true;
                continue;
            }
            if ( foundAfter && isFractal(p) ){
                return p;
            }
        }
        return null;
    }

    static boolean isTickContains(Tick price, Tick price2)
    {
        if (price instanceof CompositeTick) {
            // long priceHigh = ((SecurityCompositePrice)
            // price).getHighPoint();
            // long priceLow = ((SecurityCompositePrice)
            // price).getLowPoint();
            long priceHigh = price.getHigh();
            long priceLow = price.getLow();
            return ((priceHigh>= price2.getHigh()) && (priceLow<=price2.getLow()))
                    || ((price2.getHigh()>= priceHigh) && (price2.getLow()<= priceLow));
        } else {
            return ((price.getHigh() >= price2.getHigh()) && (price.getLow()<= price2.getLow()))
                    || ((price2.getHigh()>= price.getHigh()) && (price2.getLow()<= price.getLow()));
        }
    }

    static boolean isTickOverlapped(Tick ...ticks){
        if ( ticks.length<3 ){
            return false;
        }
        //检查3段是否重叠部分
        Tick s1 = ticks[0];
        Tick s2 = ticks[1];
        Tick s3 = ticks[2];
        //S1 VS S2
        long h1 = Math.min(s1.getHigh(), s2.getHigh());
        long l1 = Math.max(s1.getLow(), s2.getLow());

        //S2 VS S3
        long h2 = Math.min(s2.getHigh(), s3.getHigh());
        long l2 = Math.max(s3.getLow(), s3.getLow());

        //now check h1-l1 and h2-l2
        long hx = Math.min(h1, h2);
        long lx = Math.max(l1, l2);
        if ( hx<lx ){
            //无重叠
            return false;
        }
        return true;
    }

    /**
     * t的交易时段是交易日的开始.
     *
     * TODO 需要处理夜盘
     */
    static boolean isOpenTick(Tick last, Tick tick)
    {
        if ( last==null ){
            return false;
        }
        LocalDate lastDate = last.getEndTime().toLocalDate();
        LocalDate openDate = tick.getBeginTime().toLocalDate();
        if ( !lastDate.equals(openDate) ){
            return true;
        }
        return false;
    }

    static TickPattern getHighestTick(TickPattern ...ticks){
        TickPattern high = ticks[0];
        for(TickPattern t:ticks){
            if ( high.getHighPoint().isLower(t.getHighPoint())){
                high = t;
            }
        }
        return high;
    }

    static TickPattern getLowestTick(TickPattern ...ticks){
        TickPattern low = ticks[0];
        for(TickPattern t:ticks){
            if ( low.getLowPoint().isHigher(t.getLowPoint())){
                low = t;
            }
        }
        return low;
    }

}
