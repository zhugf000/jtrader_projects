package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;

/**
 * 分析K线合并相关代码
 */
public class TickAnalyser extends Analyser {

	private TimeSeries timeSeries;
	private LinkedList<Tick> ticks;

	public TickAnalyser(TimeSeries timeSeries){
		this.timeSeries = timeSeries;
	}

	public List<Tick> parse(AnalyserOptions options){
		if ( ticks ==null ){
			processTicks(options);
		}
		return ticks;
	}

	/**
	 * 对原始K线处理包含关系
	 */
	private void processTicks(AnalyserOptions options) {
		ticks = new LinkedList<>();
		ticks.add(timeSeries.getTick(0));
		for(int i=1;i<timeSeries.getTickCount();i++){
			Tick lt = ticks.peekLast();
			Tick t = timeSeries.getTick(i);
			if ( !options.ignoreOpenGap && isOpenTick(lt, t)){
				ticks.add(t);
				continue;
			}
			if (isTickContains(lt, t)) {
				lineCombineLast(t);
			} else {
				ticks.add(t);
			}
		}
	}

	private void lineCombineLast(Tick price2) {
		Tick price = ticks.removeLast();
		Tick price0 = null;
		if (ticks.size() > 0) {
			price0 = ticks.peekLast();
		}
		boolean isLong = true;
		if (price0 != null && (price.getHigh() > price0.getHigh())) {
			isLong = true;
		}
		if (price0 != null && (price.getLow() < price0.getLow())) {
			isLong = false;
		}
		ticks.add(new CompositeTick(price, price2, isLong));
	}

}