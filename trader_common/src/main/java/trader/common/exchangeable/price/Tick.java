package trader.common.exchangeable.price;

import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import trader.common.util.DateUtil;
import trader.common.util.PriceUtil;

/**
 * 一段时间的价量信息
 */
public class Tick extends Point {
    protected Duration duration;

    protected Point beginPoint;
    protected Point endPoint;
    protected Point highPoint;
    protected Point lowPoint;

    public Tick(LocalDateTime beginTime, LocalDateTime endTime, long open, long high, long low, long close, long volume, long turnover){
        beginPoint = new Point(beginTime, open);
        endPoint = new Point(endTime, close);
        highPoint = new Point(endTime, high);
        lowPoint = new Point(endTime, low);
        this.volume = volume;
        this.turnover = turnover;
    }

    public Tick(Point beginPoint, Point endPoint){
        setBeginPoint(beginPoint);
        setEndPoint(endPoint);
        if ( beginPoint.isHigher(endPoint)){
            setHighPoint(beginPoint);
            setLowPoint(endPoint);
        }else{
            setHighPoint(endPoint);
            setLowPoint(beginPoint);
        }
        volume = endPoint.getVolume() - beginPoint.getVolume();
        turnover = endPoint.getTurnover() - beginPoint.getTurnover();
    }

    protected Tick()
    {

    }

    public boolean getDirection(){
        return getOpen()<=getLow();
    }

    @Override
    public LocalDateTime getTime(){
        return getBeginPoint().getTime();
    }

    public Point getBeginPoint(){
        return beginPoint;
    }

    public Point getEndPoint(){
        return endPoint;
    }

    public Point getHighPoint(){
        return highPoint;
    }

    public Point getLowPoint(){
        return lowPoint;
    }

    public LocalDateTime getBeginTime(){
        return getBeginPoint().getTime();
    }

    public LocalDateTime getEndTime(){
        return getEndPoint().getTime();
    }

    public Duration getDuration(){
        if ( duration==null ){
            duration = Duration.between(getBeginTime(), getEndTime());
        }
        return duration;
    }

    public long getOpen(){
        return getBeginPoint().getPrice();
    }

    public long getClose(){
        return getEndPoint().getPrice();
    }

    public long getHigh(){
        return highPoint.getPrice();
    }

    public long getLow(){
        return lowPoint.getPrice();
    }

    protected void setBeginPoint(Point p){
        this.beginPoint = p;
    }

    protected void setEndPoint(Point p){
        this.endPoint = p;
    }

    protected void setHighPoint(Point p){
        this.highPoint = p;
    }

    protected void setLowPoint(Point p){
        this.lowPoint = p;
    }

    protected boolean showDateOnly(){
        LocalTime beginTime = getBeginPoint().getTime().toLocalTime();
        LocalTime endTime = getEndPoint().getTime().toLocalTime();
        return beginTime.toSecondOfDay()==0 && endTime.toSecondOfDay()==0;
    }

    /**
     * 更新价量数据.
     * <BR>注意这里的turnover/volume是累加的
     */
    public void update(Point lastPoint)
    {
        if ( beginPoint!= null){
            turnover = lastPoint.getTurnover()-beginPoint.getTurnover();
            volume = lastPoint.getVolume() - beginPoint.getVolume();
        }else{
            this.turnover += lastPoint.getTurnover();
            this.volume += lastPoint.getVolume();
            throw new RuntimeException("Immutable Tick, no begin point");
        }
        setEndPoint(lastPoint);
        if ( lastPoint.isHigher(getHighPoint())){
            setHighPoint(lastPoint);
        }
        if ( lastPoint.isLower(getLowPoint())){
            setLowPoint(lastPoint);
        }
        duration = null;
    }

    public Tick getTopPrice(){
        return this;
    }

    public void dump(String prefix, PrintWriter pw) {
        pw.print(prefix);
        pw.println(this);
    }

    @Override
    public String toString(){
        return "Tick["
                +( showDateOnly()? DateUtil.date2str(getTime().toLocalDate()): DateUtil.date2str(getTime()) )
                +", O "+PriceUtil.long2str(getOpen())
                +", C "+PriceUtil.long2str(getClose())
                +", H "+PriceUtil.long2str(getHigh())
                +", L "+PriceUtil.long2str(getLow())
                +", "+getTurnover()
                +", "+getVolume()+"]";
    }

    /**
     * 返回时间在前的点
     */
    public static Tick earlier(Tick p1, Tick p2){
        if ( p1.getBeginTime().isBefore(p2.getBeginTime()) ) {
            return p1;
        }
        return p2;
    }

    /**
     * 返回时间靠后的点
     * @param p1
     * @param p2
     * @return
     */
    public static Tick later(Tick p1, Tick p2){
        if ( p1.getBeginTime().isAfter(p2.getBeginTime()) ) {
            return p1;
        }
        return p2;
    }

    public static List<Tick> convertTo(List<Tick> ticks, int mergeRatio){
        List<Tick> result = new ArrayList<>(ticks.size()/mergeRatio);
        List<Tick> ticksToMerge = new ArrayList<>();
        for(Tick tick:ticks){
            ticksToMerge.add(tick);
            if ( ticksToMerge.size()>= mergeRatio ){
                result.add(Tick.merge(ticksToMerge));
                ticksToMerge.clear();
            }
        }
        if ( ticksToMerge.size()>0 ){
            result.add(Tick.merge(ticksToMerge));
            ticksToMerge.clear();
        }
        return result;
    }

    public static Tick merge(List<Tick> ticks){
        Tick result = new Tick(ticks.get(0).getBeginPoint(), ticks.get(ticks.size()-1).getEndPoint());
        Point highPoint = ticks.get(0).getHighPoint();
        Point lowPoint = ticks.get(0).getLowPoint();
        long turnover = 0;
        long volume = 0;
        for(Tick tick:ticks){
            if ( tick.getHighPoint().isHigher(highPoint)){
                highPoint = tick.getHighPoint();
            }
            if ( tick.getLowPoint().isLower(lowPoint)){
                lowPoint = tick.getLowPoint();
            }
            turnover += tick.getTurnover();
            volume += tick.getVolume();
        }
        result.setHighPoint(highPoint);
        result.setLowPoint(lowPoint);
        result.setTurnover(turnover);
        result.setVolume(volume);
        return result;
    }
}
