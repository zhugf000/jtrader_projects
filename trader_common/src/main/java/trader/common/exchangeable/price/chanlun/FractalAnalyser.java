package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Tick;

/**
 * 分析分形相关代码
 */
public class FractalAnalyser extends Analyser {

    private List<Tick> ticks;

    public FractalAnalyser(List<Tick> ticks){
        this.ticks = ticks;
    }

    /**
     * 将处理过的K线解析为顶底分形
     */
    public List<TickPattern> parse(AnalyserOptions options) {
        LinkedList<TickPattern> patterns = new LinkedList<>();
        Tick[] priceArrays = ticks.toArray(new Tick[ticks.size()]);
        int lastUsedIndex = -1;
        int index = 0;

        // Find first fractal index
        while (index + 2 < priceArrays.length) {
            Tick k0 = priceArrays[index];
            Tick k1 = priceArrays[index + 1];
            Tick k2 = priceArrays[index + 2];

            if ( !options.ignoreOpenGap ){ //do nothing if the 3 ticks are not in the same day
                if ( isOpenTick(k0, k1) ){
                    //create line
                    //TODO test it
                    index++;
                    patterns.add(createPatternLine(patterns.peekLast(), priceArrays, lastUsedIndex + 1, index));
                    lastUsedIndex = index;
                    continue;
                }
            }

            if (isFractal(true, k0, k1, k2) && canCreateFractal(patterns, true, priceArrays, index)) { // 顶分形
                if (lastUsedIndex < index - 1) {
                    patterns.add(createPatternLine(patterns.peekLast(), priceArrays, lastUsedIndex + 1, index));
                }
                TickPattern pattern = TickPattern
                        .create(TickPattern.Type.Fractal, true, k0, k1, k2);
                patterns.add(pattern);
                index += 3;
                lastUsedIndex = index - 1;
                continue;
            }
            if (isFractal(false, k0, k1, k2) && canCreateFractal(patterns, false, priceArrays, index)) {// 底分形
                if (lastUsedIndex < index - 1) {
                    patterns.add(createPatternLine(patterns.peekLast(), priceArrays, lastUsedIndex + 1, index));
                }
                TickPattern pattern = TickPattern.create(TickPattern.Type.Fractal, false, k0, k1, k2);
                patterns.add(pattern);
                index += 3;
                lastUsedIndex = index - 1;
                continue;
            }
            index++;
        }
        if ( lastUsedIndex < priceArrays.length - 1) {
            patterns.add(createPatternLine(patterns.peekLast(), priceArrays, lastUsedIndex, priceArrays.length));
        }
        return patterns;
    }

    /**
     * 从一系列K线创建 LongLine/ShortLine Pattern
     */
    private TickPattern createPatternLine(TickPattern lastPattern, Tick[] prices,
            int indexBegin, int indexEnd)
    {
        if ( indexBegin<0 ){
            indexBegin = 0;
        }
        if (indexEnd - indexBegin == 1) {
            boolean lineDirection = true;
            Tick t = prices[indexBegin];
            if ( t.getBeginPoint().isHigher(t.getEndPoint()) ){
                lineDirection = false;
            }else{
                lineDirection = true;
            }
//            // 如果只有一根K线时,改为跟前一个顶或底最高/最低点相比.
//            // （原来的标准是：独立的一根K线跟底分型的右侧K线相比，高于右侧K线，则为上升K线；下降K线类似）
//            if (lastPattern != null && lastPattern.getType() == TickPattern.Type.Fractal) {
//                boolean lastDirection = lastPattern.getDirection();
//                if (lastDirection) {
//                    lineDirection = (priceBegin.getHigh() >= lastPattern.getHigh());
//                    // priceBegin.getHigh()>=lastPattern.getHigh();
//                } else {
//                    lineDirection = (priceBegin.getLow() >= lastPattern.getLow());
//                    // priceBegin.getLow()>=lastPattern.getLow();
//                }
//            } else {
//                // 前面没有分形,跟下一个线比较
//                Tick priceNext = prices[indexBegin + 1];
//                lineDirection = (priceBegin.getHigh() < priceNext.getHigh());
//                // priceBegin.getHigh()<priceNext.getHigh();
//            }
            return assertLine( TickPattern.create(TickPattern.Type.Line, lineDirection,
                    new Tick[] { t }));
        }
        Tick bt = prices[indexBegin];
        Tick et = prices[indexEnd - 1];
        Tick subTicks[] = new Tick[indexEnd - indexBegin];
        System.arraycopy(prices, indexBegin, subTicks, 0, subTicks.length);
        boolean lineDirection = true;
        if ( (bt.getOpen()< et.getClose())) {
            lineDirection = true;
        } else if ((bt.getOpen()> et.getClose())) {
            lineDirection = false;
        } else {
            // 在目前放宽条件的情况下,如果K线的方向无法确定, 就与上个分形的顶点比较
            if (lastPattern != null && lastPattern.getType() == TickPattern.Type.Fractal) {
                boolean lastDirection = lastPattern.getDirection();
                if (lastDirection) {
                    lineDirection = (et.getHigh() >= lastPattern.getHigh());
                } else {
                    lineDirection = (et.getLow() >= lastPattern.getLow());
                }
            }
        }
        return assertLine(TickPattern.create(TickPattern.Type.Line, lineDirection, subTicks));
    }

    private TickPattern assertLine(TickPattern line){
        if ( line.getDirection() ){
            assert(line.getEndPoint().isHighOrEquals(line.getBeginPoint()));
        }else{
            assert(line.getBeginPoint().isHighOrEquals(line.getEndPoint()));
        }
        return line;
    }

    /**
     * @param patterns
     * @param fractalDir
     * @param p
     * @return
     */
    private boolean canCreateFractal(List<TickPattern> patterns, boolean fractalDir, Tick[] priceArrays, int index)
    {
        boolean result = true;
        TickPattern lastFractal = null;
        for (int i = patterns.size() - 1; i >= 0; i--) {
            if (patterns.get(i).getType() == TickPattern.Type.Fractal) {
                lastFractal = patterns.get(i);
                break;
            }
        }
        Tick p0 = priceArrays[index];
        Tick p1 = priceArrays[index + 1];
        Tick p2 = priceArrays[index + 2];
        Tick p3 = null;
        Tick p4 = null;
        if (priceArrays.length > index + 3) {
            p3 = priceArrays[index + 3];
        }
        if (priceArrays.length > index + 4) {
            p4 = priceArrays[index + 4];
        }
        if (lastFractal != null && (fractalDir == lastFractal.getDirection())) {
            // 如果出现连续的顶,中间没有K线或只有下降K线,并且后顶低于前顶,则不生成后顶.
            // 如果出现连续的底,中间没有K线或只有上升K线,并且后底高于前底,则不生成后底.
            if (fractalDir && lastFractal.getDirection()) {
                result = (p1.getHigh() >= lastFractal.getHigh());
            }
            if (!fractalDir && !lastFractal.getDirection()) {
                result = (p1.getLow() <= lastFractal.getLow());
            }
        }
        // 顶分形必定高于前面的底分形, 底分形必定低于前面的顶分形
        if (result && (lastFractal != null) && (fractalDir != lastFractal.getDirection())) {
            if (fractalDir) { // 后面的顶分形必须高于前面的底分形
                result = p1.getHigh() > lastFractal.getHigh();
            } else {// 后面的底分形必须低于前面的顶分形
                result = p1.getLow() < lastFractal.getLow();
            }
        }
        if (result && p3 != null && p4 != null) {
            // 如果 p3p4p5 形成相同分形,且高于/低于p1,本次分形不生成
            if (isFractal(fractalDir, p2, p3, p4)) {
                if (fractalDir) {
                    result = p1.getHigh() > p3.getHigh();
                } else {
                    result = p1.getLow() < p3.getLow();
                }
            }
        }
        return result;
        //return true;
    }

    /**
     * 是否是连续顶/底分形,这种情况下, 只生成后一个
     */
    private boolean isContinueFractal(LinkedList<TickPattern> patterns, Tick[] prices, int index) {
        TickPattern lastFractal = null;
        for (int i = patterns.size() - 1; i >= 0; i--) {
            if (patterns.get(i).getType() == TickPattern.Type.Fractal) {
                lastFractal = patterns.get(i);
                break;
            }
        }
        if (lastFractal == null) {
            return false;
        }
        if (prices.length <= index + 4) {
            return false;
        }
        Tick p0 = prices[index];
        Tick p1 = prices[index + 1];
        Tick p2 = prices[index + 2];
        Tick p3 = prices[index + 3];
        Tick p4 = prices[index + 4];
        // 两个连续顶分形的中间一点,必定形成底分形
        // 该底分形是否生成, 要看底分形的底是否要低
        if (isFractal(true, p0, p1, p2) && isFractal(true, p2, p3, p4)) {
            if ( p1.getHigh() < p3.getHigh()) {
                return true;
            }
        }
        // 连续底分形中间一点, 为顶分形
        // 该顶分形是否生成, 看顶分形的顶是否要高
        if (isFractal(false, p0, p1, p2) && isFractal(false, p2, p3, p4)) {
            if ( p1.getLow() > p3.getLow()) {
                return true;
            }
        }
        return false;
    }

}