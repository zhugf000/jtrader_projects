package trader.common.exchangeable.price.chanlun;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.PriceLevel;
import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

/**
 * 证券趋势分析
 */
public class TickTrendAnalyser {

    protected TimeSeries timeSeries;
    protected List<TickPattern> fractals;
    protected List<TickPattern> strokes;
    protected List<TickPattern> sections;
    protected List<TickPattern> centrums;

    private AnalyserOptions options = new AnalyserOptions();

    public TickTrendAnalyser(TimeSeries timeSeries) {
        this.timeSeries = timeSeries;
    }

    public List<TickPattern> upgradeCentres(List<TickPattern> centres)
    {
        return (new CentreUpgrader(centres)).parse(getOptions());
    }

    /**
     * 只分析中枢，不做中枢扩张操作
     */
    public List<TickPattern> analyseCentres(Type sourceType, List<TickPatternEdge> edgePoints)
    {
        List<TickPattern> s = null;
        PriceLevel l = null;
        if( sourceType==Type.Stroke){
            s = strokes;
            l = timeSeries.getPriceLevel().levelDown();
        }else if (sourceType==Type.Section){
            s = sections;
            l = timeSeries.getPriceLevel();
        }else
            throw new RuntimeException("Unsupported centrum source type: "+sourceType);
        CentreAnalyser centrumAnalyser = (new CentreAnalyser(s, l));
        if ( edgePoints!=null ){
            centrumAnalyser.setEdgePoints(edgePoints);
        }
        return centrumAnalyser.parse(getOptions());
    }

    public List<TickPattern> analyseSection(List<TickPattern> strokes)
    {
        return (new SectionAnalyser(strokes)).parse(getOptions());
    }

    public AnalyserOptions getOptions() {
        return options;
    }

    /**
     * 分析并输出趋势
     *
     * @param target
     *            目标趋势类型(顶底分型,线段,中枢尚未完成)
     * @return 分析完成的数据
     * @throws IOException
     */
    public List<TickPattern> analyse(TickPattern.Type target) {
        if (timeSeries.getTickCount() == 0) {
            return Collections.EMPTY_LIST;
        }
        if ( target.ordinal()>TickPattern.Type.Centre.ordinal()){
            throw new RuntimeException("Unable to parse centrums from stracth");
        }
        //Preprocess options
        options.exchangeable = timeSeries.getExchangeable();
        options.priceLevel = timeSeries.getPriceLevel();
        switch(options.priceLevel){
        case DAY:
        case WEEK:
        case MONTH:
            options.ignoreOpenGap = true;
            break;
        }

        if (sections == null) {
            // 合并包含K线
            List<Tick> klines = (new TickAnalyser(timeSeries)).parse(getOptions());
            // 分析K线为顶底
            fractals = (new FractalAnalyser2(klines)).parse(getOptions());
            // 分析笔划
            //strokes = StrokeAnalyser.parseStrokePatterns(fractals);
            strokes = (new StrokeAnalyser3(fractals)).parse(getOptions());
            // 分析线段
            if ( target.ordinal()>= TickPattern.Type.Section.ordinal() ){
                sections = (new SectionAnalyser(strokes)).parse(getOptions());
            }
            if ( target.ordinal()>= TickPattern.Type.Centre.ordinal() ){
                centrums = (new CentreAnalyser(sections, timeSeries.getPriceLevel())).parse(getOptions());
            }
        }
        switch(target){
        case Fractal:
            return fractals;
        case Stroke:
            return strokes;
        case Section:
            return sections;
        case Centre:
            return centrums;
        default:
            throw new RuntimeException("Unsupported target type: "+target);
        }
    }

    public List<TickPattern> get(TickPattern.Type type){
        switch(type){
        case Fractal:
            return fractals;
        case Stroke:
            return strokes;
        case Section:
            return sections;
        case Centre:
            return centrums;
        default:
            throw new RuntimeException("Unsupported target type: "+type);
        }
    }

    /**
     * Get stroke/section begin/endpoint as edge
     */
    public List<TickPatternEdge> getEdges(Type sourceType){
        List<TickPattern> strokeOrSections = null;
        if( sourceType==Type.Stroke){
            strokeOrSections = strokes;
        }else if (sourceType==Type.Section){
            strokeOrSections = sections;
        }else
            throw new RuntimeException("Unsupported edge source type: "+sourceType);

        LinkedList<TickPatternEdge> result = new LinkedList<>();
        int index = 0;
        TickPattern lp=null;
        for(TickPattern p:strokeOrSections){
            if ( lp!=null && lp.getType()==Type.Section ){
                assert(lp.getEndPoint().equals(p.getBeginPoint()));
            }
            if ( result.isEmpty()){
                //need begin point
                TickPatternEdge edge = new TickPatternEdge(p.getBeginPoint(), !p.getDirection());
                edge.index = index++;
                result.add(edge);
            }
            TickPatternEdge edge = new TickPatternEdge(p.getEndPoint(), p.getDirection());
            edge.index = index++;
            result.add(edge);
            lp = p;
        }
        return result;
    }
}
