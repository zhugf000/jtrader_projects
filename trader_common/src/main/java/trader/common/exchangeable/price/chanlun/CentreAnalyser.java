package trader.common.exchangeable.price.chanlun;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.PriceLevel;

/**
 * 分析中枢.
 * <BR>确定极值走势的上下
 * <BR>分析原始中枢
 * <BR>分析中枢的扩展和合并
 */
public class CentreAnalyser extends Analyser {
    private final static Logger logger = LoggerFactory.getLogger(CentreAnalyser.class);

    static enum EdgeType{
        High,
        Low,
        /**
         * Begin or end, direction is unknown
         */
        Begin,
        End;

    	public EdgeType opposite(){
    		switch(this){
    		case High:
    			return Low;
    		case Low:
    			return High;
    		case Begin:
    			return End;
    		case End:
    			return Begin;
    		}
			return null;
    	}
    }

    static class EdgePoint{
        int index;

        /**
         * 极点
         */
        Point point;

        /**
         * 高/低点
         */
        EdgeType type;

        EdgePoint(EdgeType type, int index, Point point){
            this.type = type;
            this.index = index;
            this.point = point;
        }
    }

    private ArrayList<TickPattern> patterns;
    private List<EdgePoint> edgePoints;
    private LinkedList<TickPattern> centres;
    private PriceLevel level;

    public CentreAnalyser(List<TickPattern> patterns, PriceLevel level){
        this.patterns = new ArrayList<>(patterns);
        this.level = level;
    }


    public void setEdgePoints(List<TickPatternEdge> edgePoints) {
        if ( edgePoints==null ){
            return;
        }
        //cut patterns that are before than first edgePoint
        {
            ArrayList<TickPattern> npatterns = new ArrayList<>();
            Point fp = edgePoints.get(0).getBeginPoint();
            for(TickPattern p:patterns){
                if ( p.getEndPoint().isBefore(fp) ){
                    continue;
                }
                npatterns.add(p);
            }
            this.patterns = npatterns;
        }
        this.edgePoints = new LinkedList<>();
        for(TickPatternEdge edge:edgePoints){
            Point point = edge.getTopPoint();
            int pIndex = -1;
            int index = 0;
            for(TickPattern p:patterns){
                if ( p.getHighPoint().equals(point)){
                    pIndex = index;
                    break;
                }
                if ( p.getLowPoint().equals(point)){
                    pIndex = index;
                    break;
                }
                if ( p.getEndPoint().equals(point)){
                    pIndex = index;
                    break;
                }
                if ( p.getBeginPoint().equals(point)){
                    pIndex = index;
                    break;
                }
                index++;
            }
            assert(pIndex>=0);
            EdgePoint ep = new EdgePoint( edge.getDirection()?EdgeType.High:EdgeType.Low
                    ,pIndex
                    ,point
                    );
            this.edgePoints.add(ep);
        }
    }

    public List<TickPattern> parse(AnalyserOptions options) {
        centres = new LinkedList<>();
        if (patterns.size() < 4) {
            return centres;
        }
        if ( edgePoints==null ){
            edgePoints = parseEdgePoints2();
        }
        if ( options.createCentreEdgeOnly ){
            for(EdgePoint edgePoint: edgePoints){
                centres.add(createEdge(edgePoint));
            }
            return centres;
        }

        //中枢分段创建
        EdgePoint lastEdgePoint = null;
        for(EdgePoint edgePoint: edgePoints){
            if ( lastEdgePoint==null ){
                lastEdgePoint = edgePoint;
                if ( !options.ignoreCentreEdge && (edgePoint.type==EdgeType.High||edgePoint.type==EdgeType.Low)){
                    centres.add(createEdge(edgePoint));
                }
                continue;
            }
            parseCentres(lastEdgePoint, edgePoint);
            lastEdgePoint = edgePoint;
            if ( !options.ignoreCentreEdge && (edgePoint.type==EdgeType.High||edgePoint.type==EdgeType.Low)){
                centres.add(createEdge(edgePoint));
            }
        }
        return centres;
    }

    private TickPattern createEdge(EdgePoint edgePoint){
        assert(edgePoint.type==EdgeType.High || edgePoint.type==EdgeType.Low);
        TickPatternEdge result = new TickPatternEdge(edgePoint.point, edgePoint.type==EdgeType.High);
        result.index = edgePoint.index;
        return result;
    }

    private void parseCentres(EdgePoint begin, EdgePoint end){
        if ( begin.index>=end.index ){
            return;
        }
        assert(!begin.point.equals(end.point));
        assert(begin.index<end.index);

        boolean centreTrend = true;
        if( end.type==EdgeType.High || begin.type==EdgeType.Low){
            assert(begin.point.isLower(end.point));
            centreTrend = true;
        }else if ( end.type==EdgeType.Low || begin.type==EdgeType.High){
            assert(begin.point.isHigher(end.point));
            centreTrend = false;
        }else{
            throw new RuntimeException("should not run here");
        }
        if ( end.index-begin.index <4 ){
            logger.info("No centrum between index "+begin.index+":"+end.index+", "+begin.point+"--"+end.point);
            return;
        }
        int index = begin.index;
        if ( centreTrend != patterns.get(index).getDirection()){
        	index++;
        }
        while(index<=end.index){
            TickPattern lastCentre = peekLastCentre();
            TickPattern p = patterns.get(index);
            if ( extendCentre(lastCentre, p) ){ //检查中枢延伸
                index++;
                continue;
            }
            if ( p.getDirection() == centreTrend ){
                index++;
                continue;
            }
            TickPattern centre = detectCentre(centreTrend, index, end.index);
            if ( centre==null ){
                index++;
                continue;
            }
            centres.add(centre);
            lastCentre = centre;
            index +=3;
        }
    }

    private TickPattern peekLastCentre(){
        for( Iterator<TickPattern> it=centres.descendingIterator(); it.hasNext();){
            TickPattern p = it.next();
            if ( p.getType()==TickPattern.Type.Centre ){
                return p;
            }
        }
        return null;
    }

    /**
     * 从三个相邻线段分析中枢. <BR>
     * 中枢不成立的情况必定是两边的线段不重合(s1.high>s3.high && s1.low>s3.high)
     */
    private TickPattern detectCentre(boolean centreTrend, int patternIndex, int endIndex) {
        if ( patternIndex> endIndex-2 ){
            return null;
        }
        TickPattern s1 = patterns.get(patternIndex);
        TickPattern s2 = patterns.get(patternIndex+1);
        TickPattern s3= patterns.get(patternIndex+2);
        assert(patternIndex+2<=endIndex);
        assert(s1.getDirection()!=centreTrend);
        assert(s2.getDirection()==centreTrend);
        assert(s3.getDirection()!=centreTrend);

        if ( centreTrend){
            if ( s1.getHighPoint().isLower(s3.getLowPoint())){
                return null;
            }
        }else{
            if ( s1.getLowPoint().isHigher(s3.getHighPoint())){
                return null;
            }
        }
        if ((s1.getHigh() > s3.getHigh()) && (s1.getLow()> s3.getHigh())) {
            return null;
        }
        if ((s1.getHigh() > s3.getHigh()) && (s1.getLow()> s3.getHigh())) {
            return null;
        }
        Point high = Point.min(s1.getHighPoint(), s2.getHighPoint(), s3.getHighPoint());
        Point low = Point.max(s1.getLowPoint(), s2.getLowPoint(), s3.getLowPoint());

        if ( !high.isLower(low)) {
            if( high.getPrice()==low.getPrice() ){
                logger.info("Centre High "+high+" has the same price as Low: "+low);
            }
            TickPattern result = TickPatternCentre.create(level, centreTrend, high, low, s1, s2, s3);
            return result;
        } else {
            throw new RuntimeException("Should not run here!");
        }
    }

    /**
     * 中枢延伸
     */
    private boolean extendCentre(TickPattern centre, TickPattern pattern){
        if ( centre==null ){
            return false;
        }
        List<TickPattern> centrePatterns = centre.getPatterns();
        int patternIndex = patterns.indexOf(pattern);
        int lastCentrumPatternIndex = patterns.indexOf( centrePatterns.get(centrePatterns.size()-1) );
        if( lastCentrumPatternIndex+2!=patternIndex
                || centre.getDirection()!=!pattern.getDirection()
                || !isCentrumOverlapped((TickPatternCentre)centre, pattern) )
        {
            return false;
        }
        ((TickPatternCentre)centre).extend(patterns.subList(lastCentrumPatternIndex+1, patternIndex+1));
        return true;
    }

    /**
     * 检测中枢存在的可能性
     * <BR>返回中枢的第一个笔划的index
     */
    @Deprecated
    private int detectCentre(List<TickPattern> centreTicks, int index, int lastCentreEndTickIndex) {
        TickPattern t1 = centreTicks.get(index);
        TickPattern t2 = centreTicks.get(index+1);
        TickPattern t3 = centreTicks.get(index+2);
        TickPattern t4 = centreTicks.get(index+3);
        //tick1-4
        TickPattern low = getLowestTick(t1, t2, t3, t4);
        TickPattern high = getHighestTick(t1, t2, t3, t4);
        //三角形走势,由第一个笔划决定
        if( low==high ){
            if ( low==t1 ){ //第一个笔划力度最大
                return index+1;
            }
            return -1;
        }
        if ( low.isBefore(high) ){//向上
            if ( t1.getDirection() ){
                assert(!t2.getDirection());
                if ( isTickOverlapped(t2, t3, t4)){
                    return index+1;
                }
            }else{ //走势向上，但是第一个笔划向下
                //如果上一个中枢结束tick不紧挨着
                if ( lastCentreEndTickIndex>0
                        && lastCentreEndTickIndex<index-1
                        && isTickOverlapped(t1, t2, t3) )
                {

                    return index;
                }
            }
        }else{//向下
            if ( !t1.getDirection()){
                assert(t2.getDirection());
                if ( isTickOverlapped(t2, t3, t4)){
                    return index+1;
                }
            }else{ //走势向下，但是第一个笔划向上
                //如果上一个中枢结束tick不紧挨着
                if ( lastCentreEndTickIndex>0
                        && lastCentreEndTickIndex<index-1
                        && isTickOverlapped(t1, t2, t3) )
                {
                    return index;
                }
            }
        }
        return -1;
    }

    /**
     * 确定极值点
     * TODO 获取区域化的极值点
     */
    List<EdgePoint> parseEdgePoints()
    {
        LinkedList<EdgePoint> result = new LinkedList<>();
        //Get global high/low points
        Point highPoint = null;
        int highIndex = -1;
        Point lowPoint = null;
        int lowIndex = -1;

        {
            int index = 0;
            for(TickPattern p:patterns){
                Point hp = p.getHighPoint();
                Point lp = p.getLowPoint();
                if ( highPoint==null || highPoint.isLower(hp) ){
                    highPoint = hp;
                    highIndex = index;
                }
                if ( lowPoint==null || lowPoint.isHigher(lp) ){
                    lowPoint = lp;
                    lowIndex = index;
                }
                index++;
            }
        }

        //Now we have highPoint and lowPoint, beginPoint and endPoint
        EdgePoint edgeHigh = new EdgePoint(EdgeType.High, highIndex, highPoint);
        EdgePoint edgeLow = new EdgePoint(EdgeType.Low, lowIndex, lowPoint);
        if ( highIndex==lowIndex ){
            throw new RuntimeException("edge point detection: global High == global Low!");
        }
        EdgePoint edgeBegin = new EdgePoint(EdgeType.Begin, 0, patterns.get(0).getBeginPoint());
        EdgePoint edgeEnd = new EdgePoint(EdgeType.End, patterns.size()-1, patterns.get(patterns.size()-1).getEndPoint());
        result = new LinkedList<>();
        result.add(edgeHigh);
        result.add(edgeLow);
        if ( highIndex<lowIndex ){
            //open-high-low-close
            result.addAll(detectLocalEdgePoints(edgeBegin, edgeHigh));
            result.addAll(detectLocalEdgePoints(edgeHigh, edgeLow));
            result.addAll(detectLocalEdgePoints(edgeLow, edgeEnd));
        }else if (highIndex>lowIndex){
            //open-low-high-close
            result.addAll(detectLocalEdgePoints(edgeBegin, edgeLow));
            result.addAll( detectLocalEdgePoints(edgeLow, edgeHigh) );
            result.addAll( detectLocalEdgePoints(edgeHigh, edgeEnd) );
        }else{
            throw new RuntimeException("Should not run here");
        }
        Collections.sort(result, new Comparator<EdgePoint>(){
            @Override
            public int compare(EdgePoint p1, EdgePoint p2) {
                return p1.point.getTime().compareTo(p2.point.getTime());
            }
        });
        if ( !edgePointsContains(result, 0)){
            if ( result.get(0).type==EdgeType.High ){
                edgeBegin.type = EdgeType.Low;
            }else{
                edgeBegin.type = EdgeType.High;
            }
            result.add(0, edgeBegin);
        }
        if ( !edgePointsContains(result, edgeEnd.index)){
            if ( result.get(result.size()-1).type==EdgeType.High ){
                edgeEnd.type = EdgeType.Low;
            }else{
                edgeEnd.type = EdgeType.High;
            }
            result.add(edgeEnd);
        }
        return result;
    }

    /**
     * 确定区域极值点
     * TODO check localized edge point which are not global high/low
     * @param edgeBegin
     * @param edgeEnd
     * @return
     */
    private List<EdgePoint> detectLocalEdgePoints(EdgePoint edgeBegin, EdgePoint edgeEnd){
        LinkedList<EdgePoint> result = new LinkedList<>();
        if ( edgeBegin.index>=edgeEnd.index || Math.abs(edgeBegin.index-edgeEnd.index)<=4 ){
            return result;
        }

        int beginIndex = edgeBegin.index;
        int endIndex = edgeEnd.index;
        if ( edgeBegin.type!=EdgeType.Begin){
            beginIndex++;
        }

        Point highPoint = null;
        int highIndex = -1;
        Point lowPoint = null;
        int lowIndex = -1;
        for(int index=beginIndex; index<edgeEnd.index; index++){
            TickPattern p = patterns.get(index);
            Point hp = p.getHighPoint();
            Point lp = p.getLowPoint();
            if ( highPoint==null || highPoint.isLower(hp) ){
                highPoint = hp;
                highIndex = index;
            }
            if ( lowPoint==null || lowPoint.isHigher(lp) ){
                lowPoint = lp;
                lowIndex = index;
            }
        }
        EdgePoint highEdge = null;
        EdgePoint lowEdge = null;
        if ( highIndex>=0
                && highIndex!=beginIndex
                && highIndex!=endIndex
                && highPoint.isHigher(edgeEnd.point)
                && highPoint.isHigher(edgeBegin.point) )
        {
            highEdge = new EdgePoint(EdgeType.High, highIndex, highPoint);
            result.add(highEdge);
        }
        if ( lowIndex>=0
                && lowIndex!=beginIndex
                && lowIndex!=endIndex
                && lowPoint.isLower(edgeEnd.point)
                && lowPoint.isLower(edgeBegin.point) )
        {
            lowEdge = new EdgePoint(EdgeType.Low, lowIndex, lowPoint);
            result.add(lowEdge);
        }
        return result;
    }

    static boolean edgePointsContains(List<EdgePoint> edgePoints, int index){
        for(EdgePoint edge: edgePoints){
            if ( edge.index == index){
                return true;
            }
        }
        return false;
    }

    /**
     * 确定极值点
     */
    List<EdgePoint> parseEdgePoints2(){
        LinkedList<EdgePoint> points = new LinkedList<>();

        EdgePoint prevEdge = null;
        EdgePoint lastEdge = null;
        if ( patterns.get(0).getDirection() ){
            lastEdge = new EdgePoint(EdgeType.Low, 0, patterns.get(0).getLowPoint() );
        }else{
            lastEdge = new EdgePoint(EdgeType.High, 0, patterns.get(0).getHighPoint() );
        }
        points.add(lastEdge);

        //        LinkedList<EdgePoint> highEdgePoints = new LinkedList<>();
        //        LinkedList<EdgePoint> lowEdgePoints = new LinkedList<>();
        //        EdgePoint lastHigh = new EdgePoint(EdgeType.High, 0, patterns.get(0).getHighPoint());
        //        EdgePoint lastLow = new EdgePoint(EdgeType.Low, 0, patterns.get(0).getLowPoint());
        //        EdgePoint prevHigh = null;
        //        EdgePoint prevLow = null;
        //        EdgePoint lastEnd = new EdgePoint(EdgeType.End, 0, patterns.get(0).getEndPoint());
        //        highEdgePoints.add(lastHigh);
        //        lowEdgePoints.add(lastLow);
        TickPattern lastLong = null;
        TickPattern lastShort = null;
        int pIndex=0;
        for(TickPattern p:patterns){
            Point highPoint = p.getHighPoint();
            Point lowPoint = p.getLowPoint();
            Point endPoint = p.getEndPoint();
            EdgePoint newEdge = null;
            if ( p.getDirection() ){
                //比较高点
                if ( lastLong!=null && lastLong.getHighPoint().isLower(highPoint) ){
                    //发现新的highEdgePoint
                    newEdge = new EdgePoint(EdgeType.High, pIndex, highPoint);
                    if ( lastEdge.type==EdgeType.High && newEdge.point.isLower(lastEdge.point) ){
                        newEdge = null;
                    }
                }
                //更新高点
                if ( lastEdge.type==EdgeType.High && endPoint.isHigher(lastEdge.point) ){
                    newEdge = new EdgePoint(EdgeType.High, pIndex, endPoint);
                }
            }else{
                //比较低点
                if ( lastShort!=null && lastShort.getLowPoint().isHigher(lowPoint) ){
                    //发现新的lowEdgePoint
                    newEdge = new EdgePoint(EdgeType.Low, pIndex, lowPoint);
                    if ( lastEdge.type==EdgeType.Low && newEdge.point.isHigher(lastEdge.point) ){
                        newEdge = null;
                    }
                }
                //更新低点
                if ( lastEdge.type==EdgeType.Low && endPoint.isLower(lastEdge.point) ){
                    newEdge = new EdgePoint(EdgeType.Low, pIndex, endPoint);
                }
            }
            if (newEdge!=null ){
                if (lastEdge.type==newEdge.type) {
                    //直接更新，不改变prevEdge
                    points.remove(lastEdge);
                    logger.debug("Edge point candidate : "+newEdge+", last edge was removed: "+lastEdge);
                    lastEdge = newEdge;
                    points.add(lastEdge);
                }else {
                    assert(lastEdge!=null && lastEdge.type!=newEdge.type);
                    prevEdge = lastEdge;
                    logger.debug("Edge point candidate : "+newEdge);
                    lastEdge = newEdge;
                    points.add(lastEdge);
                }

                if ( prevEdge!=null && lastEdge!=null ){
                    assert( prevEdge.type==EdgeType.High&&lastEdge.type==EdgeType.Low
                            || prevEdge.type==EdgeType.Low&&lastEdge.type==EdgeType.High );
                }
            }
            pIndex++;
            if ( p.getDirection() ){
                lastLong = p;
            }else{
                lastShort = p;
            }
        }
        //合并靠近的极点
        LinkedList<EdgePoint> result = new LinkedList<>();

        EdgePoint lastHigh=null;
        EdgePoint lastLow = null;
        EdgePoint last = null;
        for(EdgePoint p:points){
            boolean removeThis = false;
            boolean removeLast = false;
            if( last!=null && (p.index-last.index<=3) ){
                if ( p.type==EdgeType.High ){
                    if ( lastHigh!=null && lastHigh.point.isLower(p.point)){
                        removeLast = true;
                    }
                    if ( lastHigh!=null && lastHigh.point.isHigher(p.point)){
                        removeThis = true;
                    }
                }else {
                    if ( lastLow!=null && lastLow.point.isHigher(p.point)){
                        removeLast = true;
                    }
                    if ( lastLow!=null && lastLow.point.isLower(p.point)){
                        removeThis = true;
                    }
                }
            }
            if ( removeLast ){
                logger.debug("Edge point removed last: "+last);
                result.remove(last);
            }
            if( !removeThis ){
                //check if need to merge with last one
                EdgePoint last0 = result.peekLast();
                if( last0!=null && last0.type==p.type ){
                    boolean keepNew = false;
                    if( p.type==EdgeType.High && p.point.isHigher(last0.point)){
                        keepNew = true;
                    }
                    if ( p.type==EdgeType.Low && p.point.isLower(last0.point)){
                        keepNew = true;
                    }
                    if( keepNew ){
                        logger.debug("Edge point remove last before append new: "+p+", (last to remove: "+result.peekLast());
                        result.pollLast();
                        result.add(p);
                    }else{
                        //keep last, ignore new
                        logger.debug("Edge point ignore new: "+p);
                    }
                }else{
                    result.add(p);
                }
            }
            last = p;
            if ( p.type==EdgeType.High ){
                lastHigh = p;
            }else if ( p.type==EdgeType.Low){
                lastLow = p;
            }else{
                throw new RuntimeException("Should not run here");
            }
        }
        points = null;
        EdgePoint edgeBegin = new EdgePoint(EdgeType.Begin, 0, patterns.get(0).getBeginPoint());
        EdgePoint edgeEnd = new EdgePoint(EdgeType.End, patterns.size()-1, patterns.get(patterns.size()-1).getEndPoint());
        if ( !edgePointsContains(result, 0) ){
        	EdgePoint firstEdge = result.get(0);
        	if ( firstEdge.index>=3 ){
        		edgeBegin.type = firstEdge.type.opposite();
        	}else{
        		edgeBegin.type = firstEdge.type;
        		result.remove(0);
        	}
            logger.debug("Edge point insert begin: "+edgeBegin);
            result.add(0, edgeBegin);
        }
        if ( !edgePointsContains(result, edgeEnd.index)){
        	EdgePoint edge0 = result.get(result.size()-1);
        	if ( result.size()-edge0.index>=3 ){
        		edgeEnd.type = edge0.type.opposite();
        	}else{
        		edgeEnd.type = edge0.type;
        		result.remove(result.size()-1);
        	}
            logger.debug("Edge point append end: "+edgeEnd);
            result.add(edgeEnd);
        }
        return result;
    }

}
