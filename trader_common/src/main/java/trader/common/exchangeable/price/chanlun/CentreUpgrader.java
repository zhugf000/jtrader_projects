package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.PriceLevel;
import trader.common.exchangeable.price.chanlun.TickPattern.Type;

/**
 * 中枢升级：扩张，扩展，延伸
 */
public class CentreUpgrader extends Analyser {

    private List<TickPattern> centries;

    public CentreUpgrader(List<TickPattern> centrums){
        this.centries = centrums;
    }

    public List<TickPattern> parse(AnalyserOptions options){
        LinkedList<TickPattern> result = new LinkedList<>();

        TickPatternCentre lastCentrum = null;
        TickPattern lastp = null;
        for(TickPattern p:centries){
            if ( lastp!=null ){
                if ( p.getType()==Type.Centre
                        && isCentreExpandable(lastCentrum, (TickPatternCentre)p))
                {
                    //delete until lastCentrum
                    while( !result.isEmpty() && result.pollLast()!=lastCentrum );
                    result.offerLast( expandCentrum(lastCentrum, (TickPatternCentre)p) );
                }else{
                    result.add(p);
                }
            }else{
                result.add(p);
            }
            if ( p.getType()==Type.Centre ){
                lastCentrum = (TickPatternCentre)p;
            }
            lastp = p;
        }

        return result;
    }

    private TickPatternCentre expandCentrum(TickPatternCentre c1, TickPatternCentre c2){
        PriceLevel level = c1.getPriceLevel().levelUp();
        Point h = Point.max(c1.getHighPoint(), c2.getHighPoint());
        Point l = Point.min(c1.getLowPoint(), c2.getLowPoint());
        return (TickPatternCentre)TickPatternCentre.create(level, c1.getDirection(), h, l, c1, c2);
    }

}
