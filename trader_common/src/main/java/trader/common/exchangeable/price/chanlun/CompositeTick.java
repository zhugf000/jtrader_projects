package trader.common.exchangeable.price.chanlun;

import java.io.PrintWriter;
import java.time.Duration;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;

class CompositeTick extends Tick {

    protected boolean isLong;
    private List<Tick> ticks;

    /**
     *
     * @param price  K线1
     * @param price2 K线2
     * @param isLong 方向,TRUE向上,FALSE向下
     */
    public CompositeTick(Tick price, Tick price2,boolean isLong){
        super(price.getBeginPoint(), price2.getEndPoint());
        setVolume(price.getVolume()+price2.getVolume());
        setTurnover(price.getTurnover()+price2.getTurnover());
        this.isLong = isLong;
        Point high = price.getHighPoint();
        Point high2 = price2.getHighPoint();
        Point low = price.getLowPoint();
        Point low2 = price2.getLowPoint();
        if ( isLong ){
            setHighPoint( high.isHigher(high2)? high: high2);
            setLowPoint( low.isHigher(low2)? low: low2 );

            if ( getBeginPoint().isLower(getLowPoint())){
                setBeginPoint( getBeginPoint().changePrice(getLowPoint().getPrice()) );
            }
            if ( getEndPoint().isLower(getLowPoint())){
                setEndPoint( getEndPoint().changePrice(getLowPoint().getPrice()) );
            }
        }else{
            setHighPoint( high.isLower(high2)? high: high2 );
            setLowPoint( low.isLower(low2)? low: low2 );
            //Limit the begin/end range
            if ( getBeginPoint().isHigher(getHighPoint())){
                setBeginPoint( getBeginPoint().changePrice(getHighPoint().getPrice()) );
            }
            if ( getEndPoint().isHigher(getHighPoint())){
                setEndPoint( getEndPoint().changePrice(getHighPoint().getPrice()) );
            }
        }


        ticks = new LinkedList<>();
        if(price instanceof CompositeTick ){
            ticks.addAll( ((CompositeTick)price).ticks );
        }else{
            ticks.add(price);
        }
        ticks.add(price2);
    }

    @Override
    public Duration getDuration(){
        if ( duration==null ){
            long total =0;
            for(Tick t:this.ticks){
                total += t.getDuration().getSeconds();
            }
            duration = Duration.ofSeconds(total);
        }
        return duration;
    }

    public List<Tick> getTicks(){
        return ticks;
    }

    public Tick getMaxTick(){
        Tick mt = ticks.get(0);
        for(Tick t:ticks){
            if( mt.getHighPoint().isLower(t.getHighPoint())){
                mt = t;
            }
        }
        return mt;
    }

    @Override
    public Tick getTopPrice(){
        Tick p1 = ticks.get(0);
        Tick p2 = ticks.get(1);
        if ( isLong ){
            if ( p1.getHigh()>p2.getHigh()){
                return p1.getTopPrice();
            }else{
                return p2.getTopPrice();
            }
        }else{
            if ( p1.getLow() <= p2.getLow()){
                return p1.getTopPrice();
            }else{
                return p2.getTopPrice();
            }
        }
    }

    @Override
    public void dump(String prefix, PrintWriter pw){
        pw.print(prefix);
        pw.println( this );
        for(int i=0;i<ticks.size();i++){
            pw.println(prefix+"\t"+ticks.get(i));
        }
    }

    public boolean isLong(){
        return isLong;
    }

    @Override
    public String toString(){
        return "C"+super.toString();
    }

}