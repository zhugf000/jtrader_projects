package trader.common.exchangeable.price;

public interface PriceRange {

    public long getHigh();

    public long getLow();
}
