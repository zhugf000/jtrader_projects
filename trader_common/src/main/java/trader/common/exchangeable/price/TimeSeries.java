package trader.common.exchangeable.price;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import trader.common.exchangeable.Exchangeable;
import trader.common.exchangeable.ExchangeableData;

/**
 * 价格序列，可更新
 */
public class TimeSeries implements TickSeries {
    protected Exchangeable    exchangeable;
    protected LocalDateTime   beginDateTime;
    protected LocalDateTime   endDateTime;
    protected List<LocalDate> suspensionDays;
    protected List<Tick> series;
    protected PriceLevel period;
    protected volatile int version = 0;

    protected TimeSeries(PriceLevel period)
    {
        this.period = period;
    }

    /**
     * Updated since last getSeries() invocation
     */
    @Override
    public int getVersion(){
        return version;
    }

    /**
     * 时间级别
     */
    public PriceLevel getPriceLevel(){
        return period;
    }

    @Override
    public Exchangeable getExchangeable()
    {
        return exchangeable;
    }

    public LocalDateTime getBeginDateTime()
    {
        return beginDateTime;
    }

    public LocalDateTime getEndDateTime()
    {
        return endDateTime;
    }

    public List<LocalDate> getSuspensionDays()
    {
        return Collections.unmodifiableList(suspensionDays);
    }

    public boolean hasSuspension()
    {
        return !suspensionDays.isEmpty();
    }

    @Override
    public List<Tick> getTicks()
    {
        return series;
    }

    public void addTick(Tick tick){
        version++;
        if ( series.size()==0 ){
            beginDateTime = tick.getBeginTime();
        }
        series.add(tick);
        endDateTime = tick.getEndTime();
    }

    public void updateTick(Point point){
        version++;
        Tick lastTick = getLastTick();
        if ( point!=null ){
            lastTick.update(point);
        }
        endDateTime = lastTick.getEndTime();
    }

    public Tick getLastTick()
    {
        if ( series.size()==0 ){
            return null;
        }
        return series.get(series.size()-1);
    }

    @Override
    public int getTickCount(){
        return series.size();
    }

    @Override
    public Tick getTick(int index){
        return series.get(index);
    }

    public static TimeSeries createTemplate(Exchangeable exchangeable, PriceLevel level){
        TimeSeries timeSeries = new TimeSeries(level);
        timeSeries.exchangeable = exchangeable;
        return timeSeries;
    }

    @Override
    public TimeSeries clone(){
        TimeSeries r = new TimeSeries(this.getPriceLevel());
        r.exchangeable = exchangeable;
        r.beginDateTime = beginDateTime;
        r.endDateTime = endDateTime;
        r.period = period;
        r.series = new ArrayList<>(series);
        r.suspensionDays = new ArrayList<>(suspensionDays);
        r.version = 0;
        return r;
    }

    /**
     * 转换级别.
     */
    public TimeSeries convertTo(PriceLevel targetLevel)
    {
        if( targetLevel.ordinal()<getPriceLevel().ordinal()
           || (targetLevel.getMinutePeriod()% this.getPriceLevel().getMinutePeriod() != 0)
        ){
            throw new RuntimeException("Convert "+getPriceLevel()+" to "+targetLevel+" is not supported");
        }
        TimeSeries result = new TimeSeries(targetLevel);
        result.exchangeable = this.exchangeable;
        result.beginDateTime = this.beginDateTime;
        result.endDateTime = this.endDateTime;
        result.suspensionDays = new ArrayList<>(suspensionDays);
        int mergeRatio = targetLevel.getMinutePeriod() / this.getPriceLevel().getMinutePeriod();
        result.series = Tick.convertTo(getTicks(), mergeRatio);

        return result;
    }

    public static TimeSeries fromTemplate(TimeSeries template, List<Tick> newTicks)
    {
        TimeSeries result = new TimeSeries(template.getPriceLevel());
        result.exchangeable = template.exchangeable;
        result.suspensionDays = template.suspensionDays;
        result.series = new ArrayList<>(newTicks);
        if ( newTicks.size()>0 ){
            result.beginDateTime = newTicks.get(0).getBeginTime();
            result.endDateTime = newTicks.get(newTicks.size() - 1).getEndTime();
        }
        return result;
    }

    public static TimeSeries loadData(ExchangeableData data, Exchangeable exchangeable, LocalDate beginDate, LocalDate endDate, ExchangeableData.Classification... classifications)
        throws IOException
    {
        TimeSeriesLoader loader = new TimeSeriesLoader(data, exchangeable);
        loader.setBeginDate(beginDate);
        loader.setEndDate(endDate);
        loader.setUseRawData(true);

        boolean hasClassification = false;
        for(ExchangeableData.Classification c:classifications){
            if ( data.exists(exchangeable, c, beginDate)){
                loader.setClassification(c);
                hasClassification = true;
                break;
            }
        }
        if ( !hasClassification ){
            for(ExchangeableData.Classification c:classifications){
                if (  c.getAlternate()!=null && data.exists(exchangeable, c.getAlternate(), beginDate)){
                    loader.setClassification(c);
                    hasClassification = true;
                    break;
                }
            }
        }
        if ( !hasClassification ){
            loader.setClassification(classifications[0]);
        }
        return loader.load();
    }

}
