package trader.common.exchangeable.price;

import java.util.List;

import trader.common.exchangeable.Exchangeable;

public interface TickSeries {

    public Exchangeable getExchangeable();

    public int getVersion();

    public int getTickCount();

    public Tick getTick(int tickIndex);

    public List<Tick> getTicks();

}
