package trader.common.exchangeable.price.chanlun;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;
import trader.common.util.DateUtil;
import trader.common.util.PriceUtil;

public class TickPattern extends Tick{
    public static enum Type {
        /**
         * 缺口
         */
        Gap,
        /**
         * 分形
         */
        Fractal,
        /**
         * K线
         */
        Line,
        /**
         * 笔划
         */
        Stroke,
        /**
         * 线段
         */
        Section,
        /**
         * 走势.极值
         */
        Edge,
        /**
         * 走势.中枢
         */
        Centre,
        /**
         * 走势.盘整
         */
        Dull,
        /**
         * 走势.趋势
         */
        Trend
    }

    protected boolean direction;
    protected TickPattern.Type type;
    protected List<Tick> prices = new LinkedList<>();
    protected List<TickPattern> patterns = new LinkedList<>();

    public static TickPattern createComposite(Type type, boolean compProcessingMethod, TickPattern p1, TickPattern p2)
    {
        TickPattern result = new TickPatternStroke();

        result.setVolume(p1.getVolume()+p2.getVolume());
        result.setTurnover(p1.getTurnover()+p2.getTurnover());
        result.direction = p1.getDirection();

        result.setBeginPoint(p1.getBeginPoint());
        result.setEndPoint(p2.getEndPoint());

        if (compProcessingMethod) {// 以向上笔开始的线段的特征序列，只考察顶分型,在处理包含关系的时候，按照方向向上处理
            result.setHighPoint( Point.max(p1.getHighPoint(), p2.getHighPoint()));
            result.setLowPoint( Point.max(p1.getLowPoint(), p2.getLowPoint()));

            if ( result.getEndPoint().isLower(result.getLowPoint())){
                result.setEndPoint( result.getEndPoint().changePrice(result.getLow()));
            }
            if ( result.getBeginPoint().isLower(result.getLowPoint())){
                result.setBeginPoint( result.getBeginPoint().changePrice(result.getLow()));
            }
        } else {
            result.setHighPoint( Point.min(p1.getHighPoint(), p2.getHighPoint()));
            result.setLowPoint( Point.min(p1.getLowPoint(), p2.getLowPoint()));

            if ( result.getEndPoint().isHigher(result.getHighPoint())){
                result.setEndPoint( result.getEndPoint().changePrice(result.getHigh()));
            }
            if ( result.getBeginPoint().isHigher(result.getHighPoint())){
                result.setBeginPoint( result.getBeginPoint().changePrice(result.getHigh()));
            }
        }
        result.getPatterns().add(p1);
        result.getPatterns().add(p2);
        assert(result.getOpen()!=0);
        assert(result.getClose()!=0);
        return result;
    }

    public static TickPattern create(TickPattern.Type type, boolean directionLong, Tick... ticks) {
        return create(type, directionLong, null, null, ticks);
    }

    public static TickPattern create(TickPattern.Type type, boolean directionLong, LocalDateTime beginDate, LocalDateTime endDate,
            Tick... prices)
    {
        TickPattern result = null;
        if (type == Type.Fractal) {
            result = new TickPatternFractal();
        } else if (type == Type.Stroke) {
            throw new RuntimeException("Not supported type: "+type+", should invoke method createStroke instead");
        } else if (type == Type.Section) {
            return createSection(directionLong, beginDate, endDate, prices);
        } else if (type == Type.Centre) {
            throw new RuntimeException("Not supported type: "+type+", should invoke method createCentrum instead");
        } else {
            result = new TickPattern();
        }
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;
        if (beginDate == null) {
            result.setBeginPoint(prices[0].getBeginPoint());
        } else {
            beginTime = beginDate;
            result.setBeginPoint(new Point(beginDate,0));
        }
        if (endDate == null) {
            result.setEndPoint( prices[prices.length - 1].getEndPoint());
        } else {
            endTime = endDate;
            result.setEndPoint(new Point(endDate,0));
        }
        result.type = type;
        result.direction = directionLong;
        result.prices = new LinkedList<>();
        result.setHighPoint(new Point(endTime,0));
        result.setLowPoint(new Point(endTime,Long.MAX_VALUE));
        result.turnover = 0;
        for (int i = 0; i < prices.length; i++) {
            Tick price = prices[i];
            if (price instanceof TickPattern) {
                TickPattern pattern = (TickPattern) price;
                result.patterns.add(pattern);
                List<Tick> patternPrices = pattern.getTicks();
                for (int j = 0; j < patternPrices.size(); j++) {
                    Tick patternPrice = patternPrices.get(j);
                    LocalDateTime priceTime = patternPrice.getTime();
                    if (beginTime != null && priceTime.isBefore(beginTime)) {
                        continue;
                    }
                    if (endTime != null && priceTime.isAfter(endTime)) {
                        break;
                    }
                    addPatternPrice(result, patternPrice);
                }
            } else {
                LocalDateTime priceTime = price.getTime();
                if (beginTime != null && priceTime.isBefore(beginTime)) {
                    continue;
                }
                if (endTime != null && priceTime.isAfter(endTime)) {
                    break;
                }
                addPatternPrice(result, price);
            }
        }
        if (type == Type.Stroke
                //|| type == Type.Section
                ) {
            Point b = result.getBeginPoint(); Point e = result.getEndPoint();
            result.setHighPoint( b.isHigher(e)?  b: e);
            result.setLowPoint( b.isLower(e)? b: e);
        }
        return result;
    }


    static TickPattern createStroke(boolean direction, Point beginPoint, Point endPoint, Tick... prices)
    {
        TickPattern result = null;
        result = new TickPatternStroke();
        LocalDateTime beginTime = null;
        LocalDateTime endTime = null;

        result.setBeginPoint(beginPoint);
        result.setEndPoint(endPoint);
        result.type = TickPattern.Type.Stroke;
        result.direction = direction;
        result.prices = new LinkedList<>();
        result.setHighPoint(new Point(endTime,0));
        result.setLowPoint(new Point(endTime,Long.MAX_VALUE));
        result.turnover = 0;
        for (int i = 0; i < prices.length; i++) {
            Tick price = prices[i];
            if (price instanceof TickPattern) {
                TickPattern pattern = (TickPattern) price;
                result.patterns.add(pattern);
                List<Tick> patternPrices = pattern.getTicks();
                for (int j = 0; j < patternPrices.size(); j++) {
                    Tick patternPrice = patternPrices.get(j);
                    LocalDateTime priceTime = patternPrice.getTime();
                    if (beginTime != null && priceTime.isBefore(beginTime)) {
                        continue;
                    }
                    if (endTime != null && priceTime.isAfter(endTime)) {
                        break;
                    }
                    addStrokePatternPrice(result, patternPrice);
                }
            } else {
                LocalDateTime priceTime = price.getTime();
                if (beginTime != null && priceTime.isBefore(beginTime)) {
                    continue;
                }
                if (endTime != null && priceTime.isAfter(endTime)) {
                    break;
                }
                addStrokePatternPrice(result, price);
            }
        }

        Point b = result.getBeginPoint(); Point e = result.getEndPoint();
        result.setHighPoint( b.isHigher(e)?  b: e);
        result.setLowPoint( b.isLower(e)? b: e);

        return result;
    }

    private static void addStrokePatternPrice(TickPattern pattern, Tick price) {
        pattern.prices.add(price);
        if (pattern.getHighPoint().isLower(price.getHighPoint())) {
            pattern.setHighPoint( price.getHighPoint() );
        }
        if (pattern.getLowPoint().isHigher( price.getLowPoint())) {
            pattern.setLowPoint(price.getLowPoint());
        }
        if (pattern.getBeginPoint().getPrice() == 0) {
            pattern.setBeginPoint(pattern.getBeginPoint().changePrice(price.getOpen()));
        }
        pattern.setTurnover(pattern.getTurnover()+price.getTurnover());
        pattern.setVolume( pattern.getVolume()+price.getVolume());
        pattern.duration = null;
    }



    private static TickPattern createSection(boolean direction, LocalDateTime beginTime, LocalDateTime endTime, Tick... prices)
    {
        TickPatternSection result = new TickPatternSection();
        result.type = TickPattern.Type.Section;
        result.direction = direction;
        result.prices = new LinkedList<>();
        result.setBeginPoint(prices[0].getBeginPoint());
        result.setEndPoint(prices[prices.length-1].getEndPoint());
        result.setHighPoint(new Point(endTime,0));
        result.setLowPoint(new Point(endTime,Long.MAX_VALUE));
        assert(beginTime.equals(prices[0].getBeginPoint().getTime()));
        assert(endTime.equals(prices[prices.length-1].getEndPoint().getTime()));
        result.turnover = 0;
        result.volume = 0;
        for (int i = 0; i < prices.length; i++) {
            Tick price = prices[i];
            assert(price instanceof TickPattern);
            TickPattern pattern = (TickPattern) price;
            addSectionPattern(result, pattern);
        }
        return result;
    }

    @Override
    protected void setHighPoint(Point p){
        super.setHighPoint(p);
    }

    @Override
    protected void setLowPoint(Point low){
        super.setLowPoint(low);
    }

    private static void addSectionPattern(TickPattern section, TickPattern stroke) {
        section.patterns.add(stroke);
        if (section.getHighPoint().isLower(stroke.getHighPoint())) {
            section.setHighPoint( stroke.getHighPoint() );
        }
        if (section.getLowPoint().isHigher( stroke.getLowPoint())) {
            section.setLowPoint(stroke.getLowPoint());
        }
        section.setTurnover( section.getTurnover()+stroke.getTurnover());
        section.setVolume( section.getVolume()+stroke.getVolume());
        section.duration = null;
    }

    private static void addPatternPrice(TickPattern pattern, Tick price) {
        pattern.prices.add(price);
        if (pattern.getHighPoint().isLower(price.getHighPoint())) {
            pattern.setHighPoint( price.getHighPoint() );
        }
        if (pattern.getLowPoint().isHigher( price.getLowPoint())) {
            pattern.setLowPoint(price.getLowPoint());
        }
        if (pattern.getBeginPoint().getPrice() == 0) {
            pattern.setBeginPoint(pattern.getBeginPoint().changePrice(price.getOpen()));
        }
        pattern.getEndPoint().changePrice(pattern.getEndPoint().getPrice());
        pattern.setTurnover( pattern.getTurnover()+price.getTurnover());
        pattern.setVolume( pattern.getVolume()+price.getVolume());
        pattern.duration = null;
    }

    TickPattern() {

    }

    @Override
    public Duration getDuration(){
        if ( duration==null ){
            long total =0;
            if ( prices.size()>0 ){
                for(Tick t:prices){
                    total += t.getDuration().getSeconds();
                }
            }else{
                for(TickPattern tp:patterns){
                    total += tp.getDuration().getSeconds();
                }
            }
            duration = Duration.ofSeconds(total);
        }
        return duration;
    }

    public Period getPeriod(){
        return Period.between(getBeginPoint().getTime().toLocalDate(), getEndPoint().getTime().toLocalDate());
    }

    public long getTopValue(){
        return getTopPoint().getPrice();
    }

    public Point getTopPoint(){
        if ( direction ){
            return getHighPoint();
        }else{
            return getLowPoint();
        }
    }

    public TickPattern.Type getType() {
        return type;
    }

    @Override
    public boolean getDirection() {
        return direction;
    }

    /**
     * 包含的模式（可能继续包含模式）
     */
    public List<TickPattern> getPatterns() {
        return patterns;
    }

    /**
     * 返回所包含类型的Type
     */
    public TickPattern.Type getSubType() {
        if (patterns.size() == 0) {
            return null;
        }
        return patterns.get(0).getType();
    }

    /**
     * 包含的模式,直到指定的模式类型为止
     */
    public List<TickPattern> getPatterns(TickPattern.Type type) {
        if (this.type == type && patterns.get(0).getType() != type) {
            List<TickPattern> result = new LinkedList<>();
            result.add(this);
            return result;
        }
        List<TickPattern> result = new LinkedList<>();
        List<TickPattern> tmp = patterns;

        while (true) {
            for (Iterator<TickPattern> it = tmp.iterator(); it.hasNext();) {
                TickPattern pattern = it.next();
                if (pattern.getPatterns().size() == 0) {
                    result.add(pattern);
                }
                TickPattern firstChild = pattern.getPatterns().get(0);
                if (pattern.getType() == type && firstChild.getType() != type) {
                    result.add(pattern);
                } else {
                    result.addAll(pattern.getPatterns());
                }
            }
            if (tmp.size() == result.size()) {
                break;
            }
            tmp = result;
            result = new LinkedList<>();
        }
        return result;
    }

    public TickPattern getLast(TickPattern.Type type) {
        for (int i = patterns.size() - 1; i >= 0; i--) {
            TickPattern pattern = patterns.get(i);
            if (pattern.getType() == type) {
                return pattern;
            }
        }
        return null;
    }

    public List<Tick> getTicks() {
        return prices;
    }

    public Tick getTick(LocalDateTime date) {
        for (Iterator<Tick> it = prices.iterator(); it.hasNext();) {
            Tick price = it.next();
            if (price.getTime().equals(date)) {
                return price;
            }
        }
        return null;
    }

    public void dump(PrintWriter pw) {
        dump("", pw);
        pw.flush();
    }

    @Override
    public void dump(String prefix, PrintWriter pw) {
        pw.println(prefix + toString());
        if (patterns.size() > 0) {
            for (int i = 0; i < patterns.size(); i++) {
                patterns.get(i).dump(prefix + "\t", pw);
            }
        } else {
            for (int i = 0; i < prices.size(); i++) {
                prices.get(i).dump(prefix + "\t", pw);
            }
        }
    }

    protected static final SimpleDateFormat minuteFormat = new SimpleDateFormat("yyyyMMddHHmm");

    @Override
    public String toString() {
        boolean showDateOnly = showDateOnly();
        long days = ChronoUnit.DAYS.between(getBeginPoint().getTime().toLocalDate(), getEndPoint().getTime().toLocalDate());
        return "TickPattern[" + getType().toString().toUpperCase() + " " + (direction ? "L" : "S")
                +" "+(showDateOnly? DateUtil.date2str(getBeginTime().toLocalDate()) +" ("+days+")"
                        : DateUtil.date2str(getBeginTime())+ " ("+ DateUtil.duration2str(getDuration())+")")
                +", O "+PriceUtil.long2str(getOpen())
                +" , C "+PriceUtil.long2str(getClose())
                +" , H "+PriceUtil.long2str(getHigh())
                +" , L "+PriceUtil.long2str(getLow())
                +" , "+getTurnover()
                +" , "+getVolume()
                +" ("+(showDateOnly? DateUtil.date2str(getEndTime().toLocalDate()): DateUtil.date2str(getEndTime()))+")"
                + "]";
    }

    @Override
    public boolean equals(Object o){
        if ( o==null || o.getClass()!=getClass()){
            return false;
        }
        TickPattern p = (TickPattern)o;
        if ( getType() == p.getType()
                && getDirection()==p.getDirection()
                && getBeginPoint().equals(p.getBeginPoint())
                && getHighPoint().equals(p.getHighPoint())
                && getLowPoint().equals(p.getLowPoint())
                && getEndPoint().equals(p.getEndPoint())
                )
        {
            return true;
        }
        return false;
    }

}