package trader.common.exchangeable.price.chanlun;

import java.util.LinkedList;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;

/**
 * 缺口
 */
public class TickPatternGap extends TickPattern{

    TickPatternGap(Point beginPoint, long endPrice){
        this.type = Type.Gap;
        Point b = beginPoint;
        Point e = beginPoint.changePrice(endPrice);
        Tick gapTick = new Tick(b ,e);
        prices = new LinkedList<>();
        prices.add(gapTick);

        setBeginPoint(b);
        setEndPoint(e);
        if ( b.isHigher(e)){
            setHighPoint(b);
            setLowPoint(e);
        }else{
            setHighPoint(e);
            setLowPoint(b);
        }
    }

}
