package trader.common.exchangeable.price.chanlun;

import java.time.LocalDateTime;
import java.util.List;

import trader.common.exchangeable.price.Point;
import trader.common.exchangeable.price.Tick;

/**
 * 顶点
 */
class TickPatternFractal extends TickPattern{

    @Override
    public LocalDateTime getTime(){
        return getFractalPoint().getTime();
    }

    @Override
    public Tick getTopPrice(){
        return getFractalPoint();
    }

    @Override
    public Point getTopPoint(){
        if ( getDirection() ){
            return getTopPrice().getHighPoint();
        }else{
            return getTopPrice().getLowPoint();
        }
    }

    /**
     * 返回顶底分形的分形点
     */
     public Tick getFractalPoint() {
        List<Tick> prices = getTicks();
        Tick result = prices.get(0);
        if (getDirection()) {
            for (int i = 0; i < prices.size(); i++) {
                Tick price = prices.get(i);
                if ( result.getHigh() < price.getHigh() ) {
                    result = price;
                }
            }
        } else {
            for (int i = 0; i < prices.size(); i++) {
                Tick price = prices.get(i);
                if ( result.getLow() > price.getLow()) {
                    result = price;
                }
            }
        }
        return result;
    }
}