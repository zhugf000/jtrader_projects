package trader.common.exchangeable.price;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import trader.common.exchangeable.*;
import trader.common.util.CSVDataSet;
import trader.common.util.CSVUtil;
import trader.common.util.PriceUtil;

public class TimeSeriesLoader {
    private static Logger logger = LoggerFactory.getLogger(TimeSeriesLoader.class);

    private ExchangeableData data;
    private Exchangeable exchangeable;
    private ExchangeableData.Classification classification;
    private LocalDate beginDate;
    private LocalDate endDate;
    private boolean useRawData;
    private Map<LocalDate, Double> ratioMap = null;

    public TimeSeriesLoader(ExchangeableData data, Exchangeable exchangeable){
        this.data = data;
        this.exchangeable = exchangeable;
        this.useRawData = true;
    }

    public TimeSeriesLoader setBeginDate(LocalDate d){
        this.beginDate = d;
        return this;
    }

    public TimeSeriesLoader setEndDate(LocalDate d){
        this.endDate = d;
        return this;
    }

    public TimeSeriesLoader setClassification(ExchangeableData.Classification c)
    {
        this.classification = c;
        return this;
    }

    /**
     * 是否使用原始价格数据.
     */
    public TimeSeriesLoader setUseRawData(boolean v){
        this.useRawData = v;
        return this;
    }

    public TimeSeries load() throws IOException
    {
        if ( classification==ExchangeableData.DAY ){
            return loadDayData();
        }

        Exchange e = exchangeable.exchange();
        beginDate = MarketDayUtil.thisOrNextMarketDay(e, beginDate, false);
        LocalDateTime seriesBeginTime = null;
        LocalDateTime seriesEndTime = null;
        List<LocalDate> suspensionDays = new LinkedList<>();
        List<Tick> allSeries = new ArrayList<>();
        for (LocalDate tradingDay = beginDate; tradingDay.compareTo(endDate) <= 0; tradingDay = MarketDayUtil.nextMarketDay(e, tradingDay)) {
            if ( seriesBeginTime==null ) {
                seriesBeginTime = exchangeable.getOpenCloseTime(tradingDay)[0];
            }
            seriesEndTime = exchangeable.getOpenCloseTime(tradingDay)[1];

            if ( data.exists(exchangeable, classification, tradingDay) ) { //直接加载
                allSeries.addAll( loadSeries(classification, tradingDay) );
                continue;
            }
            //从辅助数据加载并转换
            if ( classification.getAlternate()!=null && data.exists(exchangeable, classification.getAlternate(), tradingDay) ) {
                List<Tick> altSeries = loadSeries(classification.getAlternate(), tradingDay);

                int mergeRatio = classification.getPriceLevel().getMinutePeriod() / classification.getAlternate().getPriceLevel().getMinutePeriod();
                altSeries = Tick.convertTo(altSeries, mergeRatio);

                allSeries.addAll( altSeries );
                continue;
            }
            //从SINA的证券秒TICK数据加载和转换
            if ( data.exists(exchangeable, ExchangeableData.TICK_SINA, tradingDay)) {
                allSeries.addAll( loadMinutesFromSinaSecondData() );
                continue;
            }
            logger.info(exchangeable+" has no market data on "+tradingDay);
            suspensionDays.add(tradingDay);
            continue;
        }
        TimeSeries result = new TimeSeries(classification.getPriceLevel());
        result.exchangeable = exchangeable;
        result.beginDateTime = seriesBeginTime;
        result.endDateTime = seriesEndTime;
        result.suspensionDays = suspensionDays;
        result.series = allSeries;
        return result;
    }

    private double getRatio(LocalDate date) throws IOException
    {
        if ( exchangeable.getType()!=ExchangeableType.STOCK ){
            return 1.0;
        }
        if ( ratioMap==null ){
            String dayCsv = data.load(exchangeable, ExchangeableData.DAY, null);
            CSVDataSet dayDataSet = CSVUtil.parse(dayCsv);
            ratioMap = new LinkedHashMap<>();
            while (dayDataSet.next()) {
                LocalDate currDate = dayDataSet.getDate(ExchangeableData.COLUMN_DATE);
                if ( dayDataSet.hasColumn(ExchangeableData.COLUMN_SUBSCRIPTION_PRICE_FACTOR) ){
                    double ratio = dayDataSet.getDouble(ExchangeableData.COLUMN_SUBSCRIPTION_PRICE_FACTOR);
                    ratioMap.put(currDate, ratio);
                }
            }
        }
        Double value = ratioMap.get(date);
        if ( value==null ){
            return 1.0;
        }
        return value;
    }

    /**
     * load day data
     * @return
     * @throws IOException
     */
    private TimeSeries loadDayData()
            throws IOException
    {
        String dayCsv = data.load(exchangeable, ExchangeableData.DAY, null);
        CSVDataSet dayDataSet = CSVUtil.parse(dayCsv);
        List<Tick> series = new LinkedList<>();
        LocalDate realBeginDate = null;
        LocalDate realEndDate = null;
        while (dayDataSet.next()) {
            LocalDate date = dayDataSet.getDate(ExchangeableData.COLUMN_DATE);
            if ( (beginDate!=null && beginDate.isAfter(date))
                    || (endDate!=null && endDate.isBefore(date))){
                continue;
            }
            if ( realBeginDate==null ){
                realBeginDate = date;
            }
            realEndDate = date;
            long open = dayDataSet.getPrice(ExchangeableData.COLUMN_OPEN);
            long high = dayDataSet.getPrice(ExchangeableData.COLUMN_HIGH);
            long low = dayDataSet.getPrice(ExchangeableData.COLUMN_LOW);
            long close = dayDataSet.getPrice(ExchangeableData.COLUMN_CLOSE);
            long volume = dayDataSet.getLong(ExchangeableData.COLUMN_VOLUME);
            long turnover = 0;
            if ( dayDataSet.hasValue(ExchangeableData.COLUMN_TURNOVER)){
                turnover =  dayDataSet.getPrice(ExchangeableData.COLUMN_TURNOVER);
            }
            if ( !useRawData ){
                double ratio = getRatio(date);
                open = (long)((open)*ratio);
                high = (long)((high)*ratio);
                low = (long)((low)*ratio);
                close = (long)((close)*ratio);
            }
            Tick price = new Tick(date.atStartOfDay(), date.atStartOfDay(), open, high, low, close, volume, turnover);
            series.add(price);
        }
        TimeSeries result = new TimeSeries(ExchangeableData.DAY.getPriceLevel());
        result.exchangeable = exchangeable;

        result.beginDateTime = realBeginDate.atStartOfDay();
        result.endDateTime = realEndDate.atStartOfDay();
        result.suspensionDays = null;
        result.series = new ArrayList<>(series);
        return result;
    }

    private List<Tick> loadSeries(ExchangeableData.Classification classificationToLoad, LocalDate tradingDay) throws IOException
    {
        LocalDateTime seriesBeginTime = null;
        LocalDateTime seriesEndTime = null;

        LocalDateTime currMarketTimes[] = exchangeable.getOpenCloseTime(tradingDay);
        LocalDateTime currMarketOpenTime = currMarketTimes[0];
        LocalDateTime currMarketCloseTime = currMarketTimes[1];
        LocalDateTime currFirstMinuteTime = currMarketOpenTime;
        if ( classification.getPriceLevel().getMinutePeriod()>0 ){
            currFirstMinuteTime = currMarketOpenTime.plusMinutes(classification.getPriceLevel().getMinutePeriod());
        }
        seriesEndTime = exchangeable.getOpenCloseTime(tradingDay)[1];

        List<Tick> daySeries = new ArrayList<>();
        String csvText = data.load(exchangeable, classification, tradingDay);
        CSVDataSet dataSet = CSVUtil.parse(csvText);
        while (dataSet.next()) {
            LocalDateTime beginDateTime = null;
            LocalDateTime endDateTime = null;
            if ( dataSet.hasColumn(ExchangeableData.COLUMN_BEGIN_TIME)){
                beginDateTime = dataSet.getDateTime(ExchangeableData.COLUMN_BEGIN_TIME);
                endDateTime = dataSet.getDateTime(ExchangeableData.COLUMN_END_TIME);
            }else if ( dataSet.hasColumn(ExchangeableData.COLUMN_DATE)){
                LocalDate date = dataSet.getDate(ExchangeableData.COLUMN_DATE);
                beginDateTime = date.atTime(currMarketOpenTime.toLocalTime());
                endDateTime = date.atTime(currMarketCloseTime.toLocalTime());
            }else if ( dataSet.hasColumn(ExchangeableData.COLUMN_TIME)){
                LocalDateTime dateTime = dataSet.getDateTime(ExchangeableData.COLUMN_TIME);
                endDateTime = dateTime;
                beginDateTime = dateTime.minusMinutes(classification.getPriceLevel().getMinutePeriod());
            }
            if (beginDateTime.compareTo(currMarketOpenTime) < 0
                    || endDateTime.compareTo(currMarketCloseTime) > 0
                    || currFirstMinuteTime.isAfter(endDateTime)
                    ) {
                continue;
            }

            //First time should be OpenTime+minutePeriod
            long open = 0;
            if ( dataSet.hasColumn("beginPrice")){
                open = PriceUtil.str2long(dataSet.get("beginPrice"));
            }else{
                open = PriceUtil.str2long(dataSet.get(ExchangeableData.COLUMN_OPEN));
            }
            long close = 0;
            if ( dataSet.hasColumn("lastPrice")){
                close = PriceUtil.str2long(dataSet.get("lastPrice"));
            }else{
                close = PriceUtil.str2long(dataSet.get(ExchangeableData.COLUMN_CLOSE));
            }
            long high = 0;
            if ( dataSet.hasColumn("highPrice")){
                high = PriceUtil.str2long(dataSet.get("highPrice"));
            }else{
                high = PriceUtil.str2long(dataSet.get(ExchangeableData.COLUMN_HIGH));
            }

            long low = 0;
            if ( dataSet.hasColumn("lowPrice") ){
                low = PriceUtil.str2long(dataSet.get("lowPrice"));
            }else{
                low = PriceUtil.str2long(dataSet.get(ExchangeableData.COLUMN_LOW));
            }
            if ( !useRawData ){
                double ratio = getRatio(beginDateTime.toLocalDate());
                open = (long)((open)*ratio);
                high = (long)((high)*ratio);
                low = (long)((low)*ratio);
                close = (long)((close)*ratio);
            }
            int volume = (int) dataSet.getDouble(ExchangeableData.COLUMN_VOLUME);
            long amount = 0;
            if ( dataSet.hasColumn(ExchangeableData.COLUMN_AMOUNT)){
                amount = (long) dataSet.getDouble(ExchangeableData.COLUMN_AMOUNT);
            }else if ( dataSet.hasColumn(ExchangeableData.COLUMN_TURNOVER)){
                amount = (long) dataSet.getDouble(ExchangeableData.COLUMN_TURNOVER);
            }

            if ( seriesBeginTime==null){
                seriesBeginTime = beginDateTime;
            }
            Tick price = new Tick(beginDateTime, endDateTime, open, high, low, close, volume, amount);
            daySeries.add(price);
        }

        return daySeries;
    }

    /**
     * 从股票的秒级别数据加载并自动转换为MINUTE数据
     */
    private List<Tick> loadMinutesFromSinaSecondData() throws IOException
    {
        return null;
    }

}
