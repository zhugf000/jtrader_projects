package trader.common.exchangeable.indicator;

import trader.common.exchangeable.price.TickSeries;

public interface Indicator {

    public TickSeries getTickSeries();

    public long getValue();

    public long getValue(int index);

}
