package trader.common.exchangeable.indicator;

import trader.common.exchangeable.price.TimeSeries;

public class ClosePriceIndicator extends AbstractIndicator {

    public ClosePriceIndicator(TimeSeries series)
    {
        super(series);
    }

    @Override
    public long getValue(int index)
    {
        return getTickSeries().getTick(index).getClose();
    }

}
