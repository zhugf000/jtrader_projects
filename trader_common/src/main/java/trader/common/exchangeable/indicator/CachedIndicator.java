package trader.common.exchangeable.indicator;

import java.util.ArrayList;

import trader.common.exchangeable.price.TickSeries;

public abstract class CachedIndicator extends AbstractIndicator{

    private ArrayList<Long> cachedValues;
    private int lastVersion = -1;

    public CachedIndicator(TickSeries series)
    {
        super(series);
        cachedValues = new ArrayList<>();
    }

    @Override
    public long getValue(int index)
    {
        TickSeries timeSeries = getTickSeries();
        if ( lastVersion != timeSeries.getVersion() ){
            cachedValues.clear();
        }
        if ( lastVersion == timeSeries.getVersion()
                && cachedValues.size()>index
                && cachedValues.get(index)!=null)
        {
            return cachedValues.get(index);
        }
        long t = calculate(index);
        if ( index>= cachedValues.size()){
            cachedValues.addAll(java.util.Collections.<Long>nCopies(index-cachedValues.size()+1, null));
        }
        cachedValues.set(index, t);
        lastVersion = timeSeries.getVersion();
        return t;
    }

    protected abstract long calculate(int index);


}
