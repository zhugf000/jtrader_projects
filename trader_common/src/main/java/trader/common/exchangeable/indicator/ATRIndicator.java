package trader.common.exchangeable.indicator;

import trader.common.exchangeable.price.TimeSeries;

/**
 * Average True Range
 */
public class ATRIndicator extends CachedIndicator{

    private int timeFrame;
    private TRIndicator tr;

    public ATRIndicator(TimeSeries series, int timeFrame)
    {
        super(series);
        this.timeFrame = timeFrame;
        this.tr = new TRIndicator(series);
    }

    @Override
    protected long calculate(int index)
    {
        if (index == 0) {
            return 0;
        }
        int period = timeFrame;
        long atr=0;
        for(int i=Math.max(0, index-period+1);i<=index;i++){
            long trValue = tr.getValue(i);
            if ( atr==0 ){
                atr = trValue;
            }else{
                atr = (atr*(period-1)+trValue)/period;
            }
        }
        return atr;
    }

}
