package trader.common.exchangeable.indicator;

import trader.common.exchangeable.price.Tick;
import trader.common.exchangeable.price.TimeSeries;

/**
 * True Range
 */
public class TRIndicator extends AbstractIndicator {

    public TRIndicator(TimeSeries series)
    {
        super(series);
    }

    @Override
    public long getValue(int index)
    {
        if ( index==0){
            return 0;
        }
        Tick prev = this.getTickSeries().getTick(index-1);
        Tick curr = this.getTickSeries().getTick(index);

        long v = computeTR(prev, curr);
        return v;
    }

    public static long computeTR(Tick prev, Tick today)
    {
        long prevClose = prev.getClose();
        long currHigh = today.getHigh();
        long currLow = today.getLow();

        long v1 = Math.abs(prevClose - currLow);
        long v2 = Math.abs(currHigh - prevClose);
        long v3 = Math.abs(currHigh - currLow);

        return Math.max(Math.max(v1, v2), v3);
    }

}
