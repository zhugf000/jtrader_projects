package trader.common.exchangeable.indicator;

/**
 * Simple Moving Average
 *
 */
public class SMAIndicator extends CachedIndicator {

    private final Indicator indicator;
    private final int       timeFrame;

    public SMAIndicator(Indicator indicator, int timeFrame)
    {
        super(indicator.getTickSeries());
        this.indicator = indicator;
        this.timeFrame = timeFrame;
    }

    @Override
    protected long calculate(int index)
    {
        long sum = 0;
        for (int i = Math.max(0, index - timeFrame + 1); i <= index; i++) {
            sum += (indicator.getValue(i));
        }
        final int realTimeFrame = Math.min(timeFrame, index + 1);
        return sum / realTimeFrame;
    }

    @Override
    public String toString()
    {
        return getClass().getSimpleName() + "(" + timeFrame + ")";
    }

}
