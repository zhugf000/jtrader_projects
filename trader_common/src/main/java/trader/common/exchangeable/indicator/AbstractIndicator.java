package trader.common.exchangeable.indicator;

import trader.common.exchangeable.price.TickSeries;

public abstract class AbstractIndicator implements Indicator {

    private TickSeries series;

    public AbstractIndicator(TickSeries series) {
        this.series = series;
    }

    @Override
    public TickSeries getTickSeries()
    {
        return series;
    }

    @Override
    public long getValue()
    {
        return getValue(series.getTickCount()-1);
    }

}
