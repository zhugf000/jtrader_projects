package trader.common.util;

import java.io.*;

public class IOUtil {

	public static BufferedWriter createBufferedFileWriter(String file) throws IOException {
		return createBufferedFileWriter(new File(file),"UTF-8",false);
	}

	public static BufferedWriter createBufferedFileWriter(File file) throws IOException {
		return createBufferedFileWriter(file,"UTF-8",false);
	}

    public static BufferedWriter createBufferedFileWriter(File file, boolean append) throws IOException {
        return createBufferedFileWriter(file,"UTF-8",append);
    }

	public static BufferedWriter createBufferedFileWriter(File file, String encoding,boolean append) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,append),encoding));
		return writer;
	}

	public static BufferedReader createBufferedReader(InputStream is) throws IOException {
		return new BufferedReader(new InputStreamReader(is,"UTF-8"));
	}

    public static BufferedReader createBufferedFileReader(File file) throws IOException {
        return new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
    }

    public static String readAsString(InputStream is) throws IOException {
        StringWriter writer = new StringWriter();
        char cbuf[] = new char[4096];
        int len=0;
        try(Reader reader =new InputStreamReader(is,"UTF-8");){
            while( (len=reader.read(cbuf))>0 ) {
                writer.write(cbuf, 0, len);
            }
        }
        return writer.toString();
    }

}
