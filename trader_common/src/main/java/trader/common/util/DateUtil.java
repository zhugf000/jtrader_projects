package trader.common.util;

import java.text.FieldPosition;
import java.text.ParseException;
import java.time.*;
import java.util.Date;

import trader.common.exchangeable.Exchange;

public class DateUtil {

    private static TimeSource timeSource = ()->{ return LocalDateTime.now();};

    public static void setTimeSource(TimeSource t){
        timeSource = t;
    }

    public static LocalDateTime getCurrentTime()
    {
        return timeSource.getTime();
    }

    public static String getCurrentTimeAsString(){
        return date2str(getCurrentTime());
    }

    public static Date add(Date date, long ms){
        return new Date(date.getTime()+ms);
    }

    public static String date2str(long date){
        return FormatUtil.getLocalFormats().dateFormat.format(new Date(date));
    }

    public static String date2str(Date date){
        return FormatUtil.getLocalFormats().dateFormat.format(date);
    }

    public static String date2hhmmss(Date submitTime) {
        return FormatUtil.getLocalFormats().hhmmss.format(submitTime);
    }

    public static Date str2date(String str){
        try {
            return FormatUtil.getLocalFormats().dateFormat.parse(str);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date string: "+str);
        }
    }

    public static long datetime2long(String dateInyyyymmdd, int time ){
        try {
            Date d = FormatUtil.getLocalFormats().yyyymmdd.parse(dateInyyyymmdd);
            return d.getTime()+time;
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }


    /**
     * @param dateInyyyymmdd
     * @param time HH:MM:SS
     * @param millisec 毫秒数
     * @return
     */
    public static long datetime2long(String dateInyyyymmdd, String time, int millisec)
    {
        try {
            Date d = FormatUtil.getLocalFormats().yyyymmdd.parse(dateInyyyymmdd);
            long timeOffset = millisec;
            if ( time.indexOf(':')==1 ){
                time = "0"+time;
            }
            int hour = Integer.parseInt(time.substring(0,2));
            int minute = Integer.parseInt(time.substring(3,5));
            int second = Integer.parseInt(time.substring(6,8));

            int seconds = hour*3600+minute*60+second;
            timeOffset += seconds*1000;

            return d.getTime()+timeOffset;
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }


    /**
     * Date Time 转为 HH:MM:SS 格式字符串
     */
    public static String milliSec2hhmmss(long time){
        long hour = (time/(3600*1000));
        long time2 = (time - hour*3600*1000);
        long minute = time2/(60*1000);
        long time3 = time2-minute*60*1000;
        long second = time3/1000;
        StringBuffer buffer = new StringBuffer();
        return FormatUtil.getLocalFormats().hmsFormat.format(new Object[]{hour,minute,second}, buffer, new FieldPosition(0)).toString();
    }

    /**
     * HH:MM:SS转为当日0时起毫秒数
     */
    public static int hhmmss2MilliSec(String hmsStr, int millisec){
        int hour = Integer.parseInt(hmsStr.substring(0,2));
        int minute = Integer.parseInt(hmsStr.substring(3,5));
        int second = 0;
        if ( hmsStr.length()>5) {
            second = Integer.parseInt(hmsStr.substring(6,8));
        }

        int seconds =hour*3600+minute*60+second;

        return seconds*1000 + millisec;
    }



    public static String date2str(LocalDate date){
        return FormatUtil.DATE_FORMATTERS[0].format(date);
    }

    public static String date2str(LocalDateTime date){
        return FormatUtil.DATETIME_FORMATTERS[0].format(date);
    }

    public static String time2hhmmss(LocalTime time) {
        return FormatUtil.getLocalFormats().hhmmss2.format(time);
    }

    public static String timestamp2str(LocalDateTime timestamp){
        return FormatUtil.getLocalFormats().dateFormatTimestamp.format(timestamp);
    }

    public static LocalDateTime str2timestamp(String str){
    	if ( !StringUtil.notEmpty(str) ) {
            return null;
        }
        return LocalDateTime.parse(str,FormatUtil.getLocalFormats().dateFormatTimestamp);
    }

    public static LocalDateTime str2localdatetime(String str){
    	if ( !StringUtil.notEmpty(str) ) {
            return null;
        }
    	if ( str.indexOf(".")>0 ) {
            return LocalDateTime.parse(str,FormatUtil.getLocalFormats().dateFormatTimestamp);
        }
        return FormatUtil.formatDateTime(str);
    }

    /**
     * 转换 YYYY-MM-DD/YYYYMMDD 格式为LocalDate
     */
    public static LocalDate str2localdate(String str){
        if ( !StringUtil.notEmpty(str) ) {
            return null;
        }
        return FormatUtil.formatDate(str);
    }

    /**
     *
     * @param dateInyyyymmdd
     * @param timeHHCMMCSS time in format "10:15:30"
     * @param millisec
     * @return
     */
    public static LocalDateTime str2localdatetime(String dateInyyyymmdd, String timeHHCMMCSS, int millisec)
    {
        LocalDate localDate = str2localdate(dateInyyyymmdd);
        if ( timeHHCMMCSS.length()==0 ){
            return localDate.atTime(0, 0, 0);
        }
        if ( timeHHCMMCSS.length()==7) {
            timeHHCMMCSS = "0"+timeHHCMMCSS;
        }
        LocalTime localTime = LocalTime.parse(timeHHCMMCSS);
        return localDate.atTime(localTime.getHour(), localTime.getMinute(), localTime.getSecond(), millisec*1000000);
    }

    public static LocalDateTime str2localdatetime(String dateInyyyymmdd, LocalTime time)
    {
        LocalDate localDate = str2localdate(dateInyyyymmdd);
        return localDate.atTime(time);
    }

    public static LocalTime str2localtime(String str)
    {
        if ( !StringUtil.notEmpty(str) ) {
            return null;
        }
        return LocalTime.parse(str, FormatUtil.getLocalFormats().hhmmss2);
    }

    public static LocalDateTime toLocalDate(Exchange e,Date d){
        ZoneId zoneId= null;
        if ( e!=null ) {
            zoneId = e.getZoneId();
        } else {
            zoneId = ZoneId.systemDefault();
        }
        return LocalDateTime.ofInstant(d.toInstant(), zoneId);
    }

    public static Instant min(Instant d1, Instant d2){
        if ( d1.isBefore(d2) ) {
            return d1;
        }
        return d2;
    }

    public static ZonedDateTime min(ZonedDateTime d1, ZonedDateTime d2){
        if ( d1.isBefore(d2) ) {
            return d1;
        }
        return d2;
    }

    public static ZonedDateTime max(ZonedDateTime d1, ZonedDateTime d2){
        if ( d1.isAfter(d2) ) {
            return d1;
        }
        return d2;
    }

    public static Instant max(Instant d1, Instant d2){
        if ( d1.isAfter(d2) ) {
            return d1;
        }
        return d2;
    }
    public static LocalDateTime min(LocalDateTime d1, LocalDateTime d2){
        if ( d1.isBefore(d2) ) {
            return d1;
        }
        return d2;
    }

    public static LocalDateTime max(LocalDateTime d1, LocalDateTime d2){
        if ( d1.isAfter(d2) ) {
            return d1;
        }
        return d2;
    }

    public static int quarter(Month month){
        switch(month){
        case JANUARY:
        case FEBRUARY:
        case MARCH:
            return 1;
        case APRIL:
        case MAY:
        case JUNE:
            return 2;
        case JULY:
        case AUGUST:
        case SEPTEMBER:
            return 3;
        case OCTOBER:
        case NOVEMBER:
        case DECEMBER:
            return 4;
        }
        return 0;
    }

    public static int quarter(LocalDate date){
        return quarter(date.getMonth());
    }

    public static int quarter(LocalDateTime date){
        return quarter(date.getMonth());
    }

    public static String duration2str(Duration duration){
        long seconds = duration.getSeconds();
        long hours = seconds / (60*60);
        int minutes = (int) ((seconds % (60*60)) / (60));
        int secs = (int) (seconds % (60));

        StringBuilder buf = new StringBuilder(64);
        if ( hours>0 ){
            buf.append(hours).append(hours>1?" hours":" hour");
        }
        if ( minutes>0 ){
            buf.append(" ").append(minutes).append(minutes>1?" mins":" min");
        }
        if ( secs>0 ){
            buf.append(" ").append(secs).append(secs>1?" secs":" sec");
        }
        return buf.toString().trim();
    }

    /**
     * Check if day is between begin and end
     */
    public static boolean between(LocalDate begin, LocalDate end, LocalDate day){
        if ( begin.compareTo(day)<=0 && end.compareTo(day)>=0 ){
            return true;
        }
        return false;
    }

    /**
     * Check if time is between begin and end
     */
    public static boolean between(LocalTime begin, LocalTime end, LocalTime day){
        if ( begin.compareTo(day)<=0 && end.compareTo(day)>=0 ){
            return true;
        }
        return false;
    }

    /**
     * Check if time is between begin and end
     */
    public static boolean between(LocalDateTime begin, LocalDateTime end, LocalDateTime day){
        if ( begin==null && day.compareTo(end)<=0 ){
            return true;
        }
        if ( end==null && day.compareTo(begin)>=0 ){
            return true;
        }
        if ( begin!=null && end!=null && begin.compareTo(day)<=0 && day.compareTo(end)<=0 ){
            return true;
        }
        return false;
    }

    public static long secondsBetween(LocalDateTime ldt, LocalDateTime ldt2 ){
        ZoneOffset zoneOffset = ldt.atZone(ZoneId.systemDefault()).getOffset();
        return Math.abs(ldt.atOffset(zoneOffset).toEpochSecond()-ldt2.atOffset(zoneOffset).toEpochSecond());
    }

}
