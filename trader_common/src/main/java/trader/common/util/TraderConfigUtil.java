package trader.common.util;

import java.io.File;
import java.io.IOException;

public class TraderConfigUtil {

    public static final String TRADER_ACCOUNTS = "trader_accounts.ini";

    public static File getTraderHome(){
        String traderHome = System.getProperty("trader.home");
        if (traderHome==null ){
            traderHome = System.getenv("TRADER_HOME");
        }
        if ( traderHome==null ){
            String osName = System.getProperty("os.name").toLowerCase();
            if( osName.indexOf("windows")>=0 ){
            	String[] paths = {"C:\\traderHome", "D:\\traderHome", "Z:\\traderHome"};
            	for(String path:paths){
            		File f=new File(path);
            		if ( f.exists() && f.isDirectory() ){
            			traderHome = f.getAbsolutePath();
            			break;
            		}
            	}
            }else{
                String home = System.getProperty("user.home");
                File f = new File(home,"traderHome");
                if( f.exists() && f.isDirectory() ){
                    traderHome = f.getAbsolutePath();
                }
            }
        }
        if (traderHome==null ){
            traderHome = ResourceUtil.detectJarFile(TraderConfigUtil.class).getParentFile().getParent();
        }
        if ( System.getProperty("trader.home")==null) {
            System.setProperty("trader.home", traderHome);
        }
        return new File(traderHome);
    }

    public static IniFile loadIniFile(String iniFileName) throws IOException
    {
        File traderHome = getTraderHome();
        File configFile = new File(traderHome,"etc/"+iniFileName);
        if ( configFile.exists() ){
            return new IniFile(configFile);
        }
        throw new IOException("Trader home "+traderHome+" has no config file: "+iniFileName);
    }

}
