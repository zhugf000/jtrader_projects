package trader.common.util;

import java.io.*;
import java.util.*;

/**
 * A simple ini file parser
 */
public class IniFile {
    public static class Section {
        private String name;
        private String text;
        private Properties props;

        void setText(String text) {
            this.text = text;
        }

        public String getName() {
            return name;
        }

        public String getText() {
            return text;
        }

        public Properties getProperties() {
            if (props == null) {
                props = new Properties();
                try {
                    props.load(new StringReader(text));
                } catch (IOException e) {
                }
            }
            return props;
        }

        public String get(String key) {
            return getProperties().getProperty(key);
        }
    }

    private List<Section> sections = new LinkedList<>();

    public IniFile(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new StringReader(FileUtil.load(file)))) {
            Section section = null;
            String line = null;
            StringBuilder sectionText = new StringBuilder();
            while ((line = br.readLine()) != null) {
                String tline = line.trim();
                if (section == null && tline.length() == 0) {
                    continue;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                if (tline.startsWith("[") && tline.endsWith("]")) {
                    if (section != null) {
                        section.setText(sectionText.toString());
                    }
                    section = new Section();
                    section.name = tline.substring(1, tline.length() - 1);
                    sectionText = new StringBuilder();
                    sections.add(section);
                    continue;
                }
                if (section != null) {
                    if (sectionText.length() > 0) {
                        sectionText.append("\n");
                    }
                    sectionText.append(line);
                }
            }
            section.setText(sectionText.toString());
        }
    }

    public Collection<Section> getAllSections() {
        return Collections.unmodifiableList(sections);
    }

    public Section getSection(String name) {
        for (Section s : sections) {
            if (s.name.equals(name)) {
                return s;
            }
        }
        return null;
    }
}
