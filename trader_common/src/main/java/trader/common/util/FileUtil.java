package trader.common.util;

import java.io.*;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

public class FileUtil {

    public static String createTempDirectory(File parentDir, String prefix) throws IOException
    {
        return Files.createTempDirectory(parentDir.toPath(), prefix).toString();
    }

    /**
     * 删除一个目录和所有的子目录，文件
     */
    public static void deleteDirectory(File dirToDelete){
        if ( !dirToDelete.exists() )
            return;
        LinkedList<File> files = new LinkedList<>();
        searchFiles(files, dirToDelete);
        for(File file:files){
            file.delete();
        }
    }

    private static void searchFiles(List<File> files, File dir){
        File subs[] = dir.listFiles();
        if ( subs!=null&&subs.length>0){
            for(int i=0;i<subs.length;i++){
                if ( subs[i].isFile() )
                    files.add(subs[i]);
                else
                    searchFiles(files,subs[i]);
            }
        }
        files.add(dir);
    }

    public static byte[] loadAsBytes(File file) throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream((int)file.length());
        try(InputStream is = new FileInputStream(file)){
            byte buf[] = new byte[4096];
            int len;
            while ( (len=is.read(buf))>0 ){
                os.write(buf,0,len);
            }
        }
        return os.toByteArray();
    }

    public static String load(File file) throws IOException{
        StringWriter writer = new StringWriter();
        try(BufferedReader reader = IOUtil.createBufferedFileReader(file)){
            char cbuf[] = new char[4096];
            int clen;
            while ( (clen=reader.read(cbuf))>0 ){
                writer.write(cbuf,0,clen);
            }
        }
        return writer.toString();
    }

    public static List<String> loadLines(File file) throws IOException{
        LinkedList<String> lines = new LinkedList<>();
        try(BufferedReader reader = IOUtil.createBufferedFileReader(file)){
            String line = null;
            while( (line=reader.readLine())!=null ){
                line = line.trim();
                if ( line.length()>0 )
                    lines.add(line);
            }
        }
        return lines;
    }

    public static void save(File file,String text) throws IOException {
        try(BufferedWriter writer = IOUtil.createBufferedFileWriter(file)){
            writer.write(text);
        }
        return;
    }

    public static void copy(File src, File tgt) throws IOException {
        int read;
        byte [] buffer = new byte [4096];
        try( InputStream is = new FileInputStream(src);
             OutputStream os = new FileOutputStream (tgt); )
        {
            while ((read = is.read (buffer)) != -1) {
                os.write(buffer, 0, read);
            }
        }
        tgt.setLastModified(src.lastModified());
        if ( tgt.length()!=src.length()){
            tgt.delete();
            throw new IOException("Copy "+src+" to "+tgt+" failed, file length is not matched.");
        }
    }

    public static void copy(InputStream srcIs, File tgt ) throws IOException {
        int read;
        byte [] buffer = new byte [4096];
        try( InputStream is = srcIs;
             OutputStream os = new FileOutputStream (tgt); )
        {
            while ((read = is.read (buffer)) != -1) {
                os.write(buffer, 0, read);
            }
        }
    }

    public static String getFileMainName(File file) {
        String fname = file.getName();
        if ( fname.indexOf('.')<0) {
            return fname;
        }
        return fname.substring(0, fname.indexOf('0'));
    }

}
