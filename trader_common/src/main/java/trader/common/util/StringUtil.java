package trader.common.util;

import java.nio.charset.Charset;
import java.util.Collection;

public class StringUtil {

    public static final Charset UTF8 = Charset.forName("utf8");

    public static boolean isEmpty(String str) {
        if ( str==null ) {
            return true;
        }
        if ( str.length()==0 ){
            return true;
        }
        return str.trim().length()==0;
    }

    public static String trim(String str)
    {
        if ( str==null ) {
            return null;
        }
        str = str.trim();
        if ( str.length()>0 ){
            int c0 = str.charAt(0);
            int cn = str.charAt(str.length()-1);
            if ( c0==160 ){
                str = str.substring(1);
                return trim(str);
            }
            if ( cn == 160 ){
                str = str.substring(0, str.length()-1);
                return trim(str);
            }
        }
        return str;
    }

    public static boolean notEmpty(String str)
    {
        if ( str!=null && str.length()>0 ) {
            return true;
        }
        return false;
    }

    public static String getValue(String str1, String defaultValue){
        if ( str1!=null && str1.length()>0 ) {
            return str1;
        }
        return defaultValue;
    }

    public static String[] str2array(String str, String delimiter)
    {
        return str.split(delimiter);
    }

    public static String array2str(String[] array, String delimiter){
        if ( array==null||array.length==0 ) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        for(String s:array){
            if ( builder.length()>0 ) {
                builder.append(delimiter);
            }
            builder.append(s);
        }
        return builder.toString();
    }

    public static String collection2str(Collection<String> array, String delimiter){
        StringBuilder builder = new StringBuilder();
        if ( array==null||array.size()==0 ) {
            return null;
        }
        for(String s:array){
            if ( builder.length()>0 ) {
                builder.append(delimiter);
            }
            builder.append(s);
        }
        return builder.toString();
    }

    public static boolean arrrayEquals(String[] arr1, String[] arr2, boolean ignoreCase){
        if ( arr1==null && arr2==null ) {
            return true;
        }
        if (arr1.length!=arr2.length) {
            return false;
        }
        for(int i=0;i<arr1.length;i++){
            if (ignoreCase){
                if (!arr1[i].equalsIgnoreCase(arr2[i])){
                    return false;
                }
            }else{
                if (arr1[i].equalsIgnoreCase(arr2[i])){
                    return false;
                }
            }
        }
        return true;
    }
}
