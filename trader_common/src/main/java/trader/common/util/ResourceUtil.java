package trader.common.util;

import java.io.*;
import java.net.URL;

public class ResourceUtil {

    public static InputStream load(String... paths) throws IOException
    {
        InputStream is = null;
        for(String path: paths){
            File f = new File(path);
            if ( f.exists() ){
                is = new FileInputStream(f);
                break;
            }
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
            if ( is!=null ){
                break;
            }
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("/"+path);
            if ( is!=null ){
                break;
            }
            is = ClassLoader.getSystemResourceAsStream(path);
            if ( is!=null ){
                break;
            }
            is = ClassLoader.getSystemResourceAsStream("/"+path);
            if ( is!=null ){
                break;
            }
            is = ResourceUtil.class.getClassLoader().getResourceAsStream(path);
            if ( is!=null ){
                break;
            }
            is = ResourceUtil.class.getClassLoader().getResourceAsStream("/"+path);
            if ( is!=null ){
                break;
            }
        }
        return is;
    }

    public static File detectJarFile(Class clazz)
    {
        String fullClassFile = clazz.getName().replaceAll("\\.", "/")+".class";
        String jarFile = clazz.getClassLoader().getResource( fullClassFile ).getPath();
        if ( jarFile.indexOf("!")>0){
            jarFile = jarFile.substring(0, jarFile.indexOf('!'));
        }
        if ( jarFile.endsWith(fullClassFile)){
            jarFile = jarFile.substring(0, jarFile.length()-fullClassFile.length());
        }
        if ( jarFile.startsWith("file:///")){
            jarFile = jarFile.substring("file:///".length());
        }else if (jarFile.startsWith("file:")){
            jarFile = jarFile.substring(5);
        }
        return new File(jarFile);
    }

    public static File getFile(String resName, Class clazz )
    {
        URL url = null;
        if ( clazz!= null ){
            url = clazz.getClassLoader().getResource(resName);
        }
        if ( url==null && clazz!=null ){
            url = clazz.getClassLoader().getResource("/"+resName);
        }
        if ( url==null ){
            url = ClassLoader.getSystemResource(resName);
        }
        if ( url==null ){
            url = ClassLoader.getSystemResource("/"+resName);
        }
        if ( url==null ){
            url = Thread.currentThread().getContextClassLoader().getResource(resName);
        }
        if ( url== null ){
            throw new RuntimeException("Resource \""+resName+"\" is not found with class "+clazz);
        }
        String file = url.getFile();
        return new File(file);
    }
}
