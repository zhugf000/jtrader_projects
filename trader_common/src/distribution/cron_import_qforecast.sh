#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR


$JAVA_HOME/bin/java \
    -cp "lib/*" trader.tools.dataimport.DataImportTool \
    --rootDir="/home/zhugf/traderHome/data/quantum" \
    --source=quantum \
    --securityIds=ALL \
    --dataTypes=forecast,season \
    --overwrite=false \
    --url="http://42.96.146.213:11080/TradeDataSvlt-201602/DataService" \
    --checkTradingDay=true \
    >> ../logs/import_forecast.log 2>&1
