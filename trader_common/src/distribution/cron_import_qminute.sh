#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

$JAVA_HOME/bin/java \
    -cp "lib/*" trader.tools.dataimport.DataImportTool \
    --rootDir="/home/zhugf/traderHome/data/market" \
    --url="http://42.96.146.213:11080/TradeDataSvlt-201602/DataService" \
    --source=quantum \
    --securityIds=ALL \
    --dataTypes=minute1,minute5,minute15,minute30 \
    --overwrite=true \
    --checkTradingDay=true \
    >> ../logs/import_qminute.log 2>&1
