#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

$JAVA_HOME/bin/java \
    -cp "lib/*" trader.tools.dataimport.DataImportTool \
    --rootDir="/home/zhugf/traderHome/data/market" \
    --source=sina \
    --securityIds=ALL \
    --dataTypes=second \
    --overwrite=true \
    --thread=5 \
    >> ../logs/import_second_sina.log 2>&1
