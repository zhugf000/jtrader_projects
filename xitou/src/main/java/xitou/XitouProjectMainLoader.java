package xitou;

import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import trader.common.util.NetUtil;

/**
 * 喜利多加载
 * @author zhugf
 *
 */
public class XitouProjectMainLoader extends XitouProjectLoader {

    /**
     * URL: http://www.xitouwang.com/invest_list/main.html?limit_type=0&borrow_type=0&status_type=0&page=3&query=1476151160371
     */
    private static final String url_plusList = "http://www.xitouwang.com/plan/plusList";

    @Override
    List<XitouProjectInfo> load(XitouApp xitouApp) throws Exception
    {
        List<XitouProjectInfo> projects = new LinkedList<>();
        String text = NetUtil.readHttpAsText(url_plusList, NetUtil.HttpMethod.GET, null, Utils.utf8);
        Document doc = Jsoup.parse(text);
        for(Element li: doc.select("li")){
            XitouProjectInfo p = new XitouProjectInfo();

            Element a = li.select("a").first();
            p.name = a.text().trim();
            p.url = "http://www.xitouwang.com"+a.attr("href");
            p.id = Utils.uri2id(a.attr("href"));
            {
                Elements ems = li.select("em");
                if( ems.size()>0 && ems.get(0).text().trim().startsWith("手")){
                    p.manual = true;
                }
            }
            Elements spans = li.select("span");
            p.totalValueString =  spans.get(2).text().trim();
            p.totalValue = Utils.str2decimal(p.totalValueString);
            p.interest = spans.get(4).text().trim();
            p.duration = spans.get(6).text().trim();
            p.status = spans.get(9).text().trim();
            if( p.status.indexOf("0%")>=0 && p.status.indexOf("100%")<0 ){
                Elements e = li.select(".btn-gray.fr.b-samll.af");
                if (e.size()>0){
                    String bidTime = e.get(0).text().trim();
                    p.bidTime = Utils.str2bidtime(bidTime);
                }
            }
            projects.add(p);
        }
        return projects;
    }

}
