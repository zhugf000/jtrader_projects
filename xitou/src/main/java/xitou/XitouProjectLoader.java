package xitou;

import java.util.List;

public abstract class XitouProjectLoader {

    abstract List<XitouProjectInfo> load(XitouApp xitouApp) throws Exception;

}
