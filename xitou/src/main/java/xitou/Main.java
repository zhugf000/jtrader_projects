package xitou;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

import org.apache.commons.cli.*;
import org.apache.commons.text.StringEscapeUtils;

import trader.common.util.*;

public class Main {
    private static final String CONFIG_FILE = "xitou_accounts.ini";
    private static final ZoneId ZONEID_BEIJING = ZoneId.of("Asia/Shanghai");
    private static final ZoneOffset ZONEOFFSET_BEIJING = LocalDateTime.now().atZone(ZONEID_BEIJING).getOffset();

    private static String optProjectId;
    private static String optAccount;
    private static BigDecimal optMaxBidAmount = null;

    private static IniFile loadConfig() throws IOException
    {
        return TraderConfigUtil.loadIniFile(CONFIG_FILE);
    }

    private static void usage(){
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "xitou [options] (show|bid|list|listTransfer)", buildOptions() );
    }

    private static Options buildOptions(){
        Options options = new Options();
        Option projectIdOption = Option.builder("projectId").hasArg().desc("项目ID,如果未指定,则投标第一个项目").build();
        Option accountOption = Option.builder("account").hasArg().desc("喜投帐户名").build();
        Option amountOption = Option.builder("amount").hasArg().desc("投标金额, 如果未指定,所有可用金额").build();
        options.addOption(projectIdOption);
        options.addOption(accountOption);
        options.addOption(amountOption);
        return options;
    }

    private static String parseArgs(String[] args) throws Exception
    {
        CommandLine cmdLine = (new DefaultParser()).parse(buildOptions(), args);
        optAccount = cmdLine.getOptionValue("account");
        optMaxBidAmount = new BigDecimal( cmdLine.getOptionValue("amount", "99999999") );
        optProjectId = cmdLine.getOptionValue("projectId");
        if ( cmdLine.getArgs().length >0 ){
            return cmdLine.getArgs()[0];
        }
        return null;
    }

    public static void main(String[] args) throws Exception
    {
        if ( args.length==0 ){
            usage();
            System.exit(0);
        }
        String action = parseArgs(args);
        //List action need nothing
        if ( "list".equalsIgnoreCase(action)){
            doList();
            return;
        }else if ( "listTransfer".equalsIgnoreCase(action)){
            doListTransfer();
            return;
        }
        //Other actions need the account option
        if ( optAccount==null ){
            System.out.println("账户未指定");
            System.exit(1);
        }
        IniFile configIni = loadConfig();
        if ( configIni==null ){
            System.out.println("无法加载配置文件: "+CONFIG_FILE);
            System.exit(1);
        }
        IniFile.Section section = null;
        for(IniFile.Section s:configIni.getAllSections()){
            if ( optAccount.equalsIgnoreCase(s.getName())){
                section = s;
                break;
            }
        }
        if ( section==null ){
            System.out.println("无此喜投帐户:  "+optAccount);
            System.exit(1);
        }
        Properties accountProps = section.getProperties();
        XitouApp app = createXitouApp(accountProps);
        if ( "show".equalsIgnoreCase(action)){
            doShow(app);
        }else if ("bid".equalsIgnoreCase(action)){
            doBid(app);
        }else{
            System.out.println("不支持的动作: "+action);
        }
    }

    private static XitouApp createXitouApp(Properties accountProps) throws Exception
    {
        String user = accountProps.getProperty("user");
        XitouLoginInfo loginInfo = new XitouLoginInfo(user,accountProps.getProperty("password"), accountProps.getProperty("payPassword"));
        XitouApp app = new XitouApp();
        if ( !app.login(loginInfo) ){
            throw new Exception("帐户 "+loginInfo.user+" 登录失败");
        }
        return app;
    }

    private static void doShow(XitouApp app) throws Exception
    {
        final XitouAccountInfo accountInfo = app.loadAccountInfo();
        System.out.println("帐户 \""+app.loginInfo.user+"\" 总值: "+accountInfo.totalValue+", 可用: "+accountInfo.availableValue);
    }

    private static void doList() throws Exception
    {
        final String PROJECT_FORMAT = "%-6s%-25s%-15s%-10s%-12s%-12s%-15s";
        XitouApp app = new XitouApp();
        List<XitouProjectInfo> projects = app.loadProjects(true);

        for(XitouProjectInfo project: projects){
            if( !project.canBid()){
                continue;
            }
            String line = String.format(PROJECT_FORMAT, project.id, project.name, project.totalValueString, project.duration, project.interest, project.status, project.bidTime);
            System.out.println(line);
        }
    }

    private static void doListTransfer() throws Exception
    {
        final String PROJECT_FORMAT = "%-6s%-25s%-15s%-10s%-12s%-12s%-15s";
        XitouApp app = new XitouApp();

        List<XitouProjectInfo> projects = null;
        while(true){
            projects = app.loadTransferProjects();
            if ( projects.size()>0 ){
                break;
            }
            LocalDateTime now = LocalDateTime.now();
            System.out.println("当前时间: "+DateUtil.date2str(now)+" 无可债权转让项目");
            Thread.sleep(60*1000);
        }
        for(XitouProjectInfo project: projects){
            if( !project.canBid()){
                continue;
            }
            String line = String.format(PROJECT_FORMAT, project.id, project.name, project.totalValueString, project.duration, project.interest, project.status, project.bidTime);
            System.out.println(line);
        }
        //Save project page
        for(XitouProjectInfo project: projects){
            String text = NetUtil.readHttpAsText(project.url, NetUtil.HttpMethod.GET, null, Utils.utf8);
            FileUtil.save(new File("transfer-"+project.id+".html"), text);
        }
    }

    private static int doBid(XitouApp app) throws Exception
    {
        String user = app.loginInfo.user;
        final XitouAccountInfo accountInfo = app.loadAccountInfo();
        System.out.println("帐户 \""+user+"\" 总值: "+accountInfo.totalValue+", 可用: "+accountInfo.availableValue);

        if ( accountInfo.availableValue.longValue()<100 ){
            System.out.println("可用余额不足");
            return 1;
        }
        List<XitouProjectInfo> projectsToBid = getProjectsToBid(app);
        if ( projectsToBid==null || projectsToBid.isEmpty() ) {
            return 1;
        }
        //Print projects to bid
        BigDecimal bidAmount = accountInfo.availableValue;
        if ( optMaxBidAmount!=null && optMaxBidAmount.compareTo(bidAmount)<0 ){
            bidAmount = optMaxBidAmount;
        }
        System.out.println("投标金额: "+bidAmount+", 时间: "+projectsToBid.get(0).bidTime);
        for(XitouProjectInfo p:projectsToBid){
            System.out.println("\t"+p.id+"\t"+p.name);
        }
        waitForBid(app, projectsToBid.get(0));
        bidProjects(app, projectsToBid, bidAmount);
        final XitouAccountInfo accountInfo2 = app.loadAccountInfo();
        System.out.println("投标结束, 帐户"+user+" 总值: "+accountInfo2.totalValue+", 可用: "+accountInfo2.availableValue);
        return 0;
    }

    private static List<XitouProjectInfo> getProjectsToBid(XitouApp app) throws Exception
    {
        List<XitouProjectInfo> projects = app.loadProjects(true);
        List<XitouProjectInfo> projectsToBid = new ArrayList<>();
        if ( optProjectId!=null ){
            for(XitouProjectInfo p:projects){
                if ( optProjectId.equals(p.id) ){
                    projectsToBid.add(p);
                    break;
                }
            }
            if ( projectsToBid.size()==0 ){
                System.out.println("待投标项目 "+optProjectId+" 未发现");
                return null;
            }
        }else{
            for(XitouProjectInfo p:projects){
                if ( p.canBid() ){
                    projectsToBid.add(p);
                }
            }
            if ( projectsToBid.size()==0 ){
                System.out.println("无待投标项目");
                return null;
            }
            Collections.sort(projectsToBid, new Comparator<XitouProjectInfo>(){
                @Override
                public int compare(XitouProjectInfo o1, XitouProjectInfo o2) {
                    return o1.bidTime.compareTo(o2.bidTime);
                }
            });
            LocalDateTime t = projectsToBid.get(0).bidTime;
            for(Iterator<XitouProjectInfo> it=projectsToBid.iterator();it.hasNext();){
                XitouProjectInfo p =it.next();
                if ( !p.bidTime.equals(t) ){
                    it.remove();
                }
            }
        }
        return projectsToBid;
    }

    private static void waitForBid(XitouApp app, XitouProjectInfo projectToBid) throws Exception
    {
        if ( projectToBid.bidTime==null ) {
            return;
        }
        LocalDateTime bidTime = projectToBid.bidTime;
        long bidSeconds = bidTime.toEpochSecond(ZONEOFFSET_BEIJING);
        long lastPrintTime =0;
        while(true){
            LocalDateTime now = LocalDateTime.now();
            if ( now.equals(bidTime) || now.isAfter(bidTime) ){
                break;
            }
            Thread.sleep(100);
            long nowSeconds = now.toEpochSecond(ZONEOFFSET_BEIJING);
            if ( lastPrintTime!=nowSeconds && Math.abs(nowSeconds-bidSeconds)%60==0 ){
                System.out.println("当前时间: "+DateUtil.date2str(now)+", 投标时间: "+DateUtil.date2str(bidTime)+", 等待: "+(ChronoUnit.SECONDS.between(now, bidTime)+10)/60+" 分钟");
                lastPrintTime = nowSeconds;
            }
            if ( (nowSeconds != bidSeconds) && Math.abs(nowSeconds-bidSeconds)%600==0 ){
                //每十分钟,logout/login一次
                try {
                    app.logout();
                    app.relogin();
                    app.loadAccountInfo();
                }catch(Throwable t) {
                    System.out.println("临时网络失败: "+t);
                }
            }
        }
    }

    private static boolean bidProjects(XitouApp app, List<XitouProjectInfo> projectsToBid, BigDecimal bidAmount) throws Exception
    {
        boolean bidComplete = false;
        Instant beginTime = Instant.now();
        while(true){
            for(XitouProjectInfo p:projectsToBid){
                long nanoBegin = System.nanoTime();
                String bidResult = app.bid(p, bidAmount );
                long nanoEnd = System.nanoTime();
                long milliUsed = (nanoEnd-nanoBegin)/1000000;

                bidResult = unescapeString(bidResult);
                System.out.println(LocalDateTime.now().toString()+" "+milliUsed+" ms, bid result: "+bidResult);
                if ( XitouApp.canStopBid(bidResult) ){
                    bidComplete = true;
                    break;
                }
            }
            if ( bidComplete ){
                break;
            }
            //最多尝试10秒
            Instant curr = Instant.now();
            if ( (curr.getEpochSecond()-beginTime.getEpochSecond())>=10 ){
                break;
            }
            Thread.sleep(500);
        }
        return bidComplete;
    }

    private static String unescapeString(String str){
        if ( str==null ){
            return str;
        }
        return StringEscapeUtils.unescapeJava(str);
    }

}
