package xitou;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static final Charset gbk = Charset.forName("GBK");
    public static final Charset utf8 = Charset.forName("UTF-8");

    public static final String url_host = "http://www.xitouwang.com";

    public static String uri2id(String uri)
    {
        Pattern p = Pattern.compile(".*/(\\d+).html");
        Matcher m = p.matcher(uri);
        if ( m.find()){
            return m.group(1);
        }
        return null;
    }

    public static String str2duration(String dur){
        dur = dur.trim();
        if (dur.endsWith("期限")){
            dur = dur.substring(0, dur.length()-2).trim();
        }
        return dur;
    }

    public static BigDecimal str2decimal(String decimalText){
        long multiple = 1;
        if ( decimalText.startsWith("￥")){
            decimalText = decimalText.substring(1);
        }
        if ( decimalText.endsWith("元")){
            decimalText = decimalText.substring(0, decimalText.length()-1).trim();
        }
        if ( decimalText.endsWith("万")){
            multiple = 10000;
            decimalText = decimalText.substring(0, decimalText.length()-1).trim();
        }
        decimalText = decimalText.replaceAll(",", "");
        return (new BigDecimal(decimalText)).multiply(new BigDecimal(multiple));
    }

    public static LocalDateTime str2bidtime(String bidTimeStr){
        if ( bidTimeStr!=null && bidTimeStr.endsWith("开启")){
            bidTimeStr = bidTimeStr.substring(0, bidTimeStr.length()-2).trim();
        }

        //04-13 17:00补足为: 2018-04-13 17:00
        if ( bidTimeStr.length()<=11) {
            bidTimeStr = (""+LocalDateTime.now().getYear())+"-"+bidTimeStr;
        }
        for(int i=0;i<datetimeFormatters.length;i++) {
            try {
                return LocalDateTime.parse(bidTimeStr, datetimeFormatters[i]);
            }catch(Throwable t) {}
        }
        return null;
    }

    private static final DateTimeFormatter[] datetimeFormatters = new DateTimeFormatter[] {
            DateTimeFormatter.ofPattern("MM-dd HH:mm"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"),
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    };

}
