package xitou;

import java.math.BigDecimal;

/**
 * 应收项目
 */
public class XitouReceivableProjectInfo {
    public String projectName;
    public String projectDuration;
    public BigDecimal projectMoney;
    public String projectPeriod;
    public String nextReturnDate;
    public String status;
}
