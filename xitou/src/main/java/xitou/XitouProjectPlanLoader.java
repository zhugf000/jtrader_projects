package xitou;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import trader.common.util.NetUtil;

/**
 * 喜利多, 喜优选项目加载
 */
public class XitouProjectPlanLoader extends XitouProjectLoader {
    private static final String url_plusList = "http://www.xitouwang.com/plan/plusList";
    private static final String url_autoList = "http://www.xitouwang.com/plan/autoList";

    @Override
    public List<XitouProjectInfo> load(XitouApp xitouApp) throws Exception
    {
        List<XitouProjectInfo> projects = new ArrayList<>();
        loadPlus(xitouApp, projects);
        loadAuto(xitouApp, projects);

        return projects;
    }

    /**
     * 喜利多
     */
    private void loadPlus(XitouApp xitouApp, List<XitouProjectInfo> projects) throws Exception
    {
        String text = NetUtil.readHttpAsText(url_plusList, NetUtil.HttpMethod.GET, null, Utils.utf8);
        Document doc = Jsoup.parse(text);
        Element divContent = doc.select("div.content").first();
        for(Element a : divContent.select("a")){
            XitouProjectInfo project = new XitouProjectInfo();
            project.url = a.attr("href");
            String href = a.attr("href");
            project.id = href.substring(href.lastIndexOf("/")+1);
            String projectText = NetUtil.readHttpAsText(project.url, NetUtil.HttpMethod.GET, null, Utils.utf8);
            if (projectText.indexOf("手动标")>0) {
                project.manual = true;
            }
            Elements divs = a.select("div");
            project.name = divs.get(1).text().trim();
            project.totalValueString = divs.get(6).text().trim();
            project.totalValue = Utils.str2decimal( project.totalValueString );
            project.interest = divs.get(4).text().trim();
            project.duration = Utils.str2duration( divs.get(8).text().trim() );
            project.status = divs.get(12).text();
            project.bidTime = Utils.str2bidtime(project.status);
            projects.add(project);
        }
    }

    /**
     * 喜优选
     */
    private void loadAuto(XitouApp xitouApp, List<XitouProjectInfo> projects) throws Exception
    {
        String text = NetUtil.readHttpAsText(url_autoList, NetUtil.HttpMethod.GET, null, Utils.utf8);
        Document doc = Jsoup.parse(text);
        Element divContent = doc.select("div.content").first();
        for(Element a : divContent.select("a")){
            XitouProjectInfo project = new XitouProjectInfo();
            project.url = a.attr("href");
            String href = a.attr("href");
            project.id = href.substring(href.lastIndexOf("/")+1);
            Elements divs = a.select("div");
            project.name = divs.get(1).text().trim();
            project.totalValueString = divs.get(6).text().trim();
            project.totalValue = Utils.str2decimal( project.totalValueString );
            project.interest = divs.get(4).text().trim();
            project.duration = Utils.str2duration( divs.get(8).text().trim() );
            String statusOverview = divs.get(12).text().trim();
            if ( statusOverview.indexOf("匹配中")>=0) {
                continue;
            }
            String projectText = NetUtil.readHttpAsText(project.url, NetUtil.HttpMethod.GET, null, Utils.utf8);
            if (projectText.indexOf("手动标")>0) {
                project.manual = true;
            }
            Document projectDoc = Jsoup.parse(projectText);
            List<Element> spans = projectDoc.select("span");
            Element spanTime = null;
            for(Element span:spans) {
                if ( span.text().trim().indexOf("发布时间")>=0) {
                    spanTime = span.nextElementSibling();
                    break;
                }
            }
            if ( spanTime!=null ) {
                project.status = spanTime.text().trim();
                project.bidTime = Utils.str2bidtime(project.status);
            }
            projects.add(project);
        }
    }

}
