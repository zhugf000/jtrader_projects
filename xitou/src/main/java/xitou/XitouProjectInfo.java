package xitou;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;

public class XitouProjectInfo {

    public String name;

    public String id;

    public String url;

    /**
     * 状态
     */
    public String status = "";

    /**
     * 利率
     */
    public String interest;

    /**
     * 发布日期
     */
    public String publishingDate;

    /**
     * 总值
     */
    public BigDecimal totalValue;

    /**
     * 总值(万)
     */
    public String totalValueString;

    /**
     * 期限
     */
    public String duration;

    /**
     * 普通手动标
     */
    public boolean manual;

    public LocalDateTime bidTime;

    public String getBidURL() throws MalformedURLException
    {
        if ( url.indexOf("plus")>0 ) {
            URL url0 = new URL(url);
            return url0.getProtocol()+"://"+url0.getHost()+"/plan/plusTender";
        }else if ( url.indexOf("auto")>0 ) {
            URL url0 = new URL(url);
            return url0.getProtocol()+"://"+url0.getHost()+"/plan/autoTender";
        }
        return null;
    }

    public boolean canBid()
    {
        if ( bidTime!=null )
        {
            return manual;
        }
        return false;
    }

    public boolean canBidNow(){
        if ( (name.indexOf("喜利多")>=0 && status.indexOf("立即")>=0) ){
            return true;
        }
        if ( (name.indexOf("喜乐多")>=0 && manual && status.indexOf("立即")>=0) ){
            return true;
        }
        if ( manual && status.indexOf("立即")>=0){
            return true;
        }
        if ( manual && status.indexOf("0%")>=0 && status.indexOf("100%")<0 ){
            //10-11 17:00
            return LocalDateTime.now().isAfter(bidTime);
        }
        return false;
    }

    public BigDecimal getMinimumBidAmount(){
        if ( name.indexOf("喜利多")>=0){
            return new BigDecimal(1000);
        }
        return new BigDecimal(100);
    }

    public BigDecimal getBidAmount(BigDecimal availAmount){
        //喜利多是100的整数倍
        if ( name.indexOf("喜利多")>=0 ){
            return new BigDecimal( availAmount.longValue()/1000*1000 );
        }else{ //喜乐多和普通手动标50元起
            return new BigDecimal( availAmount.longValue()/1*1 );
        }
    }

    @Override
    public boolean equals(Object o){
        if ( o instanceof XitouProjectInfo){
            XitouProjectInfo p = (XitouProjectInfo)o;
            return id.equals(p.id);
        }
        return false;
    }

}
