package xitou;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import trader.common.util.NetUtil;

public class XitouApp {

    static{
        CookieHandler.setDefault( new CookieManager( null, CookiePolicy.ACCEPT_ALL ) );
    }

    XitouLoginInfo loginInfo;

    public XitouApp()
    {

    }

    public boolean login(XitouLoginInfo loginInfo) throws IOException
    {
        this.loginInfo = loginInfo;
        String url = Utils.url_host+"/user/loginSubmit";
        Map<String, String> props = new HashMap<>();
        props.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        props.put("Referer", "http://www.xitouwang.com/user/login");
        props.put("Host", "www.xitouwang.com");
        String jsonText = NetUtil.readHttpAsText(url, NetUtil.HttpMethod.POST, "username="+loginInfo.user+"&password="+loginInfo.password+"&remember", Utils.utf8, props);
        JSONObject rootData = new JSONObject(jsonText);
        String status = ""+rootData.get("status");
        return "200".equalsIgnoreCase(status);
    }

    public boolean relogin() throws IOException
    {
        return login(loginInfo);
    }

    public void logout() throws IOException
    {
        String url = Utils.url_host+"/user/logout";
        NetUtil.readHttpAsText(url, Utils.utf8);
    }

    public XitouAccountInfo loadAccountInfo() throws IOException
    {
        XitouAccountInfo result = new XitouAccountInfo();
        String jsonText = NetUtil.readHttpAsText(Utils.url_host+"/user/assetOverview", NetUtil.HttpMethod.GET, null, Utils.utf8);
        Document doc = Jsoup.parse(jsonText);
        //匹配: 账户总额（元）1621225.12元
        Pattern availPattern = Pattern.compile(".+可用余额：\\D*(\\d+\\.\\d+).+");
        Pattern totalPattern = Pattern.compile(".+账户总额（元） 帮助 \\D*(\\d+\\.\\d+)元.+");
        result.availableValue = BigDecimal.ZERO;
        Matcher availMatcher = availPattern.matcher(doc.text());
        if ( availMatcher.matches() ) {
            result.availableValue = new BigDecimal(availMatcher.group(1).trim());
        }
        Matcher totalMatcher = totalPattern.matcher(doc.text());
        if ( totalMatcher.matches()){
            result.totalValue = new BigDecimal(totalMatcher.group(1).trim());
        }
        return result;
    }

    /**
     * 抓取主页面中的项目
     */
    public List<XitouProjectInfo> loadProjects(boolean loadMainProjects) throws Exception
    {
        List<XitouProjectInfo> projects = new ArrayList<>();
        //        if ( loadMainProjects ){
        //            projects.addAll((new XitouProjectMainLoader()).load(this));
        //        }
        for(XitouProjectInfo p:(new XitouProjectPlanLoader()).load(this)){
            if ( !projects.contains(p) ){
                projects.add(p);
            }
        }
        return projects;
    }

    /**
     * 普通手动项目投标.
     * <BR>正常投标结果返回：{"status":200,"url":"\/account\/logList"}
     */
    public String bid(XitouProjectInfo projectInfo, BigDecimal availMoney) throws Exception
    {
        String url = projectInfo.getBidURL();
        BigDecimal bidAmount = projectInfo.getBidAmount(availMoney);
        if ( bidAmount.longValue()==0 ){
            System.out.println("可用资金不足: "+availMoney);
            return null;
        }
        String money = ""+bidAmount.intValue();

        String postBody = "amount="+money+"&planId="+projectInfo.id;
        Map<String,String> props = new HashMap<>();
        props.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        props.put("Host", "www.xitouwang.com");
        String text = NetUtil.readHttpAsText(url, NetUtil.HttpMethod.POST, postBody, Utils.utf8, props);
        return text.trim();
    }

    /**
     * 普通债权转让项目投标
     */
    public String bidTransfer(XitouProjectInfo projectInfo, BigDecimal availMoney) throws Exception
    {
        String url = Utils.url_host+"/index.php?user&q=code/borrow/transfer_tender";
        BigDecimal bidAmount = projectInfo.getBidAmount(availMoney);
        if ( bidAmount.longValue()==0 ){
            System.out.println("可用资金不足: "+availMoney);
            return null;
        }
        String money = ""+bidAmount;
        String postBody = "money="+money+"&paypassword="+loginInfo.payPassword+"&id="+projectInfo.id+"&transfer_id="+projectInfo.id;
        String text = NetUtil.readHttpAsText(url, NetUtil.HttpMethod.POST, postBody, Utils.utf8);
        return text.trim();
    }

    public List<XitouProjectInfo> loadTransferProjects() throws Exception
    {
        List<XitouProjectInfo> result = new ArrayList<>();
        result.addAll( (new XitouTransferProjectLoader()).load(this));
        return result;
    }

    public static boolean canStopBid(String bidResult){
        if ( bidResult==null ){
            return false;
        }
        bidResult = bidResult.toLowerCase();
        if ( bidResult.indexOf("投标成功")>=0
                || bidResult.indexOf("密码不正确")>=0
                || bidResult.indexOf("此标已满")>=0
                || bidResult.indexOf("余额不足")>=0
                || bidResult.indexOf("BORROWISFUll")>=0
                //200 投标成功
                || bidResult.indexOf("200")>=0
                )
        {
            return true;
        }
        return false;
    }


}
