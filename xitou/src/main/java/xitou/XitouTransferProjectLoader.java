package xitou;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import trader.common.util.NetUtil;

/**
 * URL: http://www.xitouwang.com/transfer_list/main.html
 */
public class XitouTransferProjectLoader extends XitouProjectLoader {

    private static final String url = "http://www.xitouwang.com/transfer_list/main.html";

    private static final String url2 = "http://www.xitouwang.com/transfer_list/main.html?limit_type=0&borrow_type=0&status_type=1&page=1&query=";

    @Override
    List<XitouProjectInfo> load(XitouApp xitouApp) throws Exception {
        List<XitouProjectInfo> result = new ArrayList<>();
        String url = url2+(new Date()).getTime();
        String text = NetUtil.readHttpAsText(url, NetUtil.HttpMethod.GET, null, Utils.utf8);
        Document doc = Jsoup.parse(text);
        for(Element li: doc.select("li")){
            XitouProjectInfo p = new XitouProjectInfo();

            Element a = li.select("a").first();

            p.name = a.text();
            p.url = "http://www.xitouwang.com"+a.attr("href");
            p.id = Utils.uri2id(a.attr("href"));

            p.manual = true;
            p.interest = li.select("span[class=w12]").first().text();

            Elements w10Spans = li.select("span[class=w10]");
            p.duration = w10Spans.get(0).text();
            p.totalValueString = w10Spans.get(1).text();
            p.totalValue = Utils.str2decimal(p.totalValueString);
            p.status = w10Spans.get(2).text();
            result.add(p);
        }
        return result;
    }

}
