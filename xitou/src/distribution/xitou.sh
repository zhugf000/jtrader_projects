#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

$JAVA_HOME/bin/java -Dfile.encoding="UTF-8" -classpath "$DIR/lib/*" xitou.Main "$@"
